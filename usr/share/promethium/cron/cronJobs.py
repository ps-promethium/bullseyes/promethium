#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

# Native Python Lib/Class 
#import os.path 
from crontab import CronTab
import sys



userCron     = 'root'
python3Path  = '/usr/share/promethium-p3/bin/python3'
cronTaskPath = '/usr/share/promethium/promethiumClass/job'
cronCepdPath = '/usr/share/promethium/promethiumClass/cepd'

cron = CronTab(user=userCron)
cron.remove_all(comment='promethium')  


if len(sys.argv) > 1:
    if 'start' in str(sys.argv[1]):
        ## Mailbox Scan
        # Only Master Node
        cmdMaster=python3Path + ' ' + cronTaskPath + '/masterJob.py'
        Master = cron.new(command=cmdMaster,comment='promethium')  
        Master.minute.every(1)


        # Each Node Jobs 
        cmdNode=python3Path + ' ' + cronTaskPath + '/nodeJob.py'
        Node = cron.new(command=cmdNode,comment='promethium')  
        Node.minute.every(1)

        ## Delete Messages older than
        # Only Master Node
        cmdMaster=python3Path + ' ' + cronCepdPath + '/masterJob.py'
        MasterCepd = cron.new(command=cmdMaster,comment='promethium')  
        MasterCepd.every().dom() 

        # Each Node Jobs 
        cmdNode=python3Path + ' ' + cronCepdPath + '/nodeJob.py'
        NodeCepd = cron.new(command=cmdNode,comment='promethium')  
        NodeCepd.every().dom() 



cron.write()
