#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url

from django.urls import include, path


urlpatterns = [
    # Logout
    path('accounts/login/', include('promethiumAuth.urls')),
    path('accounts/login/main/', include('promethiumAuth.urls')),
    
    # API
    path('api/auth/', include('promethiumAPIAuth.urls')),
    path('api/basket/', include('promethiumAPIBasket.urls')),
    path('api/chrono/', include('promethiumAPIChrono.urls')),
    path('api/domain/', include('promethiumAPIDomain.urls')),
    path('api/group/', include('promethiumAPIGroup.urls')),
    path('api/host/', include('promethiumAPIHost.urls')),
    path('api/import/', include('promethiumAPIImport.urls')),
    path('api/message/', include('promethiumAPIMessage.urls')),
    path('api/monitor/', include('promethiumAPIMonitor.urls')),
    path('api/search/', include('promethiumAPISearch.urls')),
    path('api/stats/', include('promethiumAPIStats.urls')),
    path('api/tags/', include('promethiumAPITags.urls')),
    
    # User API
    path('api/user/', include('promethiumAPIUser.urls')),
    path('api/user/', include('promethiumAPIUserFavorites.urls')),
    path('api/user/', include('promethiumAPIUserTags.urls')),
    
    # WEB/UI
    path('', include('promethiumAuth.urls')),
    path('home/', include('promethiumHome.urls')),
    path('manage/', include('promethiumManage.urls')),
    path('message/', include('promethiumMessage.urls')),
    path('monitor/', include('promethiumMonitor.urls')),
    path('user/', include('promethiumUser.urls')),
    path('params/', include('promethiumParams.urls')),
    path('search/', include('promethiumSearch.urls')),
    path('stats/', include('promethiumStats.urls')),
]
        
