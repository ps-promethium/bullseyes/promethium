#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

# Python lib
from bson.son import SON
import json
import jwt
import io
from pymemcache.client.hash import HashClient
import os.path 
import requests
import sys, traceback
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.config import config
#from promethiumClass.auth import auth
#from promethiumClass.auth import _bluemind
from promethiumClass.auth import _internal, _cypher
#from promethiumClass.auth import _ldap

from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid


@csrf_exempt
def login(request):     
    """ URL : /api/auth/login
    
    Method : POST
    
    The login method is stepped as :
        1) Retrieve from NoSQL account params
        2) Try to authenticate to remote if not local
        3) If remote access failure, then authenticate to local
        4) If remote auth succeed then update NoSQL account password
        5) Return  sesssion token if successfull
    
    POST BODY JSON: 
        {
            'login'    : the account login
            'password' : account's password

        }
    
    Return:
        JSON {'response': 'authenticated', 'token' : uuid , 'user' : {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed, post method only accepted...) 
    """

    if request.method == 'POST':
        res_q_uuid = None
        body_unicode = request.body.decode('utf-8')
  
        rJsData = json.loads(body_unicode)
        login   = rJsData['login']
        passwd  = rJsData['password']
              
              

        # 1) Retrieve from NoSQL account params
        #q_uuid = 'select usr, authsrv from `pm_user` usr join `pm_domain` dom on keys usr.domain join `pm_host` authsrv on keys dom.authserver where usr.login="' + login + '"'
        q_uuid = [
                    {"$lookup": {'from': "pm_domain",'localField': "domain",'foreignField': "uuid",'as': "doms"}},
                    {"$unwind" :"$doms"},
                    {"$lookup": {"from": "pm_host", 'localField': "doms.authserver",'foreignField': "uuid",'as': "authsrv"}},
                    {"$unwind" : "$authsrv"},
                    {"$match":{"$and": [{"login" : login}]}}
                ]

        try :
            ns = _nosql._nosql()
            res_q_uuid = [ r for r in ns.aggregate(bucket='pm_user',query=q_uuid)]
        except:
            apiLog.critical('NO DATA : ' + traceback.format_exc())
               
        if res_q_uuid is None:
            return HttpResponseForbidden('Error occured with NoSQL Servers')
        elif len(res_q_uuid) == 0:
            return HttpResponseForbidden('No account found')
        elif len(res_q_uuid) > 1 :
            apiLog.warning('Login : ' + str(len(res_q_uuid)) + ' accounts found with that login '+ login + ', what the hell !!')
            return HttpResponseForbidden('More than 1 account found !!')
        elif not res_q_uuid[0]['enable']:
            apiLog.info('Login :  '+ login + ' is not active.')
            return HttpResponseForbidden('Login :  '+ login + ' is not active.')
        else:
            pass       
        
        
        # 2) What kind of auth method 
        authMetod = res_q_uuid[0]['authsrv']['mechanism'] 
        if authMetod == 'BlueMind':
            try: 
                ctrl = _bluemind._bluemind()
                
            except Exception as e: 
                i_log = 'Bluemind Server is unavailable, try internal auth'
                apiLog.info(i_log)
                ctrl = _internal._internal()
            finally:
                HttpResponseForbidden('No auth server found')


        if authMetod == 'LDAP':
            try: 
                ctrl = _ldap._ldap()
                
            except Exception as e: 
                i_log = 'LDAP Server is unavailable, try internal auth'
                apiLog.info(i_log)
                ctrl = _internal._internal()
            finally:
                HttpResponseForbidden('No auth server found')
        

        if authMetod == 'Internal':
            ctrl = _internal._internal()
            
        # Auth now
        res = ctrl.authenticateOn(login,passwd)

        if res:
            jsDict = res_q_uuid[0]

            del jsDict['_id']
            del jsDict['doms']
            del jsDict['authsrv']
            if authMetod != 'Internal':
                jsDict['passwd'] =  _cypher._cypher().hashPassword(passwd) 
                #ctrl.upsert(bucket='pm_user', uuid=jsDict['uuid'], jsDict=jsDict)
                
            token= newToken()  
            setTokenToMemcache(token, jsDict['uuid'])
            js = {'state': 'authenticated', 'token': token, 'user': jsDict}
            return JsonResponse(js)
        else:
            return HttpResponseForbidden('No valid session token')    
    else:
        
        return HttpResponseForbidden('Only POST method accepted')

    
    
def controlSessionToken(request):    
    """
    URL : None
    
    Check if token is still present in memcache
    
    Params :
        HTTP HEADER : X-PM-APIKEY containing token
    
    Return :
        HTTP Forbidden Code (403) if failed
    """
    if 'HTTP_X_PM_APIKEY' in  request.META.keys():
        token = request.META['HTTP_X_PM_APIKEY']
        if getTokenToMemcache(token):
            return True
        else:
            return False
    else:
        return False



def setTokenToMemcache(token, uuid):
    """
    URL : none
    
    Store user session token in memcache 
    
    Params :
        uuid (str)  : account uuid
        token (str) : user session token
   
    Return:
        True or False (if caching failed)
        
    """
    nodes = [ ]
    for server in config.config().memcache['server']:
        srv, port = server.split(':')
        nodes.append((srv, int(port)))
        
    client = HashClient(nodes)
    memcExpirTime =  config.config().memcache['expire']
    
    res = False
    try:
        client.set(token, uuid, memcExpirTime)
        
        i_log = 'Login : ' + uuid + ' :: Token : ' + token
        apiLog.debug(i_log)
        res = True
    except Exception as e: 
        apiLog.fatal(e, exc_info=True)
        i_log = 'Login : caching token failed'
        apiLog.debug(i_log)
        pass  

    return res



def getTokenToMemcache(token):
    """
    URL : None
    
    Test if a token exists in memcached 
    
    Params:
        token (str) : user session token 
        
    Return:  
        True or False (if exists or not)
    
    """
    res = False
    nodes = [ ]
    for server in config.config().memcache['server']:
        srv, port = server.split(':')
        nodes.append((srv, int(port)))
    
    
    client = HashClient(nodes)
    memcExpirTime =  config.config().memcache['expire']

    if client.get(token):
        res = True
    
    
    return res
    
    

def newToken():
    """
    URL : none
    
    Return a token string 
           
    Return:  
        token (str)
    
    """
    token   = _uuid._uuid().new()
    
    return token
    


def retrieveUserElementFromToken(token):
    """
    URL : None
    
    Retrieve uuid from memcached while token
    Then get this uuid role & domain 
    
    Params:
        token (str) : user session token 
        
    Return:  
        res (dict) containing {'uuid': <uuid>, 'role': <role>, 'domain' : <domain uuid>}
        or None  in other cases
    """
    res = None
    nodes = [ ]
    for server in config.config().memcache['server']:
        srv, port = server.split(':')
        nodes.append((srv, int(port)))
        

    client = HashClient(nodes)
    memcExpirTime =  config.config().memcache['expire']
    
    if client.get(token):
        
        uuid = client.get(token).decode("utf-8")
        q_uuid = {"uuid": uuid}
        ns = _nosql._nosql()
        res_q_uuid = [ {'usr' : r } for r in ns.search(bucket='pm_user',query=q_uuid)]

    
        if len(res_q_uuid) == 1:
            jsDict = {}
            jsDict['uuid']           = res_q_uuid[0]['usr']['uuid']
            jsDict['role']           = res_q_uuid[0]['usr']['role']
            jsDict['domain']         = res_q_uuid[0]['usr']['domain']
            jsDict['subscribedguid'] = res_q_uuid[0]['usr']['subscribedguid']
            res = jsDict
            
    
    return res



@csrf_exempt  
def ping(request):
    """
    URL : /api/auth/ping
    
    Method : POST
    
    Refresh token expire datetime
    
    Returns:  
        True or False 
    
    """
    if request.method == 'POST':
        if controlSessionToken(request) is False:
            return HttpResponseForbidden('No valid session token')
        else:
            
            token = request.META['HTTP_X_PM_APIKEY']
            
            nodes = [ ]
            for server in config.config().memcache['server']:
                srv, port = server.split(':')
                nodes.append((srv, int(port)))
        
            client = HashClient(nodes)
            memcExpirTime =  config.config().memcache['expire']
            
            if client.get(token):
                uuid = client.get(token).decode("utf-8")                
                client.set(token, uuid, memcExpirTime)
                
                return JsonResponse({'state': 'authenticated'})
            
            else:
                
                return HttpResponseForbidden('No valid session token')

    else:
        return HttpResponseForbidden('Only POST method accepted')
    
