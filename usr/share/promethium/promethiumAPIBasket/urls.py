#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url
from django.urls import include, path


from . import views


urlpatterns = [
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/_all$', views._all),
    
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/add$', views.add),
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)/update$', views.update),
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)/delete$', views.delete),
    
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)/addRule$', views.addRule),
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)/updateRule/(?P<ruuid>[a-zA-Z0-9\-]+)', views.updateRule),
    
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)/details$', views.details),
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)/getRules$', views.getRules),
    url(r'^(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)/delRule/(?P<ruuid>[a-zA-Z0-9\-]+)', views.deleteRule),

]
