#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control, _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views

@csrf_exempt 
def add(request,domainUuid): 
    """
    URL : /api/basket/{domainUuid}/add
            
    Method : PUT
            
    Add a new auth or storage kind host (admin only)
        
    PUT URI Parameters:    
        domainUuid (str) : the domain uuid       
    
    PUT BODY JSON:      
        {
            # Mandatory
            "enable" <True|False if want to activate it>,
            "displayname": <human comprehensive name> ,
            "server": <mailbox server name>,
            "protocol": <imap|imaps|pop|pops>,
            "port": <TCP Port according to protocol>,
            "account": <email address> ,
            "passwd": <password according to email>,
            
            
            #Options 
            "verifypeer": <True|False if want cert control>,
            "automaticGroup": [ <UUID Group List>, ],
            "x-header": [ <x-header>, ], X-HEADER field you want to be catched from email e.g  "X-Spam-Score"
            "filterFile": [ <extension>, ], list of file extension which would not be stored
            "defaultColor": <#HTML color>, overline message with if set
            "every" : define every minute(s) the mailbox is parsed
            "imapBackupFolder": folder where to store email after parsing
            "imapFolder": folder where mail are stored before parsing / folder where to connect
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight( domainUuid=domainUuid ,token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
    if request.method == 'PUT':
        res = {'status': False} 
        
        mandatoryTupl = ('enable','displayname','server','protocol','port','account','passwd')
        optionsTupl   = ('verifypeer','asSender','automaticGroup','filterFile', 'x-header', 'every', 'imapFolder', 'imapBackupFolder','defaultColor')

        allTupl = mandatoryTupl + optionsTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        # Mandatories fields are present
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            uuid =  _uuid._uuid().new()
            
            if not 'verifypeer' in data.keys():
                jsd['verifypeer'] = False
            else: 
                jsd['verifypeer'] = data['verifypeer']
                
            if not 'every' in data.keys():
                jsd['every'] = 5
            else:
                jsd['every'] = int(data['every'])
            
            jsd['uuid']        = uuid
            jsd['owner']       = domainUuid
            jsd['enable']      = data['enable']
            jsd['displayname'] = data['displayname']
            jsd['server']      = data['server']
            jsd['protocol']    = data['protocol']
            jsd['port']        = data['port']
            jsd['account']     = data['account']
            jsd['passwd']      = _cypher._cypher().encypherIt(data['passwd'])
            
            
            if 'asSender' in data.keys():
                jsd['asSender'] = data['asSender']
              
            if 'automaticGroup' in data.keys():
                jsd['automaticGroup'] = data['automaticGroup']     

            if 'filterFile' in data.keys():
                jsd['filterFile'] = data['filterFile'] 

            if 'x-header' in data.keys():
                jsd['x-header'] = data['x-header']
                        
            if 'imapFolder' in data.keys():
                jsd['imapFolder'] = data['imapFolder']        
                
            if 'imapBackupFolder' in data.keys():
                jsd['imapBackupFolder'] = data['imapBackupFolder'] 

            if 'defaultColor' in data.keys():
                jsd['defaultColor'] = data['defaultColor'] 

                
            ns = _nosql._nosql()    
            resUpsert = ns.upsert(bucket='pm_basket',uuid=uuid, upsert= jsd )
    
            res = { 'status': resUpsert }
        return JsonResponse(res) 


@csrf_exempt 
def update(request,domainUuid,uuid): 
    """
    URL : /api/basket/{domainUuid}/{uuid}/update
            
    Method : POST     
      
    Update a domain basket entry (admin or domain only)
          
    POST URI Parameters:    
        domainUuid (str) : the domain uuid 
        uuid (str)       : the basket uuid      
          
    POST BODY JSON:      
        {
            # Mandatory
            "enable" <True|False if want to activate it>,
            "displayname": <human comprehensive name> ,
            "server": <mailbox server name>,
            "protocol": <imap|imaps|pop|pops>,
            "port": <TCP Port according to protocol>,
            "account": <email address> ,
            "passwd": <password according to email>,
            
            
            #Options 
            "verifypeer": <True|False if want cert control>,
            "automaticGroup": [ <UUID Group List>, ],
            "x-header": [ <x-header>, ], X-HEADER field you want to be catched from email e.g  "X-Spam-Score"
            "filterFile": [ <extension>, ], list of file extension which would not be stored
            "defaultColor": <#HTML color>, overline message with if set
            "every" : define every minute(s) the mailbox is parsed
            "imapBackupFolder": folder where to store email after parsing
            "imapFolder": folder where mail are stored before parsing / folder where to connect
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'POST':
        res = {'status': False} 
        
        mandatoryTupl = ('enable','displayname','server','protocol','port','account','passwd')
        optionsTupl   = ('verifypeer','asSender','automaticGroup','filterFile', 'x-header', 'every', 'imapFolder', 'imapBackupFolder','defaultColor')
        
        allTupl = mandatoryTupl + optionsTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        # Mandatories fields are present
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            
            if not 'verifypeer' in data.keys():
                jsd['verifypeer'] = False
            else:
                jsd['verifypeer'] = data['verifypeer']
                
                
            if not 'every' in data.keys():
                jsd['every'] = 5
            else:
                jsd['every'] = int(data['every'])
                
            jsd['uuid']        = uuid
            jsd['owner']       = domainUuid
            jsd['enable']      = data['enable']
            jsd['displayname'] = data['displayname']
            jsd['server']      = data['server']
            jsd['protocol']    = data['protocol']
            jsd['port']        = data['port']
            jsd['account']     = data['account']
            jsd['passwd']      = _cypher._cypher().encypherIt(data['passwd'])
            
            
            if 'asSender' in data.keys():
                jsd['asSender'] = data['asSender']
              
            if 'subscribedguid' in data.keys():
                jsd['subscribedguid'] = data['subscribedguid']     

            if 'filterFile' in data.keys():
                jsd['filterFile'] = data['filterFile'] 

            if 'x-header' in data.keys():
                jsd['x-header'] = data['x-header']
                
                        
            if 'imapFolder' in data.keys():
                jsd['imapFolder'] = data['imapFolder']        
                
            if 'imapBackupFolder' in data.keys():
                jsd['imapBackupFolder'] = data['imapBackupFolder'] 

            if 'defaultColor' in data.keys():
                jsd['defaultColor'] = data['defaultColor'] 
            
            ns = _nosql._nosql()    
            resUpsert = ns.upsert(bucket='pm_basket',uuid=uuid, upsert= jsd )
     
            res = { 'status': resUpsert }

            
            
        return JsonResponse(res) 


@csrf_exempt 
def delete(request,domainUuid,uuid): 
    """
    URL : /api/basket/{domainUuid}/{uuid}/delete
            
    Method : DELETE
            
    Delete basket if not in use (admin only)
            
    DELETE URI Parameters:    
        domainUuid (str) : the domain uuid 
        uuid (str)       : the basket uuid
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'DELETE':
        res = {'status': False}

        ns = _nosql._nosql()
        resRem = ns.remove(bucket='pm_basket',uuid=uuid )

        res = {'status': resRem} 

        return JsonResponse(res) 



def details(request,domainUuid,uuid): 
    """
    URL : /api/basket/{domainUuid}/{uuid}/details
            
    Method : GET
            
    Retrieve all information of basket (admin or domain only)
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
        uuid (str)       : the basket uuid
            
    Return :    
        JSON {'details': ['basket': {...}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'details': None}
        q_uuid = {"uuid": uuid}
        ns = _nosql._nosql()
        resSearch = [ {'basket':r} for r  in ns.search(bucket='pm_basket',query=q_uuid )]
        
        if len(resSearch) > 0:    
            if 'passwd' in resSearch[0]['basket'].keys():
                resSearch[0]['basket']['passwd'] = _cypher._cypher().decypherIt(resSearch[0]['basket']['passwd'])['passwd']

        res = {'details': resSearch} 
        return JsonResponse(res) 
    
    
    
def _all(request,domainUuid): 
    """
    URL : /api/basket/{domainUuid}/_all
            
    Method : GET
            
    Retrieve all host uuids & displayname (admin only)
            
    GET URI Parameters:      
            
    Return :    
        JSON {'all': [{"displayname": "test", "uuid": "09D2B621-9852-4A52-8737-937328F8DB40"},]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'all': None}
        q_uuid = [{"$match":{'owner': domainUuid}},
                  {"$sort":{"displayname":1}},
                  {"$unset":["_id"]}, 
                  ]
        ns = _nosql._nosql()
        try:
            resSearch = [ r for r  in ns.aggregate(bucket='pm_basket', query= q_uuid )]
            res = {'all': resSearch} 
        except Exception as e:
            pass   
        
        return JsonResponse(res) 


        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates basket "' + request.POST.get('displayname') + '"'   
        else:
            i_log = request.session['user']['login'] + ' failed  updating basket "' + request.POST.get('displayname') + '"'  
        
        frontOfficeLog.info(i_log)



@csrf_exempt 
def addRule(request, domainUuid, uuid):
    """
    URL : /api/basket/{domainUuid}/{uuid}/addRule
            
    Method : POST
            
    Add  a new filter rule for some basket (admin domain only)
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
        uuid (str)       : the basket uuid

    POST BODY JSON:      
        {
            # Mandatory
            "match" <AND|OR>,
            "displayname": <human comprehensive name> ,
            "rules": {'uuid': {'field': <From|Subject|Body>, 'verb: <is|contains>, 'string': 'some string'}, ...},
            "subscribedguid": ["group uuid", "group uuid"],
        }
        
    Return :    
        JSON {'status': None|True}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'POST':
        res = {'status': None}
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        mandatoryTupl = ('match','displayname','rules','subscribedguid')

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            q_uuid = {"uuid": uuid}
            ns = _nosql._nosql()
            resSearch = [ {'basket':r} for r  in ns.search(bucket='pm_basket',query=q_uuid )]
            
            if len(resSearch) > 0:              
                jsd = resSearch[0]['basket']
                ruuid =  _uuid._uuid().new()
                
                if not 'rules' in jsd.keys():
                    jsd['rules'] = {}               
                jsd['rules'][ruuid] = data
                ns = _nosql._nosql()    
                resUpsert = ns.upsert(bucket='pm_basket',uuid=uuid, upsert= jsd )
                res = { 'status': resUpsert }
        
        return JsonResponse(res) 
    
    
    
def getRules(request,domainUuid,uuid): 
    """
    URL : /api/basket/{domainUuid}/{uuid}/getRules
            
    Method : GET
            
    Retrieve all information of basket (admin domain only)
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
        uuid (str)       : the basket uuid
            
    Return :    
        JSON {'rules': {'uuid': {'field': <From|Subject|Body>, 'verb: <is|contains>, 'string': 'some string'}, ...}}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'rules': None}
        q_uuid = {"uuid": uuid}
        ns = _nosql._nosql()
        resSearch = [ {'basket':r} for r  in ns.search(bucket='pm_basket',query=q_uuid )]
        
        if len(resSearch) > 0:    
            if 'rules' in resSearch[0]['basket'].keys():
                rules = resSearch[0]['basket']['rules']

        res = {'rules': rules} 
        return JsonResponse(res) 
    
    

@csrf_exempt 
def updateRule(request,domainUuid,uuid,ruuid):
    """
    URL : /api/basket/{domainUuid}/{uuid}/updateRule/{ruuid}
            
    Method : POST
            
    Update  a filter rule for some basket (admin domain only)
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
        uuid (str)       : the basket uuid
        ruuid (str)      : the rule uuid

    POST BODY JSON:      
        {
            # Mandatory
            "match" <AND|OR>,
            "displayname": <human comprehensive name> ,
            "rules": {'uuid': {'field': <From|Subject|Body>, 'verb: <is|contains>, 'string': 'some string'}, ...},
            "subscribedguid": ["group uuid", "group uuid"],
        }
        
    Return :    
        JSON {'status': None|True}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    if request.method == 'POST':
        res = {'status': None}
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        mandatoryTupl = ('match','displayname','rules','subscribedguid')

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            q_uuid = {"uuid": uuid}
            ns = _nosql._nosql()
            resSearch = [ {'basket':r} for r  in ns.search(bucket='pm_basket',query=q_uuid )]

            if len(resSearch) > 0:              
                jsd = resSearch[0]['basket']
                              
                jsd['rules'][ruuid] = data
                
                ns = _nosql._nosql()    
                resUpsert = ns.upsert(bucket='pm_basket',uuid=uuid, upsert= jsd )
                res = { 'status': resUpsert }
        
        return JsonResponse(res) 



@csrf_exempt 
def deleteRule(request,domainUuid,uuid,ruuid):
    """
    URL : /api/basket/{domainUuid}/{uuid}/delRule/{ruuid}
            
    Method : DELETE
            
    Delete a filter rule (ruuid) for some basket (uuid) (admin domain only)
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
        uuid (str)       : the basket uuid
        ruuid (str)      : the rule uuid to delete
        
    Return :    
        JSON {'status': None|True}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'DELETE':
        res = {'status': None}
        
        q_uuid = {"uuid": uuid}
        ns = _nosql._nosql()
        resSearch = [ {'basket':r} for r  in ns.search(bucket='pm_basket',query=q_uuid )]
            
        if len(resSearch) > 0:              
            jsd = resSearch[0]['basket']
            
            del jsd['rules'][ruuid]
            ns = _nosql._nosql()    
            resUpsert = ns.upsert(bucket='pm_basket',uuid=uuid, upsert= jsd )
            res = { 'status': resUpsert }
        
    return JsonResponse(res) 

