#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
import promethiumAPIAuth.views as promethiumAPIAuth_views



def add(request,domainUuid): 
    """
    URL : None
            
    Method : Internal
            
    Add a new domain chrono index
    Called when new domain created
    
    PUT URI Parameters:    
        domainUuid (str) : the domain uuid       
    
    PUT BODY JSON:      
        {
            # Mandatory
            "counter" <int value>           
            
            #Options 
            "reserved": <int value>,
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """

    res = {'status': False} 
    jsd = {}
    jsd['uuid']     = domainUuid
    jsd['counter']  = 1
    jsd['reserved'] = 0
            
            
    ns = _nosql._nosql()    
    resUpsert = ns.upsert(bucket='pm_chrono',uuid=domainUuid, upsert= jsd )
    

    res = { 'status': resUpsert }
    return JsonResponse(res) 



@csrf_exempt 
def update(request,domainUuid): 
    """
    URL : /api/chrono/{domainUuid}/update
            
    Method : POST     
      
    Update a domain chrono counter entry
          
    POST URI Parameters:    
        domainUuid (str) : the domain uuid      
          
    POST BODY JSON:      
        {
            # Mandatory
            
            #Options 
            "counter": <integer>,
            "reserved": <integer>
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        res = {'status': False} 
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        q_uuid = {'uuid': domainUuid}
        ns = _nosql._nosql()
        resSearch = [ {'chrono': r} for r  in ns.search(bucket='pm_chrono',query=q_uuid )]


        jsd = {}
        jsd['uuid']     = domainUuid
        jsd['counter']  = resSearch[0]['chrono']['counter']
        jsd['reserved'] = resSearch[0]['chrono']['reserved']
        
        # Mandatories fields are present
        if data['counter']:
            jsd['counter'] = int(data['counter'])
      
        ns = _nosql._nosql()    
        resUpsert = ns.upsert(bucket='pm_chrono',uuid=domainUuid, upsert= jsd )
        res =  {'status': resUpsert} 

            
        return JsonResponse(res) 



@csrf_exempt 
def delete(request,domainUuid): 
    """
    URL : /api/chrono/{domainUuid}/delete
            
    Method : DELETE
            
    Delete the chrono entry in Elastic for this domain
            
    DELETE URI Parameters:    
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'DELETE':
        res = {'status': False}

        ns = _nosql._nosql()
        try:
            resRem = ns.remove(bucket='pm_chrono',uuid=domainUuid)
            res = {'status': resRem}
        except:
            pass

        return JsonResponse(res) 



def get(request,domainUuid): 
    """
    URL : /api/chrono/{domainUuid}/get
            
    Method : GET
            
    Retrieve the last free numerical id
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'chrono': {'counter':<int>, 'reserved':<int>}}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """


    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res= {'chrono':{'counter':1, 'reserved':0}}      
        q_uuid = {'uuid': domainUuid}
        ns = _nosql._nosql()
        resSearch = [ {'chrono': r} for r  in ns.search(bucket='pm_chrono',query=q_uuid )]
        res['chrono']['counter']  =  resSearch[0]['chrono']['counter']
        res['chrono']['reserved'] =  resSearch[0]['chrono']['reserved']
        return JsonResponse(res) 
    
   

def getUpdateCounter(request,domainUuid): 
    """
    URL : /api/chrono/{domainUuid}/getUpdateCounter
            
    Method : GET
            
    Retrieve the last free numerical id and update it after
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'chrono': {'counter':<int>, 'reserved':<int>}}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':     
        res = {'status': False}
        q_uuid = {'uuid': domainUuid}
        ns = _nosql._nosql()
        resSearch = [ {'chrono': r}  for r  in ns.search(bucket='pm_chrono',query=q_uuid )]

        jsd = {}
        jsd['uuid']     = domainUuid
        jsd['counter']  = int(resSearch[0]['chrono']['counter']) + 1 
        jsd['reserved'] = resSearch[0]['chrono']['reserved']
        
        ns = _nosql._nosql()    
        resUpsert = ns.upsert(bucket='pm_chrono',uuid=domainUuid, upsert= jsd )
        res =  {'status': resUpsert} 
        
        return JsonResponse(res)    
   
   
@csrf_exempt
def reserve(request,domainUuid): 
    """
    URL : /api/chrono/{domainUuid}/reserve
            
    Method : POST
            
    Reserve a numerical id for an outgoing message 
            
    POST URI Parameters:      
            
    Return :    
        JSON {'all': [{"displayname": "test", "uuid": "09D2B621-9852-4A52-8737-937328F8DB40"},]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'POST':
        res = {'status': False} 
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        ns = _nosql._nosql()
        q_uuid = {'uuid': domainUuid}
        resSearch = [ {'chrono': r} for r  in ns.search(bucket='pm_chrono',query=q_uuid )]

        jsd = {}
        jsd['uuid']     = domainUuid
        jsd['counter']  = resSearch[0]['chrono']['counter']
        jsd['reserved'] = resSearch[0]['chrono']['reserved']
        
        # Mandatories fields are present
        if data['reserved']:
            jsd['reserved'] = int(data['reserved'])  
            
            if 'counter' in data.keys():
                if data['counter'] == data['reserved']:
                    jsd['counter'] = int(data['counter']) + 1 
            
        ns = _nosql._nosql()    
        resUpsert = ns.upsert(bucket='pm_chrono',uuid=domainUuid, upsert= jsd )
        res =  {'status': resUpsert} 
            
        return JsonResponse(res) 


   
