#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control, _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views
import promethiumAPIChrono.views as promethiumAPIChrono_views

@csrf_exempt 
def add(request): 
    """
    URL : /api/domain
            
    Method : PUT
            
    Add a new domain (admin only)
          
    PUT BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
            "authserver": <auth server uuid>
            "storeserver": <store server uuid>
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'PUT':
        res = {'status': False} 
        mandatoryTupl = ('displayname','authserver','storeserver')
        allTupl       = mandatoryTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            uuid =  _uuid._uuid().new()
                
            jsd['uuid']         = uuid
            jsd['displayname']  = data['displayname']
            jsd['authserver']   = data['authserver']
            jsd['storeserver']  = data['storeserver']
            jsd['logo']         = data['logo']
            jsd['cepdTreshold'] = data['cepdTreshold']
            
            ns = _nosql._nosql()
            resUpsert = ns.upsert(bucket='pm_domain',uuid=uuid, upsert= jsd )            
            res = { 'status': resUpsert }
            promethiumAPIChrono_views.add(request, uuid)
            i_log = 'adding domain ' + data['displayname'] + ' status ' + str(resUpsert)
        else:
            i_log = 'adding domain failed, one of field (' + ','.join(mandatoryTupl) + ') missing'
        
        apiLog.info(i_log)

        return JsonResponse(res) 



@csrf_exempt 
def update(request,uuid): 
    """
    URL : /api/domain/{uuid}/update
            
    Method : POST
            
    Update a domain (admin only)
        
    POST URI Parameters:      
        uuid (str)   : the domain uuid      
          
    POST BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
            "authserver": <auth server uuid>
            "storeserver": <store server uuid>
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'POST':
        res = {'status': False}
        mandatoryTupl = ('displayname','authserver','storeserver')
        allTupl       = mandatoryTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            jsd['uuid']         = uuid
            jsd['displayname']  = data['displayname']
            jsd['authserver']   = data['authserver']
            jsd['storeserver']  = data['storeserver']
            jsd['logo']         = data['logo']
            jsd['cepdTreshold'] = data['cepdTreshold']
            
            ns = _nosql._nosql()
            resUpsert = ns.upsert(bucket='pm_domain',uuid=uuid, upsert= jsd )
            res = { 'status': resUpsert }
            i_log = 'updating domain ' + data['displayname'] + ' status ' + str(resUpsert)
        else:
            i_log = 'updating domain failed, one of field (' + ','.join(mandatoryTupl) + ') missing'
        
        apiLog.info(i_log)

        return JsonResponse(res) 



@csrf_exempt 
def updatePart(request,uuid): 
    """
    URL : /api/domain/{uuid}/updatePart
            
    Method : POST
            
    Update a domain (admin only)
        
    POST URI Parameters:      
        uuid (str)   : the domain uuid      
          
    POST BODY JSON:      
        {
            # Optionnal
            "logo":
            "defaultpage":
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'POST':
        res = {'status': False} 
        jsd = {} 
        
        q_uuid = {'uuid':uuid}
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.search(bucket='pm_domain',query=q_uuid )]

        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        jsd['uuid']         = uuid
        jsd['displayname']  = resSearch[0]['displayname']
        jsd['authserver']   = resSearch[0]['authserver']
        jsd['storeserver']  = resSearch[0]['storeserver']
        jsd['logo']         = resSearch[0]['logo']
        
        if 'smtp' in resSearch[0].keys():
            jsd['smtp'] = resSearch[0]['smtp']
        
        if 'defaultpage' in resSearch[0].keys():
            jsd['defaultpage'] = resSearch[0]['defaultpage']
            
        if 'logo' in data.keys():
            jsd['logo'] = data['logo']
        
        if 'defaultpage' in data.keys():
            jsd['defaultpage'] = data['defaultpage']  
        
        ns = _nosql._nosql()
        resUpsert = ns.upsert(bucket='pm_domain',uuid=uuid, upsert= jsd )
        res = { 'status': resUpsert }
        i_log = 'updating domain ' + jsd['displayname'] + 'element  status ' + str(resUpsert)
        apiLog.info(i_log)

        return JsonResponse(res) 



@csrf_exempt 
def delete(request,uuid): 
    """
    URL : /api/domain/{uuid}/delete
            
    Method : DELETE
            
    Delete domain and all what it contains
            
    DELETE URI Parameters:      
        uuid (str)   : the domain uuid
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    res = {'status': False}
    
    if request.method == 'DELETE':
        ns = _nosql._nosql()
        resRem = ns.remove(bucket='pm_domain',uuid=uuid )
        res = {'status': resRem} 
        i_log = 'deleting domain ' + uuid + ' status ' + str(resRem)
    else:
        i_log = 'deleting domain failed'
    
    apiLog.info(i_log)
            
    return JsonResponse(res) 



def details(request,uuid): 
    """
    URL : /api/domain/{uuid}/details
            
    Method : GET
            
    Retrieve all information of domain
            
    GET URI Parameters:      
        uuid (str)   : the domain uuid
            
    Return :    
        JSON {'details': [{'pm_domain': {...}}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
    
    
    if request.method == 'GET':
        res = {'details': None}
        q_uuid = {"uuid": uuid}
        ns = _nosql._nosql()
        try:
            resSearch = [ {'pm_domain':r } for r  in ns.search(bucket='pm_domain',query=q_uuid )]
            res = {'details': resSearch} 
        except:
            pass
        return JsonResponse(res) 
    
    
    
def _all(request): 
    """
    URL : /api/domain/_all
            
    Method : GET
            
    Retrieve all domain spec (admin only)
            
    GET URI Parameters:      
            
    Return :    
        JSON {'all': [{"displayname": "test", "uuid": "09D2B621-9852-4A52-8737-937328F8DB40","authserver":"...", "storeserver":"..."},]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'all': None}
        
        #q_uuid = 'select * from `pm_domain` order by displayname;'
        q_uuid =[{"$sort":{"displayname":1}},{"$unset":["_id"]}]
        ns = _nosql._nosql()
        resSearch = [ r  for r  in ns.aggregate(bucket='pm_domain',query=q_uuid )]
        res = {'all': resSearch} 
        return JsonResponse(res) 

    

def _admins(request,uuid): 
    """
    URL : /api/domain/{uuid}/_admins
            
    Method : GET
            
    Retrieve all admins of domain (admin only)
            
    GET URI Parameters:      
        uuid (str)   : the domain uuid
            
    Return :    
        JSON {'admins': [{...}}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'admins': None}
        
        q_uuid = {"$and":[{"domain": uuid} , {"role":"domain"}]}
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.search(bucket='pm_user',query=q_uuid )]
        
        res = {'admins': resSearch} 
        return JsonResponse(res) 
   

def smtp(request,uuid): 
    """
    URL : /api/domain/{uuid}/smtp
            
    Method : GET
            
    Retrieve smtp params of domain (admin only)
            
    GET URI Parameters:      
        uuid (str)   : the domain uuid
            
    Return :    
        JSON {'smtp':None | {'server': '....', 'account': '....', 'passwd': '...', 'protocol': '...', 'port': '..', 'verifypeer': 'True|False'}}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
    
    if not _control._control().controlDomainAccessRight(domainUuid=uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'GET':
        res = {'smtp': None}
        q_uuid = {"uuid": uuid}
        ns = _nosql._nosql()
        try:
            resSearch = [ {'pm_domain':r } for r  in ns.search(bucket='pm_domain',query=q_uuid )]
            smtp = resSearch[0]['pm_domain']['smtp']
            SMTP = {"server": smtp['server'],
                    "account": smtp['account'] ,
                    "passwd": _cypher._cypher().decypherIt(smtp['passwd'])['passwd'],
                    "protocol": smtp['protocol'] ,
                    "port": smtp['port'] ,
                    "verifypeer": smtp['verifypeer'] }
            
            res = {'smtp': SMTP} 
        except:
            pass
        return JsonResponse(res) 


@csrf_exempt 
def smtpSet(request,uuid): 
    """
    URL : /api/domain/{uuid}/smtp/set
            
    Method : POST
            
    Set smtp params of domain (admin only)
            
    POST BODY JSON:      
        {
            # Mandatory
            "server": <mailbox server name>,
            "protocol": <smtp|smtps|ssl-tls>,
            "port": <TCP Port according to protocol>,
            "account": <email address> ,
            "passwd": <password according to email>,
            
            #Options 
            "verifypeer": <True|False if want cert control>,
        }
            
    Return :    
        JSON {'status': True|False]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'POST':
        res = {'status': False} 
        jsd = {} 

        mandatoryTupl = ('server','protocol','port','account','passwd')

        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        # Mandatories fields are present
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            
            q_uuid = {'uuid':uuid}
            ns = _nosql._nosql()
            resSearch = [ r for r  in ns.search(bucket='pm_domain',query=q_uuid )]
                
            jsd['uuid']         = uuid
            jsd['displayname']  = resSearch[0]['displayname']
            jsd['authserver']   = resSearch[0]['authserver']
            jsd['storeserver']  = resSearch[0]['storeserver']
            jsd['logo']         = resSearch[0]['logo']
            jsd['cepdTreshold'] = resSearch[0]['cepdTreshold']
            
            if 'defaultpage' in resSearch[0].keys():
                jsd['defaultpage'] = resSearch[0]['defaultpage']
            
            if 'smtp' in resSearch[0].keys() and resSearch[0]['smtp'] is not None:
                jsd['smtp'] = resSearch[0]['smtp']
            else:
                jsd['smtp'] = {"server":"", "account":"", "passwd": "", "protocol": "" , "port":"" , "verifypeer": False, }
            
            if len(data['server']) > 0:
                jsd['smtp']['server']     = data['server']
                jsd['smtp']['account']    = data['account']
                jsd['smtp']['passwd']     = _cypher._cypher().encypherIt(data['passwd'])
                jsd['smtp']['protocol']   = data['protocol']
                jsd['smtp']['port']       = data['port'] 
                
            if 'verifypeer' in data.keys():
                jsd['smtp']['verifypeer'] = data['verifypeer']
            else:
                jsd['smtp']['verifypeer'] = False

            ns = _nosql._nosql()
            resUpsert = ns.upsert(bucket='pm_domain',uuid=uuid, upsert= jsd )
            res = { 'status': resUpsert }
            i_log = 'updating domain ' + jsd['displayname'] + 'element  status ' + str(resUpsert)
            apiLog.info(i_log)

        return JsonResponse(res) 



def isSmtpEnabled(request,uuid): 
    """
    URL : /api/domain/{uuid}/smtp/enabled
            
    Method : GET
            
    Get info if smtp params defined 
            
    POST URI Parameters:      
        uuid (str)   : the domain uuid
            
    Return :    
        JSON {'enabled': True|False]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')

    if request.method == 'GET':
        res = {'enabled': False}
        q_uuid = {"uuid": uuid}
        ns = _nosql._nosql()
        try:
            resSearch = [ {'pm_domain':r } for r  in ns.search(bucket='pm_domain',query=q_uuid )]
            if 'server' in resSearch[0]['pm_domain']['smtp'].keys():
                res = {'enabled': True} 
        except:
            pass
        
        return JsonResponse(res) 



@csrf_exempt 
def smtpReset(request,uuid): 
    """
    URL : /api/domain/{uuid}/smtp/reset
            
    Method : POST
            
    Reset smtp params of domain (admin only)
            
    POST URI Parameters:      
        uuid (str)   : the domain uuid
            
    Return :    
        JSON {'status': True|False]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'POST':
        res = {'status': False} 
        jsd = {} 
        
        q_uuid = {'uuid':uuid}
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.search(bucket='pm_domain',query=q_uuid )]
               
        jsd['uuid']         = uuid
        jsd['displayname']  = resSearch[0]['displayname']
        jsd['authserver']   = resSearch[0]['authserver']
        jsd['storeserver']  = resSearch[0]['storeserver']
        jsd['logo']         = resSearch[0]['logo']
        jsd['cepdTreshold'] = resSearch[0]['cepdTreshold']
        jsd['smtp']         = None
        if 'defaultpage' in resSearch[0].keys():
            jsd['defaultpage'] = resSearch[0]['defaultpage']
        
        ns = _nosql._nosql()
        resUpsert = ns.upsert(bucket='pm_domain',uuid=uuid, upsert= jsd )
        res = { 'status': resUpsert }
        i_log = 'updating domain ' + jsd['displayname'] + 'element  status ' + str(resUpsert)
        apiLog.info(i_log)

        return JsonResponse(res) 
