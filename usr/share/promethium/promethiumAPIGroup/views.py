#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
 # *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views




def _all(request,domainUuid): 
    """
    URL : /api/group/{domainUuid}/_all
            
    Method : GET
            
    Retrieve all group for Domain {domainUuid}
            
    GET URI Parameters:      
        domainUuid (str)   : the domain uuid
            
    Return :    
        JSON {'all': {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDispatcherRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'GET':
        res = {}
        res['all'] = None 

        q_uuid = [{"$match":{'owner': domainUuid}},
                  {"$sort":{"displayname":1}},
                  {"$unset":["_id"]}
                  ]
        ns = _nosql._nosql()
        try:
            res_q_uuid = [ r  for r in ns.aggregate(bucket='pm_group',query=q_uuid)]
            if len(res_q_uuid) > 0 :
                res['all'] = res_q_uuid
        except Exception as e:
            pass   
            
        return JsonResponse(res) 

            
@csrf_exempt 
def add(request,domainUuid):
    """
    URL : /api/group/{domainUuid}/add
            
    Method : PUT
            
    Add a new domain group (need admin|domain role)
           
    PUT URI Parameters: 
        domainUuid (str) : the domain uuid  

    PUT BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
            "parent": <Root|group parent uuid>
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'PUT':
            res = {'status': False} 
            
            mandatoryTupl = ('displayname','parent')
            
            allTupl       = mandatoryTupl 
            
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)

            if all(jskey in data.keys() for jskey in mandatoryTupl):
                jsd = {}
                uuid =  _uuid._uuid().new()
                    
                jsd['uuid']        = uuid
                jsd['displayname'] = data['displayname']
                jsd['owner']       = domainUuid
                if data['parent'] is None:
                    jsd['parent'] = 'Root'
                else:
                    jsd['parent']      = data['parent']
                
                ns = _nosql._nosql()
                resUpsert = ns.upsert(bucket='pm_group',uuid=uuid, upsert= jsd )
        
                res = { 'status': resUpsert }

            return JsonResponse(res) 



@csrf_exempt 
def update(request,domainUuid,uuid):
    """
    URL : /api/group/{domainUuid}/{uuid}/update
            
    Method : POST
            
    Update a domain group (need admin|domain role)
       
    POST URI Parameters: 
        domainUuid (str) : the domain uuid  
        uuid (str)       : the group uuid
        
    POST BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
            "parent": <None|group parent uuid>
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDispatcherRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'POST':
        res = {'status': False} 
        
        mandatoryTupl = ('displayname','parent')
        
        allTupl       = mandatoryTupl 
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            
            jsd['uuid']        = uuid
            jsd['displayname'] = data['displayname']
            jsd['owner']       = domainUuid
            jsd['parent']      = data['parent']
            
            ns = _nosql._nosql()
            resUpsert = ns.upsert(bucket='pm_group',uuid=uuid, upsert= jsd )
    
            
            res = { 'status': resUpsert }

        return JsonResponse(res) 



def controlIfGroupInUse(domainUuid,uuid):
    """ 
    URL : None
        
    Try to query data from NoSQL
        
    Parameters:
        domainUuid (str) : the domain uuid
        uuid (str)       : the group uuid 

    Returns:
        JSON { 'list' : [ {'login': 'value'}, ] }  
    """    
    res = {'list': None}
    #q_uuid = 'select login from `pm_user` where  domain="' + domainUuid + '"  and subscribedguid like "%' + uuid + '%" ;'   
    ns = _nosql._nosql()
    q_uuid = [{"$and":[{'domain':domainUuid},{'subscribedguid':uuid}]},{ 'login': 1}]
    try: 
        res_q_uuid = [ {'pm_user': r} for r in ns.aggregate(bucket='pm_domain',query=q_uuid)]
        if len(res_q_uuid) > 0:
            res = {'list': res_q_uuid}
    except:
        pass
        
    return res 



@csrf_exempt 
def delete(request,domainUuid,uuid):
    """
    URL : /api/group/{domainUuid}/{uuid}/delete
            
    Method : DELETE
            
    DELETE domain group and any reference of it
            
    POST URI Parameters: 
        domainUuid (str) : the domain uuid  
        uuid (str)       : the group uuid
            
    Return :    
        JSON {'status': True} if OK or list of subscribed user(s)
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDispatcherRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'DELETE':
        res = {'status': False}
        isInUse = controlIfGroupInUse(domainUuid,uuid)

        if isInUse['list'] is not None :
            i_log = 'UUID group ' + uuid + ' is still in use'
            apiLog.info(i_log)
            return JsonResponse(isInUse) 
        else:
            ns = _nosql._nosql()
            resRem = ns.remove(bucket='pm_group',uuid=uuid )
    
            
            res = {'status': resRem} 
            return JsonResponse(res) 



def _allSubscribed(request,domainUuid,uuid):
    """
    URL : /api/group/{domainUuid}/{uuid}/_allSubscribed
            
    Method : GET
            
    Retrieve all subscribed user for this domain {domainUuid} group {uuid}
            
    Get Parameters:      
        domainUuid (str) : the domain uuid
        uuid (str)       : the group uuid
            
    Return :    
        JSON {'subscribedUser': {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDispatcherRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    
    if request.method == 'GET':
        res = {}
        res['subscribedUser'] = None 
        q_uuid = [{"$match":{"$and":[{'domain': domainUuid}, {'subscribedguid':uuid}]}},
                  {"$project": {"_id":0,"uuid":1,"login":1,"firstname":1,"lastname":1}},
                 ]  
        ns = _nosql._nosql()
        res_q_uuid = [ r for r in ns.aggregate(bucket='pm_user',query=q_uuid)]      
        
        if len(res_q_uuid) > 0 :
            res['subscribedUser'] = res_q_uuid
            
            
        return JsonResponse(res) 


def _allUnsubscribed(request,domainUuid,uuid):
    """
    URL : /api/group/{domainUuid}/{uuid}/_allUnsubscribed
            
    Method : GET
            
    Retrieve all unsubscribed user for this domain {domainUuid} group {uuid}
            
    Get Parameters:      
        domainUuid (str) : the domain uuid
        uuid (str)       : the group uuid
            
    Return :    
        JSON {'unsubscribedUser': {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """

    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDispatcherRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'GET':
        res = {}
        allUuid = []
        res['unsubscribedUser'] = None
        ns = _nosql._nosql()
        
        # 1rst get all members
        q_alluuid = [{"$match":{"domain": domainUuid}},{"$project": {"_id":0,"uuid":1,"login":1,"firstname":1,"lastname":1}},]
        res_q_all= [ r for r in ns.aggregate(bucket='pm_user',query=q_alluuid)]

        # 2nd get all subscribed members
        q_uuid =[{"$match":{"$and":[{'domain': domainUuid}, {'subscribedguid':uuid}]}}, {"$project": {"_id":0,"uuid":1,"login":1,"firstname":1,"lastname":1}},]  
        res_q_uuid = [ r for r in ns.aggregate(bucket='pm_user',query=q_uuid)]      

        
        if len(res_q_uuid) > 0 :
            res['unsubscribedUser'] = [ x for x in res_q_all if x not in res_q_uuid ]
        else:
            res['unsubscribedUser'] = res_q_all
            
        return JsonResponse(res) 




def details(request,domainUuid,uuid):
    """
    URL : /api/group/{domainUuid}/{uuid}/details
            
    Method : GET
            
    Retrieve details of group {uuid} (need admin|domain role)
            
    Get Parameters:
        domainUuid (str) : the domain uuid
        uuid (str)       : the group uuid
            
    Return :    
        JSON {'details': ['pm_group':{...}}] if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'GET':
        res = {}
        res['details'] = None

        q_uuid = {'uuid':uuid}
        ns = _nosql._nosql()
        try: 
            res_q_uuid = [ {'pm_group':r} for r in ns.search(bucket='pm_group',query=q_uuid)]
            if len(res_q_uuid) > 0 :
                res['details'] = res_q_uuid
        except:
            pass
            
        return JsonResponse(res) 



def displayname(request,domainUuid,uuid):
    """
    URL : /api/group/{domainUuid}/{uuid}/displayname
            
    Method : GET
            
    Retrieve displayname of group {uuid}
            
    Get Parameters:      
        uuid (str)       : the group uuid
            
    Return :    
        JSON {'displayname': <displayname>} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
    
    
    tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])  
    
    if not _control._control().controlAccessRight(uuid=tokenElmt['uuid'], token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    
    if request.method == 'GET':
        res = {}
        res['displayname'] = None 
        q_uuid = [{"$match":{'uuid': uuid}},
                  {"$project": {"_id":0,"displayname":1}},
                 ]  
        ns = _nosql._nosql()
        res_q_uuid = [ r for r in ns.aggregate(bucket='pm_group',query=q_uuid)]
        if len(res_q_uuid) > 0 :
            res['displayname'] = res_q_uuid
            
            
        return JsonResponse(res) 


@csrf_exempt 
def getGroupIdFromName(request,domainUuid):
    """ 
    URL : /api/group/{domainUuid}/uuidFromDisplayname
        
    Method : POST
                
    Retrieve id group for Domain {domainUuid} where post
                
    POST URI Parameters:      
        domainUuid (str)   : the domain uuid
              
    POST BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
        }          
              
    Return :    
        JSON {'uuid': 'xxxx-xxxxxx-xxxxx'} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
                
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        res = {'uuid': None} 
        
        mandatoryTupl = ('displayname',)
        
        allTupl       = mandatoryTupl 
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            displayname = data['displayname']
        
            ns = _nosql._nosql()
            q_uuid = [{"$match":{"$and":[{'owner': domainUuid}, {'displayname':displayname}]}},{"$project": {"_id":0,"uuid":1}},]
            try: 
                res_q_uuid = [ r for r in ns.aggregate(bucket='pm_group',query=q_uuid)]
                if len(res_q_uuid) == 1:
                    res['uuid'] = res_q_uuid[0]['uuid']
            except:
                pass
            
        return JsonResponse(res)  



def _getGroupIdFromName(domainUuid, displayname):
    """ 
    URL : None
        
    Try to query data from NoSQL
        
    Parameters:
        domainUuid (str) : the domain uuid
        displayname (str)       : the group name 

    Returns:
        value  
    """ 
    res = None
   
    ns = _nosql._nosql()
    q_uuid = [{"$match":{"$and":[{'owner': domainUuid}, {'displayname':displayname}]}},{"$project": {"_id":0,"uuid":1}},]
    try: 
        res_q_uuid = [ r for r in ns.aggregate(bucket='pm_group',query=q_uuid)]
        if len(res_q_uuid) == 1:
            res = res_q_uuid[0]['uuid']
    except:
        pass
        
    return res 

