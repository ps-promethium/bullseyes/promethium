#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control, _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views

yamlFile          = '/etc/promethium/promethium.yml'



@csrf_exempt 
def add(request): 
    """
    URL : /api/host/add
            
    Method : PUT
            
    Add a new auth or storage kind host (admin only)
          
    PUT BODY JSON:      
        {
            # Mandatory  both case (store or auth)
            "displayname": <human comprehensive name> 
            "role": <store|auth>
            
            #Auth params
            "mechanism": <Internal|LDAP|BlueMind|SAML>, mandatory
            # If not internal
            "URI": <URI>
            # If LDAP auth
            "baseDn": <DN where to start query>
            "userDn": <admin dn for user add autocompletion> ,
            "userPassword": <admin  password>
            "searchAttribute": <mail|uid|login|...>
            "verifypeer": <True|False if want cert control>
            # If SAML auth
            "sp_proto": <https|http>
            "sp_fqdn": the SP FQDN, your promethium instance FQDN
            "idp_entity_id": The IdP entity id
            "idp_url": the IdP URL 
            "idp_x509" : the IdP X509 cert 

            # Storage params
            "mechanism": <Internal|CEPh|SWIFT>, mandatory
            "storePoint": <FileSystemPath|Object Storage URI>
            "usr": <user if authentication is needed> ,
            "passwd": <user password if authentication needed>
            "verifypeer": <True|False if want cert control>
            
            
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'PUT':
        res = {'status': False} 
        
        mandatoryTupl = ('displayname','role','mechanism')
        authTupl      = ('URI',)
        ldapTupl      = ('baseDn','userDn','userPassword','searchAttribute')
        storeTupl     = ('storePoint',)
        samlTupl      = ('sp_proto','sp_fqdn','idp_entity_id','idp_url','idp_x509')
        storeObjTupl  = ('usr','passwd')
        
        allTupl       = mandatoryTupl + authTupl + ldapTupl + storeTupl + storeObjTupl

        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            uuid =  _uuid._uuid().new()
            # Mandatories fields are present
            if not 'verifypeer' in data.keys():
                jsd['verifypeer'] = False
            
            
            if (data['role'] == 'auth') and ('mechanism' in data.keys()): 
                jsd['uuid']        = uuid
                jsd['displayname'] = data['displayname']
                jsd['role']        = data['role']
                jsd['mechanism']    = data['mechanism']
                
                
                # LDAP / Bluemind
                if 'URI' in data.keys():
                    jsd['URI'] = data['URI']
                
                # LDAP only
                if 'baseDn' in data.keys():
                    jsd['baseDn'] = data['baseDn']     
                        
                if 'userDn' in data.keys():
                    jsd['userDn'] = data['userDn']
                       
                if 'userPassword' in data.keys():
                    jsd['userPassword'] = _cypher._cypher().encypherIt(data['userPassword'])
                
                if 'searchAttribute' in data.keys():
                    jsd['searchAttribute'] = data['searchAttribute']    
                    
                    
                # SAML    
                if 'sp_proto'  in data.keys():
                    jsd['sp_proto'] = data['sp_proto']
                    
                if 'sp_fqdn'  in data.keys():
                    jsd['sp_fqdn'] = data['sp_fqdn']
                    
                if 'sp_x509'  in data.keys():
                    jsd['sp_x509'] = data['sp_x509']
                    
                if 'idp_entity_id'  in data.keys():
                    jsd['idp_entity_id'] = data['idp_entity_id']
                
                if 'idp_url'  in data.keys():
                    jsd['idp_url'] = data['idp_url']
                
                if 'idp_x509'  in data.keys():
                    jsd['idp_x509'] = data['idp_x509']
                

            if (data['role'] == 'store') and ('mechanism' in data.keys()):
                jsd['uuid']        = uuid
                jsd['displayname'] = data['displayname']
                jsd['role']        = data['role']
                jsd['mechanism']   = data['mechanism']
                
                if 'storePoint' in data.keys():
                    jsd['storePoint'] = data['storePoint']
                
                if 'usr' in data.keys():
                    jsd['usr'] = data['usr']     
                        
                if 'passwd' in data.keys():
                    jsd['userPassword'] = _cypher._cypher().encypherIt(data['passwd'])
                    
            
            ns = _nosql._nosql()
            resUpsert = ns.upsert(bucket='pm_host',uuid=uuid, upsert= jsd )
    
            res = { 'status': resUpsert }

            i_log = 'adding host ' + data['displayname'] + ' status ' + str(resUpsert)
            apiLog.info(i_log)
            
        return JsonResponse(res) 



@csrf_exempt 
def update(request,uuid): 
    """
    URL : /api/host/{uuid}/update
            
    Method : POST
            
    Update an auth or storage kind host (admin only)
      
    POST URI Parameters:
        uuid (str) : the host uuid
      
    POST BODY JSON:      
        {
            # Mandatory  both case (store or auth)
            "displayname": <human comprehensive name> 
            "role": <store|auth>
            
            #Auth params
            "mechanism": <Internal|LDAP|BlueMind>
            # If not internal
            "URI": <URI>
            # If LDAP auth
            "baseDn": <DN where to start query>
            "userDn": <admin dn for user add autocompletion> ,
            "userPassword": <admin  password>
            "searchAttribute": <mail|uid|login|...>
            "verifypeer": <True|False if want cert control>

            # Storage params
            "mechanism": <Internal|CEPh|SWIFT>
            "storePoint": <FileSystemPath|Object Storage URI>
            "usr": <user if authentication is needed> ,
            "passwd": <user password if authentication needed>
            "verifypeer": <True|False if want cert control>
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        res = {'status': 'False'} 
        
        mandatoryTupl = ('displayname','role')
        authTupl      = ('mechanism','URI')
        ldapTupl      = ('baseDn','userDn','userPassword','searchAttribute')
        storeTuple    = ('mechanism','storePoint','encrypt')
        storeObjTupl  = ('usr','passwd')
        
        allTupl       = mandatoryTupl + authTupl + ldapTupl + storeTuple + storeObjTupl

        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            # Mandatories fields are present
            if not 'verifypeer' in data.keys():
                jsd['verifypeer'] = False
            
            if (data['role'] == 'auth') and ('mechanism' in data.keys()): 
                jsd['uuid']        = uuid
                jsd['displayname'] = data['displayname']
                jsd['role']        = data['role']
                jsd['mechanism']    = data['mechanism']
                
                if data['mechanism'] != 'internal':
                    
                    # LDAP & Bluemind
                    if 'URI' in data.keys():
                        jsd['URI'] = data['URI']
                
                    # LDAP
                    if 'baseDn' in data.keys():
                        jsd['baseDn'] = data['baseDn']     
                        
                    if 'userDn' in data.keys():
                        jsd['userDn'] = data['userDn']
                        
                    if 'userPassword' in data.keys():
                        jsd['userPassword'] = _cypher._cypher().encypherIt(data['userPassword'])
                
                    if 'searchAttribute' in data.keys():
                        jsd['searchAttribute'] = data['searchAttribute']                
                
                    # SAML    
                    if 'sp_proto'  in data.keys():
                        jsd['sp_proto'] = data['sp_proto']
                        
                    if 'sp_fqdn'  in data.keys():
                        jsd['sp_fqdn'] = data['sp_fqdn']
                        
                    if 'sp_x509'  in data.keys():
                        jsd['sp_x509'] = data['sp_x509']
                    
                    if 'idp_entity_id'  in data.keys():
                        jsd['idp_entity_id'] = data['idp_entity_id']
                    
                    if 'idp_url'  in data.keys():
                        jsd['idp_url'] = data['idp_url']
                    
                    if 'idp_x509'  in data.keys():
                        jsd['idp_x509'] = data['idp_x509']


            if (data['role'] == 'store') and ('mechanism' in data.keys()):
                jsd['uuid']        = uuid
                jsd['displayname'] = data['displayname']
                jsd['role']        = data['role']
                jsd['mechanism']   = data['mechanism']
                
                if 'encrypt' not in data.keys():
                    jsd['encrypt'] = False
                else:
                    jsd['encrypt'] = data['encrypt']
                
                
                if 'storePoint' in data.keys():
                    jsd['storePoint'] = data['storePoint']
                
                if 'usr' in data.keys():
                    jsd['usr'] = data['usr']     
                        
                if 'passwd' in data.keys():
                    jsd['userPassword'] = _cypher._cypher().encypherIt(data['passwd'])
            
            ns = _nosql._nosql()
            resUpsert = ns.upsert(bucket='pm_host',uuid=uuid, upsert= jsd )
    
            res = { 'status': resUpsert }
            
            i_log = 'updating host ' + data['displayname'] + ' status ' + str(resUpsert)
            apiLog.info(i_log)

        return JsonResponse(res) 



@csrf_exempt 
def delete(request,uuid): 
    """
    URL : /api/host/{uuid}/delete
            
    Method : DELETE
            
    Delete host if not in use (admin only)
            
    DELETE URI Parameters:      
        uuid (str)   : the host uuid
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'DELETE':
        res = {'status': False}


        isInUse = controlIfHostInUse(uuid)
        if len(isInUse['list']) > 0 :
            i_log = 'UUID Host ' + uuid + ' is still in use'
            apiLog.info(i_log)
            return JsonResponse(isInUse) 
        else:
            ns = _nosql._nosql()
            resRem = ns.remove(bucket='pm_host',uuid=uuid )
    
            res = {'status': resRem} 
            
            i_log = 'deleting host ' + uuid + ' status ' + str(resRem)
            apiLog.info(i_log)

            return JsonResponse(res) 



def controlIfHostInUse(uuid):
    """ 
    URL : None
        
    Try to query data from NoSQL
        
    Parameters:
        uuid (str)  : the host uuid 

    Returns:
        JSON { 'list' : [ {'displayname': 'value'}, ] }  
    """    
    q_uuid = { "$or": [{ "authserver": uuid}, {"storeserver": uuid} ]}
    ns = _nosql._nosql()
    res_q_uuid = [ r for r in ns.search(bucket='pm_domain',query=q_uuid)]
    
    return {'list': res_q_uuid}



def details(request,uuid): 
    """
    URL : /api/host/{uuid}/details
            
    Method : GET
            
    Retrieve all information of host (admin only)
            
    GET URI Parameters:      
        uuid (str)   : the host uuid
            
    Return :    
        JSON {'details': [{'host': {...}}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'details': None}
        q_uuid = { "uuid": uuid}
        ns = _nosql._nosql()
        resSearch = [ {'host': r } for r  in ns.search(bucket='pm_host',query=q_uuid )]
 
        
        if len(resSearch) > 0:
            if 'userPassword' in resSearch[0].keys():
                resSearch[0]['userPassword'] = _cypher._cypher().decypherIt(resSearch[0]['host']['userPassword'])['passwd']
                
            if 'passwd' in resSearch[0].keys():
                resSearch[0]['passwd'] = _cypher._cypher().decypherIt(resSearch[0]['host']['passwd'])['passwd']

        res = {'details':resSearch}
        return JsonResponse(res) 
    
    
    
def _all(request): 
    """
    URL : /api/host/_all
            
    Method : GET
            
    Retrieve all host uuids,displayname & role (admin only)
            
    GET URI Parameters:      
            
    Return :    
        JSON {'all': [{"displayname": "test", "uuid": "09D2B621-9852-4A52-8737-937328F8DB40", "role":"store"},]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    if request.method == 'GET':
        res = {'all': None}

        q_uuid = {}
        ns = _nosql._nosql()
        
        resSearch = [ r for r  in ns.search(bucket='pm_host',query = q_uuid )]

        res = {'all': resSearch} 
        return JsonResponse(res) 



def _allStore(request): 
    """
    URL : /api/host/_allStore
            
    Method : GET
            
    Retrieve all store host uuids & displayname (admin only)
            
    GET URI Parameters:       
            
    Return :    
        JSON {'allStore': [{"displayname": "test", "uuid": "09D2B621-9852-4A52-8737-937328F8DB40"}, ]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    if request.method == 'GET':
        res = {'allStore': None}
        #q_uuid = {"role": "store"}
        q_uuid = [{"$match": {"role":"store"}},{"$sort":{"displayname": 1}},{"$unset":["_id"]}]
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.aggregate(bucket='pm_host',query=q_uuid )]

        
        res = {'allStore': resSearch} 
        return JsonResponse(res)
    
    

def _allAuth(request): 
    """
    URL : /api/host/_allAuth
            
    Method : GET
            
    Retrieve all auth host uuids & displayname (admin only)
            
    GET URI Parameters:      
            
    Return :    
        JSON {'allAuth': [{"displayname": "test", "uuid": "09D2B621-9852-4A52-8737-937328F8DB40"}, ]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAdminAccessRight(token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'allAuth': None}      
        q_uuid = [{"$match": {"role":"auth"}}, {"$sort":{"displayname": 1}},{"$unset":["_id"]}]
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.aggregate(bucket='pm_host',query=q_uuid )]

        res = {'allAuth': resSearch} 
        return JsonResponse(res) 
   
   
