#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control, _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views

yamlFile          = '/etc/promethium/promethium.yml'



@csrf_exempt 
def ingest(request, d_uuid): 
    """
    URL : /api/import/{domainUuid}/ingest
            
    Method : PUT
            
    Add a new auth or storage kind host (admin only)
          
    PUT BODY JSON:      
        {           
            
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight( domainUuid=d_uuid ,token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')     
        
    if request.method == 'PUT':
        res = {'status': True} 
      
      
        mandatoryTupl = ('pm_message','pm_tika','pm_event','pm_attachment','pm_comment','pm_tags', 'payloads', 'basket')

        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        ns = _nosql._nosql()

        if all(jskey in data.keys() for jskey in mandatoryTupl):

            for pm_bucket in ('pm_message','pm_tika','pm_event','pm_attachment','pm_comment','pm_tags'):
                if len(data[pm_bucket]) >0:
                    for dat in data[pm_bucket]:
                        if len(dat)>0:
                            try:
                                jsd = dat
                                uuid = dat['uuid']
                                    
                                resUpsert = ns.upsert(bucket=pm_bucket,uuid=uuid, upsert= jsd )
                                res = { 'status': resUpsert }
                                    
                                i_log = 'INGEST :: ingest ' + dat['uuid'] + 'in ' + pm_bucket + ' status ' + str(resUpsert)
                                apiLog.info(i_log)
                                    
                                if resUpsert:
                                    pass
                                else:
                                    break
                            except:
                                print(pm_bucket)
                                print(dat)
                                pass
                        
            if len(data['payloads']) > 0:
                          
                q_uuid = [
                        {"$lookup": {"from": "pm_host", "localField": "storeserver", "foreignField": "uuid", "as": "storesrv"}}, 
                        {"$unwind" : "$storesrv"},
                        {"$match":{"$and":[{"uuid" : d_uuid}]}} 
                    ]
                
                res_q_uuid = [ r for r in ns.aggregate(bucket='pm_domain',query=q_uuid)]

                if len(res_q_uuid) == 1:
                    storeSrvSpecs =  res_q_uuid[0]['storesrv']
                    storeMethod   = {'Internal':'_localdrive','CEPh':'_ceph','SWIFT':'_swift'}
                    
                    if '_localdrive' in storeMethod[storeSrvSpecs['mechanism']]:
                        from promethiumClass.store import _localdrive
                        store = _localdrive._localdrive()

                    if 'S3' in storeSrvSpecs:
                        from promethiumClass.store import _S3
                        store = _S3._S3()
                
                    params = {"storePath": storeMethod['storePoint'], "basket":data['basket']}
                    store.setParams(**params)
                    for payload in data['payloads']:
                        pid = str(os.getpid())
                        store.put(uuid=payload['uuid'], payload=payload['payload'], _id=pid)
                
                
        return JsonResponse(res) 















    
    
    

   
