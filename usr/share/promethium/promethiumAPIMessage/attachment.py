#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import base64
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse 
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control
from promethiumClass._nosql import _nosql
import promethiumAPIAuth.views as promethiumAPIAuth_views


def details(request,u_uuid,f_uuid):     
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'GET':
        """
        URL : /api/message/{u_uuid}/attachment/{f_uuid}/details
            
        Method : GET
            
        Retrieve the attachment details
            
        Get Parameters:      
            u_uuid (str)  : the account uuid
            f_uuid (str)  : the file/attachment uuid
            
        Return :    
            JSON { "attachment" : {"@timestamp": "<timestamp>", "filename": "<filename>", "filesize": "<filesize>"...},]} or {'attachment': None}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
        res = {'attachment': None}
        q_uuid = { "uuid": f_uuid}
        try:
            ns = _nosql._nosql()
            resSearch = [ r for r  in ns.search(bucket='pm_attachment',query=q_uuid )]
            if len(resSearch) == 1:
                res['attachment'] = resSearch[0]
        except:
            pass
        return JsonResponse(res) 





def download(request,u_uuid,f_uuid):   
    
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'GET':
        """
        URL : /api/message/{uuid}/attachment/{fuuid}/download
            
        Method : GET
            
        Download the attachment 
            
        Get Parameters:      
            u_uuid (str)   : the account uuid
            f_uuid (str)  : the file/attachment uuid
            
        Return :    
            JSON { "payload" : {"@timestamp": "<timestamp>", "filename": "<filename>", "filesize": "<filesize>"...},]} or {'attachment': None}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
        res = {'payload': None}
        q_uuid = { "uuid": f_uuid}
        try:
            ns = _nosql._nosql()
            resSearch = [ r for r  in ns.search(bucket='pm_attachment',query=q_uuid )]

            if len(resSearch) == 1:
                filename     = resSearch[0]['filename'];
                filesize     = resSearch[0]['filesize'];
                content_type = resSearch[0]['mail_content_type'];
                
                domain = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']            
                b64 = retrieveB64(request, domain, f_uuid)
                    
                res =  {'payload': {'filename':'', 'filesize':'', 'content_type':'', 'base64':''}}
                res['payload']['filename']     = filename
                res['payload']['filesize']     = filesize
                res['payload']['content_type'] = content_type
                res['payload']['base64']       = b64.decode('ascii') 
        except:
            pass
        return JsonResponse(res) 
    
    
    
def retrieveB64(request, domain, f_uuid):  

    # Step 1 : retrieve store  
    q_uuid = [
                {"$lookup": {'from': "pm_host",'localField': "storeserver",'foreignField': "uuid",'as': "host"}},
                {"$unwind" :"$host"},
                {"$project": {"_id": 0,"logo": 0, 'host._id':0}}, 
                {"$match":{"$and": [{"uuid" : domain}]}}
            ]

    ns = _nosql._nosql()
    storeDetail = [ r for r  in ns.aggregate(bucket='pm_domain',query=q_uuid )]
   
    if len(storeDetail) == 1 :
        storePoint = storeDetail[0]['host']['storePoint']
        
        if storeDetail[0]['host']['mechanism'] == 'Internal':
            from promethiumClass.store import _localdrive
            store = _localdrive._localdrive()

            if 'encrypt' in storeDetail[0]['host'].keys():
                encrypt = storeDetail[0]['host']['encrypt']
                store.setParam(encrypt=encrypt)

            store.setParam(storePath=storePoint)
            return store.get(uuid=f_uuid)
            
            
            
def __getAttachment(request, f_uuid):
    res = {'payload': None}
    q_uuid = { "uuid": f_uuid}
    try:
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.search(bucket='pm_attachment',query=q_uuid )]
        
        if len(resSearch) == 1:
            filename     = resSearch[0]['filename'];
            filesize     = resSearch[0]['filesize'];
            content_type = resSearch[0]['mail_content_type'];
                
            domain = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']            
            b64 = retrieveB64(request, domain, f_uuid)
                    
            res =  {'payload': {'filename':'', 'filesize':'', 'content_type':'', 'base64':''}}
            res['payload']['filename']     = filename
            res['payload']['filesize']     = filesize
            res['payload']['content_type'] = content_type
            res['payload']['base64']       = base64.b64decode(b64.decode('ascii'))
            
    except:
        pass
    
    return res
    


    
