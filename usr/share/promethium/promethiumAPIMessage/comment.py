#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import io
import json
import jwt
import os.path 
from pymemcache.client import base
import requests
import sys
import time
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views
import promethiumAPIUser.views as promethiumAPIUser_views




 
def get(request,u_uuid,m_uuid):     
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'GET':
        """
        URL : /api/message/{u_uuid}/comment/{m_uuid}
            
        Method : GET
            
        Retrieve comments if exist
            
        Get Parameters:      
            u_uuid (str)   : the account uuid
            m_uuid (str)  : the message uuid
            
        Return :    
            JSON { "comments" : {"@timestamp": "<timestamp>", "subject": "<subject>"},]} or {'comment': None}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
        tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
        res = {'comments': []}
        q_uuid = { "$and": [{"m_uuid": m_uuid}, {'$or':[{ "u_uuid": u_uuid}, {"accessright" : "public"}]}]}
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.search(bucket='pm_comment',query=q_uuid )]

        if len(resSearch) > 0: 
            for comment in resSearch:
                cmnt = {'body':'', 'uuid':'', 'u_uuid':'', 'ownerPrettyName': '', 'date':'', 'accessright':'' }
                
                cmnt['body']        = comment['body']
                cmnt['uuid']        = comment['uuid']
                cmnt['u_uuid']      = comment['u_uuid']
                
                if u_uuid == comment['u_uuid']:
                    cmnt['ownerPrettyName'] = 'thisIsYou'
                else:
                    data = promethiumAPIUser_views.displayname(request,tokenElmt['domain'],comment['u_uuid'])
                    body_unicode = data.content.decode('utf-8')        
                    owner        = json.loads(body_unicode)
                    #print(owner)
                    if 'firstname' in owner['details'][0]['user'].keys():
                        cmnt['ownerPrettyName']      = owner['details'][0]['user']['firstname'] + ' ' + owner['details'][0]['user']['lastname']              
                    else:
                        cmnt['ownerPrettyName']      = owner['details'][0]['user']['lastname']
                        
                cmnt['date']        = comment['@timestamp']
                cmnt['accessright'] = comment['accessright']
                
                res['comments'].append(cmnt)
        
        return JsonResponse(res) 



@csrf_exempt 
def add(request, u_uuid, m_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')       
        
    if request.method == 'POST':
        """
        URL : /api/message/{u_uuid}/comment/{m_uuid}/add
                
        Method : POST
                
        Add a new comment on message
    
        POST BODY JSON:      
            {
                # Mandatory
                "body" : <body text>
                
                # Option
                "accessright" : <public|private> | default private
            }
            
        Return :    
            HTTP 200 if OK
            HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """    
        
        res = {'status': False} 
            
        mandatoryTupl = ('body',)
            
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
            
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
                          
            jsd['@timestamp']  = int(time.time()) *1000
            jsd['uuid']        =  _uuid._uuid().new()
            jsd['m_uuid']      = m_uuid
            jsd['u_uuid']      = u_uuid
            jsd['body']        = data['body']
            jsd['accessright'] = 'private'
                
            if 'accessright' in data.keys():
                jsd['accessright'] = data['accessright']
                
                
            ns = _nosql._nosql()    
            resUpsert = ns.upsert(bucket='pm_comment',uuid=jsd['uuid'], upsert=jsd )
         
            res = { 'status': resUpsert }

        return JsonResponse(res, safe=False)  
    
 

def getCommentId(u_uuid, m_uuid):
    """
    URL : None
            
    Method : internal
            
    Retrieve document reference for this user & message in comment index 
            
    Parameters:
        u_uuid (str)  : the user uuid
        m_uuid (str) : the message uuid 
            
    Return :    
        <'_id'> if OK else None
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """ 
    res = None
    q_uuid =[ {'$match': {'$and': [{'u_uuid': u_uuid}, {'m_uuid': m_uuid}]}}, {'$project': {'_id': 0, 'uuid': 1}} ]
    try:
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.aggregate(bucket='pm_comment',query=q_uuid )]
        if len(resSearch) == 1:
            res = resSearch[0]['uuid']
    except:
        pass
    print(res)
    return res   
    


@csrf_exempt 
def update(request, u_uuid, m_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        """
        URL : /api/message/{u_uuid}/comment/{m_uuid}/update
                
        Method : POST
                
        Update a comment on message
    
        POST BODY JSON:      
            {
                # Mandatory
                "body" : <body text>
                
                # Option
                "accessright" : <public|private> | default private
            }
            
        Return :    
            HTTP 200 if OK
            HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """    
        if request.method == 'POST':
            res = {'status': False} 
            
            mandatoryTupl = ('body')
            
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)
            q_uuid = { "$and": [{ "u_uuid": u_uuid}, {"m_uuid": m_uuid} ]}
            ns = _nosql._nosql()
            resSearch = [ r for r  in ns.search(bucket='pm_comment',query=q_uuid )]
    
            if all(jskey in data.keys() for jskey in mandatoryTupl):
                if len(resSearch) == 1:
                    jsd = resSearch[0]

                    jsd['body']        = data['body']
                    jsd['accessright'] = 'private'
                    
                    if 'accessright' in data.keys():
                        Jsd['accessright'] = data['accessright']
                    
                    ns = _nosql._nosql()    
                    resUpsert = ns.upsert(bucket='pm_comment',uuid=jsd['uuid'], upsert=jsd )
             
                    res = { 'status': resUpsert }
                    
            return JsonResponse(res) 



@csrf_exempt
def delete(request, u_uuid, m_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'DELETE':
        """
        URL : /api/message/{u_uuid}/comment/{m_uuid}/delete
                
        Method : DELETE
                
        Delete an comment on message
            
        Return :    
            Couchbase json response
            HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """  
        res = {'status': False} 
        try:
            ns = _nosql._nosql()
            resRem = ns.remove(bucket='pm_comment',uuid=getCommentId(u_uuid,m_uuid))
            res = {'status': resRem} 
        except:
            pass

        return JsonResponse(res, safe=False) 
    
