#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql

from promethiumClass.auth import _control
import promethiumAPIDomain.views as promethiumAPIDomain_views
import promethiumAPIAuth.views as promethiumAPIAuth_views

@csrf_exempt 
def delete(request,u_uuid,m_uuid): 
    """
    URL : /api/message/{u_uuid}/message/{m_uuid}/delete
            
    Method : DELETE
            
    Delete message and all its relevant datas
    "domain" or "dispatcher" right mandatory        
            
    DELETE URI Parameters:    
        u_uuid (str)  : the account uuid
        m_uuid (str)  : the message uuid
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    
    tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
    
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDispatcherRight(domainUuid=tokenElmt['domain'], token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'DELETE':
        i_log = u_uuid+ ' wants to delete doc ref ' + m_uuid + ' in indexes'
        apiLog.info(i_log)
        
        status = {'pm_event':'', 'pm_comment':'', 'pm_alert':'', 'pm_attachment':'','pm_message':'','pm_tika':'','pm_favorites':'',}

        for bucket in ('pm_event', 'pm_comment', 'pm_alert', 'pm_tika','pm_favorites'):
            res = deleteInbucket(bucket, u_uuid, m_uuid) 
            status[bucket] = res
        
        status['pm_attachment'] = deleteAttachment(u_uuid, m_uuid, tokenElmt['domain'])
        status['pm_message']    = deleteMessage(u_uuid, m_uuid)

    return JsonResponse(status) 
    

   
def deleteAttachment(u_uuid, m_uuid, d_uuid): 
    """
    URL : None
            
    Method : internal
            
    Retrieve user subscribed group uid   
            
    Parameters:      
        u_uuid (str)       : the user uuid
        m_uuid (str)       : the message uuid
        d_uuid (str)       : the domain uuid
        
    Return :    
        JSON {'details': [{'subscribedguid': ''}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    q_uuid = {'m_uuid':m_uuid}
    ns = _nosql._nosql()
    resSearch = [ {'message':r} for r  in ns.search(bucket='pm_message',query=q_uuid )]
                
    storeParams = retrieveStoragePoint(d_uuid)

    #storeType = getattr(sys.modules[__name__], self.store) # _localdrive|_swift|_ceph
    if 'Internal' in storeParams['store']:
        from promethiumClass.store import _localdrive
        store = _localdrive._localdrive()

    if 'S3' in storeParams['store']:
        from promethiumClass.store import S3
        store = _S3._S3()
         
    store.setParam(**storeParams)
                
    error = []
    res   = {'res': True}
    if len(resSearch) > 0:
        if len(resSearch[0]['message']['attachment']) > 0:
            attachmentList = resSearch[0]['message']['attachment'].split(',')
            for _id in attachmentList:
                
                #Delete file on System
                dfs = store.delete(uuid=_id)       
                if dfs:
                    dfs_log = 'delete file ' + _id + ' in ' + storeParams['storePath'] + ' by ' + u_uuid + ' succeed'
                    apiLog.info(dfs_log)
                else:
                    dfs_log =  'delete file ' + _id + ' in ' + storeParams['storePath'] + ' by ' + u_uuid + ' failed'
                    apiLog.info(dfs_log)
                    error.append(_id)
                
                #Delete file in Couchbase                
                dfe = ns.remove(bucket='pm_attachment',uuid=_id)
                if dfe:
                    dfe_log = 'delete entry ref ' + _id + ' in attachment by ' + u_uuid + ' succeed'
                    apiLog.info(dfe_log)
                else:
                    dfe_log =  'delete entry ref ' + _id + 'in attachment by ' + u_uuid + ' failed'
                    apiLog.info(dfe_log)
                    error.append(_id)
                    
            if len(error) > 0:
                res['res'] = error
    
    return res



def deleteInbucket(bucket, u_uuid, m_uuid): 
    """
    URL : None
            
    Method : internal
            
    Delete all docs in index relevant to message with ref muuid   
            
    Parameters:      
        bucket (str)  : the index name where to delete entries relevant to message (muuid)
        u_uuid (str)  : the user uuid
        m_uuid (str)  : the message uuid
        
    Return :    
        dict {'res': True } if OK or {'res': ['_ref',...] } if KO for some entr(y|ies)            
    """
    
    q_uuid = {'m_uuid':m_uuid}    
    ns = _nosql._nosql()
    resSearch = [ r for r  in ns.search(bucket=bucket,query=q_uuid )]

    error = []
    res   = {'res': True}
    if len(resSearch) > 0:
        for entry in resSearch:
            res = ns.remove(bucket=bucket, uuid=entry['uuid'])
            if res:
               i_log = 'delete bucket ref ' + entry['uuid'] + ' in ' + bucket + ' by ' + u_uuid + ' succeed'
               apiLog.info(i_log)
            else:
                i_log =  'delete bucket ref ' + entry['uuid'] + ' in ' + bucket + ' by ' + u_uuid + ' failed'
                apiLog.info(i_log)
                error.append(entry['uuid'])
                
        if len(error) > 0:
            res['res'] = error
    
    return res

    

def deleteMessage(u_uuid, m_uuid):
    """
    URL : None
            
    Method : internal
            
    Delete ref m_uuid in pm_message bucket    
            
    Parameters:      
        u_uuid (str)       : the user uuid
        m_muuid (str)      : the message uuid
    Return :    
        dict {'res': True } if OK or {'res': False, 'uuid':m_uuid } if KO 
            
    """   
    ns   = _nosql._nosql()
    resd = ns.remove(bucket='pm_message',uuid=m_uuid)

    if resd:
        i_log = 'delete entry ref ' + m_uuid + ' in pm_message by ' + u_uuid + ' succeed'
        apiLog.info(i_log)
        res   = {'res': True}
    else:
        i_log =  'delete entry ref ' + m_uuid + ' in pm_message by ' + u_uuid + ' failed'
        apiLog.info(i_log)
        res   = {'res': False, 'uuid':m_uuid}

    return res



def retrieveStoragePoint(uuid): 
    """
    URL : None
            
    Method : internal
            
    Retrieve store element from domain uuid   
            
    Parameters:      
        uuid (str) : the domain uuid
    Return :    
        dict {'res': True, 'store': '', 'storePath';'', 'storeUser':'', 'storePassword':'' } if OK or {'res': False} if KO 
            
    """
    params = {} 
    
    #qmb = 'select storesrv from `pm_domain` dom join `pm_host` storesrv on keys dom.storeserver where dom.uuid="' + uuid + '";'
    qmb = [ {"$lookup": {"from": "pm_host","localField": "storeserver","foreignField": "uuid","as": "storSrv"}}, 
            {"$project": {"_id":0,"dom._id": 0, "logo": 0}},
            {"$match":{"$and":[{"uuid" : uuid}]}} ]

    try:
        ns = _nosql._nosql()
        res_qmb = [ r for r in ns.aggregate(bucket='pm_domain',query=qmb)]
        if len(res_qmb) == 1:
            params['res']       = True
            params['store']     = res_qmb[0]['storSrv'][0]['mechanism']
            params['storePath'] = res_qmb[0]['storSrv'][0]['storePoint']
                
            params['storeUser'] = ''
            if 'usr' in res_qmb[0]['storSrv'][0].keys():
                params['storeUser'] = res_qmb[0]['storSrv'][0]['usr']
                    
            params['storePassword'] = ''    
            if 'passwd' in res_qmb[0]['storSrv'][0].keys():
                params['storePassword'] = res_qmb[0]['storSrv'][0]['passwd']
    except:
        pass
    return params

    
