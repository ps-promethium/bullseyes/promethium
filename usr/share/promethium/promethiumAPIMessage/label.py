#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import io
import json
import jwt
import os.path 
from pymemcache.client import base
import requests
import sys
import time
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views




 
def get(request,u_uuid,m_uuid):     
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'GET':
        """
        URL : /api/message/{u_uuid}/label/{m_uuid}
            
        Method : GET
            
        Retrieve labels if exist
            
        Get Parameters:      
            u_uuid (str)   : the account uuid
            m_uuid (str)  : the message uuid
            
        Return :    
            JSON { "label" : [{"uuid": "<uuid>", "u_uuid": "<u_uuid>", "l_uuid": "<l_uuid>"},]} or {'label': None}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
     

        res = {'label': None}
        
        q_uuid = 'select * from `pm_label` `label` where  '
        q_uuid += ' u_uuid="' + u_uuid + '" '
        q_uuid += ' AND '
        q_uuid += ' m_uuid="' + m_uuid + '" '
        q_uuid += ';'
    
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.search(bucket='pm_label',query=q_uuid )]

        if len(resSearch) == 1:
            res['label'] = resSearch[0]['label']['l_uuid'].split(" ")
        
        return JsonResponse(res) 



def getLabelId(uuid, muuid):
    """
    URL : None
            
    Method : internal
            
    Retrieve label reference for this user & message in label index 
            
    Parameters:
        uuid (str)  : the user uuid
        muuid (str) : the message uuid 
            
    Return :    
       <_id'> if OK else None
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """ 
    
    res = None

        
    if hit['hits']['total']['value'] == 1:
        res = hit['hits']['_id']
        
    return res  


@csrf_exempt 
def add(request, uuid, muuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        """
        URL : /api/message/{uuid}/label/{muuid}/add
                
        Method : POST
                
        Add labels on message.
        Labels must space separated
    
        POST BODY JSON:      
            {
                # Mandatory
                "label" : "<l_uuid l_uuid1 l_uuid2 ...>" 
            }
            
        Return :    
            HTTP 200 if OK
            HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """    
        
        if request.method == 'POST':
            res = {'status': False} 
            
            mandatoryTupl = ('label')
            
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)

            if all(jskey in data.keys() for jskey in mandatoryTupl):
                jsd = {}
                          
                jsd['uuid']   = muuid
                jsd['u_uuid'] = uuid
                jsd['l_uuid'] = data['label']
                
                rqe = mailboxReqElastic.mailboxReqElastic()
                res = rqe.post(index='label', data=jsquery)

            return JsonResponse(res) 
    
    

@csrf_exempt 
def update(request, uuid, muuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        """
        URL : /api/user/{uuid}/comment/{muuid}/update
                
        Method : POST
                
        Update labels on message
    
        POST BODY JSON:      
            {
                # Mandatory
                "label" : "<l_uuid l_uuid1 l_uuid2 ...>" 
            }
            
        Return :    
            HTTP 200 if OK
            HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """    
        
        if request.method == 'POST':
            res = {'status': False} 
            
            mandatoryTupl = ('label')
            
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)

            if all(jskey in data.keys() for jskey in mandatoryTupl):
                jsd = {}

                jsd['l_uuid']        = data['label']
                
                jsquery = {'doc': jsd }
                rqe = mailboxReqElastic.mailboxReqElastic()
                res = rqe.update(index='label', _type='_doc', ref=getLabelId(uuid,muuid), data=jsquery)

            return JsonResponse(res) 



@csrf_exempt
def delete(request, uuid, muuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'DELETE':
        """
        URL : /api/user/{uuid}/comment/{muuid}/delete
                
        Method : DELETE
                
        Delete an comment on message
            
        Return :    
            Elasticsearch json response
            HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """  
        
        rqe = mailboxReqElastic.mailboxReqElastic()
        res = rqe.delete(index='comment', _type='_doc', ref=getAlertId(uuid,muuid))

        return JsonResponse(res) 
    
