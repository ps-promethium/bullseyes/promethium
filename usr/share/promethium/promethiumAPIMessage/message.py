#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import base64
from collections import defaultdict
from datetime import datetime, timezone
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import time
from tzlocal import get_localzone
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control, _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.smtp import _sendMail
from promethiumClass.various import _uuid, _conf

import promethiumAPIAuth.views as promethiumAPIAuth_views
import promethiumAPIUser.views as promethiumAPIUser_views
from . import status
from . import attachment



def get(request,u_uuid,m_uuid):     
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'GET':
        """
        URL : /api/message/{u_uuid}/message/{m_uuid}
            
        Method : GET
            
        Retrieve the message content
            
        Get Parameters:      
            u_uuid (str)   : the account uuid
            m_uuid (str)  : the message uuid
            
        Return :    
            JSON { "msg" : {"@timestamp": "<timestamp>", "subject": "<subject>"},]} or {'msg': None}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
        res = {'msg': None}
       
        #q_uuid = 'select * from `pm_message` `msg` where  uuid="' + m_uuid + '" ;'
        q_uuid = {'uuid':m_uuid}
        try:
            ns = _nosql._nosql()
            resSearch = [ {'msg':r} for r  in ns.search(bucket='pm_message',query=q_uuid )]
            #
            if len(resSearch) == 1:
                status.setRead(request,u_uuid,m_uuid)
                tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
                
                #if (tokenElmt['role'] in ("domain", "dispatcher")) or (jskey in tokenElmt['subscribedguid'] for jskey in resSearch[0]['msg']['dispatchedguid']):
                if (tokenElmt['role'] in ("domain","dispatcher")) or ((jskey in tokenElmt['subscribedguid'] for jskey in resSearch[0]['msg']['dispatchedguid']) and (len(resSearch[0]['msg']['dispatchedguid']))):
                    res['msg'] = resSearch[0]['msg']
        except:
            pass
        return JsonResponse(res) 



def getHeader(m_uuid, token):
    """
    URL : None
            
    Method : Internal
            
    Retrieve the message content
            
    Get Parameters:      
        m_uuid (str)  : the message uuid
        token (str)   : the user session token    
    Return :    
        dict { "msg" : {"@timestamp": "<timestamp>", "subject": "<subject>"},]} or {'msg': None}
            
    """
    res = {'msg': None}
    try:
        tokenElmt = _control._control().retrieveUserElementFromToken(token)
        msg = {'uuid': m_uuid, 'from': '' , 'date':'', 'subject':'', 'attachment': '', 'x_priority':'', 'status':'unread', 'chrono':'', 'outBound': False, 'dispatched':False,}
        q_uuid = {"uuid":m_uuid}    
        ns = _nosql._nosql()
        resSearch = [ {'msg':r} for r  in ns.search(bucket='pm_message',query=q_uuid )]
        if len(resSearch) == 1:     
            hit = resSearch[0]['msg']
            if (tokenElmt['role'] in ("domain","dispatcher")) or ((jskey in tokenElmt['subscribedguid'] for jskey in hit['dispatchedguid']) and (len(hit['dispatchedguid']))):
                msg['from']       = hit['from']
                msg['fromPretty'] = hit['from'].split('<')[0]
                msg['date']       = hit['date']
                msg['chrono']     = hit['chrono']
                msg['subject']    = hit['subject']
                msg['attachment'] = True if len(hit['attachment'])>0 else False 
                msg['x_priority'] = int(hit['x_priority'])
                msg['status']     = status.getStatus(tokenElmt['uuid'],m_uuid)['status']
                msg['outBound']   = hit['outBound']
                msg['dispatched'] = True if len(hit['dispatchedguid'])>0 else False
                res['msg'] = msg
    except:
        pass
    return res     
    
    

@csrf_exempt 
def getByDate(request,u_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'POST':
        """
        URL : /api/message/{u_uuid}/message/_bydate
            
        Method : POST
            
        Retrieve all messages on date
            
        URI Parameters:      
            u_uuid (str)   : the account uuid
        
        
        POST BODY JSON:      
            {
                # Option
                "date" : <micro timestamp> | default today
                "nbOfDays": the of of days to return from now
                "orderByDate": -1|1  sort list descendant|ascendant
            }
        Return :    
            JSON { "total": , "msg" : [
                                        {"uuid":"<msg uuid>", 
                                        "date": "<date>", 
                                        "from":"<msg sender address>", 
                                        "subject": "<subject>",
                                        "attachment": <True|False>, 
                                        "x_priority":<1|2|3|4|5>,
                                        "status": "<read|unread>",
                                        "outBound": <true|false>,
                                        "dispatched",
                                        "defaultColor",
                                        "chrono",
                                        }, 
                                    ]
                } 
                    or 
                {'total': 0 , 'msg': []}
                
            HTTP Errno 403 and some contextual errortext (authentication failed...)

            
        """
        res = {'total': 0 , 'msg': []}
        data = {} 
        conf = _conf._conf().get()
        if len(request.body) > 0 :
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)
            
        tokenElmt = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
        nbOfDays  = int(conf['user']['nbOfDays'])
        if 'nbOfDays' in data.keys() :
            if data['nbOfDays'] is not None:
                nbOfDays = int(data['nbOfDays'])
        
        orderByDate = -1
        if 'orderByDate' in data.keys() and len(data['orderByDate']):
            orderByDate = int(data['orderByDate'])
        
        q_uuid = [{"$match":{"$and":[{'domain': tokenElmt['domain']}]}},
                  {"$sort":{"date":orderByDate, "chrono": orderByDate}},
                  {"$unset":["_id"]}
                ]
        
        if not tokenElmt['role'] in ("domain", "dispatcher"):
            q_uuid[0]['$match']['$and'].append({'dispatchedguid': { "$in": tokenElmt['subscribedguid']}})
        
        if len(data)>0 and 'date' in data.keys():
            q_uuid[0]['$match']['$and'].append({'date': {'$gte': int(data['date'])}})
            q_uuid[0]['$match']['$and'].append({'date':{"$lt":int(data['date']) + 86400000}})
        else:   
            data['date'] = int(time.time()*1000)
            q_uuid[0]['$match']['$and'].append({'date': {"$gte": data['date']-(86400000*(nbOfDays-1))}})
            q_uuid[0]['$match']['$and'].append({'date':{"$lt": data['date'] + 86400000}})
        
        ns = _nosql._nosql()
        resSearch = [ {'msg':r} for r  in ns.aggregate(bucket='pm_message',query=q_uuid )]       
        if len(resSearch) > 0:
            res['total'] = len(resSearch)
            for hit in resSearch:     
                if (tokenElmt['role'] in ("domain","dispatcher")) or ((jskey in tokenElmt['subscribedguid'] for jskey in hit['msg']['dispatchedguid']) and (len(hit['msg']['dispatchedguid']))):
                    m_uuid = hit['msg']['uuid']
                    msg = {'uuid': m_uuid, 'from': '' , 'date':'', 'subject':'', 'attachment': '', 'x_priority':'', 'status':'unread', 'chrono':'','dispatched': False}
                    
                    msg['from']         = hit['msg']['from']
                    msg['fromPretty']   = hit['msg']['from'].split('<')[0]
                    
                    msg['date']         = hit['msg']['date']
                    msg['subject']      = hit['msg']['subject']
                    msg['attachment']   = True if len(hit['msg']['attachment'])>0 else False 
                    msg['x_priority']   = int(hit['msg']['x_priority'])
                    msg['status']       = status.getStatus(u_uuid,m_uuid)['status']
                    msg['outBound']     = hit['msg']['outBound']
                    msg['defaultColor'] = hit['msg']['defaultColor']
                    msg['chrono']       = hit['msg']['chrono']
                    msg['dispatched']   = True if len(hit['msg']['dispatchedguid'])>0 else False

                    res['msg'].append(msg)     
        return JsonResponse(res) 

    

@csrf_exempt 
def getByLabel(request,u_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'POST':
        """
        URL : /api/message/{u_uuid}/message/_bylabel
            
        Method : POST
            
        Retrieve all messages from label
            
        URI Parameters:      
            u_uuid (str)   : the account uuid
        
        
        POST BODY JSON:      
            {
                # mandatory
                "label" : <label uuid>
            }
        Return :    
            JSON { "total": , "msg" : [
                                        {"uuid":"<msg uuid>", 
                                        "date": "<date>", 
                                        "from":"<msg sender address>", 
                                        "subject": "<subject>",
                                        "attachment": <True|False>, 
                                        "x_priority":<1|2|3|4|5>,
                                        "status": "<read|unread>"}, 
                                    ]
                } 
                    or 
                {'total': 0 , 'msg': []}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
    
        if request.method == 'POST':
            res = {'total': 0 , 'msg': []}
            mandatoryTupl = ('label')           
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)         
           
            q_uuid = [{"$match":{"$and":[{'u_uuid': u_uuid}, {'l_uuid': data['label']}]}},
                      {"$project": {"_id":0,"m_uuid": 1}},
                     ]  
            ns = _nosql._nosql()
            resSearch = [ {'msg':r} for r  in ns.aggregate(bucket='pm_label',query=q_uuid )]

            if len(resSearch) > 0:
                res['total'] = len(resSearch)
                for hit in resSearch:     
                    m_uuid = hit['msg']['m_uuid']
                    msg = getHeader(u_uuid, m_uuid)
                    res['msg'].append(msg)
        
        return JsonResponse(res) 



def getUnread(request,u_uuid):     
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'GET':
        """
        URL : /api/message/{u_uuid}/message/_unread
            
        Method : GET
            
        Retrieve all unread messages in time DESC order for user {uuid}
            
        Get Parameters:      
            u_uuid (str)   : the account uuid
            
        Return :    
            JSON { "total": , "msg" : [
                                        {"uuid":"<msg uuid>", 
                                        "date": "<date>", 
                                        "from":"<msg sender address>", 
                                        "subject": "<subject>",
                                        "attachment": <True|False>, 
                                        "x_priority":<1|2|3|4|5>,
                                        "status": "<read|unread>"}, 
                                    ]
                } 
                    or 
                {'total': 0 , 'msg': []}
                
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
        res = {'total': 0 , 'msg': []}
        q_uuid = [{"$match":{"$and":[{'u_uuid': u_uuid}, {'status': "unread"}]}},
                  {"$project": {"_id":0,"m_uuid": 1}},
                 ]  
        
        ns = _nosql._nosql()
        resSearch = [ {'msg':r} for r  in ns.aggregate(bucket='pm_event',query=q_uuid )]

        if len(resSearch) > 0:
            res['total'] = len(resSearch)
            for hit in resSearch:     
                m_uuid = hit['msg']['m_uuid']
                msg = getHeader(u_uuid, m_uuid)
                res['msg'].append(msg)
        
        
        return JsonResponse(res) 



def event(request,u_uuid,m_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
    res = {'events': []}    
    if request.method == 'GET':
        """
        URL : /api/message/{u_uuid}/message/{m_uuid}/event
            
        Method : GET
            
        Retrieve who viewed the message
            
        Get Parameters:      
            u_uuid (str)   : the account uuid
            m_uuid (str)  : the message uuid
            
        Return :    
            JSON { "events" : {"@timestamp": "<timestamp>", "uuid": "<user uuid>"},]} or {'event': []}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """      
        q_uuid = {'m_uuid':m_uuid}
        ns = _nosql._nosql()
        resSearch = [ {'msg':r} for r  in ns.search(bucket='pm_event',query=q_uuid )]
        
        uniq = []
        if len(resSearch) > 0:
            for hts in resSearch:
                if hts['msg']['u_uuid'] not in uniq:
                    evt = {'@timestamp':'', 'uuid':'' }
                    evt['@timestamp'] = hts['msg']['@timestamp']
                    evt['uuid'] = hts['msg']['u_uuid']
                
                    res['events'].append(evt)
                    uniq.append(hts['msg']['u_uuid'])

    return JsonResponse(res)

  

""" Forwarding message """


@csrf_exempt
def forwardMessage(request,u_uuid,m_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    if request.method == 'POST':
        """
        URL : /api/message/{u_uuid}/message/{m_uuid}/forwardMessage
            
        Method : POST
            
        Forward message to a list of destinators/email
            
        URI Parameters:      
            u_uuid (str)   : the account uuid
            m_uuid (str)  : the message uuid
            
            
        POST BODY JSON:      
        {
            # Mandatory
            "destinators": { 'to': [], 'cc':[] },
        }    
            
        Return :    
            JSON {'status': None|True}
            HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
        res = {'status': None}
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        mandatoryTupl = ['destinators']

        if all(jskey in data.keys() for jskey in mandatoryTupl):               
            try:
                message = __getMessage(u_uuid,m_uuid)
                smtp    = __getSmtpParams(_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain'])['smtp']
                user    = {"user":  _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['uuid']}
                    
                _from = {"domainName": __getDomainName(_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain'])['domName'], "account":smtp['account']}
                    
                i_log = 'Forwarding message with internal ref. ' + m_uuid  + ' asked by user ' + u_uuid
                apiLog.info(i_log)    
                    
                sm =  _sendMail._sendMail()
                sm.setXUserHeader(**user)
                sm.setMailFrom(**_from)
                sm.setMailDest(**data['destinators'])
                sm.setMailSubject(**message)
                sm.setMailBody(**__setMailBody(**message))
                    
                if len(message['attachment']) >0:
                    attachments = message['attachment'].split(",")
                    for f_uuid in attachments:
                        part = attachment.__getAttachment(request, f_uuid)['payload']
                        sm.addAttachment(**part)              
                    
                sm.setSmtpParams(**smtp)
                sm.smtpConnect()
                sm.smtpSend()    
            except:
                pass
        return JsonResponse(res, safe=False)  



def __getDomainName(uuid):
    res = {'domName': None}
    q_uuid = {"uuid": uuid}
    ns = _nosql._nosql()
    try:
        resSearch = [ {'pm_domain':r } for r  in ns.search(bucket='pm_domain',query=q_uuid )]
        res = {'domName': resSearch[0]['pm_domain']['displayname']} 
    except:
        pass
    return res 
        


def __getSmtpParams(uuid):
    res = {'smtp': None}
    q_uuid = {"uuid": uuid}
    ns = _nosql._nosql()
    try:
        resSearch = [ {'pm_domain':r } for r  in ns.search(bucket='pm_domain',query=q_uuid )]
        smtp = resSearch[0]['pm_domain']['smtp']
        SMTP = {"server": smtp['server'],
                "account": smtp['account'] ,
                "passwd": _cypher._cypher().decypherIt(smtp['passwd'])['passwd'],
                "protocol": smtp['protocol'] ,
                "port": smtp['port'] ,
                "verifypeer": smtp['verifypeer'] }
            
        res = {'smtp': SMTP} 
    except:
        pass
    return res



def __getMessage(u_uuid,m_uuid):
    res = None
    q_uuid = {'uuid':m_uuid}
    ns = _nosql._nosql()
    resSearch = [ {'msg':r} for r  in ns.search(bucket='pm_message',query=q_uuid )]

    if len(resSearch) == 1:
        res = resSearch[0]['msg']

    return res



def __setMailBody(**kwargs):   
    html  ="<BR><BR>-------- Message --------"
    html +="<TABLE CELLPADDING='0' CELLSPACING='0' BORDER='0' class='moz-email-headers-table'>"
    html +="<TR>"
    html +="<TH VALIGN='BASELINE' ALIGN='RIGHT' NOWRAP>Sujet: </TH>"
    html +="<TD> " + kwargs['subject'] + "</TD>"
    html +="</TR>"
    html +="<TR>"
    html +="<TH VALIGN='BASELIN'E ALIGN='RIGHT' NOWRAP>Date: </TH>"
    html +="<TD> " + __getDateFromMicroTimestamp(kwargs['date']) + "</TD>"
    html +="</TR>"
    html +="<TR>"
    html +="<TH VALIGN='BASELINE' ALIGN='RIGHT' NOWRAP>De: </TH>"
    html +="<TD> " + kwargs['from'].replace('<', '&lt;').replace('>', '&gt;') + "</TD>"
    html +="</TR>"
    html +="<TR>"
    html +="<TH VALIGN='BASELINE' ALIGN='RIGHT' NOWRAP>Chrono: </TH>"
    html +="<TD> " + str(kwargs['chrono']) + "</TD>"
    html +="</TR>"
    html +="<TR>"
    html +=" </TABLE>"
            
    html += "<BR><BR>"
    
    html += kwargs['body']
    

    return {'body':html}



def __getDateFromMicroTimestamp(mctmstp):
    tz = get_localzone() # local timezone 
    d = datetime.fromtimestamp(mctmstp/1000, tz) # or some other local date 
    utc_offset = d.utcoffset().total_seconds()
    date = datetime.fromtimestamp(mctmstp/1000 + utc_offset)
    return str(date)
