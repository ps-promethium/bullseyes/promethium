#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
from collections import defaultdict
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import time
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
from promethiumClass.various import _uuid
import promethiumAPIAuth.views as promethiumAPIAuth_views




def setRead(request,u_uuid,m_uuid):     
    setResp = { 'res': False }

    evt = defaultdict(dict)
    uuid = getEventUuid(u_uuid,m_uuid)
    if uuid is not None:
        evt['uuid'] = uuid
    else:
        evt['uuid'] = _uuid._uuid().new()
   
    evt['@timestamp'] = int(time.time())*1000
    evt['m_uuid']     = m_uuid 
    evt['u_uuid']     = u_uuid
    evt['status']     = 'read'
    evt['datechange'] = int(time.time())*1000

    try:
        ns = _nosql._nosql()    
        resUpsert = ns.upsert(bucket='pm_event',uuid=evt['uuid'], upsert=evt )
        setResp['res'] = resUpsert
    except:
        pass
    return JsonResponse(setResp) 



def getEventUuid(u_uuid,m_uuid):
    """
    URL : None
            
    Method : internal
            
    Retrieve document reference for this user & message in event index 
            
    Parameters:
        uuid (str)  : the user uuid
        muuid (str) : the message uuid 
            
    Return :    
        <uuid> if OK else None
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """ 
    res = None
    q_uuid = [{"$match":{"$and":[{'u_uuid': u_uuid}, {'m_uuid': m_uuid}]}},
              {"$project": {"_id":0,"uuid": 1}},
             ]
    try:
        ns = _nosql._nosql()
        resSearch = [ {'evt':r} for r  in ns.aggregate(bucket='pm_event',query=q_uuid )]
        if  len(resSearch) == 1:
            if 'evt' in resSearch[0].keys():
                res = resSearch[0]['evt']['uuid']
    except:
        pass
    return res



def getStatus(u_uuid,m_uuid):
    """
    URL : None
            
    Method : internal
            
    Retrieve read status for this user & message in event index 
            
    Parameters:
        u_uuid (str)  : the user uuid
        m_uuid (str) : the message uuid 
            
    Return :    
        JSON {'status': {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """ 
    res = {'status': 'unread'}
    q_uuid = [{"$match":{"$and":[{'u_uuid': u_uuid}, {'m_uuid': m_uuid}]}},
              {"$project": {"_id":0,"status": 1}},
              {"$sort": {'datechange': -1}},
              {"$limit" : 1 }
             ]
    try:
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.aggregate(bucket='pm_event',query=q_uuid )]
        if  len(resSearch)>= 1:
            if 'status' in resSearch[0].keys():
                res['status'] = 'read'
    except:
        pass
    return res 
