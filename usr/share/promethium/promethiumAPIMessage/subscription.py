#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
from collections import defaultdict
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import time
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views


def getGroupMembers(request, guid):    
    data = []
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.META['HTTP_X_PM_APIKEY']}
    
    tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
    
    URI = 'http://127.0.0.1:8000/api/group/' + tokenElmt['domain'] +'/' + guid + '/_allSubscribed'
    rd = requests.get( URI, data=json.dumps(data), headers=Headers).json()   
    
    if rd['subscribedUser'] is not None:
        data = [ user['uuid'] for user in rd['subscribedUser'] ]
    return data



@csrf_exempt 
def update(request,d_uuid, u_uuid, m_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDispatcherRight(uuid=u_uuid, domainUuid=d_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
        
    res = {'res': None}
    if request.method == 'POST':
        """
        URL : /api/message/{d_uuid}/{u_uuid}/subscription/{m_uuid}
            
        Method : POST
            
        Update message subscription list, must have 'domain' or 'dispatcher' role
            
        POST URI Parameters:    
            d_uuid (str) : the domain uuid
            u_uuid (str) : the user uuid
            m_uuid (str) : the muuid uuid

        POST BODY JSON:      
            {
                # mandatory
                "dispatchedguid" : [goup uuid, ] (list of group uuid)
            }
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
        """
        mandatoryTupl = ('dispatchedguid',)           
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)    

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            Group = [ group for group in data['dispatchedguid']]
            members = []
                
            for guid in Group:
                members.extend(getGroupMembers(request, guid))
                    
            dispatched     = list(set(members))
            dispatchedguid = Group     
            
            #q_uuid = 'select * from `pm_message` `msg` where  uuid="' + m_uuid + '" ;'
            q_uuid = {'uuid': m_uuid} 
            ns = _nosql._nosql()
            resSearch = [ {'msg':r} for r  in ns.search(bucket='pm_message',query=q_uuid )]
            if len(resSearch) == 1:     
                jsd = resSearch[0]['msg']
                
                jsd['dispatchedguid'] = dispatchedguid
                jsd['dispatched']     = dispatched
           
                resUpsert = ns.upsert(bucket='pm_message',uuid=jsd['uuid'], upsert=jsd )
                res = {'res': resUpsert}
    

       
    return JsonResponse(res) 


@csrf_exempt 
def updateSubscribedGroupByDelegated(request, d_uuid, u_uuid ,m_uuid): 
    """
    URL :  /api/message/{d_uuid}/{u_uuid}/subscription/{m_uuid}/updateSubscribedGroupByDelegated
    Method : POST     
            
    Update message subscription list, must have 'domain' or 'dispatcher' or 'delegated' role
        
    POST URI Parameters:    
        d_uuid (str) : the domain uuid
        u_uuid (str) : the user uuid
        m_uuid (str) : the muuid uuid
    
    PUT BODY JSON:      
        {
            # Mandatory
            "action" <add|remove>,
            "groupid": <group uuid> ,
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')

    if not _control._control().controlDelegatedRight(uuid=u_uuid, domainUuid=d_uuid, token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        res = {'status': 'False'} 
        
        mandatoryTupl = ('action','groupid')
        optionsTupl   = ()

        
        allTupl = mandatoryTupl + optionsTupl 
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        tokenElmt = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
        
        
        groupid = data['groupid']
        if not groupid in tokenElmt['subscribedguid']:
            i_log = 'user ' + tokenElmt['uuid'] + ' tried adding group ' + groupid + ' which is not in his own ones'
        else:    
            # Mandatories fields are present
            if all(jskey in data.keys() for jskey in mandatoryTupl):
                q_uuid = {'uuid': m_uuid} 
                ns = _nosql._nosql()
                resSearch = [ {'msg':r} for r  in ns.search(bucket='pm_message',query=q_uuid )]
                if len(resSearch) == 1:     
                    jsd = resSearch[0]['msg']

                    if 'add' in data['action']:
                        jsd['dispatchedguid'].append(groupid)
                    else:
                        jsd['dispatchedguid'].remove(groupid)

                    resUpsert = ns.upsert(bucket='pm_message', uuid=m_uuid, upsert= jsd )
                    res = { 'status': resUpsert }
                    i_log = 'update user subscribed group for message ' + m_uuid + ' by ' + u_uuid

            else:
                i_log = 'update user subscribed group failed for message ' + m_uuid + ' by ' + u_uuid +', one of field (' + ','.join(mandatoryTupl) + ') missing'
            
        apiLog.info(i_log)    
        return JsonResponse(res) 
