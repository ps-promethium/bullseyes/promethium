#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import io
import json
import jwt
import os.path 
from pymemcache.client import base
import requests
import sys
import time
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
from promethiumClass.various import _uuid

import promethiumAPIAuth.views as promethiumAPIAuth_views
from . import status
 
def get(request,u_uuid,m_uuid):     
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'GET':
        """
            URL : /api/message/{u_uuid}/tag/{m_uuid}
                
            Method : GET
                
            Retrieve tags if exist
                
            Get Parameters:      
                u_uuid (str)   : the account uuid
                m_uuid (str)  : the message uuid
                
            Return :    
                JSON { "tags" : [{"uuid": "<uuid>", "u_uuid": "<u_uuid>", "l_uuid": "<l_uuid>"},]} or {'label': None}
                HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """
        res = {'tags': []}
        tokenElmt = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
        q_uuid = [
                {"$lookup": {"from": "pm_usertags", "localField": "t_uuid", "foreignField": "uuid", "as": "tag"}},
                {"$project": {"_id":0, "tag._id":0}},
                {"$match":{"$and":[{"$or":[{'u_uuid':u_uuid},{'u_uuid':tokenElmt['domain']}]},{'m_uuid':m_uuid}]}},
              ] 
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.aggregate(bucket='pm_tags',query=q_uuid )]
        
        for t in resSearch:
            if isDomainTag(tokenElmt['domain'],t['t_uuid']):
                res['tags'].append({'uuid': t['uuid'], 't_uuid': t['t_uuid'], 'displayname': "[d] " + t['tag'][0]['displayname']})
            else:
                res['tags'].append({'uuid': t['uuid'], 't_uuid': t['t_uuid'], 'displayname': t['tag'][0]['displayname']})
                                    
        return JsonResponse(res) 

    
def isDomainTag(domainUuid,t_uuid):     
        res = False
        q_uuid = [
                {"$project": {"_id":0}},
                {"$match":{"$and":[{'owner':domainUuid},{'uuid':t_uuid}]}},
              ] 
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.aggregate(bucket='pm_usertags',query=q_uuid )]
        if len(resSearch) == 1 :
            res = True

        return res 
  
    
@csrf_exempt 
def delete(request,u_uuid, t_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')  

    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action') 

    if request.method == 'DELETE':
        """
            URL : /api/message/{u_uuid}/tag/{t_uuid}/delete
                    
            Method : DELETE
                    
            DELETE a user tag applied to message
                    
            DELETE URI Parameters: 
                u_uuid (str) : the user uuid
                t_uuid (str) : the tag uuid  
                    
            Return :    
                JSON {'status': True} if OK or list of subscribed user(s)
                HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """
        res = {'status': False}
        
        ns = _nosql._nosql()
        try:
            q_uuid = {'uuid': t_uuid}
            
            resRem = ns.remove_many(bucket='pm_tags',query=q_uuid)
            res = {'status': resRem} 
        except:
            pass
        return JsonResponse(res) 


@csrf_exempt 
def upsert(request, u_uuid, m_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
    if request.method == 'POST':
        """
        URL : /api/message/{u_uuid}/tag/{m_uuid}/upsert'
                
        Method : POST
                
        Update tags on message
    
        POST BODY JSON:      
            {
                # Mandatory
                "subscribedtags" : [t_uuid1, t_uuid2, ]
            }
            
        Return :    
            HTTP 200 if OK
            HTTP Errno 403 and some contextual errortext (authentication failed...)
                
        """    
        mandatoryTupl = ('subscribedtags',)           
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)          
        res = {'status': False}
        
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            tags = [ tag for tag in data['subscribedtags']]
            domainUuid = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']
            ns = _nosql._nosql()
            for t_uuid in tags: 

                if isDomainTag(domainUuid,t_uuid):
                    q_uuid = {'m_uuid': m_uuid, 'u_uuid': domainUuid, 't_uuid': t_uuid } 
                    jsd = {'uuid':'', 'm_uuid': m_uuid, 'u_uuid': domainUuid, 't_uuid': t_uuid }
                else:
                    q_uuid = {'m_uuid': m_uuid, 'u_uuid': u_uuid, 't_uuid': t_uuid } 
                    jsd = {'uuid':'', 'm_uuid': m_uuid, 'u_uuid': u_uuid, 't_uuid': t_uuid }                   
                resSearch = [ {'tags':r} for r  in ns.search(bucket='pm_tags',query=q_uuid )]
                if len(resSearch) == 1:     
                    jsd['uuid'] = resSearch[0]['tags']['uuid']
                else:
                    jsd['uuid'] =  _uuid._uuid().new()
                    
                res['status'] = ns.upsert(bucket='pm_tags',uuid=jsd['uuid'], upsert=jsd )


        return JsonResponse(res) 



def retrieveMessageFromTag(request, u_uuid, t_uuid):
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
    if request.method == 'GET':
        tokenElmt = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
        res = {'total': 0 , 'msg': []}
        try:
            ns = _nosql._nosql()
            q_uuid = [
                {"$lookup": {"from": "pm_message", "localField": "m_uuid", "foreignField": "uuid", "as": "msg"}},
                {"$project": {"_id":0, "msg._id":0}},
                {"$match":{"$and":[{"$or":[{'u_uuid':u_uuid},{'u_uuid':tokenElmt['domain']}]},{'t_uuid':t_uuid}]}},
              ] 
            resSearch = [ {'msg':r['msg'][0]} for r  in ns.aggregate(bucket='pm_tags',query=q_uuid )]  

            if len(resSearch) > 0:
                for hit in resSearch:     
                    if (tokenElmt['role'] in ("domain","dispatcher")) or ((jskey in tokenElmt['subscribedguid'] for jskey in hit['msg']['dispatchedguid']) and (len(hit['msg']['dispatchedguid']))):
                        res['total'] += 1
                        m_uuid = hit['msg']['uuid']
                        msg = {'uuid': m_uuid, 'from': '' , 'date':'', 'subject':'', 'attachment': '', 'x_priority':'', 'status':'unread', 'chrono':'','dispatched': False}
                        
                        msg['from']         = hit['msg']['from']
                        msg['fromPretty']   = hit['msg']['from'].split('<')[0]
                        
                        msg['date']         = hit['msg']['date']
                        msg['subject']      = hit['msg']['subject']
                        msg['attachment']   = True if len(hit['msg']['attachment'])>0 else False 
                        msg['x_priority']   = hit['msg']['x_priority']
                        msg['status']       = status.getStatus(u_uuid,m_uuid)['status']
                        msg['outBound']     = hit['msg']['outBound']
                        msg['defaultColor'] = hit['msg']['defaultColor']
                        msg['chrono']       = hit['msg']['chrono']
                        msg['dispatched']   = True if len(hit['msg']['dispatchedguid'])>0 else False

                        res['msg'].append(msg)  
        except:
            pass
        return JsonResponse(res) 
    
