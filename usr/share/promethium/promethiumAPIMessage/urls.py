#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url

from django.urls import include, path

from . import alert
from . import attachment
from . import comment
from . import message
from . import status
from . import subscription
from . import delete
from . import tag

urlpatterns = [
    
    # time based info by user on this message
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/alert/(?P<m_uuid>[a-zA-Z0-9\-]+)/add', alert.add),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/alert/(?P<m_uuid>[a-zA-Z0-9\-]+)/update', alert.update),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/alert/(?P<m_uuid>[a-zA-Z0-9\-]+)/delete', alert.delete),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/alert/_now', alert.getAllInTimeAlert),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/alert/(?P<m_uuid>[a-zA-Z0-9\-]+)', alert.get),
    
    # inner message atachment
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/attachment/(?P<f_uuid>[a-zA-Z0-9\-]+)/details', attachment.details),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/attachment/(?P<f_uuid>[a-zA-Z0-9\-]+)/download', attachment.download),   
    
    # comment(s) on message
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/comment/(?P<m_uuid>[a-zA-Z0-9\-]+)/add', comment.add),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/comment/(?P<m_uuid>[a-zA-Z0-9\-]+)/update', comment.update),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/comment/(?P<m_uuid>[a-zA-Z0-9\-]+)/delete', comment.delete),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/comment/(?P<m_uuid>[a-zA-Z0-9\-]+)', comment.get),
        
    # get message    
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/message/_bydate', message.getByDate),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/message/_bylabel', message.getByLabel),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/message/_unread', message.getUnread),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/message/(?P<m_uuid>[a-zA-Z0-9\-]+)', message.get),
    
    # Forward message
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/forward/(?P<m_uuid>[a-zA-Z0-9\-]+)', message.forwardMessage),
    
    # who read message
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/event/(?P<m_uuid>[a-zA-Z0-9\-]+)', message.event),
    
    # who has access to message
    url(r'^(?P<d_uuid>[a-zA-Z0-9\-]+)/(?P<u_uuid>[a-zA-Z0-9\-]+)/subscription/(?P<m_uuid>[a-zA-Z0-9\-]+)/updateSubscribedGroupByDelegated$', subscription.updateSubscribedGroupByDelegated),
    url(r'^(?P<d_uuid>[a-zA-Z0-9\-]+)/(?P<u_uuid>[a-zA-Z0-9\-]+)/subscription/(?P<m_uuid>[a-zA-Z0-9\-]+)', subscription.update),


    # delete message 
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/delete/(?P<m_uuid>[a-zA-Z0-9\-]+)', delete.delete),
    
    # tag message 
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/tag/(?P<t_uuid>[a-zA-Z0-9\-]+)/delete$', tag.delete),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/tag/(?P<m_uuid>[a-zA-Z0-9\-]+)/upsert$', tag.upsert), 
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/tag/(?P<t_uuid>[a-zA-Z0-9\-]+)/retrieve$', tag.retrieveMessageFromTag), 
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/tag/(?P<m_uuid>[a-zA-Z0-9\-]+)', tag.get),
    

]
