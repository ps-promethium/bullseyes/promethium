#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml
import pprint

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
import promethiumAPIAuth.views as promethiumAPIAuth_views


def get(request,domainUuid): 
    """
    URL : /api/monitor/{domainUuid}
            
    Method : GET
            
    Retrieve the last free numerical id
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'domainUsers':0, 'domainDisabledUser':0, 'domainAdmin':0, 'domainDispatcher':0, 'domainGroup':0, 'domainMessage':0, 
              'domainMessageIncoming':0, 'domainMessageOutgoing':0, 'domainMessageUndistributed':0  }  
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """


    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res= {'domainUsers':0, 'domainDisabledUser':0, 'domainAdmin':0, 'domainDispatcher':0, 'domainGroup':0, 'domainMessage':0, 
              'domainMessageIncoming':0, 'domainMessageOutgoing':0, 'domainMessageUndistributed':0  }      
 
        res['domainUsers']                = __getUserCounter(request)
        res['domainDisabledUser']         = __getDisableUserCounter(request)
        res['domainAdmin']                = __getDomainUserCounter(request)
        res['domainDispatcher']           = __getDispatcherUserCounter(request)
        res['domainGroup']                = __getGroupCounter(request)
        res['domainMessage']              = __getMessageCounter(request) 
        res['domainMessageIncoming']      = __getMessageCounterIncoming(request) 
        res['domainMessageOutgoing']      = __getMessageCounterOutgoing(request) 
        res['domainMessageUndistributed'] = __getMessageCounterUndistributed(request)  
        return JsonResponse(res) 




def __getUserCounter(request):
    c = 0
    bucket='pm_user'
    try:    
        ns = _nosql._nosql()
        query = {'$and':[{'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']}]}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 
        
def __getDisableUserCounter(request):
    c = 0
    bucket='pm_user'
    try:    
        ns = _nosql._nosql()
        query = {'$and':[{'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']},{'enable': "False"}]}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 

def __getDomainUserCounter(request):
    c = 0
    bucket='pm_user'
    try:    
        ns = _nosql._nosql()
        query = {'$and':[{'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']},{'role': "domain"}]}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 

def __getDispatcherUserCounter(request):
    c = 0
    bucket='pm_user'
    try:    
        ns = _nosql._nosql()
        query = {'$and':[{'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']},{'role': "dispatcher"}]}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 

def __getGroupCounter(request):
    c = 0
    bucket='pm_group'
    try:    
        ns = _nosql._nosql()
        query = {'owner':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 

def __getMessageCounter(request):
    c = 0
    bucket='pm_message'
    try:    
        ns = _nosql._nosql()
        query = {'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 

def __getMessageCounterIncoming(request):
    c = 0
    bucket='pm_message'
    try:    
        ns = _nosql._nosql()
        query = {'$and':[{'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']},{'outBound': False}]}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 

def __getMessageCounterOutgoing(request):
    c = 0
    bucket='pm_message'
    try:    
        ns = _nosql._nosql()
        query = {'$and':[{'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']},{'outBound': True}]}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c 

def __getMessageCounterUndistributed(request):
    c = 0
    bucket='pm_message'
    try:    
        ns = _nosql._nosql()
        query = {'$and':[{'domain':_control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])['domain']},{'dispatchedguid': ''}]}
        resSearch = ns.search(bucket=bucket,query=query)
        c = len(list(resSearch.clone()))
    except:
        pass
        
    return c


def getDbStat(request,domainUuid):
    """
    URL : /api/monitor/{domainUuid}/dbstat
            
    Method : GET
            
    Retrieve the last free numerical id
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'stat': {'db': 'promethium', 'collections': 15,...}  
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.debug(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    if request.method == 'GET':
        res= { 'stat': None }     

        try:    
            ns = _nosql._nosql()
            res['stat'] = ns.stat()
        except:
            pass
        return JsonResponse(res) 
