#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
import promethiumAPIAuth.views as promethiumAPIAuth_views
import promethiumAPIMessage.message as promethiumAPIMessage_message
import promethiumAPIMessage.status as promethiumAPIMessage_status
import promethiumAPIUser.views as promethiumAPIUser_views

@csrf_exempt 
def quickSearchInMessage(request,domainUuid): 
    """
    URL : /api/search/{domainUuid}/message/quick
            
    Method : POST     
      
    Search in message index only on text fields ('from', 'to', 'cc', 'bcc' , 'subject', 'body')
    Limit search to 30 past days.
          
    POST URI Parameters:    
        domainUuid (str) : the domain uuid      
          
    POST BODY JSON:      
        {
            # Mandatory
            #"field": <string>, the field name where to search
            "string": <string>,
        }
        
    Return :    
        JSON { "total: , "msg" : [
                                        {"uuid":"<msg uuid>", 
                                        "date": "<date>", 
                                        "from":"<msg sender address>", 
                                        "subject": "<subject>",
                                        "attachment": <True|False>, 
                                        "x_priority":<1|2|3|4|5>,
                                        "status": "<read|unread>",
                                        "outBound": <true|false>,
                                        "dispatched",
                                        "defaultColor"
                                        }, 
                                    ]
                } 
                    or 
                {'total': 0 , 'msg': []}
                
        HTTP Errno 403 and some contextual errortext (authentication failed...)    
    """
    
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    #if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], 
                                                  token = request.META['HTTP_X_PM_APIKEY']):    
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'POST':
        res = {'total': 0 , 'msg': []}
        data = {}
        jsquery = {}
        role = _control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['role']

        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        # Mandatories fields are present
        if len(data['string'])>0: # and len(data['field'])>0:
            #field  = data['field']
            string = data['string']
        
            tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
            
            q_uuid = [
                {"$match":
                    {"$and":[{'domain':tokenElmt['domain']},
                             {"$text": {"$search":string}}
                             ]}
                },
                {"$project": {"_id":0}}
            ]  
            
            #if field in ('from', 'to', 'cc', 'bcc','body', 'subject'): 
                #q_uuid[0]['$match']['$and'].append({"$text": {"$search":string}})
                       
            if tokenElmt['role'] not in ("domain", "dispatcher"):
                OR = []
                OR.append({'dispatched':tokenElmt['uuid']})
                
                g_uuidList = promethiumAPIUser_views._getUserGuid(tokenElmt['uuid'])['details'][0]['subscribedguid']
                if g_uuidList is not None:
                    for guid in g_uuidList:
                        OR.append({'dispatchedguid': guid})
                
                q_uuid[0]['$match']['$and'].append({'$or':OR})

            ns = _nosql._nosql()
            resSearch = [ {'msg':r} for r  in ns.aggregate(bucket='pm_message',query=q_uuid )]
            
            if len(resSearch) > 0:
                res['total'] = len(resSearch)
                
                for hit in resSearch:                
                    uuid = hit['msg']['uuid']
                    msg = {'uuid': uuid, 'from': '' , 'date':'', 'subject':'', 'attachment': '', 'x_priority':'', 'chrono': 0}
                    
                    
                    msg['from']         = hit['msg']['from']
                    msg['fromPretty']   = hit['msg']['from'].split('<')[0]
                    
                    msg['date']         = hit['msg']['date']
                    msg['subject']      = hit['msg']['subject']
                    msg['attachment']   = True if len(hit['msg']['attachment'])>0 else False 
                    msg['x_priority']   = hit['msg']['x_priority']
                    msg['status']       = promethiumAPIMessage_status.getStatus(_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'],uuid)['status']
                    msg['outBound']     = hit['msg']['outBound']
                    msg['dispatched']   = hit['msg']['dispatchedguid']
                    msg['defaultColor'] = hit['msg']['defaultColor']
                    msg['chrono']       = hit['msg']['chrono']
                    
                    #if (tokenElmt['role'] in ("domain")) or (jskey in tokenElmt['subscribedguid'] for jskey in hit['msg']['dispatchedguid']):
                    if (tokenElmt['role'] in ("domain","dispatcher")) or ((jskey in tokenElmt['subscribedguid'] for jskey in hit['msg']['dispatchedguid']) and (len(hit['msg']['dispatchedguid']))):     
                        res['msg'].append(msg)
        
            
        return JsonResponse(res) 



@csrf_exempt 
def complexSearchInMessage(request,domainUuid): 
    """
    URL : /api/chrono/{domainUuid}/delete
            
    Method : POST
            
    Delete the chrono entry in Elastic for this domain
            
    DELETE URI Parameters:    
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], 
                                                  token = request.META['HTTP_X_PM_APIKEY']):   
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'POST':
        res = {'total': 0 , 'msg': []}
        data = {}
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
        q_uuid = [
                {"$match":
                    {"$and":[{'domain':tokenElmt['domain']},
                             data
                             ]}
                },
                {"$project": {"_id":0, "uuid":1}}
            ]  
        
        ns = _nosql._nosql()
        resSearch = [ {'msg':r} for r  in ns.aggregate(bucket='pm_message',query=q_uuid )]
        #resSearch = [{'msg': {'uuid': '0EAEADFD-6E61-4C86-964D-E8BC89762C8B'}}]
        for msg in resSearch:
            m =  promethiumAPIMessage_message.getHeader(msg['msg']['uuid'],request.META['HTTP_X_PM_APIKEY'])
            if m['msg'] is not None:
                res['msg'].append(m['msg'])
                res['total'] += 1
        return JsonResponse(res) 

    
   

@csrf_exempt 
def fulltextSearch(request,domainUuid): 
    """
    URL : /api/search/{domainUuid}/message/fulltext
            
    Method : POST
            
    Retrieve all msg from pm_tika
            
    DELETE URI Parameters:    
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    #if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], 
                                                  token = request.META['HTTP_X_PM_APIKEY']): 
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'POST':
        res = {'total': 0 , 'msg': []}
        muuid = []
        data = {}
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])
        q_uuid = [
                {"$match":
                    {"$and":[{'domain':tokenElmt['domain']},
                             data
                             ]}
                },
                {"$project": {"_id":0, "m_uuid":1}}
            ]  
        
        ns = _nosql._nosql()
        resSearch = [ {'msg':r} for r  in ns.aggregate(bucket='pm_tika',query=q_uuid )]
        #resSearch = [{'msg': {'m_uuid': '0EAEADFD-6E61-4C86-964D-E8BC89762C8B'}}]
        for msg in resSearch:
            m =  promethiumAPIMessage_message.getHeader(msg['msg']['m_uuid'],request.META['HTTP_X_PM_APIKEY'])
            
            if m['msg'] is not None and msg['msg']['m_uuid'] not in muuid:
                res['msg'].append(m['msg'])
                res['total'] += 1
                muuid.append(msg['msg']['m_uuid']) 
                
        return JsonResponse(res) 
   



   
