#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt
import io
from pymemcache.client import base
import os.path 
import requests
import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass._nosql import _nosql
from promethiumClass.auth import _control
import promethiumAPIAuth.views as promethiumAPIAuth_views

def globalCounters(request,domainUuid): 
    """
    URL : /api/stats/{domainUuid}/globalCounters
            
    Method : GET     
      
    Get the incoming and outgoing message counters, by date
          
    GET URI Parameters:    
        domainUuid (str) : the domain uuid      

    Return :    
        JSON {
            'incoming': [
                {'date': '2021-08-13', 'count': 1}, 
                {'date': '2021-09-15', 'count': 1}, 
                {'date': '2021-10-07', 'count': 2}, 
                {'date': '2022-11-03', 'count': 1}], 
            'outgoing': [
                {'date': '2022-06-08', 'count': 2}]
            }
               
        HTTP Errno 403 and some contextual errortext (authentication failed...)    
    """
    
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    #if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], 
                                                  token = request.META['HTTP_X_PM_APIKEY']):    
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'GET':
        res = {'incoming': [], 'outgoing': []}
        tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY']) 
        q_uuid = [
                    {
                        '$match': {
                            '$and': [
                                {
                                    'domain':tokenElmt['domain']
                                }
                            ]
                        }
                    }, {
                        '$group': {
                            '_id': {
                                'outBound': '$outBound', 
                                'litDate': '$litDate'
                            }, 
                            'count': {
                                '$sum': 1
                            }
                        }
                    }, {
                        '$sort': {
                            '_id.litDate': 1
                        }
                    }, {
                        '$group': {
                            '_id': '$_id.outBound', 
                            'byDate': {
                                '$push': {
                                    'date': '$_id.litDate', 
                                    'count': '$count'
                                }
                            }, 
                            'count': {
                                '$sum': '$count'
                            }
                        }
                    }
                ] 



        ns = _nosql._nosql()
        resSearch = [ {'msg':r} for r  in ns.aggregate(bucket='pm_message',query=q_uuid )]

        for cnt in resSearch:
            if cnt['msg']['_id']:
                res['outgoing'] = cnt['msg']['byDate']
            else:
                res['incoming'] = cnt['msg']['byDate']
            
        return JsonResponse(res) 



@csrf_exempt 
def topSenders(request,domainUuid): 
    """
    URL : /api/stats/{domainUuid}/topSenders
            
    Method : POST
            
    Delete the chrono entry in Elastic for this domain
            
    DELETE URI Parameters:    
        domainUuid (str) : the domain uuid 
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], 
                                                  token = request.META['HTTP_X_PM_APIKEY']):   
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'POST':
        pass
   



   
