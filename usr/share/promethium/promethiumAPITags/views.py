#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import requests
import time
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid
import promethiumAPIAuth.views as promethiumAPIAuth_views
import promethiumAPIMessage.message as promethiumAPIMessage_message



def _all(request,domainUuid): 
    """
    URL : /api/tags/{domainUuid}/_all
            
    Method : GET
            
    Retrieve all tags item  for domain {domainUuid}
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid
            
    Return :    
        JSON {'domainTags': {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'GET':
        res = {}
        res['domainTags'] = None 
        q_uuid = [ {"$match":{"owner": domainUuid }},
                  {"$sort":{"displayname":1}},
                  {"$project" : { "_id" : 0, }}
                 ]
        try:
            res_q_uuid = [ r for r in _nosql._nosql().aggregate(bucket='pm_usertags',query=q_uuid)]
            if len(res_q_uuid) > 0 :
                res['domainTags'] = res_q_uuid
        except: 
            pass
        return JsonResponse(res) 


            
@csrf_exempt 
def add(request,domainUuid):
    """
    URL : /api/tags/{domainUuid}/add
            
    Method : PUT
            
    Add a new domain tag item in tree 
           
    PUT URI Parameters: 
        domainUuid (str) : the domain uuid
        

    PUT BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight( domainUuid=domainUuid ,token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'PUT':
            res = {'status': False} 
            mandatoryTupl = ('displayname',)
            allTupl       = mandatoryTupl 
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)

            if all(jskey in data.keys() for jskey in mandatoryTupl):
                jsd = {}
                tuuid =  _uuid._uuid().new()
                    
                jsd['uuid']        = tuuid
                jsd['displayname'] = data['displayname']
                jsd['owner']       = domainUuid

                ns = _nosql._nosql()
                try:
                    resUpsert = ns.upsert(bucket='pm_usertags',uuid=tuuid, upsert= jsd )
                    res = { 'status': resUpsert }
                except:
                    pass
                
            return JsonResponse(res) 



@csrf_exempt 
def update(request,domainUuid,t_uuid):
    """
    URL : /api/tags/{domainUuid}/{t_uuid}/update
            
    Method : POST
            
    Update an user tag item
       
    POST URI Parameters: 
        domainUuid (str) : the domain uuid
        t_uuid (str) : the tag uuid  
        
    POST BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight( domainUuid=domainUuid ,token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'POST':
        res = {'status': False} 
        mandatoryTupl = ('displayname',)
        allTupl       = mandatoryTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            
            jsd['uuid']        = t_uuid
            jsd['displayname'] = data['displayname']
            jsd['owner']       = domainUuid
            
            ns = _nosql._nosql()
            try:
                resUpsert = ns.upsert(bucket='pm_usertags',uuid=t_uuid, upsert= jsd )
                res = { 'status': resUpsert }
            except:
                pass
        return JsonResponse(res) 



@csrf_exempt 
def delete(request,domainUuid,t_uuid):
    """
    URL : /api/tags/{domainUuid}/{t_uuid}/delete
            
    Method : DELETE
            
    DELETE an user tag item
            
    DELETE URI Parameters: 
        domainUuid (str) : the domain uuid
        t_uuid (str) : the tag uuid  
            
    Return :    
        JSON {'status': True} if OK or list of subscribed user(s)
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight( domainUuid=domainUuid ,token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action') 
        
    if request.method == 'DELETE':
        res = {'status': False}
        
        ns = _nosql._nosql()
        try:
            resRem = ns.remove(bucket='pm_usertags',uuid=t_uuid )
            q_uuid = {'t_uuid': t_uuid, 'u_uuid':domainUuid}
            ns.remove_many(bucket='pm_tags',query=q_uuid)
            res = {'status': resRem} 
        except:
            pass
        return JsonResponse(res) 



def details(request,domainUuid,t_uuid):
    """
    URL : /api/tags/{domainUuid}/{t_uuid}/details
            
    Method : GET
            
    Retrieve all information of user tag item
            
    GET URI Parameters:      
        domainUuid (str)  : the domain uuid
        t_uuid (str) : the tag uuid 
            
    Return :    
        JSON {'details': ['tags': {...}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'GET':
        res = {'details': None}
        
        #q_uuid = 'select * from `pm_usertags` `tags` where  owner="' + u_uuid + '" and uuid="' + t_uuid + '" ;'
        q_uuid = [{"$match":{"$and":[{'owner': domainUuid}, {'uuid':t_uuid}]}},
                  {"$project" : { "_id" : 0, }}
                  ]
        ns = _nosql._nosql()
        try:
            resSearch = [ {'tags':r} for r  in ns.aggregate(bucket='pm_usertags',query=q_uuid )]
            if len(resSearch) == 1 :
                res = {'details': resSearch[0]['tags']}         
        except:
            pass

        return JsonResponse(res) 


  
@csrf_exempt
def uuidFromName(request, domainUuid):
    """
    URL : /api/tags/{domainUuid}/uuidFromName
            
    Method : POST
            
    Retrieve all information of user tag item
            
    GET URI Parameters:      
        domainUuid (str)  : the domain uuid
        t_uuid (str) : the tag uuid 
            
    Return :    
        JSON {'details': ['tags': {...}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=_control._control().retrieveUserElementFromToken(token = request.META['HTTP_X_PM_APIKEY'])['uuid'], token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    if request.method == 'POST':
        res = {'uuid': None} 
        mandatoryTupl = ('displayname',)
        allTupl       = mandatoryTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            try:
                q_uuid = [{"$match":{"$and":[{'owner': domainUuid}, {'displayname': data['displayname']}]}},
                        {"$project" : { "_id" : 0, }}
                        ]
                ns = _nosql._nosql()
                resSearch = [ {'tags':r} for r  in ns.aggregate(bucket='pm_usertags',query=q_uuid )]
                if len(resSearch) == 1 :
                    res = {'uuid': resSearch[0]['tags']['uuid']}         
            except:
                pass

        return JsonResponse(res) 



