#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import jwt

import io

from pymemcache.client import base

import os.path 

import requests

import sys
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control, _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid, _conf
import promethiumAPIAuth.views as promethiumAPIAuth_views
import promethiumAPIGroup.views as promethiumAPIGroup_views

conf = _conf._conf().get() 

@csrf_exempt 
def add(request,domainUuid): 
    """
    URL : /api/user/{domainUuid}/add
            
    Method : PUT
            
    Add a new user (admin or domain only)
        
    PUT URI Parameters:    
        domainUuid (str) : the domain uuid       
    
    PUT BODY JSON:      
        {
            # Mandatory
            "enable" <True|False if want to activate it>,
            "lastname": <lastname> ,
            "login": <login>,
            "passwd": <clear password>,
            "role": <admin|domain|dispatcher|delegated|user>,
            
            #Options 
            "firstname" <firstname>,            
            "subscribedguid": [ <UUID Group List> ]
            "lang": <language as en|fr>,
            "theme": <theme>,
            "timez": <timezone>,
            "nbOfDays": <the number of days of msg displayed>
            "hideItems": [ list of message masked items ]
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight( domainUuid=domainUuid ,token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'PUT':
        res = {'status': False} 
        
        mandatoryTupl = ('enable','lastname','login','passwd','role')
        optionsTupl   = ('firstname','subscribedguid','lang','timez','theme','nbOfDays')

        
        allTupl = mandatoryTupl + optionsTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        # Mandatories fields are present
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            if checkIfUnique(data['login']):
                jsd = {}
                uuid =  _uuid._uuid().new()
                jsd['firstname']      = None            
                jsd['subscribedguid'] = None
                jsd['lang']           = conf['user']['lang'] 
                jsd['theme']          = conf['user']['theme']
                jsd['timez']          = conf['user']['timez']
                jsd['nbOfDays']       = conf['user']['nbOfDays']
                        
                jsd['uuid']      = uuid
                jsd['domain']    = domainUuid
                jsd['enable']    = data['enable']
                jsd['lastname']  = data['lastname']
                jsd['login']     = data['login']
                jsd['role']      = data['role']
                jsd['passwd']    = _cypher._cypher().hashPassword(data['passwd'])
                
                
                if 'firstname' in data.keys():
                    jsd['firstname'] = data['firstname']            
                
                if 'subscribedguid' in data.keys():
                    jsd['subscribedguid'] = data['subscribedguid']
                    
                if 'lang' in data.keys():
                    jsd['lang'] = data['lang']   
                            
                if 'theme' in data.keys():
                    jsd['theme'] = data['theme']

                if 'timez' in data.keys():
                    jsd['timez'] = data['timez']
                    
                if 'nbOfDays' in data.keys():
                    jsd['nbOfDays'] = data['nbOfDays']
                 
                if 'hideItems' in data.keys():
                    jsd['hideItems'] = data['hideItems']
                    
                    
                ns = _nosql._nosql()
                resUpsert = ns.upsert(bucket='pm_user',uuid=uuid, upsert= jsd )
        
                res = { 'status': resUpsert }
                i_log = 'adding user ' + data['login'] + ' status ' + str(resUpsert)
              
                
            else:
                i_log = 'user ' + data['login'] + ' already exists'
                res['errorText'] = i_log 
        else:
            i_log = 'adding user failed, one of field (' + ','.join(mandatoryTupl) + ') missing'
            res['errorText'] = i_log 
            
        apiLog.info(i_log)
            
            
        return JsonResponse(res) 



@csrf_exempt 
def update(request,domainUuid,u_uuid): 
    """
    URL : /api/user/{domainUuid}/{u_uuid}/update
            
    Method : POST     
            
    Update an user (admin or domain only)
        
    POSTT URI Parameters:    
        domainUuid (str) : the domain uuid    
        u_uuid (str) : the user uuid
    
    PUT BODY JSON:      
        {
            # Mandatory
            "enable" <True|False if want to activate it>,
            "lastname": <lastname> ,
            "login": <login>,
            "passwd": <hashed password>,
            "role": <admin|domain|dispatcher|delegated|user>,
            
            #Options 
            "firstname" <firstname>,
            "subscribedguid": <True|False if want cert control>,
            "lang": <True|False if want to automatically distribute mail to group/user>,
            "theme": [ <UUID Group List> ]
            "newpasswd": <new clear password>,
            "nbOfDays": <the number of days of msg displayed>,
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        res = {'status': 'False'} 
        
        mandatoryTupl = ('enable','lastname','login','passwd','role')
        optionsTupl   = ('firstname','subscribedguid','lang','theme','newpasswd','nbOfDays')

        
        allTupl = mandatoryTupl + optionsTupl 
        
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        # Mandatories fields are present
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            jsd['firstname']      = None            
            jsd['subscribedguid'] = None
            jsd['lang']           = conf['user']['lang'] 
            jsd['theme']          = conf['user']['theme']
            jsd['timez']          = conf['user']['timez']
            jsd['nbOfDays']       = conf['user']['nbOfDays']      
                  
            jsd['uuid']      = u_uuid
            jsd['domain']    = domainUuid
            jsd['enable']    = data['enable']
            jsd['lastname']  = data['lastname']
            jsd['login']     = data['login']
            jsd['role']      = data['role']

            
            jsd['passwd']    = data['passwd']
            if 'newpasswd' in data.keys():
                jsd['passwd'] = _cypher._cypher().hashPassword(data['newpasswd'])
            
            if 'firstname' in data.keys():
                jsd['firstname'] = data['firstname']            
            
            if 'subscribedguid' in data.keys():
                jsd['subscribedguid'] = data['subscribedguid']
                
            if 'lang' in data.keys():
                jsd['lang'] = data['lang']     
                        
            if 'theme' in data.keys():
                jsd['theme'] = data['theme']

            if 'timez' in data.keys():
                jsd['timez'] = data['timez']
            
            if 'nbOfDays' in data.keys():
                jsd['nbOfDays'] = data['nbOfDays']
                    
            if 'hideItems' in data.keys():
                jsd['hideItems'] = data['hideItems']
              
            ns = _nosql._nosql()  
            resUpsert = ns.upsert(bucket='pm_user',uuid=u_uuid, upsert= jsd )
            res = { 'status': resUpsert }
            i_log = 'updating user ' + data['login'] + ' status ' + str(resUpsert)

        else:
            i_log = 'updating user failed, one of field (' + ','.join(mandatoryTupl) + ') missing'
            
        apiLog.info(i_log)    
        return JsonResponse(res) 



@csrf_exempt 
def updateSubscribedGroup(request,domainUuid,u_uuid): 
    """
    URL : /api/user/{domainUuid}/{u_uuid}/updateSubscribedGroup
            
    Method : POST     
            
    Update an user (admin or domain only)
        
    POST URI Parameters:    
        domainUuid (str) : the domain uuid    
        u_uuid (str) : the user uuid
    
    PUT BODY JSON:      
        {
            # Mandatory
            "action" <add|remove>,
            "groupname": <groupname> ,
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')

    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

        
        
    if request.method == 'POST':
        res = {'status': 'False'} 
        
        mandatoryTupl = ('action','groupname')
        optionsTupl   = ()

        
        allTupl = mandatoryTupl + optionsTupl 
        



        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)
        
        # Mandatories fields are present
        if all(jskey in data.keys() for jskey in mandatoryTupl):
            ns = _nosql._nosql()
 		 
            groupid = promethiumAPIGroup_views._getGroupIdFromName(domainUuid, data['groupname'])

            q_uuid = {"uuid":u_uuid}       
            resSearch = [ r for r  in ns.search(bucket='pm_user',query=q_uuid )]
            jsd = resSearch[0]

            if 'add' in data['action']:
                jsd['subscribedguid'].append(groupid)
            else:
                jsd['subscribedguid'].remove(groupid)

            resUpsert = ns.upsert(bucket='pm_user',uuid=u_uuid, upsert= jsd )
            res = { 'status': resUpsert }
            i_log = 'updating user subscribed group' + u_uuid + ' status ' + str(resUpsert)

        else:
            i_log = 'updating user subscribed group failed, one of field (' + ','.join(mandatoryTupl) + ') missing'
            
        apiLog.info(i_log)    
        return JsonResponse(res) 



@csrf_exempt 
def delete(request,domainUuid,u_uuid): 
    """
    URL : /api/user/{domainUuid}/{u_uuid}/delete
            
    Method : DELETE
            
    Delete user if not in use (admin only)
            
    DELETE URI Parameters:    
        domainUuid (str) : the domain uuid 
        u_uuid (str)       : the user uuid
            
    Return :    
        JSON {'status': True} if OK or list of domain(s) where it's still in use
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'DELETE':
        res = {'status': False}

        ns = _nosql._nosql()
        resRem = ns.remove(bucket='pm_user', uuid=u_uuid )
        res = {'status': resRem} 

        return JsonResponse(res) 



def details(request,domainUuid,u_uuid): 
    """
    URL : /api/user/{domainUuid}/{u_uuid}/details
            
    Method : GET
            
    Retrieve all information of user (admin or domain only)
            
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
        u_uuid (str)       : the user uuid
            
    Return :    
        JSON {'details': [{'user': {...}}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'details': None}
        q_uuid = {"uuid":u_uuid}
        ns = _nosql._nosql()
        resSearch = [ {'user': r} for r  in ns.search(bucket='pm_user',query=q_uuid )]

        res = {'details': resSearch} 
        
        return JsonResponse(res) 
    
    
    
def displayname(request,domainUuid,u_uuid): 
    """
    URL : /api/user/{domainUuid}/{u_uuid}/displayname
            
    Method : GET
            
    Retrieve displayname user from its uuid 
    
    GET URI Parameters:      
        domainUuid (str) : the domain uuid 
        u_uuid (str)       : the user uuid
            
    Return :    
        JSON {'details': [{'user': {...}}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    tokenElmt  = _control._control().retrieveUserElementFromToken(request.META['HTTP_X_PM_APIKEY'])  
    if not _control._control().controlAccessRight(uuid=tokenElmt['uuid'], token = request.META['HTTP_X_PM_APIKEY'], settings=settings.CACHES['default']['LOCATION']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    
    if request.method == 'GET':
        res = {'details': None}
        q_uuid = {"uuid":u_uuid}
        ns = _nosql._nosql()
        resSearch = [ {'user': r} for r  in ns.search(bucket='pm_user',query=q_uuid )]
        res = {'details': resSearch} 
        
        return JsonResponse(res)    


    
def _all(request,domainUuid): 
    """
    URL : /api/user/{domainUuid}/_all
            
    Method : GET
            
    Retrieve all user uuids,login,firstname,lastname,enable,role (admin or domain only)
            
    GET URI Parameters:      
            
    Return :    
        JSON {'all': [{"login": "test", "uuid": "09D2B621-9852-4A52-8737-937328F8DB40",""first},]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlDomainAccessRight(domainUuid=domainUuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'GET':
        res = {'all': None}
        #q_uuid = 'select uuid,login,enable,firstname,lastname,`role` from `pm_user` where domain="' + domainUuid + '" order by login;'
        q_uuid = [{"$match":{"domain": domainUuid}},{"$sort":{"login":1}},{"$unset":["_id"]}]
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.aggregate(bucket='pm_user',query=q_uuid )]

        res = {'all': resSearch} 
        return JsonResponse(res) 


   
def getUserGuid(u_uuid): 
    """
    URL : None
            
    Method : internal
            
    Retrieve user subscribed group uid   
            
    Parameters:      
        u_uuid (str)       : the user uuid
            
    Return :    
        JSON {'details': [{'subscribedguid': ''}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    res = {'details': None}
        
    #q_uuid = 'select subscribedguid from `pm_user` `user` where  uuid="' + u_uuid + '" ;'
    q_uuid = {"uuid":u_uuid}
    ns = _nosql._nosql()
    resSearch = [ r for r  in ns.search(bucket='pm_user',query=q_uuid )]
    
    res = {'details': resSearch} 
    return JsonResponse(res) 


def _getUserGuid(u_uuid): 
    """
    URL : None
            
    Method : internal
            
    Retrieve user subscribed group uid   
            
    Parameters:      
        u_uuid (str)       : the user uuid
            
    Return :    
        JSON {'details': [{'subscribedguid': ''}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    res = {'details': None}
        
    #q_uuid = 'select subscribedguid from `pm_user` `user` where  uuid="' + u_uuid + '" ;'
    #q_uuid = {"uuid":u_uuid}
    q_uuid = [{"$match":{'uuid': u_uuid}},
              {"$project": {"_id":0,"subscribedguid": 1}},
             ]

    ns = _nosql._nosql()
    resSearch = [ r for r  in ns.aggregate(bucket='pm_user',query=q_uuid )]
    res = {'details': resSearch} 

    return res 



def checkIfUnique(login):
    """
    URL : None
            
    Method : internal
            
    Check if login already exists  
            
    Parameters:      
        login (str)       : the user login
            
    Return :    
        False or True if exists or not 
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    res = True
    #q_uuid = 'select count(login) nb from `pm_user` `user` where  login="' + login + '" ;'
    q_uuid =[{"$match": {"login": login}}, {"$count": "nb"}]
    ns = _nosql._nosql()
    resSearch = [ r for r  in ns.aggregate(bucket='pm_user',query=q_uuid )]
    
    if len(resSearch) and resSearch[0]['nb'] > 0:
        res = False
        
    return res 
