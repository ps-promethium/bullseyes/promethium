#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url

from django.urls import include, path

from . import views


urlpatterns = [
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/_all$', views._all),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/unsubscribeAll/(?P<m_uuid>[a-zA-Z0-9\-]+)', views.unsubscribeAll),
    
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/add$', views.add),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/(?P<f_uuid>[a-zA-Z0-9\-]+)/update', views.update),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/(?P<f_uuid>[a-zA-Z0-9\-]+)/delete', views.delete),
    
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/(?P<f_uuid>[a-zA-Z0-9\-]+)/details', views.details),
    
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/(?P<f_uuid>[a-zA-Z0-9\-]+)/subscribe/(?P<m_uuid>[a-zA-Z0-9\-]+)', views.subscribe),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/(?P<f_uuid>[a-zA-Z0-9\-]+)/unsubscribe/(?P<m_uuid>[a-zA-Z0-9\-]+)', views.unsubscribe),
    url(r'^(?P<u_uuid>[a-zA-Z0-9\-]+)/favorites/(?P<f_uuid>[a-zA-Z0-9\-]+)/_allSubscribed', views._allSubscribed),


]
