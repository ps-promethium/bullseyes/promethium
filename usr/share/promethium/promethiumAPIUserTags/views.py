#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Python lib
import json
import requests
import time
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

# promethium lib/class
from promethiumClass.auth import _control
from promethiumClass._nosql import _nosql
from promethiumClass.various import _uuid
import promethiumAPIAuth.views as promethiumAPIAuth_views
import promethiumAPIMessage.message as promethiumAPIMessage_message



def _all(request,u_uuid): 
    """
    URL : /api/user/{u_uuid}/tags/_all
            
    Method : GET
            
    Retrieve all tags item  for user {uuid}
            
    GET URI Parameters:      
        u_uuid (str) : the user uuid
            
    Return :    
        JSON {'userTags': {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'GET':
        res = {}
        res['userTags'] = None 
        q_uuid = [ {"$match":{"owner": u_uuid }},
                  {"$sort":{"displayname":1}},
                  {"$project" : { "_id" : 0, }}
                 ]
        try:
            res_q_uuid = [ r for r in _nosql._nosql().aggregate(bucket='pm_usertags',query=q_uuid)]
            if len(res_q_uuid) > 0 :
                res['userTags'] = res_q_uuid
        except: 
            pass
        return JsonResponse(res) 


            
@csrf_exempt 
def add(request,u_uuid):
    """
    URL : /api/user/{u_uuid}/tags/add
            
    Method : PUT
            
    Add a new user tag item in tree 
           
    PUT URI Parameters: 
        u_uuid (str) : the user uuid
        

    PUT BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')

    if request.method == 'PUT':
            res = {'status': False} 
            mandatoryTupl = ('displayname',)
            allTupl       = mandatoryTupl 
            body_unicode = request.body.decode('utf-8')        
            data    = json.loads(body_unicode)

            if all(jskey in data.keys() for jskey in mandatoryTupl):
                jsd = {}
                tuuid =  _uuid._uuid().new()
                    
                jsd['uuid']        = tuuid
                jsd['displayname'] = data['displayname']
                jsd['owner']       = u_uuid

                ns = _nosql._nosql()
                try:
                    resUpsert = ns.upsert(bucket='pm_usertags',uuid=tuuid, upsert= jsd )
                    res = { 'status': resUpsert }
                except:
                    pass
                
            return JsonResponse(res) 



@csrf_exempt 
def update(request,u_uuid,t_uuid):
    """
    URL : /api/user/{u_uuid}/tags/{t_uuid}/update
            
    Method : POST
            
    Update an user tag item
       
    POST URI Parameters: 
        u_uuid (str) : the user uuid
        t_uuid (str) : the tag uuid  
        
    POST BODY JSON:      
        {
            # Mandatory
            "displayname": <human comprehensive name> 
        }
        
    Return :    
        HTTP 200 if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')
    
    if request.method == 'POST':
        res = {'status': False} 
        mandatoryTupl = ('displayname',)
        allTupl       = mandatoryTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            jsd = {}
            
            jsd['uuid']        = t_uuid
            jsd['displayname'] = data['displayname']
            jsd['owner']       = u_uuid
            
            ns = _nosql._nosql()
            try:
                resUpsert = ns.upsert(bucket='pm_usertags',uuid=t_uuid, upsert= jsd )
                res = { 'status': resUpsert }
            except:
                pass
        return JsonResponse(res) 



@csrf_exempt 
def delete(request,u_uuid,t_uuid):
    """
    URL : /api/user/{u_uuid}/tags/{t_uuid}/delete
            
    Method : DELETE
            
    DELETE an user tag item
            
    DELETE URI Parameters: 
        u_uuid (str) : the user uuid
        t_uuid (str) : the tag uuid  
            
    Return :    
        JSON {'status': True} if OK or list of subscribed user(s)
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
        
    if request.method == 'DELETE':
        res = {'status': False}
        
        ns = _nosql._nosql()
        try:
            resRem = ns.remove(bucket='pm_usertags',uuid=t_uuid )
            q_uuid = {'t_uuid': t_uuid, 'u_uuid':u_uuid}
            ns.remove_many(bucket='pm_tags',query=q_uuid)
            res = {'status': resRem} 
        except:
            pass
        return JsonResponse(res) 



def details(request,u_uuid,t_uuid):
    """
    URL : /api/user/{u_uuid}/tags/{t_uuid}/details
            
    Method : GET
            
    Retrieve all information of user tag item
            
    GET URI Parameters:      
        u_uuid (str)  : the user uuid
        t_uuid (str) : the tag uuid 
            
    Return :    
        JSON {'details': ['tags': {...}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    if request.method == 'GET':
        res = {'details': None}
        
        #q_uuid = 'select * from `pm_usertags` `tags` where  owner="' + u_uuid + '" and uuid="' + t_uuid + '" ;'
        q_uuid = [{"$match":{"$and":[{'owner': u_uuid}, {'uuid':t_uuid}]}},
                  {"$project" : { "_id" : 0, }}
                  ]
        ns = _nosql._nosql()
        try:
            resSearch = [ {'tags':r} for r  in ns.aggregate(bucket='pm_usertags',query=q_uuid )]
            if len(resSearch) == 1 :
                res = {'details': resSearch[0]['tags']}         
        except:
            pass

        return JsonResponse(res) 


@csrf_exempt
def subscribe(request, u_uuid, t_uuid, m_uuid):
    """
    URL : /api/user/{u_uuid}/tags/{t_uuid}/subscribe/{m_uuid}
            
    Method : POST
            
    Subscribe a message to tag item 
            
    GET URI Parameters:      
        u_uuid (str)  : the user uuid
        t_uuid (str) : the tag uuid 
        m_uuid (str) : the message uuid
            
    Return :    
        JSON {'subscribed': True|False }
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """

    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'POST':
        res = {'subscribed': False}
        
        if len(t_uuid) > 4: 
            jsd = {'@timestamp':'', 'uuid':'', 't_uuid':'', 'u_uuid':'', 'm_uuid':'' }
            
            jsd['@timestamp'] = int(time.time())*1000
            jsd['uuid']       = _uuid._uuid().new()
            jsd['m_uuid']     = m_uuid
            jsd['t_uuid']     = t_uuid
            jsd['u_uuid']     = u_uuid

            if not checkIfAlreadyExists(m_uuid,t_uuid,u_uuid):           
                ns = _nosql._nosql()
                resUpsert = ns.upsert(bucket='pm_tags',uuid=jsd['uuid'], upsert= jsd )
        
                res = { 'subscribed': resUpsert }

        return JsonResponse(res, safe=False)         



def checkIfAlreadyExists(m_uuid,t_uuid,u_uuid):
        res = False
        ns = _nosql._nosql()

        q_uuid = [{"$match":{"$and":[{'m_uuid': m_uuid}, {'u_uuid':u_uuid},{'t_uuid':t_uuid}]}},
                  {"$unset":["_id"]}
                  ]
        try:
            resSearch = [ {'fav':r} for r  in ns.aggregate(bucket='pm_tags',query=q_uuid )]
            if len(resSearch)>=1 :
                res = True
        except:
            pass
        return res



@csrf_exempt
def unsubscribe(request, u_uuid, t_uuid):
    """
    URL : /api/user/{u_uuid}/tags/{t_uuid}/unsubscribe/{m_uuid}
            
    Method : DELETE
            
    Unsubscribe a message from tag item 
            
    GET URI Parameters:      
        u_uuid (str)  : the user uuid
        t_uuid (str) : the tag uuid 
        m_uuid (str) : the message uuid
            
    Return :    
        JSON {'subscribed': True|False }
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """

    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'DELETE':
        res = {'res': False}
        
        ns = _nosql._nosql()
        q_uuid = [{"$match":{"$and":[{'m_uuid': m_uuid}, {'u_uuid':u_uuid},{'t_uuid':t_uuid}]}},
                  {"$project" : { "_id" : 0, "uuid":1}}
                  ]
        try:
            resSearch = [ {'tag':r} for r  in ns.aggregate(bucket='pm_tags',query=q_uuid )]
            if len(resSearch) == 1:            
                resd = ns.remove(bucket='pm_tags',uuid=resSearch[0]['tag']['uuid'])
                if resd:
                    i_log = 'delete entry ref ' + resSearch[0]['uuid'] + ' in tags of ' + u_uuid + ' succeed'
                    apiLog.info(i_log)
                    res   = {'res': True}
                else:
                    i_log =  'delete entry ref ' + resSearch[0]['uuid'] + ' in tags of ' + u_uuid + ' failed'
                    apiLog.info(i_log)
                    res   = {'res': False, '_id':resSearch[0]['uuid']}
        except:
            pass
        return JsonResponse(res, safe=False)  
    


@csrf_exempt
def unsubscribeAll(request, u_uuid, m_uuid):
    """
    URL : /api/user/{u_uuid}/tags/unsubscribeAll/{m_uuid}
            
    Method : DELETE
            
    Unsubscribe a message from all tags item 
            
    GET URI Parameters:      
        u_uuid (str) : the user uuid
        m_uuid (str) : the message uuid
            
    Return :    
        JSON {'subscribed': True|False }
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """

    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    
    if request.method == 'DELETE':
        res = {'res': False}
        
        ns = _nosql._nosql()
        q_uuid = {'m_uuid': m_uuid, 'u_uuid':u_uuid}
        try:
            resd = ns.remove_many(bucket='pm_tags',query=q_uuid)
            if resd:
                i_log = 'delete message entry ref ' + m_uuid + ' in tags of ' + u_uuid + ' succeed'
                apiLog.info(i_log)
                res   = {'res': True}
            else:
                i_log =  'delete message entry ref ' + m_uuid + ' in tags of ' + u_uuid + ' failed'
                apiLog.info(i_log)
                res   = {'res': False, '_id':resSearch[0]['uuid']}
        except:
            pass
        return JsonResponse(res, safe=False)  



def _allSubscribed(request,u_uuid, t_uuid):
    """
    URL : URL : /api/user/{u_uuid}/tags/{t_uuid}/_allSubscribed
    
    Method : GET
            
    Retrieve all subscribed messages for this tags items
            
    Parameters:
        u_uuid (str)  : the user uuid
        t_uuid (str) : the tag uuid 
            
    Return :    
        JSON {'msg': {....}} if OK
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """

    res = {'total':0, 'msg': [] } 
            
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')      
    
           
    if request.method == 'GET':     
        try:  
            ns = _nosql._nosql()      
            q_uuid = [{"$match":{"$and":[{'u_uuid':u_uuid},{'t_uuid':t_uuid}]}},
                    {"$project" : { "_id" : 0, "m_uuid":1}}
                    ]
                
            resSearch = [ r for r  in ns.aggregate(bucket='pm_tags',query=q_uuid )]
            for msg in resSearch:
                m =  promethiumAPIMessage_message.getHeader(msg['m_uuid'],request.META['HTTP_X_PM_APIKEY'])
                if m['msg'] is not None:
                    res['msg'].append(m['msg'])
                    res['total'] += 1
        except:
            pass
    return JsonResponse(res) 


@csrf_exempt
def uuidFromName(request, u_uuid):
    """
    URL : /api/user/{u_uuid}/tags/{t_uuid}/uuidFromName
            
    Method : POST
            
    Retrieve all information of user tag item
            
    GET URI Parameters:      
        u_uuid (str)  : the user uuid
        t_uuid (str) : the tag uuid 
            
    Return :    
        JSON {'details': ['tags': {...}]}
        HTTP Errno 403 and some contextual errortext (authentication failed...)
            
    """
    if not promethiumAPIAuth_views.controlSessionToken(request):
        i_log = 'No valid session token'
        apiLog.info(i_log)
        return HttpResponseForbidden('No valid session token')
        
    if not _control._control().controlAccessRight(uuid=u_uuid, token = request.META['HTTP_X_PM_APIKEY']):
        i_log = 'No sufficient rights for this action'
        apiLog.info(i_log)
        return HttpResponseForbidden('No sufficient rights for this action')  
    
    if request.method == 'POST':
        res = {'uuid': None} 
        mandatoryTupl = ('displayname',)
        allTupl       = mandatoryTupl 
        body_unicode = request.body.decode('utf-8')        
        data    = json.loads(body_unicode)

        if all(jskey in data.keys() for jskey in mandatoryTupl):
            try:
                q_uuid = [{"$match":{"$and":[{'owner': u_uuid}, {'displayname': data['displayname']}]}},
                        {"$project" : { "_id" : 0, }}
                        ]
                ns = _nosql._nosql()
                resSearch = [ {'tags':r} for r  in ns.aggregate(bucket='pm_usertags',query=q_uuid )]
                if len(resSearch) == 1 :
                    res = {'uuid': resSearch[0]['tags']['uuid']}         
            except:
                pass

        return JsonResponse(res) 


