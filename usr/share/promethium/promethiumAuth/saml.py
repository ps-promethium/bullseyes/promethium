#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

# Python lib
from bson.son import SON
import json
import jwt
import io
from pymemcache.client.hash import HashClient
import os.path 
#from saml2 import BINDING_HTTP_POST, md, BINDING_HTTP_REDIRECT, samlp, xmldsig
#from saml2.mdstore import InMemoryMetaData
#from saml2.sigver import get_xmlsec_binaryimport, requests
import sys, traceback
import yaml

import logging
apiLog = logging.getLogger(__name__)

# Django lib
from django.conf import settings
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.shortcuts import render, redirect
from django.utils import translation, timezone


# promethium lib/class
from promethiumClass.config import config
#from promethiumClass.auth import auth
#from promethiumClass.auth import _bluemind
from promethiumClass.auth import _internal, _cypher
#from promethiumClass.auth import _ldap
from promethiumClass.auth import _saml


from . import views

def login(request, d_uuid):
    sc = _saml.samlClient()
    params = {'domainuid':d_uuid}
    sc.setParams(**params)
    sc.saml_client()
    client = sc.getClient() 
    request_id, info = client.prepare_for_authenticate()
    redirect_url = dict(info["headers"])["Location"]

    return redirect(redirect_url)



def metadata(request, d_uuid):
    sc = _saml.samlClient()
    params = {'domainuid':d_uuid}
    sc.setParams(**params)
    xml = sc.metatdata()
    
    return HttpResponse(xml)


@csrf_exempt
def assertion_consumer_service(request, d_uuid): 
    """
    """
    if hasattr(settings, 'ORGALOGOFILENAME'):
        logoPath='images/logo/' + settings.ORGALOGOFILENAME
    else:
        logoPath='images/promethium_logo_w.png'
        
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'VERSION': config.config().VERSION,
        'logoPath': logoPath,
    }
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}

    # SAML 
    sc = _saml.samlClient()
    params = {'domainuid':d_uuid}
    sc.setParams(**params)
    sc.saml_client()
    client = sc.getClient() 
       
    r = controlSamlResponse(request.POST["SAMLResponse"])
      
    if 'token' in r.keys():
        request.session.update(r)
        request.session['domainName'] = getDomainName(request, request.session['user']['domain']) 
            
        logo = getDomainLogo(request, request.session['user']['domain']) 

        request.session['logo']       = logo
            
        context = {
            'login': request.session['user']['login'],
            'PAGE_TITLE': 'Authentication',
            'WEBSITE_TITLE': 'Promethium',
            'role': request.session['user']['role'],
            'domainName' : request.session['domainName'],
            'logo': request.session['logo'],
            'lang': request.session['user']['lang'],
            }
            
        request.session['user_time_zone'] = settings.TIME_ZONE
            
        if 'timez' in  request.session['user'].keys():
            request.session['user_time_zone']                 = request.session['user']['timez']

        request.session['serverErrorWarning']                   = ""
            
        timezone.activate(request.session['user_time_zone'])
            
        i_log = 'user ' + request.session['user']['login'] + ' successfully logged in'
        frontOfficeLog.info(i_log)
            
        return redirect('/home')
        
    else:
        i_log = 'SAML Authentication failed for : ' + r['login'] 
        frontOfficeLog.info(i_log)
            
        return render(request, r['idp_url'], context)


    



