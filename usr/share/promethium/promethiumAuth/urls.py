#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url

from django.urls import include, path


from . import views
from . import saml

 
urlpatterns = [
    #path('', views.index, name='index'),
    url(r'^$', views.index, name='index'),
    url(r'saml2/(?P<d_uuid>[a-zA-Z0-9\-]+)/login$', saml.login, name="login"),
    url(r'saml2/(?P<d_uuid>[a-zA-Z0-9\-]+)/metadata$', saml.metadata, name="metadata"),
    url(r'saml2/(?P<d_uuid>[a-zA-Z0-9\-]+)/acs', saml.assertion_consumer_service, name="acs"),
    url(r'^authenticate/', views.index,name='authenticate'),
    url(r'^logout/', views.logout,name='logoutByUser'),
    url(r'^\?next=', views.logout,name='logoutOnError'),
]
