#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
LANGUAGE_SESSION_KEY = '_language'
 
# Python lib
import base64
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone
from django.contrib.auth import logout as site_logout


# promethium lib/class
import promethiumAPIAuth.views as promethiumAPIAuth_views
from promethiumClass.config import config

def index(request): 
    """
    """
    if hasattr(settings, 'ORGALOGOFILENAME'):
        logoPath='images/logo/' + settings.ORGALOGOFILENAME
    else:
        logoPath='images/promethium_logo_w.png'
        
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'VERSION': config.config().VERSION,
        'logoPath': logoPath,
    }
    
    if request.method == 'POST':
        
        Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}

        base    = settings.INTERNALURI
        postUri = 'auth/login'
        
        Data = {'login': request.POST.get('login'), 'password': request.POST.get('password')}
        Data = json.dumps(Data)
        r = {'Response': None }
        try:
            r = requests.post( base + postUri, headers=Headers, data = Data ).json()
        except:
            pass
        
        if 'token' in r.keys():
            request.session.update(r)
            request.session['domainName'] = getDomainName(request, request.session['user']['domain']) 
            
            logo = getDomainLogo(request, request.session['user']['domain']) 

            request.session['logo']       = logo
            
            context = {
                'login': request.POST.get('login'),
                'PAGE_TITLE': 'Authentication',
                'WEBSITE_TITLE': 'Promethium',
                'role': request.session['user']['role'],
                'domainName' : request.session['domainName'],
                'logo': request.session['logo'],
                'lang': request.session['user']['lang'],
                }
            
            request.session['user_time_zone'] = settings.TIME_ZONE
            
            if 'timez' in  request.session['user'].keys():
                request.session['user_time_zone']                 = request.session['user']['timez']
            
            request.session['serverErrorWarning']                   = ""
            
            timezone.activate(request.session['user_time_zone'])
            
            i_log = 'user ' + request.POST.get('login') + ' successfully logged in'
            frontOfficeLog.info(i_log)
            
            return redirect('/home')
        
        else:
            
            i_log = 'user ' + request.POST.get('login') + ' bad login/password'
            frontOfficeLog.info(i_log)
            
            return render(request, 'login.htm', context)
        
    else:
        i_log = 'no login/password'
        frontOfficeLog.info(i_log)
            
        return render(request, 'login.htm', context)



def getDomainName(request, uuid):
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/domain/" + uuid + "/details"
    rd = requests.get( URI, headers=Headers).json()
    return rd['details'][0]['pm_domain']['displayname']
    


def getDomainLogo(request, uuid):
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/domain/" + uuid + "/details"
    rd = requests.get( URI, headers=Headers).json()

    if 'logo' in rd['details'][0]['pm_domain'].keys():
        return rd['details'][0]['pm_domain']['logo']
    else:
        return None
    


def authPingPM(request):
    """
        (void) authPingPM :
            send a ping to PM to maintain session alive.    
    """  
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/auth/ping"

    try:
        requests.post(URI, headers=Headers)
    except:
        pass



def logout(request):
    site_logout(request)
    return redirect('/') 
