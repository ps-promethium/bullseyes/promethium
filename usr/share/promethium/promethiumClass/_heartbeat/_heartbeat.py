#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import os
from pymemcache.client import base
from pymemcache.client.hash import HashClient
import socket
import sys
import time
import yaml


# Promethium inner class
sys.path.append(os.path.abspath("/usr/share/promethium"))
from promethiumClass.config import config
from promethiumClass.various import _uuid

from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()

class _heartbeat():
    
    def __init__(self):
        self.params             = defaultdict(dict)  
        self.params['ttl']      = 5 # Send heartbeat every n sec 
        self.params['entryTtl'] = 6 # n sec lifetime memcache entry  
        self.params['nodeName'] = socket.gethostname()
        
        
    def getMemcacheNodes(self):
        """
        (obj) getMemcacheNodes:
            connect to memcache nodes defined in /etc/promethium/promethium.yml
            return HashClient obj 
        """
        nodes = [ ]
        for server in config.config().memcache['server']:
            srv, port = server.split(':')
            nodes.append((srv, int(port)))
        
        self.client = HashClient(nodes)


    def sendHeartbeat(self):
        """
        (bool) sendHeartbeat:
            send a entry to memcached nodes to say to the world this node is alive
            Return True/False
        """        
        try:
            self.client.set(self.params['nodeName'], _uuid._uuid().new(), self.params['entryTtl'])
            return True
        except Exception as e: 
            backoffice.fatal(e, exc_info=True)
            return False
              
    
    def waitSeconds(self):
        """
        (void) waitSeconds:
            set a time sleep of n seconds (default 5)
        """  
        time.sleep(self.params['ttl'])
    
    
    
    def getHeartbeat(self, nodeName):
        """
        (bool) getHeartbeat:
            return True if nodeName is alive otherwise False
            Args: 
                nodeName : the hostname (short) of the node
        """       
        if self.client.get(nodeName):
            return True
        else:
            return False

    

    def getMaster(self):
        """
        (string) getMaster:
            return the master node hostname (short) otherwise None
        """   
        res = None
        for server in config.config().memcache['server']:
            nodeName = server.split(':')[0]
            this = nodeName.split(".")[0]
            if self.getHeartbeat(this):
                res = this
                break 
        return res


    def getAliveNodes(self):
        """
        (list) getAliveNodes:
           return the  hostname nodes (short) if 
        """   
        res = []
        for server in config.config().memcache['server']:
            nodeName = server.split(':')[0]
            this = nodeName.split(".")[0]
            if self.getHeartbeat(this):
                res.append(this)
        return res
