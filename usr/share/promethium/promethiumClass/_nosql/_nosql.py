#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*



# Native Python Lib/Class 
from collections import defaultdict
import json
import pymongo # import MongoClient,pymongo

import yaml


# Promethium inner class
from promethiumClass.config import config
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()


class _nosql():

    def __init__(self):
        self.params            = defaultdict(dict)
        yamlFile          = "/etc/promethium/promethium.yml"
        
        with open(yamlFile, 'r') as stream:
            try :
                CB = yaml.load(stream, Loader=yaml.FullLoader)             
                URI='mongodb://' + ",".join(CB['nosql']['server'])
                self.cluster = pymongo.MongoClient(URI, 
                                           port=int(CB['nosql']['port']), 
                                           username=str(CB['nosql']['user']), 
                                           password=str(CB['nosql']['password']),
                                           authSource='promethium',
                                           authMechanism='SCRAM-SHA-256')  
                
                self.client = self.cluster['promethium']

            except exceptions as e:
                backoffice.fatal(e, exc_info=True)
                i_log = 'init : Reading YAML File failed'
                backoffice.info(i_log)
                pass      
        stream.close()


    
    def __del__(self):
        pass
    
    
    
    def upsert(self, **kwargs):
        """ 
        URL : None
        
        Try to upsert data in NoSQL bucket
        
        Parameters:
            bucket (str)  : the bucket name
            upsert (dict) : dict containing all data for this entry

        Returns:
            True or None if successfull or not 
        """       
        
        res = None 
        if 'bucket' in kwargs.keys():
            try:
                bucket = self.client[kwargs['bucket']]
                del kwargs['bucket']
                bucket.update_one({'uuid': kwargs['upsert']['uuid']}, {"$set": kwargs['upsert']}, True)
                res = True
            
            except Exception as e:
                i_log = 'upsert : failed to upsert data'
                backoffice.info(i_log)
                pass
            
        return res
 
 

    def search(self, **kwargs):
        """ 
        URL : None
        
        Try to query data from NoSQL
        
        Parameters:
            bucket (str) : the bucket name
            query (dict) : dict containing the query elements

        Returns:
            result of query or None if successfull or not 
        """       
        
        res = None 
        if 'query' in kwargs.keys():
            try:
                bucket = self.client[kwargs['bucket']]
                del kwargs['bucket']   
                res = bucket.find(kwargs['query'],{'_id':0})
            except pymongo.errors.PyMongoError as mge :
                i_log = 'search : error on ' + str(mge)
                backoffice.info(i_log)
                pass
            except Exception as e:
                i_log = 'search : failed to query data, query : ' + str(kwargs['query'])
                backoffice.info(i_log)
                pass
        
        return res
    
    
    
    def aggregate(self, **kwargs):
        """ 
        URL : None
        
        Try to query data from NoSQL
        
        Parameters:
            bucket (str) : the bucket name
            query (dict) : dict containing the query elements 

        Returns:
            result of query or None if successfull or not 
        """       
        
        res = None 
        if 'query' in kwargs.keys():
            try:
                bucket = self.client[kwargs['bucket']]
                del kwargs['bucket']  
                res = bucket.aggregate(kwargs['query']) 
                
            except pymongo.errors.OperationFailure as mge :
                i_log = 'aggregate : error on ' + str(mge)
                backoffice.info(i_log)
                pass
            except Exception as e:
                i_log = 'aggregate : failed to query data, query : ' + str(kwargs['query'])
                backoffice.info(i_log)
                pass
        
        return res
    
    
    
    def read(self, **kwargs):
        """ 
        URL : None
        
        Try to read entry data from NoSQL
        
        Parameters:
            bucket (str) : the bucket name where to operate 
            uuid (str)   : the uuid of the entry 

        Returns:
            result of read or None if successfull or not 
        """ 
        res = None 
        if 'bucket' in kwargs.keys():
            try:
                bucket = self.client[kwargs['bucket']]
                del kwargs['bucket']               
                res = bucket.find_one({'uuid':kwargs['uuid']})  
            except NotFoundError as e:
                i_log = 'read : item not found'
                backoffice.info(i_log)
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = 'read : failed to read data'
                backoffice.info(i_log)
                pass
            
        return res



    def remove(self, **kwargs):
        """ 
        URL : None
        
        Try to delete/remove an entry from NoSQL
        
        Parameters:
            bucket (str) : the bucket name where to operate 
            uuid (str)   : the uuid of the entry 

        Returns:
            True or None if successfull or not 
        """ 
        res = None 
        if 'bucket' in kwargs.keys():
            try:
                bucket = self.client[kwargs['bucket']]
                del kwargs['bucket']  
                bucket.delete_one({'uuid':kwargs['uuid']})   
                res = True
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = 'remove : failed to remove uuid ' + kwargs['uuid'] + ' from ' + kwargs['bucket']
                backoffice.info(i_log)
                pass
            
        return res



    def remove_many(self, **kwargs):
        """ 
        URL : None
        
        Try to delete/remove an entry from NoSQL
        
        Parameters:
            bucket (str) : the bucket name where to operate 
            uuid (str)   : the uuid of the entry 

        Returns:
            True or None if successfull or not 
        """ 
        res = None 
        if 'query' in kwargs.keys():
            try:
                bucket = self.client[kwargs['bucket']]
                del kwargs['bucket']  
                bucket.delete_many(kwargs['query'])   
                res = True
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = 'remove : failed to remove  from ' + kwargs['bucket']
                backoffice.info(i_log)
                pass
            
        return res



    def createIndex(self, **kwargs):
        """ 
        URL : None
        
        Try to create index on collection/bucket 
        
        Parameters:
            bucket (str) : the bucket name where to operate 
            idx (dict) : dict containing the query elements

        Returns:
            True or None if successfull or not 
        """ 
        res = None 
        if 'idx' in kwargs.keys():
            try:
                bucket = self.client[kwargs['bucket']]
                del kwargs['bucket']  
                res = bucket.create_index(kwargs['idx']) 
                
            except pymongo.errors.OperationFailure as mge :
                i_log = 'createIndex : error on ' + str(mge)
                backoffice.info(i_log)
                pass
            except Exception as e:
                i_log = 'createIndex : failed to create index : ' + str(kwargs['idx'])
                backoffice.info(i_log)
                pass
        
        return res



    def stat(self, **kwargs):
        """ 
        URL : None
        
        Try to query data from NoSQL
        
        Parameters:
            bucket (str) : the bucket name
            query (dict) : dict containing the query elements 

        Returns:
            result of query or None if successfull or not 
        """       
        
        res = None 
        try:
            res =  self.client.command("dbstats")
        except pymongo.errors.OperationFailure as mge :
            i_log = 'aggregate : error on ' + str(mge)
            backoffice.info(i_log)
            pass
        except Exception as e:
            i_log = 'aggregate : failed to query data, query : ' + str(kwargs['query'])
            backoffice.info(i_log)
            pass
        return res
    
