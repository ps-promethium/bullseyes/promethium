#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import json


# Promethium inner class
from promethiumClass import config
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()



class _bluemind():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params = defaultdict(dict)
        self.params['verifyPeer'] = False




    def setParam(self, **kwargs):
        """
        setParam:
        """      
        if "host" in kwargs.keys():
            self.params['host'] = kwargs['host']

        if "admin" in kwargs.keys():
            self.params['admin'] = kwargs['admin']        
 
        if "password" in kwargs.keys():
            self.params['password'] = kwargs['password']           
        
        if "verifyPeer" in kwargs.keys():
            self.params['verifyPeer'] = kwargs['verifyPeer']       
           
            

    def connectSrv(self):
        pass



    def login(self):   
        pass



    def test(self):
        pass
