#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import json
import jwt
#from pymemcache.client import base
from pymemcache.client.hash import HashClient
import yaml

# Promethium inner class
from promethiumClass.config import config
from promethiumClass._nosql import _nosql
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()


class _control():
    
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params            = defaultdict(dict)
        self.yamlFile          = '/etc/promethium/promethium.yml'
        self.params = defaultdict(dict)
        self.params['verifyPeer'] = False
        self.key = ""
        
        with open(self.yamlFile, 'r') as stream:
            try :
                CB = yaml.load(stream, Loader=yaml.FullLoader)            
                self.key = CB['nosqlCypherKey']       
                
            except Exception as e: 
                backoffice.fatal(e, exc_info=True)
                i_log = 'init : Reading YAML File failed'
                backoffice.info(i_log)
                pass  
        stream.close()
  

    def retrieveUserDomainUuid(self,uuid):
        domain = None
        
        q_uuid = {'uuid': uuid}
        ns = _nosql._nosql()
        res_q_uuid = [ r for r in ns.search(bucket='pm_user',query=q_uuid)]
        #print(res_q_uuid)
        if len(res_q_uuid) == 1:
            domain = res_q_uuid[0]['domain']

        return domain



    def retrieveUserElementFromToken(self, token):
        """
        URL : None
        
        Retrieve uuid from memcached while token
        Then get this uuid role & domain 
        
        Params:
            token (str) : user session token 
            
        Return:  
            res (dict) containing {'uuid': <uuid>, 'role': <role>, 'domain' : <domain uuid>}
            or None  in other cases
        """
        res = None
        nodes = [ ]
        for server in config.config().memcache['server']:
            srv, port = server.split(':')
            nodes.append((srv, int(port)))
        
        client = HashClient(nodes)
        memcExpirTime =  config.config().memcache['expire']
        if client.get(token):
            
            uuid = client.get(token).decode("utf-8")
            q_uuid = {'uuid': uuid}
            ns = _nosql._nosql()
            res_q_uuid = [ r for r in ns.search(bucket='pm_user',query=q_uuid)]

            if len(res_q_uuid) == 1:
                jsDict = {}
                jsDict['uuid']           = res_q_uuid[0]['uuid']
                jsDict['role']           = res_q_uuid[0]['role']
                jsDict['domain']         = res_q_uuid[0]['domain']
                jsDict['subscribedguid'] = res_q_uuid[0]['subscribedguid']
                res = jsDict

        return res



    def controlAccessRight(self, **kwargs):
        """
        URL : None
        
        Test if caller has sufficient rights to request
                
        Params:
            token (str)    : session token 
            uuid (str)     : user uuid requested
        
        Return:  
            True or False if sufficient rights
        
        
        """
        res = False
        token    = kwargs['token']
        uuid     = kwargs['uuid']

        
        # What king of user is logged 
        callerTokElmt = self.retrieveUserElementFromToken(token)
        
        # Which domain this uuid is from 
        domainUuid = self.retrieveUserDomainUuid(uuid)
        
        # Admin (aka admin0@global.virt) or user itself request
        if (callerTokElmt['uuid'] == uuid)  or ('admin' in callerTokElmt['role']):
            res = True
        else:
            # must be the user domain administrator
            if callerTokElmt['role'] in ('domain'):
                if domainUuid == callerTokElmt['domain']:
                    res = True
        return res



    def controlAdminAccessRight(self, **kwargs):
        """
        URL : None
        
        Test if caller is an admin (e.g. admin0@global.virt)
                
        Params:
            token (str)    : session token 
        
        Return:  
            True or False if sufficient rights
        
        
        """
        res = False
        
        token    = kwargs['token']
        
        # What king of user is logged 
        callerTokElmt = self.retrieveUserElementFromToken(token)
        uuid = callerTokElmt['uuid']
        
        # Which domain this uuid is from 
        domainUuid = self.retrieveUserDomainUuid(uuid)
        
        
        # Admin (aka admin0@global.virt) request
        if 'admin' in callerTokElmt['role']:
            res = True
                
        return res
    
    
    
    def controlDispatcherRight(self, **kwargs):
        """
        URL : None
        
        Test if caller has domain or dispatcher rights                 
        Params:
            token (str)      : session token 
            domainUuid (str) : the domain uuid
        
        Return:  
            True or False if sufficient rights
        
        
        """
        res = False
        
        token    = kwargs['token']
        
        # What king of user is logged 
        callerTokElmt = self.retrieveUserElementFromToken(token)
        uuid = callerTokElmt['uuid']
        
        # Must be an account of this domain
        if kwargs['domainUuid'] == self.retrieveUserDomainUuid(uuid):
            # Must have right to delete message
            if callerTokElmt['role'] in ('domain', 'dispatcher'):
                res = True
                
        return res
    
    
    
    def controlDelegatedRight(self, **kwargs):
        """
        URL : None
        
        Test if caller has delegated rights                 
        Params:
            token (str)      : session token 
            domainUuid (str) : the domain uuid
        
        Return:  
            True or False if sufficient rights
        
        
        """
        res = False
        
        token    = kwargs['token']
        
        # What king of user is logged 
        callerTokElmt = self.retrieveUserElementFromToken(token)
        uuid = callerTokElmt['uuid']
        
        # Must be an account of this domain
        if kwargs['domainUuid'] == self.retrieveUserDomainUuid(uuid):
            # Must have right to delete message
            if callerTokElmt['role'] in ('domain', 'dispatcher', 'delegated'):
                res = True
                
        return res    
    
    
    
    def controlDomainAccessRight(self, **kwargs):
        """
        URL : None
        
        Test if caller has sufficient administrative rights for this domain (e.g. admin0@global.virt)
                
        Params:
            domainUuid (str) : domain uuid
            token (str)      : session token 
            settings (str)   : memcached server specs as <IP>:<PORT>
        
        Return:  
            True or False if sufficient rights
        
        
        """
        res = False
        
        token    = kwargs['token']
        
        # What king of user is logged 
        callerTokElmt = self.retrieveUserElementFromToken(token)
        uuid = callerTokElmt['uuid']
        
        if 'admin' in callerTokElmt['role']:
            res = True
                
                
        # If domain this uuid is from 
        if kwargs['domainUuid'] == self.retrieveUserDomainUuid(uuid):
            # Admin (aka admin0@global.virt) or domain admin request
            if callerTokElmt['role'] in  ('admin', 'domain'):
                res = True
                
        return res
