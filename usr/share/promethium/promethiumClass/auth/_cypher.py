#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import hashlib
import json
import jwt
from pymemcache.client import base
import uuid
import yaml

# Promethium inner class
from promethiumClass.config import config
from promethiumClass._nosql import _nosql
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()

from promethiumClass.various import _uuid

class _cypher():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params            = defaultdict(dict)
        self.yamlFile          = '/etc/promethium/promethium.yml'
        with open(self.yamlFile, 'r') as stream:
            try :
                CPH = yaml.load(stream, Loader=yaml.FullLoader)            
                self.key = CPH['nosqlCypherKey']       
                
            except Exception as e: 
                backoffice.fatal(e, exc_info=True)
                i_log = 'init : Reading YAML File failed'
                backoffice.info(i_log)
                pass  
        stream.close()
        
        
    def encypherIt(self,passwd):
        """ 
        URL : None
            
        Cypher some string. Attention, it's reversible (use  decypherIt() ), use otherwise hashPassword()
            
        Parameters:
            passwd (str)  : some password 

        Returns:
            string  
        """    
        try :      
            cyphered = jwt.encode({'passwd': passwd}, self.key, algorithm='HS256') #.decode("utf-8") 
        except Exception as e: 
            cyphered = None
            backoffice.fatal(e, exc_info=True)
            i_log = 'cypher password failed'
            backoffice.info(i_log)
            pass  
    
        return cyphered



    def decypherIt(self,passwd):
        """ 
        URL : None
            
        Uncypher password
            
        Parameters:
            cyphered passwd (str)  : some cyphered password 

        Returns:
            string  
        """  
        try :     
            cleared = jwt.decode(passwd, self.key, algorithms=["HS256"])
        except Exception as e: 
            cleared = None
            backoffice.fatal(e, exc_info=True)
            i_log = 'decypher password failed'
            backoffice.info(i_log)
            pass  
    
        return cleared 
    

 
    def hashPassword(self,string):
        # uuid is used to generate a random number
        salt = uuid.uuid4().hex
        return hashlib.sha256(salt.encode() + string.encode()).hexdigest() + ':' + salt
    
    
    
    def checkPassword(self,hashedPassword, clearPassword):
        password, salt = hashedPassword.split(':')
        return password == hashlib.sha256(salt.encode() + clearPassword.encode()).hexdigest()
