#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import json
import jwt
import yaml

# Promethium inner class
from promethiumClass.config import config
from promethiumClass._nosql import _nosql
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()
from . import _cypher


class _internal():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params            = defaultdict(dict)
        self.yamlFile          = '/etc/promethium/promethium.yml'
        self.params = defaultdict(dict)
        self.params['verifyPeer'] = False
        self.key = ""
        
        with open(self.yamlFile, 'r') as stream:
            try :
                CB = yaml.load(stream, Loader=yaml.FullLoader)            
                self.key = CB['nosqlCypherKey']       
                
            except Exception as e: 
                backoffice.fatal(e, exc_info=True)
                i_log = 'init : Reading YAML File failed'
                backoffice.info(i_log)
                pass  


    def setParam(self, **kwargs):
        """
        setParam:
        """      
        if "host" in kwargs.keys():
            self.params['host'] = kwargs['host']

        if "admin" in kwargs.keys():
            self.params['admin'] = kwargs['admin']        
 
        if "password" in kwargs.keys():
            self.params['password'] = kwargs['password']           
        
        if "verifyPeer" in kwargs.keys():
            self.params['verifyPeer'] = kwargs['verifyPeer']       
           
            

    def connectSrv(self):
        pass



    def login(self):   
        pass


    def upsert(self, **kwargs):
        jsDict = kwargs['jsDict']
        uuid   = kwargs['uuid']
        
        ns = _nosql._nosql()
        ns.upsert(bucket='pm_user', uuid=uuid, upsert=jsDict)



    def authenticateOn(self, login, passwd):
        res = False

        q_uuid = {'login': login}
        ns = _nosql._nosql()
        res_q_uuid = [ r for r in ns.search(bucket='pm_user',query=q_uuid)]
        
        if len(res_q_uuid) == 1:
            savedPasswd = res_q_uuid[0]['passwd']
            #cyphered = jwt.encode({'login': login, 'passwd': passwd}, self.key, algorithm='HS256').decode("utf-8") 
            
            if _cypher._cypher().checkPassword(savedPasswd, passwd):
                res = True
                i_log = 'Login : ' + login + ' successfull'
                backoffice.info(i_log)
            else:
                i_log = 'Login : ' + login + ' failed'
                backoffice.info(i_log)

        return res
