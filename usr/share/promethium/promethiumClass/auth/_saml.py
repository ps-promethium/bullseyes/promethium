#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

from dataclasses import dataclass

from saml2 import BINDING_HTTP_POST, md, BINDING_HTTP_REDIRECT, samlp, xmldsig
from saml2.client import Saml2Client
from saml2.config import Config
from saml2.mdstore import InMemoryMetaData
from saml2 import sigver 


# Promethium inner class
from promethiumClass.config import config
from promethiumClass._nosql import _nosql
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()

@dataclass
class IdPConfig:
    entity_id: str
    single_sign_on_url: str
    x509_cert: str

    def __hash__(self):
        return hash(self.entity_id)



class MetaDataIdP(InMemoryMetaData):
    def __init__(self, attrc, metadata: IdPConfig):
        super(MetaDataIdP, self).__init__(attrc, metadata)
        self.metadata = metadata

    def load(self, *args, **kwargs):
        idpsso_descriptor = md.IDPSSODescriptor()
        idpsso_descriptor.protocol_support_enumeration = samlp.NAMESPACE
        idpsso_descriptor.single_sign_on_service = [
            md.SingleSignOnService(
                binding=BINDING_HTTP_REDIRECT, location=self.metadata.single_sign_on_url
            )
        ]
        idpsso_descriptor.key_descriptor = [
            md.KeyDescriptor(
                use="signing",
                key_info=xmldsig.KeyInfo(
                    x509_data=[
                        xmldsig.X509Data(
                            x509_certificate=xmldsig.X509Certificate(
                                text=self.metadata.x509_cert
                            )
                        )
                    ]
                ),
            )
        ]

        entity_descriptor = md.EntityDescriptor()
        entity_descriptor.entity_id = self.metadata.entity_id
        entity_descriptor.idpsso_descriptor = [idpsso_descriptor]

        self.do_entity_descriptor(entity_descriptor)



class samlClient:
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        if sigver.get_xmlsec_binary:
            self.xmlsec_path = sigver.get_xmlsec_binary(["/opt/local/bin", "/usr/local/bin"])
        else:
            self.xmlsec_path = "/usr/bin/xmlsec1"
                                    
        self.sp_proto      = ""
        self.sp_fqdn       = ""  
        self.sp_x509       = ""
        self.idp_entity_id = ""           
        self.idp_url       = ""      
        self.idp_x509      = ""   
        self.domainuid     = ""


    def setParams(self,**kwargs):
        """
        setParam:
        
        """      

        if 'domainuid' in kwargs.keys():
            d_uuid = kwargs['domainuid']

            q_uuid = [                 
                    {"$lookup": {'from': "pm_host",'localField': "authserver",'foreignField': "uuid",'as': "authsrv"}},
                    {"$unwind" : "$authsrv"},
                    {"$match":{"$and": [{"uuid" : d_uuid}]}}
                    ]

            
            try :
                ns = _nosql._nosql()
                res_q_uuid = [ r for r in ns.aggregate(bucket='pm_domain',query=q_uuid)]
            except:
                backoffice.critical('NO DATA : ' + traceback.format_exc())
                
            if res_q_uuid is None:
                return HttpResponseForbidden('Error occured with NoSQL Servers')
            elif len(res_q_uuid) == 0:
                return HttpResponseForbidden('No domain found')
            elif len(res_q_uuid) > 1 :
                backoffice.warning('Domain : ' + str(len(res_q_uuid)) + ' entries found with that uuid ' + d_uuid + ', what the hell !!')
                return HttpResponseForbidden('More than 1 account found !!')
            else:
                pass    
            
            
            if len(res_q_uuid) == 1:
                
                authSrvSpecs =  res_q_uuid[0]['authsrv']
                
                if "sp_proto" in authSrvSpecs.keys():
                    self.sp_proto = authSrvSpecs['sp_proto']

                if "sp_fqdn" in authSrvSpecs.keys():
                    self.sp_fqdn = authSrvSpecs['sp_fqdn']        
                    
                
                if "sp_x509" in authSrvSpecs.keys():
                    self.sp_x509 = authSrvSpecs['sp_x509'] 
        
                if "idp_entity_id" in authSrvSpecs.keys():
                    self.idp_entity_id = authSrvSpecs['idp_entity_id']           
                
                if "idp_url" in authSrvSpecs.keys():
                    self.idp_url = authSrvSpecs['idp_url']       
                        
                if "idp_x509" in authSrvSpecs.keys():
                    self.idp_x509 = authSrvSpecs['idp_x509']     

                if "domainuid" in authSrvSpecs.keys():
                    self.domainuid = authSrvSpecs['domainuid']
        

    def saml_client(self):
        saml_settings = {
            # Currently xmlsec1 binaries are used for all the signing and encryption stuff.This option defines where the binary is situated.
            "xmlsec_binary": self.xmlsec_path,
            # The SP ID. It is recommended that the entityid should point to a real webpage where the metadata for the entity can be found.
            "entityid": self.sp_proto + "://" + self.sp_fqdn + "/saml2",
            # Indicates that attributes that are not recognized (they are not configured in attribute-mapping), will not be discarded.
            "allow_unknown_attributes": True,
            "service": {
                "sp": {
                    "endpoints": {
                        "assertion_consumer_service": [
                            ##as mentioned in the sequence diagram we can use either redirect or post here.
                            (self.sp_proto + "://" + self.sp_fqdn + "/saml2/" + self.domainuid +"/acs", BINDING_HTTP_POST),
                        ]
                    },
                    # Don't verify that the incoming requests originate from us via the built-in cache for authn request ids in pysaml2
                    "allow_unsolicited": True,
                    # Don't sign authn requests, since signed requests only make sense in a situation where you control both the SP and IdP
                    "authn_requests_signed": False,
                    # Assertion must be signed
                    "want_assertions_signed": True,
                    # Response signing is optional.
                    "want_response_signed": False,
                }
            },
            "metadata": [
                {
                    "class": "MetaDataIdP",
                    "metadata": [
                        (
                            IdPConfig(
                                entity_id=self.idp_entity_id,
                                single_sign_on_url=self.idp_url,
                                x509_cert=self.idp_x509
                            ),
                        )
                    ],
                }
            ],
        }

        config = Config()
        config.load(saml_settings)
        self.client =  Saml2Client(config=config)
        
    
    
    def getClient(self):
        return self.client



    def getIdPURL(self):
        return self.idp_url



    def controlSamlResponse(self, **kwargs):       
        authn_response = self.client.parse_authn_request_response(
            request.POST["SAMLResponse"], BINDING_HTTP_POST
        )
        session_info = authn_response.session_info()
        login = str(session_info["login"])
        
        js = js = {'state': 'failed', 'login': login, 'idp_url': self.idp_url}
        
        if login:
            # 1) Retrieve from NoSQL account params
            #q_uuid = 'select usr, authsrv from `pm_user` usr join `pm_domain` dom on keys usr.domain join `pm_host` authsrv on keys dom.authserver where usr.login="' + login + '"'
            q_uuid = [
                        {"$lookup": {'from': "pm_domain",'localField': "domain",'foreignField': "uuid",'as': "doms"}},
                        {"$unwind" :"$doms"},
                        {"$lookup": {"from": "pm_host", 'localField': "doms.authserver",'foreignField': "uuid",'as': "authsrv"}},
                        {"$unwind" : "$authsrv"},
                        {"$match":{"$and": [{"login" : login}]}}
                    ]

            try :
                import promethiumAPIAuth.views as promethiumAPIAuth_views
                ns = _nosql._nosql()
                res_q_uuid = [ r for r in ns.aggregate(bucket='pm_user',query=q_uuid)]
        
                jsDict = res_q_uuid[0]

                del jsDict['_id']
                del jsDict['doms']
                del jsDict['authsrv']
                token = promethiumAPIAuth_views.newToken()  
                promethiumAPIAuth_views.setTokenToMemcache(token, jsDict['uuid'])
                js = {'state': 'authenticated', 'token': token, 'user': jsDict}
                
            except:
                backoffice.critical('NO DATA : ' + traceback.format_exc())
        
        return js
    
    
    
    
    def metatdata(self):
        
        self.metadata = '''
<?xml version="1.0" encoding="utf-8"?>

<EntityDescriptor xmlns="urn:oasis:names:tc:SAML:2.0:metadata" entityID="'''+ self.sp_proto + '''://''' + self.sp_fqdn + '''">

<SPSSODescriptor AuthnRequestsSigned="false" WantAssertionsSigned="true" protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">
<NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</NameIDFormat>

<AssertionConsumerService
    Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
    Location="'''+ self.sp_proto + '''://''' + self.sp_fqdn + '''/saml2/''' + self.domainuid +'''/acs"
    index="1" isDefault="true"/>

<KeyDescriptor use="signing">
<KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
<X509Data>
<X509Certificate>
''' + self.sp_x509 + '''
</X509Certificate>
</X509Data>
</KeyInfo>

</KeyDescriptor>
<KeyDescriptor use="encryption">
<KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
<X509Data>
<X509Certificate>
''' + self.sp_x509 + '''
</X509Certificate>
</X509Data>
</KeyInfo>
</KeyDescriptor>
</SPSSODescriptor>

<Organization>
<OrganizationName xmlns="urn:oasis:names:tc:SAML:2.0:metadata"
   xml:lang="en">Promethium</OrganizationName>
<OrganizationDisplayName xmlns="urn:oasis:names:tc:SAML:2.0:metadata" xml:lang="en">Promethium</OrganizationDisplayName>
<OrganizationURL xmlns="urn:oasis:names:tc:SAML:2.0:metadata"
   xml:lang="en">http://nqicorp.local</OrganizationURL>
</Organization>

<ContactPerson contactType="technical">
<GivenName>Albert</GivenName>
<SurName>EINSTEIN</SurName>
<EmailAddress>albert.einstein@nowhere.atom</EmailAddress>
</ContactPerson>

</EntityDescriptor>      
'''
        
        return self.metadata
