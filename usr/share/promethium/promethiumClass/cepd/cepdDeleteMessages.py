#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
from datetime import *
import json
#from pymemcache.client import base
import sys
import time
import yaml

# Promethium inner class
from promethiumClass.config import config
from promethiumClass.auth import _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()



class cepdDeleteMessages():
    
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params       = defaultdict(dict)    
        self.messagesList = []



    def retrieveDomainAttributes(self,uuid):
        """
        """
        
        dfs_log = 'cepdDeleteMessages : Start job on domain ref ' + uuid
        backoffice.info(dfs_log)

        qmb = [
                {"$lookup": {"from": "pm_host", "localField": "storeserver", "foreignField": "uuid", "as": "storesrv"}},
                {"$project": {"_id":0,"logo": 0, "storesrv._id":0}},
                {"$match":{"$and":[{"uuid" : uuid}]}},
              ]     
        
        ns = _nosql._nosql()
        res_qmb = [ {'domain':r} for r in ns.aggregate(bucket='pm_domain',query=qmb)]
        
        self.params['domain'] = uuid
        self.params['storePath'] = config.config().defaultStorePath
        self.params['cepdTreshold'] = config.config().cepdTreshold
        self.params['store']     = res_qmb[0]['domain']['storesrv'][0]['mechanism']
                    
        if 'cepdTreshold' in res_qmb[0]['domain'].keys():
            self.params['cepdTreshold'] = res_qmb[0]['domain']['cepdTreshold']
            
        if 'storePoint' in res_qmb[0]['domain']['storesrv'][0].keys():
            self.params['storePath']     = res_qmb[0]['domain']['storesrv'][0]['storePoint']

        if 'encrypt' in res_qmb[0]['domain']['storesrv'][0].keys():
            self.params['encrypt'] = res_qmb[0]['domain']['storesrv'][0]['encrypt']
        
        self.params['storeUser']     = ''
        if 'usr' in res_qmb[0]['domain']['storesrv'][0].keys():
            self.params['storeUser'] = res_qmb[0]['domain']['storesrv'][0]['usr']
            
        self.params['storePassword'] = ''    
        if 'passwd' in res_qmb[0]['domain']['storesrv'][0].keys():
            self.params['storePassword'] = res_qmb[0]['domain']['storesrv'][0]['passwd']

    
    
    def getParams(self):
        print(self.params)
 
 
 
    def deleteAttachment(self,m_uuid): 
        q_uuid = {'m_uuid':m_uuid}
        ns = _nosql._nosql()
        resSearch = [ {'message':r} for r  in ns.search(bucket='pm_message',query=q_uuid )]

        #storeType = getattr(sys.modules[__name__], self.store) # _localdrive|_swift|_ceph
        if 'Internal' in self.params['store']:
            from promethiumClass.store import _localdrive
            store = _localdrive._localdrive()

        if 'S3' in self.params['store']:
            from promethiumClass.store import S3
            store = _S3._S3()
            
        store.setParam(**self.params)

        if len(resSearch) > 0:
            if len(resSearch[0]['message']['attachment']) > 0:
                attachmentList = resSearch[0]['message']['attachment'].split(',')
                for _id in attachmentList:
                    
                    #Delete file on System
                    dfs = store.delete(uuid=_id)       
                    if dfs:
                        dfs_log = 'cepdDeleteMessages : delete file ' + _id + ' in ' + storeParams['storePath'] + ' succeed'
                        backoffice.info(dfs_log)
                    else:
                        dfs_log =  'cepdDeleteMessages : delete file ' + _id + ' in ' + storeParams['storePath'] + ' failed'
                        backoffice.info(dfs_log)

                    #Delete file in Couchbase                
                    dfe = ns.remove(bucket='pm_attachment',uuid=_id)
                    if dfe:
                        dfe_log = 'cepdDeleteMessages : delete entry ref ' + _id + ' in pm_attachment succeed'
                        backoffice.info(dfe_log)
                    else:
                        dfe_log =  'cepdDeleteMessages : delete entry ref ' + _id + 'in pm_attachment failed'
                        backoffice.info(dfe_log)



    def deleteInbucket(self,bucket, m_uuid):        
        q_uuid = {'m_uuid':m_uuid}    
        ns = _nosql._nosql()
        resSearch = [ r for r  in ns.search(bucket=bucket,query=q_uuid )]
        if len(resSearch) > 0:
            for entry in resSearch:
                res = ns.remove(bucket=bucket, uuid=entry['uuid'])
                if res:
                    i_log = 'cepdDeleteMessages : delete message ref ' + entry['uuid'] + ' in ' + bucket + ' succeed'
                    backoffice.info(i_log)
                else:
                    i_log =  'cepdDeleteMessages : delete message ref ' + entry['uuid'] + ' in ' + bucket + ' failed'
                    backoffice.info(i_log)

    

    def deleteMessage(self,m_uuid):
        ns   = _nosql._nosql()
        resd = ns.remove(bucket='pm_message',uuid=m_uuid)
        if resd:
            i_log = 'cepdDeleteMessages : delete entry ref ' + m_uuid + ' in pm_message  succeed'
            backoffice.info(i_log)
        else:
            i_log =  'cepdDeleteMessages : delete entry ref ' + m_uuid + ' in pm_message failed'
            backoffice.info(i_log)

        
        
    def delete(self,m_uuid): 
        i_log = 'cepdDeleteMessages : wants to delete message ref ' + m_uuid + ' in collections'
        backoffice.info(i_log)

        for bucket in ('pm_event', 'pm_comment', 'pm_alert', 'pm_tika','pm_favorites', 'pm_tags'):
            self.deleteInbucket(bucket,m_uuid) 
            
        self.deleteAttachment(m_uuid)
        self.deleteMessage(m_uuid)


 
    def getAllMessages(self):
        olderThan = int(time.time())*1000 - 86400000*365*int(self.params['cepdTreshold'])
        qmb = [
                {"$match":{"$and":[{'domain': self.params['domain']}, ]}}, 
                {"$project": {"_id":0, "uuid":1}},
            ]
        qmb[0]['$match']['$and'].append({'date':{"$lt": olderThan}})

        ns = _nosql._nosql()
        self.messagesList = [ r for r  in ns.aggregate(bucket='pm_message',query=qmb )]
        
    
    
    def getMessagesList(self):
        print(self.messagesList)
        
    
    
    def deleteAllMessages(self):
        for msg in self.messagesList:
            self.delete(msg['uuid'])
    
        dfs_log = 'cepdDeleteMessages : End job on domain ref ' + self.params['domain']
        backoffice.info(dfs_log)
