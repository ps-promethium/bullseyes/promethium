#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import json
import os
from pymemcache.client import base
from pymemcache.client.hash import HashClient
import requests
import socket
import sys 
from time import gmtime, strftime

sys.path.append(os.path.abspath("/usr/share/promethium"))

# Promethium inner class
from promethiumClass.config import config
from promethiumClass._nosql import _nosql
from promethiumClass._heartbeat import _heartbeat

# Params
memcExpirTime=50
me=socket.gethostname()
minute = int(strftime("%M", gmtime()))

# Functions
def chunkify(lst,n):
    return [ lst[i::n] for i in range(n) ]

#Step 1 : get master 
pm =  _heartbeat._heartbeat()
pm.getMemcacheNodes()
masterHost = pm.getMaster() 


if masterHost == me:  
    # Step 2 : Connection to memcache nodes
    mcNodes = [ ]
    for server in config.config().memcache['server']:
        srv, port = server.split(':')
        mcNodes.append((srv, int(port)))
        
    client = HashClient(mcNodes)
    
    resSearch = []
    # Have to list all alive Promethium nodes 
    pmHosts = pm.getAliveNodes()
    #pmHosts = ["HPEnvy"] 
    
    # Get domain to parse
    ns = _nosql._nosql()
    q_uuid = [{"$match": {"displayname" : {"$not": { "$regex": "global.virt"}}}},{"$project" : { "_id" : 0, "uuid":1,}}]
    resSearch = [ r for r in ns.aggregate(bucket='pm_domain',query=q_uuid ) ]

    domain = []
    for bucket in resSearch:
            domain.append(bucket['uuid'])
    
    # Define Promethium nodes jobs
    jobList = chunkify(domain, len(pmHosts))

    for host in pmHosts:
        index    = "cepd-" + host
        jobList  = ','.join(jobList[pmHosts.index(host)])
        if len(jobList) > 0:
            client.set(index, jobList, memcExpirTime)
else:
    sys.exit(0)


