#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
import yaml


class config():

    pmYml   = '/etc/promethium/promethium.yml'
    version = '/usr/share/promethium/version'
    
    try:
        with open(version, 'r') as v:
            VERSION=v.readline().strip("\r\n")
            v.close()
    except:
        VERSION="1.0.0"
        pass
        
    with open(pmYml, 'r') as stream:
        try:
            CB = yaml.load(stream, Loader=yaml.FullLoader)
            stream.close()
        except yaml.YAMLError as exc:
            print(exc)
            pass
    stream.close()
    
    tikaJar            = CB['Tika']['jarPath']
    memcache           = CB['memcache']
    defaultStorePath   = CB['Storage']['path']
    user               = CB['user']
    cepdTreshold       = CB['Storage']['cepdTreshold']
