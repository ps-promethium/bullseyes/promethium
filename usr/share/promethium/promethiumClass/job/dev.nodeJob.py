#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import json
import os
from pymemcache.client import base
from pymemcache.client.hash import HashClient
import socket
import sys 
from time import sleep

sys.path.append(os.path.abspath("/usr/share/promethium"))


# Promethium inner class
from promethiumClass.config import config
from promethiumClass.job import mailCrawler

# Params
me=socket.gethostname()
jobIndex = "job-" + me

# Sleep 10 seconds, waiting master defines jobs for each Promethium node
sleep(10)




# Step 1 : Connection to memcache nodes
mcNodes = []
for server in config.config().memcache['server']:
    srv, port = server.split(':')
    mcNodes.append((srv, int(port)))  
client = HashClient(mcNodes)

# Step 2: getting all jobs for me, and do it 
jobList = client.get(jobIndex)
if jobList is not None:
    for uuid in str(jobList, 'utf-8').split(','):
        mc = mailCrawler.mailCrawler()
        mc.retrieveMailboxAttributes(uuid)
        mc.crawlMailBox()





