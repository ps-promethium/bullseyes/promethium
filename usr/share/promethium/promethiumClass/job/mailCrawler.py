#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
from collections import defaultdict
import json
from pymemcache.client import base
import sys
import yaml

# Promethium inner class
from promethiumClass.config import config
from promethiumClass.auth import _cypher
from promethiumClass._nosql import _nosql
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()

from promethiumClass.mailbox import _imap
from promethiumClass.mailbox  import _pop




class mailCrawler():
    
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params            = defaultdict(dict)    
        self.storeMethod       = {'Internal':'_localdrive','CEPh':'_ceph','SWIFT':'_swift'}
        

    def retrieveMailboxAttributes(self,uuid):
        """
        """
        qmb = 'select basket,storesrv from `pm_basket` basket join `pm_domain` dom on keys basket.owner join `pm_host` storesrv on keys dom.storeserver where basket.uuid="' + uuid + '";'
        qmb = [ 
                {"$lookup": {"from": "pm_domain", "localField": "owner", "foreignField": "uuid", "as": "dom"}}, 
                {"$lookup": {"from": "pm_host", "localField": "dom.storeserver", "foreignField": "uuid", "as": "storesrv"}}, 
                {"$unwind" : "$dom"},
                {"$project": {"_id":0,"dom._id": 0, "dom.logo": 0, "storesrv._id":0}},
                {"$match":{"$and":[{"uuid" : uuid}]}} 
            ]
        
        ns = _nosql._nosql()
        res_qmb = [ {'basket':r} for r in ns.aggregate(bucket='pm_basket',query=qmb)]
        
        self.params['tikaJar']     = config.config().tikaJar
        self.params['storePath']   = config.config().defaultStorePath
        
        self.params['domain']           = res_qmb[0]['basket']['owner']
        self.params['basket']           = res_qmb[0]['basket']['uuid']
        self.params['server']           = res_qmb[0]['basket']['server']
        self.params['account']          = res_qmb[0]['basket']['account']
        self.params['passwd']           =  _cypher._cypher().decypherIt(res_qmb[0]['basket']['passwd'])['passwd']
        self.params['port']             = int(res_qmb[0]['basket']['port'])
        self.params['verifypeer']       = res_qmb[0]['basket']['verifypeer']
        self.params['defaultColor']     = res_qmb[0]['basket']['defaultColor']
        
        
        self.params['imapFolder']       = ""
        self.params['imapBackupFolder'] = ""
        self.params['asSender']         = ""
        self.params['filterFile']       = ""
        self.params['x-header']         = ""
        self.params['subscribedguid']   = ""
        self.params['rules']            = ""

        if 'imapFolder' in  res_qmb[0]['basket'].keys() and len(res_qmb[0]['basket']['imapFolder'])>0:
            self.params['imapFolder'] = res_qmb[0]['basket']['imapFolder']
        else:
            del self.params['imapFolder']
            
        if 'imapBackupFolder' in  res_qmb[0]['basket'].keys() and len(res_qmb[0]['basket']['imapBackupFolder'])>0:
            self.params['imapBackupFolder'] = res_qmb[0]['basket']['imapBackupFolder']
        else:    
            del self.params['imapBackupFolder']
            
        if 'asSender' in  res_qmb[0]['basket'].keys():
            self.params['asSender']         = res_qmb[0]['basket']['asSender']
        
        if 'filterFile' in  res_qmb[0]['basket'].keys():
            self.params['filterFile']       = res_qmb[0]['basket']['filterFile']
        
        if 'x-header' in  res_qmb[0]['basket'].keys():
            self.params['x-header']         = res_qmb[0]['basket']['x-header']
        
        if 'subscribedguid' in  res_qmb[0]['basket'].keys():
            self.params['subscribedguid']   = res_qmb[0]['basket']['subscribedguid']
        
        if 'rules' in res_qmb[0]['basket'].keys():
            self.params['rules']   = res_qmb[0]['basket']['rules']
        
        if 'imaps' in res_qmb[0]['basket']['protocol']:
            self.params['ssl'] = True
        if 'imap' in res_qmb[0]['basket']['protocol']:
            self.mailboxType = 'imap' 
        
        if 'pops' in res_qmb[0]['basket']['protocol']:
            self.params['ssl'] = True
        if 'pop' in res_qmb[0]['basket']['protocol']:
            self.mailboxType = 'pop' 
        
  
        self.params['store']         = self.storeMethod[res_qmb[0]['basket']['storesrv'][0]['mechanism']]
        
        if 'storePoint' in res_qmb[0]['basket']['storesrv'][0].keys():
            self.params['storePath']     = res_qmb[0]['basket']['storesrv'][0]['storePoint']

        if 'encrypt' in res_qmb[0]['basket']['storesrv'][0].keys():
            self.params['encrypt'] = res_qmb[0]['basket']['storesrv'][0]['encrypt']
        
        self.params['storeUser']     = ''
        if 'usr' in res_qmb[0]['basket']['storesrv'][0].keys():
            self.params['storeUser'] = res_qmb[0]['basket']['storesrv'][0]['usr']
            
        self.params['storePassword'] = ''    
        if 'passwd' in res_qmb[0]['basket']['storesrv'][0].keys():
            self.params['storePassword'] = res_qmb[0]['basket']['storesrv'][0]['passwd']
    
    
    
    def crawlMailBox(self):
        
        if self.mailboxType == 'imap':
            msg = _imap._imap()
        else:
            msg = _pop._pop()
        
        msg.setParam(**self.params)
        msg.connectSrv()
        msg.login()
        msg.retrieveMessages()
        
       
