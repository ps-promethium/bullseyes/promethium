#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


from collections import defaultdict
from datetime import *
import base64
import imaplib
import json
import mailparser
import os
from pytz import utc, timezone
import ssl
import time






# Promethium inner class
from promethiumClass.config import config
from . import _scanner



from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()

class _imap():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.imap = None
        self.params = defaultdict(dict)
        self.params['verifypeer'] = False
        self.params['basket'] = 'internal'
        self.store = '_localdrive'



    def setParam(self, **kwargs):
        """
        setParam:
        """
        
        if "domain" in kwargs.keys():
            self.params['domain'] = kwargs['domain']
        
        if "basket" in kwargs.keys():
            self.params['basket'] = kwargs['basket']
        
        if "server" in kwargs.keys():
            self.params['server'] = kwargs['server']

        if "account" in kwargs.keys():
            self.params['account'] = kwargs['account']        
 
        if "passwd" in kwargs.keys():
            self.params['passwd'] = kwargs['passwd']           
        
        if "verifypeer" in kwargs.keys():
            self.params['verifypeer'] = kwargs['verifypeer']
        
        if "port" in kwargs.keys():
            self.params['port'] = kwargs['port']            
            
        if "ssl" in kwargs.keys():
            self.params['ssl'] = kwargs['ssl']

        if "imapFolder" in kwargs.keys():
            self.params['imapFolder'] = kwargs['imapFolder']   
            
        if "imapBackupFolder" in kwargs.keys():
            self.params['imapBackupFolder'] = kwargs['imapBackupFolder']

        if "tikaJar" in kwargs.keys():
            self.params['tikaJar'] = kwargs['tikaJar']

        if "store" in kwargs.keys():
            self.params['store'] = kwargs['store']

        if "storePath" in kwargs.keys():
            self.params['storePath'] = kwargs['storePath']
        
        if "storeUser" in kwargs.keys():
            self.params['storeUser'] = kwargs['storeUser']

        if "storePassword" in kwargs.keys():
            self.params['storePassword'] = kwargs['storePassword']
            
        if "asSender" in kwargs.keys():
            self.params['asSender'] = kwargs['asSender']
            
        if "filterFile" in kwargs.keys():    
            self.params['filterFile'] = kwargs['filterFile']
        
        if "x-header" in kwargs.keys():
            self.params['x-header'] = kwargs['x-header']
        
        if "defaultColor" in kwargs.keys():
            self.params['defaultColor'] = kwargs['defaultColor']            
            
        if "subscribedguid" in kwargs.keys():
            self.params['subscribedguid'] = kwargs['subscribedguid']
            
        if "rules" in kwargs.keys():
            self.params['rules'] = kwargs['rules']   
            

    def connectSrv(self):
        if self.params['ssl'] :
            
            ctx = ssl.create_default_context()
            if self.params['verifypeer'] is False:
                ctx.check_hostname = False
                ctx.verify_mode = ssl.CERT_NONE          
                
            try:
                self.imap = imaplib.IMAP4_SSL(self.params['server'], self.params['port'], ssl_context = ctx )

            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                return "Connection refused! Verify IMAPS parameters"
                #pass
                       
        else:
            try:
                self.imap = imaplib.IMAP4(self.params['server'], self.params['port'])
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                return "Connection refused! Verify IMAP parameters"
                #pass



    def login(self):        
        self.pid = str(os.getpid())
        
        try: 
            self.imap.login(self.params['account'], self.params['passwd'])
            i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: Connected to account : ' + self.params['account']
            backoffice.info(i_log)     
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: Connection failed to account : ' + self.params['account']
            backoffice.info(i_log)
            pass



    def test(self):
        self.pid = str(os.getpid())
        try:
            self.imap.login(self.params['account'], self.params['passwd'])
            i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: TEST :: Connected to account : ' + self.params['account']
            backoffice.info(i_log)
            return "OK"
            self.imap.close()
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: Connection failed to account : ' + self.params['account']
            backoffice.info(i_log)
            return "KO"
            pass



    def retrieveMessages(self):            
        self.params['pid'] = self.pid
     
        if 'imapFolder' in self.params:
            self.imap.select(self.params['imapFolder'])
        else:
            self.imap.select('Inbox')
            
        tmp, data = self.imap.search(None, 'ALL')        
        tmpUl, uidL = self.imap.uid('search', None, "ALL")
        
        if 'imapBackupFolder' in self.params:
            uidList = list(uidL[0].split())

            
        for num in data[0].split():
            # Only IMAP after
            typ, msg_data = self.imap.fetch(num,'(RFC822)')
            
            # Both POP/IMAP after
            s = _scanner._scanner()
            s.setParam(**self.params)
            message_id = s.scan(msg_data[0][1])
                
                
            # Only IMAP After
            # Copying message in INBOX subfolder in case of    
            if 'imapBackupFolder' in self.params:
                muid = uidList[int(num)-1]
                result = self.imap.uid('COPY', muid, self.params['imapBackupFolder'])
                
                if result[0] == 'OK':
                    i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: moving mail :: success full :: ' + message_id + ' moved to ' + self.params['imapBackupFolder']
                    backoffice.info(i_log)
                    self.imap.uid('STORE', muid  , '+FLAGS', '(\Deleted)')
                else:
                    i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: moving mail :: failed :: ' + message_id + ' not moved to ' + self.params['imapBackupFolder']
                    backoffice.error(i_log)
                
                time.sleep(1.5)
            
            
        if 'imapBackupFolder' in self.params:    
            self.imap.expunge()
 
        # Close for next time
        self.imap.close()


