#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

from collections import defaultdict
from datetime import *
import base64
import poplib
import json
import logging
import mailparser
import os
from pytz import utc, timezone
import ssl
import time



# Promethium inner class
from promethiumClass.config import config
from . import _scanner


from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()

class _pop():
    
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.pop = None
        self.params = defaultdict(dict)
        self.params['verifyPeer'] = False
        self.params['basket'] = 'internal'



    def setParam(self, **kwargs):
        """
        setParam:
        """
        if "domain" in kwargs.keys():
            self.params['domain'] = kwargs['domain']
        
        if "basket" in kwargs.keys():
            self.params['basket'] = kwargs['basket']
        
        if "server" in kwargs.keys():
            self.params['server'] = kwargs['server']

        if "account" in kwargs.keys():
            self.params['account'] = kwargs['account']        
 
        if "passwd" in kwargs.keys():
            self.params['passwd'] = kwargs['passwd']           
        
        if "verifypeer" in kwargs.keys():
            self.params['verifypeer'] = kwargs['verifypeer']
        
        if "port" in kwargs.keys():
            self.params['port'] = kwargs['port']            
            
        if "ssl" in kwargs.keys():
            self.params['ssl'] = kwargs['ssl']

        if "tikaJar" in kwargs.keys():
            self.params['tikaJar'] = kwargs['tikaJar']

        if "store" in kwargs.keys():
            self.params['store'] = kwargs['store']

        if "storePath" in kwargs.keys():
            self.params['storePath'] = kwargs['storePath']
        
        if "storeUser" in kwargs.keys():
            self.params['storeUser'] = kwargs['storeUser']

        if "storePassword" in kwargs.keys():
            self.params['storePassword'] = kwargs['storePassword']
            
        if "asSender" in kwargs.keys():
            self.params['asSender'] = kwargs['asSender']
            
        if "filterFile" in kwargs.keys():    
            self.params['filterFile'] = kwargs['filterFile']
        
        if "x-header" in kwargs.keys():
            self.params['x-header'] = kwargs['x-header']
        
        if "defaultColor" in kwargs.keys():
            self.params['defaultColor'] = kwargs['defaultColor']            
            
        if "subscribedguid" in kwargs.keys():
            self.params['subscribedguid'] = kwargs['subscribedguid']
            
        if "rules" in kwargs.keys():
            self.params['rules'] = kwargs['rules']   
            


    def connectSrv(self):
        if self.params['ssl'] :
            
            ctx = ssl.create_default_context()
            if self.params['verifyPeer'] is False:
                ctx.check_hostname = False
                ctx.verify_mode = ssl.CERT_NONE          
                
            try:
                self.pop = poplib.POP3_SSL(self.params['host'], self.params['port'])

            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                return "Connection refused! Verify POPS parameters"
                #pass
                       
        else:
            try:
                self.pop = poplib.POP3(self.params['host'], self.params['port'])
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                return "Connection refused! Verify POP parameters"
                #pass



    def login(self):      
        self.pid = str(os.getpid())
        
        try: 
            self.pop.user(self.params['mailbox'])
            self.pop.pass_(self.params['password'])
            i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: Connected to mailbox : ' + self.params['mailbox']
            backoffice.info(i_log)     
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = '[' + self.pid + '] :: ' + self.params['basket'] + ' :: Connection failed to mailbox : ' + self.params['mailbox']
            backoffice.info(i_log)
            pass



    def test(self):
        self.pid = str(os.getpid())
        
        try:
            self.pop.user(self.params['mailbox'])
            self.pop.pass_(self.params['password'])
            i_log = 'mailbox :: pop :: test :: ' + self.params['basket'] + ' :: Connected to mailbox : ' + self.params['mailbox']
            backoffice.info(i_log)
            return "OK"
            self.pop.quit()
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = self.params['basket'] + ' :: Connection failed to mailbox : ' + self.params['mailbox']
            backoffice.info(i_log)
            return "KO"
            pass



    def retrieveMessages(self):           
        pid = self.pid
        
        storeType = getattr(sys.modules[__name__], self.store) # _localdrive|_swift|_ceph
        store     = storage.storage(storeType)
        store.setParam(**self.params)
        
        rqe = mailboxReqElastic.mailboxReqElastic()
        rqe.setParam(**self.params)
        
        numMessages = len(self.pop.list()[1])
        for i in range(numMessages):            
            
            msg_data  = b"\n".join(self.pop.retr(i+1)[1])
            mail = mailparser.parse_from_bytes(msg_data)
            
            i_log = '[' + pid + '] :: ' + self.params['basket'] + ' :: ref: ' + mail.message_id
            backoffice.info(i_log)




            self.pop.dele(i+1)
            
        self.pop.quit()
        
            

