#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


import codecs
from collections import defaultdict
from datetime import *
import base64
import imaplib
import json
import mailparser
import os
#from pytz import utc, timezone
import ssl
import time
#from email.utils import *




# Promethium inner class
from promethiumClass.config import config
from promethiumClass._nosql import _nosql
from promethiumClass.tesseract import _tesseract 
from promethiumClass.tika import _tika 
from promethiumClass.various import _uuid

from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()



class _scanner():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.imap = None
        self.params = defaultdict(dict)
        self.params['verifypeer'] = False
        self.params['basket'] = 'internal'
        self.store = '_localdrive'
        self.params['asSender'] = []
        self.params['subscribedguid'] = ''
        self.params['dispatched'] = ''
        self.params['rules'] = defaultdict(dict)
        self.ns = _nosql._nosql()



    def setParam(self, **kwargs):
        """
        setParam:
        """
        if "domain" in kwargs.keys():
            self.params['domain'] = kwargs['domain']
            
        if "basket" in kwargs.keys():
            self.params['basket'] = kwargs['basket']
        
        if "server" in kwargs.keys():
            self.params['server'] = kwargs['server']

        if "account" in kwargs.keys():
            self.params['account'] = kwargs['account']        
            self.params['asSender'].append(self.params['account'])
 
        if "passwd" in kwargs.keys():
            self.params['passwd'] = kwargs['passwd']           
        
        if "verifypeer" in kwargs.keys():
            self.params['verifypeer'] = kwargs['verifypeer']
        
        if "port" in kwargs.keys():
            self.params['port'] = kwargs['port']            
            
        if "ssl" in kwargs.keys():
            self.params['ssl'] = kwargs['ssl']
            
        if "imapBackupFolder" in kwargs.keys():
            self.params['imapBackupFolder'] = kwargs['imapBackupFolder']

        if "tikaJar" in kwargs.keys():
            self.params['tikaJar'] = kwargs['tikaJar']

        if "store" in kwargs.keys():
            self.params['store'] = kwargs['store']

        if "storePath" in kwargs.keys():
            self.params['storePath'] = kwargs['storePath']
        
        if "storeUser" in kwargs.keys():
            self.params['storeUser'] = kwargs['storeUser']

        if "storePassword" in kwargs.keys():
            self.params['storePassword'] = kwargs['storePassword']
        
        if "pid" in kwargs.keys():
            self.pid = kwargs['pid']

        if "asSender" in kwargs.keys():
            self.params['asSender'].extend(kwargs['asSender'])
            
        if "filterFile" in kwargs.keys():    
            self.params['filterFile'] = kwargs['filterFile']
        
        if "x-header" in kwargs.keys():
            self.params['x-header'] = kwargs['x-header']
        
        if "defaultColor" in kwargs.keys():
            self.params['defaultColor'] = kwargs['defaultColor']
    
        if "subscribedguid" in kwargs.keys():
            self.params['subscribedguid'] = kwargs['subscribedguid']
            
        if "rules" in kwargs.keys():
            self.params['rules'] = kwargs['rules']  
            

    def retrieveMember(self, guid):      
        res = []
        #q_uuid = 'select uuid from `pm_user`  where domain="' + self.params['domain'] + '" and "'+ guid +'" in subscribedguid  ;'
        q_uuid = [{"$match":{"$and":[{'domain':self.params['domain']},{'subscribedguid':guid}]}},
                  {"$project" : { "_id" : 0, "uuid":1}}
                  ]
        try:
            res_q_uuid = [ r for r in self.ns.aggregate(bucket='pm_user',query=q_uuid)]
            if len(res_q_uuid) > 0 :
                res = [ item['uuid'] for item in res_q_uuid ]
        except:
            pass
        return res 
    


    def retrieveChrono(self):
        res = {}
        #q_uuid = 'select * from `pm_chrono` `chrono` where uuid="' + self.params['domain'] + '"  ;'
        q_uuid = {'uuid': self.params['domain']}
        try:
            res_q_uuid = [ {'chrono':r} for r in self.ns.search(bucket='pm_chrono',query=q_uuid)]
            if len(res_q_uuid) == 1 :
                res =  res_q_uuid[0]['chrono']
        except:
            pass
        return res 
    
    
    def fixUtf8(self,string):
        return string.replace('Ã¢','â').replace('Ã©','é').replace('Ã¨','è').replace('Ãª','ê').replace('Ã«','ë').replace('Ã®','î').replace('Ã¯','ï').replace('Ã´','ô').replace('Ã¶','ö').replace('Ã¹','ù').replace('Ã»','û').replace('Ã¼','ü').replace('Ã§','ç').replace('Å','œ').replace('â¬','€').replace('Â°','°').replace('Ã','à')



    def scan(self,emlBytes):            
        pid = self.pid
        
        #storeType = getattr(sys.modules[__name__], self.store) # _localdrive|_swift|_ceph
        if '_localdrive' in self.store:
            from promethiumClass.store import _localdrive
            store = _localdrive._localdrive()

        if 'S3' in self.store:
            from promethiumClass.store import _S3
            store = _S3._S3()
            
            
        store.setParam(**self.params)
        
        mail = mailparser.parse_from_bytes(emlBytes)
               
        i_log = '[' + pid + '] :: ' + self.params['basket'] + ' :: ref: ' + mail.message_id 
        backoffice.info(i_log)
            
        m_uuid =  _uuid._uuid().new()
        msg     = defaultdict(dict)
        tikamsg = defaultdict(dict)
            
        ## Retrieve all message elements we store in Elasticsearch
          
        # Dispatch list members
        if len(self.params['subscribedguid']) > 0:
            members = []
            for guid in self.params['subscribedguid']:
                m = [] 
                m = self.retrieveMember(guid)
                members.extend(m)
                
            self.params['dispatched'] = list(set(members))
          
        # Elastic info
        msg['@timestamp']  = int(time.time())*1000            
        # Msg info
        msg['dispatchedguid'] = self.params['subscribedguid']
        msg['dispatched']     = self.params['dispatched']  
        msg['domain']         = self.params['domain']
        msg['basket']         = self.params['basket']        
        msg['defaultColor']   = self.params['defaultColor']
        msg['uuid']           = m_uuid

        msg['date']           = int(time.mktime(time.strptime(str(mail.date), "%Y-%m-%d %H:%M:%S")))*1000 
        msg['litDate']        = str(mail.date).split(" ")[0]

        msg['outBound']       = False
        
        for n in mail.from_:
            if n[0]:
                from_ = n[0] + " <" + n[1] + ">" 
                fromTest = n[1]
            else:
                from_ = n[1]
                fromTest = n[1]
            
        to  = ''
        cc  = ''
        bcc = ''
            
        tol = []
        for n in mail.to:
            if n[0]:
                to = n[0] + " <" + n[1] + ">" 
            else:
                to = n[1]
            tol.append(to.strip())
        to  = ",".join(tol)
        
        ccl = []
        for n in mail.cc:
            if n[0]:
                cc = n[0] + " <" + n[1] + ">" 
            else:
                cc = n[1]
            ccl.append(to.strip())
        cc = ",".join(ccl)    

        bccl = []
        for n in mail.bcc:
            if n[0]:
                bcc = n[0] + " <" + n[1] + ">" 
            else:
                bcc = n[1]
            bccl.append(to.strip())
        bcc = ",".join(bccl)
            
        msg['to']          = to
        msg['cc']          = cc
        msg['bcc']         = bcc
        msg['from']        = from_
        
        if mail.from_[0][1] in  self.params['asSender']:
            msg['outBound'] =  True
        
        # Chrono management
        resChrono = self.retrieveChrono()
        if msg['outBound'] and resChrono['reserved'] > 0:
            msg['chrono'] = resChrono['reserved']     
            resChrono['reserved'] = 0
            self.ns.upsert(bucket='pm_chrono',uuid=self.params['domain'], upsert= resChrono )
        else:            
            msg['chrono'] = resChrono['counter']
            resChrono['counter'] = int(resChrono['counter']) + 1 
            self.ns.upsert(bucket='pm_chrono',uuid=self.params['domain'], upsert= resChrono )
        
        msg['return_path'] = mail.return_path.replace("<","").replace(">","")
            
        msg['x_priority']  = mail.X_Priority.split(" ")[0] if mail.X_Priority else "3"
    
        msg['message_id']  = mail.message_id.replace("<","").replace(">","")
        msg['in_reply_to'] = mail.in_reply_to.replace("<","").replace(">","")
        msg['references']  = mail.references.replace("<","").replace(">","").replace("\r\n","").replace(" ",",")
        msg['subject']     = mail.subject
        
        msg['x_headers']   = []         
        Headers = {}
        Headers = mail.headers
        if len(self.params['x-header']) > 0:
            for header in self.params['x-header']:
                if len(header)>0 and header in Headers.keys():
                    msg['x_headers'].append({'name':header, 'value': Headers[header]})

        # Specs to Min. Of Interieur and Thunderbird plugin priority & flash 
        if 'X-Priority-Label' in Headers.keys():
            if 'immediate' in Headers['X-Priority-Label']:
                msg['x_priority'] = 2
            if 'flash' in Headers['X-Priority-Label']:
                msg['x_priority'] = 1
       
        # Because HTML is prettiest
        if 'mail_boundary' in mail.body:
            #body = mail.body.split('--- mail_boundary ---')[1]
            body = mail.text_html[0]
            
        else: 
            body = mail.body
                
        msg['body'] = self.fixUtf8(body)
         
        ## Parse Body with Tika
        t = _tika._tika()
        t.setParam(**self.params)                   
        tb = t.getData(_id=pid, case='Body Message', data=base64.b64encode(msg['body'].encode()))
        
        
        
        ## Apply dispatching rules if defined
        ## The first rule in success will break the loop. 
        ##
        if len(self.params['rules']) > 0:
            for key in  self.params['rules'].keys():
                filterStatus = []
                
                for filtr in self.params['rules'][key]['rules']:    
                    scanStatus = 0
                    field        = filtr['field']
                    verb         = filtr['verb']
                    string       = filtr['string']
                    
                    if field == 'Subject':
                        testText = msg['subject']
                    
                    if field == 'From':
                        testText = fromTest
                    
                    if field == 'Body':
                        testText = tb
                        
                    ## We can test now
                    if 'is' in verb:
                        if testText.lower() == string.lower():
                            scanStatus = 1
                            
                    if 'contains' in verb:
                        if string.lower() in testText.lower():
                            scanStatus = 1   
        
                    filterStatus.append(scanStatus)     
                
                # Case AND
                if 'AND' in self.params['rules'][key]['match'] and len(filterStatus)>0:
                    if not 0 in filterStatus:
                        msg['dispatchedguid'] = self.params['rules'][key]['subscribedguid']
                        i_log = '[' + pid + '] :: ' + self.params['basket'] + ' :: Apply Rule : ' + self.params['rules'][key]['displayname'] 
                        backoffice.info(i_log)
                        break
                
                # Case Or
                if 'OR' in self.params['rules'][key]['match'] and len(filterStatus)>0:
                    if 1 in filterStatus:
                        msg['dispatchedguid'] = self.params['rules'][key]['subscribedguid']
                        i_log = '[' + pid + '] :: ' + self.params['basket'] + ' ::  Apply Rule : ' + self.params['rules'][key]['displayname'] 
                        backoffice.info(i_log)
                        break   
         
         
        ## End of dispatching Rules  
         
        
        # MongoDB tika index (body content)
        tikamsg['@timestamp']  = int(time.time())*1000
            
        # Msg info
        tikamsg['basket']         = self.params['basket']
        tikamsg['uuid']           = m_uuid
        tikamsg['m_uuid']         = m_uuid
        tikamsg['domain']         = self.params['domain']
        tikamsg['basket']         = self.params['basket']
        tikamsg['dispatched']     = self.params['dispatched']
        tikamsg['dispatchedguid'] = msg['dispatchedguid']
        tikamsg['body']           = tb

            
        self.ns.upsert(bucket='pm_tika',uuid=tikamsg['uuid'], upsert=tikamsg )
           
        ## Retrieve all attachment elements we store in Elasticsearch & Store point     
        attachment  = []
        if mail.attachments:
               
            for att in  mail.attachments:
                a_uuid =  _uuid._uuid().new()
            
                #Structure ['filename', 'payload', 'binary', 'mail_content_type', 'content-id', 'charset', 'content_transfer_encoding']
                t = _tika._tika()
                t.setParam(**self.params)                   
                tf = t.getData(case='attachment', data=att['payload'], _id=pid)          
           
                attach = defaultdict(dict)
                    
                # Elastic attachment index
                attach['@timestamp']                = int(time.time())*1000 
                attach['uuid']                      = a_uuid  
                attach['domain']                    = self.params['domain']
                attach['m_uuid']                    = m_uuid 
                attach['filename']                  = att['filename']
                fileExt = attach['filename'].split('.')[-1]
                attach['filesize']                  = int((len(att['payload']) * 3) / 4 - att['payload'].count('=', -2))
                attach['mail_content_type']         = att['mail_content_type']
                attach['content_transfer_encoding'] = att['content_transfer_encoding']
                
                if not fileExt in  self.params['filterFile']:
                    attachment.append(a_uuid)
                    self.ns.upsert(bucket='pm_attachment',uuid=attach['uuid'], upsert=attach )

                    # Store Payload
                    store.put(uuid=a_uuid, payload=att['payload'], _id=pid)

                    if not tf and 'pdf' in fileExt:
                        tcr = _tesseract._tesseract()
                        tcr.setParam(**self.params)
                        r = tcr.getDataFromScannedPdf(data=att['payload'], _id=pid)
                        if r is not None:
                            tf = r
                
                    if not tf and 'image' in attach['mail_content_type'] :
                        tcr = _tesseract._tesseract()
                        tcr.setParam(**self.params)
                        r = tcr.getDataFromImage(data=att['payload'], _id=pid)
                        if r is not None:
                            tf = r
                
                    # Elastic tika index (file content)
                    if tf:
                        tikaf = defaultdict(dict)
                        tikaf['@timestamp']     = int(time.time())*1000
                        tikaf['uuid']           = a_uuid
                        tikaf['m_uuid']         = m_uuid 
                        tikaf['domain']         = self.params['domain']
                        tikaf['basket']         = self.params['basket']
                        tikaf['body']       = tf
                        self.ns.upsert(bucket='pm_tika',uuid=tikaf['uuid'], upsert=tikaf )
            
            
        ## Send message data to Elastic
        msg['attachment'] = ",".join(attachment)
        self.ns.upsert(bucket='pm_message', uuid=msg['uuid'], upsert=msg)
            
        return msg['message_id']
                
                


