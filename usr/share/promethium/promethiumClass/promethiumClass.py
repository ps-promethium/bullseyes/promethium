#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

# Native Python Lib/Class 

import os.path 
import sys

sys.path.insert(0 ,'./auth')
sys.path.insert(0 ,'./cepd')
sys.path.insert(0 ,'./config')
sys.path.insert(0 ,'./_nosql')
sys.path.insert(0 ,'./_heartbeat')
sys.path.insert(0 ,'./job')
sys.path.insert(0 ,'./log')
sys.path.insert(0 ,'./mailbox')
sys.path.insert(0 ,'./smtp')
sys.path.insert(0 ,'./store')
sys.path.insert(0 ,'./tesseract')
sys.path.insert(0 ,'./tika')
sys.path.insert(0 ,'./various')
