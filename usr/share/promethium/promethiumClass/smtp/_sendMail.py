#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


from collections import defaultdict
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate, make_msgid
from email import encoders
import smtplib


class _sendMail():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params = defaultdict(dict)
        self.params['verifypeer'] = False
        self.msg = MIMEMultipart()

    
    def setMail(self): 
        self.msg['message-id'] = make_msgid()
        self.msg['Date']       = formatdate(localtime=True)
        self.msg['User-Agent'] = 'Promethium Forwarder module'
        
        
        
    def setMailDest(self,**kwargs): 
        self.msg['To'] = ",".join(kwargs['to'])
        self.dest       = kwargs['to']
        if 'cc' in kwargs.keys():
            self.msg['Cc'] = ",".join(kwargs['cc'])
            self.dest.extend(kwargs['cc'])

    

    def setXUserHeader(self,**kwargs):
        #self.msg['Forwarded-by-User'] = kwargs['user']
        self.msg.add_header('Forwarded-by-User', kwargs['user'])
    


    def setMailBody(self,**kwargs): 
        html = MIMEText(kwargs['body'], 'html')
        self.msg.attach(html)
    
    

    def setMailSubject(self,**kwargs): 
        self.msg['Subject']    = kwargs['subject']
    
    
    
    def setMailFrom(self,**kwargs): 
        self.msg['From'] = "Promethium - " + kwargs['domainName'] + ' <' + kwargs['account'] + '>'
    
    
    
    def addAttachment(self,**kwargs):  
        #{'filename': 'Cas pratique suppression images.pdf', 'filesize': 913371, 'content_type': 'application/pdf', 'base64': ''}
        part = MIMEBase(kwargs['content_type'].split('/')[0], kwargs['content_type'].split('/')[1])
        part.set_payload(kwargs['base64'])
        fname = kwargs['filename']
        part.add_header('Content-Disposition', 'attachment; filename="' + fname + '"')
      
        encoders.encode_base64(part)
             
        self.msg.attach(part)
      
    
    
    def setSmtpParams(self,**kwargs): 
        if "server" in kwargs.keys():
            self.params['server'] = kwargs['server']

        if "account" in kwargs.keys():
            self.params['account'] = kwargs['account']        
 
        if "passwd" in kwargs.keys():
            self.params['passwd'] = kwargs['passwd']           
        
        if "verifypeer" in kwargs.keys():
            self.params['verifypeer'] = kwargs['verifypeer']
        
        if "port" in kwargs.keys():
            self.params['port'] = kwargs['port']            
            
        if "protocol" in kwargs.keys():
            self.params['protocol'] = kwargs['protocol']
        
    
    
    def smtpConnect(self,**kwargs): 
        self.server = smtplib.SMTP(self.params['server'], self.params['port'])
        self.server.starttls()
        self.server.login(self.params['account'], self.params['passwd'])    
    
    
    
    def smtpSend(self): 
        text = self.msg.as_string()
        self.msg['text'] =self.msg.as_string() 
        self.server.sendmail(self.params['account'], self.dest, text)
        self.server.quit()
