#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

import base64
from collections import defaultdict
import os, sys

# Promethium inner class
from promethiumClass.config import config
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()



class _localdrive():
    
    def __init__(self):
        self.params = defaultdict(dict)
        self.params['storePath'] = "/var/lib/promethium"
        self.basket = 'internal'
        self.encrypt = False
        

    def setParam(self, **kwargs):
        """
        setParam:
        """
        if "storePath" in kwargs.keys():
            self.params['storePath'] = kwargs['storePath']

        if "basket" in kwargs.keys():
            self.basket = kwargs['basket']
            
        if "encrypt" in kwargs.keys():        
            self.encrypt = kwargs['encrypt']
 
 
 
    def put(self, **kwargs):
        uuid = kwargs['uuid']
        storePath = self.params['storePath']
        
        if not os.path.isdir(storePath):
            os.makedirs(storePath)
        
        try:  
            fp = storePath + '/' + uuid        
            file = open(fp,'w')          
            file.write(kwargs['payload']) 
            file.close() 
            i_log = '[' + kwargs['_id'] + '] :: ' + self.basket + ' :: store file :: success full :: ' + kwargs['uuid'] 
            backoffice.info(i_log)
                
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = '[' + kwargs['_id'] + '] :: ' + self.basket + ' :: store file :: failed :: ' +  kwargs['uuid']
            backoffice.info(i_log)
            pass        



    def post(self, **kwargs):
        pass


        
    def get(self, **kwargs):
        uuid = kwargs['uuid']
        storePath = self.params['storePath']
        fp = storePath + '/' + uuid
      
        return open(fp, "rb").read()

    
    
    
    def delete(self, **kwargs):    
        storePath = self.params['storePath']
        try:  
            fp = storePath + '/' + kwargs['uuid']            
            if os.path.exists(fp):
                os.remove(fp) 
                
            i_log = 'removing file :: success full :: ' + fp
            backoffice.info(i_log)
            return True
                
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = 'removing file :: failed :: ' + fp
            backoffice.info(i_log)
            return False        
    


    
