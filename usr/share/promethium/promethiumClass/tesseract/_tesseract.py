#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

import base64
from collections import defaultdict
import cv2
import numpy as np
import os
from pdf2image import convert_from_bytes
from PIL import Image
import pytesseract

# Promethium inner class
from promethiumClass.config import config
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()

class _tesseract():

    def __init__(self):
        self.params            = defaultdict(dict)
        self.params['basket'] = 'global.virt'
 


    def setParam(self, **kwargs):      
        if "basket" in kwargs.keys():
            self.params['basket'] = kwargs['basket']
            


    def tesseractImgPayload(self, payload):
        text = None
        try:
            img = base64.b64decode(payload)
            npimg = np.frombuffer(img, dtype=np.uint8)
            image = cv2.imdecode(npimg, 1)
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
            filename = "{}.png".format(os.getpid())
            cv2.imwrite(filename, gray)
            text = pytesseract.image_to_string(Image.open(filename))
            os.remove(filename)
        except: 
            pass
        return text



    def tesseractPdfPage(self, img):
        text = ""
        try:
            gray = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2GRAY)
            gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
            filename = "{}.png".format(os.getpid())
            cv2.imwrite(filename, gray)
            text = pytesseract.image_to_string(Image.open(filename))
            os.remove(filename)
        except: 
            pass
        return text



    def tesseractPDFPayload(self, payload):
        img = base64.b64decode(payload)
        npimg = np.frombuffer(img, dtype=np.uint8)
        pages = convert_from_bytes(npimg, 350)

        allPage = []
        for page in pages:
            allPage.append(self.tesseractPdfPage(page))

        return " ".join(allPage)
       


    def getDataFromImage(self, **kwargs):        
        if kwargs['data']:
            try:
                fc= self.tesseractImgPayload(kwargs['data'])
                content = " ".join(fc.replace('\t', ' ').replace('\n', ' ').replace('\u200c', ' ').split())
                if len(content):
                    i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TESSERACT :: Extract data from image'
                else:
                    i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TESSERACT :: No data extracted from image while parsed. Sorry for you'
                backoffice.info(i_log)                
                return content
            
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TESSERACT :: Extracting data failed from image'
                backoffice.info(i_log)
                pass
        else:
            i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TESSERACT :: No data extracted from PDF. Sorry for you'
            backoffice.info(i_log)



    def getDataFromScannedPdf(self, **kwargs):        
        if kwargs['data']:
            try:
                fc = self.tesseractPDFPayload(kwargs['data'])
                content = " ".join(fc.replace('\t', ' ').replace('\n', ' ').replace('\u200c', ' ').split())
                if len(content):
                    i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' ::  TESSERACT :: Extract data from PDF'
                else:
                    i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TESSERACT :: No data extracted from PDF while parsed. Sorry for you'
                backoffice.info(i_log)                
                return content
            
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TESSERACT :: Extracting data failed from PDF'
                backoffice.info(i_log)
                pass
        else:
            i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TESSERACT :: No data extracted from PDF. Sorry for you'
            backoffice.info(i_log)
