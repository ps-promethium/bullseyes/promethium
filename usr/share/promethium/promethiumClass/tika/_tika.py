#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

from collections import defaultdict


# Promethium inner class
from promethiumClass.config import config
from promethiumClass.log import promelog
backoffice = promelog.promelog().setLogger()
from tikapp import TikaApp



class _tika():

    def __init__(self):
        self.params            = defaultdict(dict)
        self.params['tikaJar'] ="/usr/share/promethium/Java/tika-app-1.20.jar"
        self.params['basket'] = 'global.virt'
 


    def setParam(self, **kwargs):
        if "tikaJar" in kwargs.keys():
            self.params['tikaJar'] = kwargs['tikaJar']
        
        if "basket" in kwargs.keys():
            self.params['basket'] = kwargs['basket']
            


    def getMeta(self, **kwargs):
        if kwargs['data']:
            try:
                tika_client = TikaApp(file_jar=self.params['tikaJar'])
                content = tika_client.extract_only_metadata(payload=kwargs['data'])
                
                i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' ::  TIKA :: Extract metadata from ' +  kwargs['case']
                backoffice.info(i_log)
                
                return content
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' ::  TIKA :: Extracting metadata failed from ' +  kwargs['case']
                backoffice.info(i_log)
                pass
        else:
            i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TIKA :: NO DATA from ' +  kwargs['case']
            backoffice.info(i_log)
            return False

        

    def getData(self, **kwargs):        
        if kwargs['data']:
            try:
                tika_client = TikaApp(file_jar=self.params['tikaJar'])
                fc = tika_client.extract_only_content(payload=kwargs['data'])
                content = " ".join(fc.replace('\t', ' ').replace('\n', ' ').replace('\u200c', ' ').split())
                
                #print (content.split())
                if len(content):
                    i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' ::  TIKA :: Extract data from ' +  kwargs['case']
                else:
                    i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' ::  TIKA :: No data extracted from ' +  kwargs['case'] + '. Next step, use TESSERACT' 
                backoffice.info(i_log)                
                
                return content
            
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TIKA :: Extracting data failed from ' +  kwargs['case']
                backoffice.info(i_log)
                pass
        else:
            i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TIKA :: NO DATA from ' +  kwargs['case']
            backoffice.info(i_log)
            return False
        
        
    
    def getAll(self, **kwargs):
        if kwargs['data']:
            try:
                tika_client = TikaApp(file_jar=self.params['tikaJar'])
                content = tika_client.extract_all_content(payload=kwargs['data'])
                
                i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' ::  TIKA :: Extract meta & data from ' +  kwargs['case']
                backoffice.info(i_log)                
                
                return content
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TIKA :: Extracting data failed from ' +  kwargs['case']
                backoffice.info(i_log)
                pass
        else:
            i_log = '[' + kwargs['_id'] + '] :: ' + self.params['basket'] + ' :: TIKA :: NO DATA from ' +  kwargs['case']
            backoffice.info(i_log)
            return False
