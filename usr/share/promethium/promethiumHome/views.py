#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
import base64
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone
from django.contrib.auth import logout as site_logout


# promethium lib/class 
import promethiumAuth.views as promethiumAuth_views 



def home(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Home',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'user': request.session['user'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    context.update(getAlerts(request))
    context.update(getDomainDefaultpage(request))
    
    
    return render(request, 'home.htm', context)



def getDomainDefaultpage(request):
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    res = {'defaultpage' : None }
    URI = "http://127.0.0.1:8000/api/domain/" + request.session['user']['domain'] + "/details"
    
    with requests.Session() as s:
        rd = s.get( URI, headers=Headers).json()

    if 'defaultpage' in rd['details'][0]['pm_domain'].keys():
        res['defaultpage'] = rd['details'][0]['pm_domain']['defaultpage']
    
    return res



def getAlerts(request):
    data = {'alerts': []}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['uuid'] +'/alert/_now' 
    try:
        with requests.Session() as s:
            rd = s.get( URI, data=json.dumps(data), headers=Headers).json()
        if rd['alert'] is not None:
            data['alerts'] = rd['alert']
    except:
        pass
    return data

