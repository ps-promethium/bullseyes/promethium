#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
 
# Python lib
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
from promethiumClass.auth import _cypher

def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/')     
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    context.update(getHtmlDatas(request))

    
    return render(request, 'manage/admins.htm', context)
    
    
    
def getHtmlDatas(request):
    """
    """  
    domain = ""
    data = {"Admins": [], "Domains": [] }
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = "http://127.0.0.1:8000/api/domain/_all"
    with requests.Session() as s:
        rd = s.get( URI, headers=Headers).json()
    
    for dom in rd['all']:
        admins = {}
        domain = dom['displayname']
        uuid   = dom['uuid']
        
        data['Domains'].append({'uuid': uuid, 'displayname': domain})
        
        URI = "http://127.0.0.1:8000/api/domain/" + dom['uuid']  + "/_admins"
        with requests.Session() as s:
            rad = s.get( URI, headers=Headers).json()
        
        if rad['admins'] is not None:
            for adm in rad['admins']:
                admin = adm
                admin['domainName'] = domain
                admin['domainUuid'] = uuid
                
                data['Admins'].append(admin)
    return data



def add(request): 
    """
    /api/user/{domainUuid}/add
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'domain': request.session['user']['domain'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    

    if request.method == 'POST': 
        URI = "http://127.0.0.1:8000/api/user/" + request.POST.get('domain') + "/add"
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        del(JSON['csrfmiddlewaretoken'])
        with requests.Session() as s:
            rad = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rad['status']:                   
            i_log = request.session['user']['login'] + ' adds new domain admin "' + request.POST.get('login') + '"'
            frontOfficeLog.info(i_log)
            return redirect('/manage/admins')
        else:
            i_log = request.session['user']['login'] + ' failed adding new domain admin "' + request.POST.get('login') + '"'   
            frontOfficeLog.info(i_log)
            context['errorText'] = rad['errorText']
            context.update(getHtmlDatas(request))
            return render(request, 'manage/admins.htm', context)
        


def update(request):
    """
    api/domain/{uuid}/update
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.POST.get('domain')  + "/" + request.POST.get('uuid') + "/details" ;
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']

        URI = "http://127.0.0.1:8000/api/user/" + request.POST.get('domain')  + "/" + request.POST.get('uuid') + "/update"

        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)


        if 'enable' not in request.POST.keys():
            JSON['enable'] = "False"
        
        del(JSON['csrfmiddlewaretoken'])
       
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates domain admin "' + request.POST.get('login') + '"'  
            frontOfficeLog.info(i_log)
            return redirect('/manage/admins')
        else:
            i_log = request.session['user']['login'] + ' failed to update domain admin "' + request.POST.get('login') + '"'
            context['errorText'] = rh['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'manage/admins.htm', context)
        
        

def delete(request,domainUuid,uuid):
    """ 
     /api/user/{domainUuid}/{uuid}/delete
    """
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/user/" + domainUuid + "/" + uuid + "/delete"
        with requests.Session() as s:
            rad = s.delete( URI, headers=Headers).json()
        
        if rad['status']:                   
            i_log = 'Deleting admin domain "' + uuid + '" succeed'
        else:
            i_log = 'Deleting admin domain failed'       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/admins')



def resetPasswd(request):
    """
    api/domain/{uuid}/update
    """
    if not 'user' in request.session.keys():
        return redirect('/')  
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.POST.get('domain')  + "/" + request.POST.get('uuid') + "/details" ;
        
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']
        JSON['newpasswd'] = request.POST.get('passwd')
    
        URI = "http://127.0.0.1:8000/api/user/" + request.POST.get('domain')  + "/" + request.POST.get('uuid') + "/update"
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' resets '  + request.POST.get('login') + ' password'   
            frontOfficeLog.info(i_log)
            return redirect('/manage/admins')
        else:
            i_log = request.session['user']['login'] + ' failed reset '  + request.POST.get('login') + 'password'
            context['errorText'] = rh['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'manage/admins.htm', context)
    
    
