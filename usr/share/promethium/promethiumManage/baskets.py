#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

# Python lib
import base64
import json
import magic

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
from . import groups as Groups


def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Baskets',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'domain': request.session['user']['domain'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'Groups': Groups._all(request,request.session['user']['domain']),
    }
    
    promethiumAuth_views.authPingPM(request)
    
    context.update(getAllBaskets(request))
    
    return render(request, 'manage/baskets.htm', context)
    
    
    
def getAllBaskets(request):
    """
    """  
    data = {"Domains": [],  "Hosts":[]}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/_all"
    with requests.Session() as s:
        rh = s.get( URI, headers=Headers).json()
    data['Baskets'] = rh['all']

    return data



def add(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Baskets',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/add"


    if request.method == 'POST': 
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        groupList              =  [ group for group in request.POST.getlist('subscribedguid')]
        JSON['subscribedguid'] = groupList

        filterFileList     = request.POST.get('filterFile').split(' ')
        JSON['filterFile'] = filterFileList

        asSenderList     =  request.POST.get('asSender').replace(' ','').split('\r\n')
        JSON['asSender'] = asSenderList


        xHeaderList      =  request.POST.get('x-header').replace(' ','').split('\r\n')
        JSON['x-header'] = xHeaderList

        if 'enable' not in request.POST.keys():
            JSON['enable'] = False
            
            
        del(JSON['csrfmiddlewaretoken'])
        
        with requests.Session() as s:
            rh = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] +  ' adds new basket "' + request.POST.get('displayname') + '"'
        else:
            i_log = request.session['user']['login'] +  ' failed adding new basket "' + request.POST.get('displayname') + '"'      
        
        frontOfficeLog.info(i_log)


    return redirect('/manage/baskets')



def update(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Baskets',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    if request.method == 'POST':              
        URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/" + request.POST.get('uuid') + "/update"
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        groupList              =  [ group for group in request.POST.getlist('subscribedguid')]
        JSON['subscribedguid'] = groupList

        filterFileList     = request.POST.get('filterFile').split(' ')
        JSON['filterFile'] = filterFileList

        asSenderList     =  request.POST.get('asSender').replace(' ','').split('\r\n')
        JSON['asSender'] = asSenderList

        xHeaderList      =  request.POST.get('x-header').replace(' ','').split('\r\n')
        JSON['x-header'] = xHeaderList
        
        if 'enable' not in request.POST.keys():
            JSON['enable'] = False

        del(JSON['csrfmiddlewaretoken'])
       

        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates basket "' + request.POST.get('displayname') + '"'   
        else:
            i_log = request.session['user']['login'] + ' failed  updating basket "' + request.POST.get('displayname') + '"'  
        
        frontOfficeLog.info(i_log)

    return redirect('/manage/baskets')
    
    
    
def delete(request,uuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Baskets',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/" + uuid + "/delete"
        
        with requests.Session() as s:
            rh = s.delete( URI, headers=Headers).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] + ' deletes basket "' + uuid + '"'
        else:
            i_log = request.session['user']['login'] + ' failed deleting basket "' + uuid + '"'       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/baskets')



def addBasketRule(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Baskets',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    if request.method == 'POST':     
        uuid = request.POST.get('uuid')
        
        URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/" + uuid + "/addRule"
        
        JSON = {'match':'AND', 'displayname':'', 'rules':[], 'subscribedguid':[]}
        
        if 'or' in request.POST.keys():
            JSON['match'] = 'OR'
        
        JSON['displayname'] = request.POST.get('displayname')
        
        groupList =  [ group for group in request.POST.getlist('subscribedguid')]
        JSON['subscribedguid'] = groupList
        
        indice = [ word.replace('field','') for word in request.POST.keys() if word[0:5]=='field' ]
        
        for i in indice:
            f = 'field' + i
            v = 'verb'  + i
            s = 'string' + i

            JSON['rules'].append({'field': request.POST.get(f), 'verb': request.POST.get(v), 'string': request.POST.get(s)})
        
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates basket ' + uuid + ', add rule "' + request.POST.get('displayname') + '"'   
        else:
            i_log = request.session['user']['login'] + ' failed adding rule "' + request.POST.get('displayname') + '" to basket ' +  uuid 
        
        frontOfficeLog.info(i_log)
    
    return redirect('/manage/baskets')
    
    

def getBasketRules(request,uuid):
    """
    """  
    data = {"Domains": [],  "Hosts":[]}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/" + uuid + "/getRules"
    with requests.Session() as s:
        rh = s.get( URI, headers=Headers).json()
    data['rules'] = rh['rules']

    return data



def updateBasketRule(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Baskets',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    if request.method == 'POST':     
        uuid  = request.POST.get('uuid')
        ruuid = request.POST.get('ruuid')
        
        URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/" + uuid + "/updateRule/" + ruuid
             
        JSON = {'match':'AND', 'displayname':'', 'rules':[], 'subscribedguid':[]}
        
        if 'or' in request.POST.keys():
            JSON['match'] = 'OR'
        
        JSON['displayname'] = request.POST.get('displayname')
        
        groupList =  [ group for group in request.POST.getlist('subscribedguid')]
        JSON['subscribedguid'] = groupList
        
        indice = [ word.replace('field','') for word in request.POST.keys() if word[0:5]=='field' ]
        
        for i in indice:
            f = 'field' + i
            v = 'verb'  + i
            s = 'string' + i

            JSON['rules'].append({'field': request.POST.get(f), 'verb': request.POST.get(v), 'string': request.POST.get(s)})
        
        
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates basket ' + uuid + ', update rule "' + request.POST.get('displayname') + '"'   
        else:
            i_log = request.session['user']['login'] + ' failed updating rule "' + request.POST.get('displayname') + '" to basket ' +  uuid 
        
        frontOfficeLog.info(i_log)
    
    return redirect('/manage/baskets')




def deleteRule(request, uuid,ruuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Baskets',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/basket/" + request.session['user']['domain'] + "/" + uuid + "/delRule/" + ruuid
        
        with requests.Session() as s:
            rh = s.delete( URI, headers=Headers).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] + ' deletes rule "' + ruuid + '" in basket ' + uuid
        else:
            i_log = request.session['user']['login'] + ' failed deleting rule "' + ruuid + '" in basket ' + uuid       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/baskets')
