#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
from itertools import groupby
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone

import logging
frontOfficeLog = logging.getLogger(__name__)



# promethium lib/class
import promethiumAuth.views as promethiumAuth_views

def displayTags(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Groups',
        'WEBSITE_TITLE': 'Promethium',
        'domain': request.session['user']['domain'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'owner': request.session['user']['domain'],
    }
    
    promethiumAuth_views.authPingPM(request)
    context.update(getData(request))

    return render(request, 'manage/tags.htm', context)

def updateTag(request):
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'close':  'yes',
    }
    
    promethiumAuth_views.authPingPM(request)
    if request.method == 'POST': 
        JSON = {}
        origin = "promethium-ui"
        Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
        
        URI = "http://127.0.0.1:8000/api/tags/" + request.session['user']['domain']  + "/" + request.POST.get('uuid') + "/update"
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)
       
        del(JSON['csrfmiddlewaretoken'])

        with requests.Session() as s:
            rpd = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rpd['status']:  
            i_log = request.session['user']['login'] + ' updates domain tag"' + request.POST.get('displayname') + '"'  
            frontOfficeLog.info(i_log)
            return redirect('/manage/tags')
        else:
            i_log = request.session['user']['login'] + ' failed to update domain tag "' + request.POST.get('displayname') + '"'
            context['errorText'] = rpd['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'manage/tags.htm', context)

    
  
def addTag(request):
    """
    /manage/tags/add
    """
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    URI = "http://127.0.0.1:8000/api/tags/" + request.session['user']['domain'] + "/add"


    if request.method == 'POST': 
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        del(JSON['csrfmiddlewaretoken'])

        with requests.Session() as s:
            rh = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] +  ' adds new domain tag "' + request.POST.get('displayname') + '"'
        else:
            i_log = request.session['user']['login'] +  ' failed adding domain tag "' + request.POST.get('displayname') + '"'      
        
        frontOfficeLog.info(i_log)


    return redirect('/manage/tags')
  
  

def delTag(request, uuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Groups',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/tags/" + request.session['user']['domain'] + "/" + uuid + "/delete"
        
        with requests.Session() as s:
            rh = s.delete( URI, headers=Headers).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] + ' deletes domain tag "' + uuid + '"'
        else:
            i_log = request.session['user']['login'] + ' failed deleting domain tag "' + uuid + '"'       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/tags')
                    
                    
                    
def getData(request):    
    data = {'userTags': []}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = 'http://127.0.0.1:8000/api/tags/' + request.session['user']['domain'] +'/_all' 
    
    with requests.Session() as s:
        rd = s.get( URI, data=json.dumps(data), headers=Headers).json()
    s.close()

    if rd['domainTags'] is not None:
        data = rd
   
    return data  


  

