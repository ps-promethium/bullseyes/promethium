#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
import base64
import json
import magic

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.http import JsonResponse, HttpResponseForbidden
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views


def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Groups',
        'WEBSITE_TITLE': 'Promethium',
        'domain': request.session['user']['domain'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    context.update(getHtmlDatas(request))
    return render(request, 'manage/groups.htm', context)
    
    
    
def getHtmlDatas(request):
    """
     /api/group/{domainUuid}/_all
    """  
    data = {"Groups": []}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/group/" + request.session['user']['domain'] + "/_all"
    
    with requests.Session() as s:
        rh = s.get( URI, headers=Headers).json()

    if rh['all'] is not None:
        groupName = { group['uuid']:group['displayname'] for group in  rh['all']}
        groupName['Root'] = 'Root'
        
        for group in rh['all']:
            group['parentDisplayname'] = groupName[group['parent']]
            data['Groups'].append(group)
    
    return data



def _all(request,domainUuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Groups',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    JSON = []
    if request.method == 'GET':      
        URI = "http://127.0.0.1:8000/api/group/" + domainUuid + "/_all"
        with requests.Session() as s:    
            rh = s.get( URI, headers=Headers).json()
        
        if rh['all'] is not None:
            for entry in rh['all']:
               JSON.append({'uuid': entry['uuid'] , 'displayname':entry['displayname'], 'parent': entry['parent'] })
       
    return JSON
    
    

def add(request): 
    """
    /api/group/{domainUuid}/add
    """
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    URI = "http://127.0.0.1:8000/api/group/" + request.session['user']['domain'] + "/add"

    if request.method == 'POST': 
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        del(JSON['csrfmiddlewaretoken'])
        with requests.Session() as s:    
            rh = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] +  ' adds new group "' + request.POST.get('displayname') + '"'
        else:
            i_log = request.session['user']['login'] +  ' failed adding new group "' + request.POST.get('displayname') + '"'      
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/groups')



def update(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Groups',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    if request.method == 'POST':      
        URI = "http://127.0.0.1:8000/api/group/" + request.session['user']['domain'] + "/" + request.POST.get('uuid') + "/update"
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        del(JSON['csrfmiddlewaretoken'])
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates group "' + request.POST.get('displayname') + '"'   
        else:
            i_log = request.session['user']['login'] + ' failed  updating group "' + request.POST.get('displayname') + '"'  
        
        frontOfficeLog.info(i_log)

    return redirect('/manage/groups')
    
    
    
def delete(request,uuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Groups',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    promethiumAuth_views.authPingPM(request)
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/group/" + request.session['user']['domain'] + "/" + uuid + "/delete"

        with requests.Session() as s:
            rh = s.delete( URI, headers=Headers).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] + ' deletes group "' + uuid + '"'
        else:
            i_log = request.session['user']['login'] + ' failed deleting group "' + uuid + '"'       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/groups')
