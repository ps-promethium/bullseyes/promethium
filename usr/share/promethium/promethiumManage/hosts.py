#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views


def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Hosts',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)

    context.update(getHtmlDatas(request))
    return render(request, 'manage/hosts.htm', context)
    
 
 
def add(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Hosts',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    URI = "http://127.0.0.1:8000/api/host/add"


    if request.method == 'POST': 
        
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        del(JSON['csrfmiddlewaretoken'])
        
        with requests.Session() as s:
            rh = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] + ' adds host "' + request.POST.get('displayname') + '"'
        else:
            i_log = request.session['user']['login'] + ' failed adding host "' + request.POST.get('displayname') + '"'       
        
        frontOfficeLog.info(i_log)


    return redirect('/manage/hosts')



def getHtmlDatas(request):
    """
    """  
    data = {"Domains": [],  "Hosts":[]}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/host/_all"
    rh = requests.get( URI, headers=Headers).json()

    data['Hosts'] = rh['all']
    
    host = {}
    for hst in data['Hosts']:
        host[hst['uuid']] = hst['displayname']
    
    URI = "http://127.0.0.1:8000/api/domain/_all"
    with requests.Session() as s:
        rd = s.get( URI, headers=Headers).json()
    
    for dom in rd['all']:
        dom['storeServer'] =  host[dom['storeserver']]
        dom['authServer']  =  host[dom['authserver']]
        data['Domains'].append(dom)

    return data



def update(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Hosts',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    
    if request.method == 'POST':      
        
        
        URI = "http://127.0.0.1:8000/api/host/" + request.POST.get('uuid') + "/update"
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        del(JSON['csrfmiddlewaretoken'])
      
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates host "' + request.POST.get('displayname') + '"'   
        else:
            i_log = request.session['user']['login'] + ' failed updating host "' + request.POST.get('displayname') + '"'  
        
        frontOfficeLog.info(i_log)

    return redirect('/manage/hosts')



def delete(request,uuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Hosts',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/host/" + uuid + "/delete"
        with requests.Session() as s:    
            rh = s.delete( URI, headers=Headers).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] + ' deletes host "' + uuid + '"'
        else:
            i_log = request.session['user']['login'] + ' failed deleting host "' + uuid + '"'       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/hosts')
