#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url

from django.urls import include, path

from . import views
from . import admins 
from . import baskets
from . import chrono
from . import domains 
from . import groups
from . import hosts
from . import users
from . import domainTags
from . import smtp


urlpatterns = [
    # Admin & Domain account
    url(r'^$', views.index, name='manage_index'),
    
    # Admin account only
    url(r'hosts$', hosts.index, name='manage_hosts_index'),
    url(r'hosts/add$', hosts.add, name='manage_hosts_add'),
    url(r'hosts/update$', hosts.update, name='manage_hosts_update'),
    url(r'hosts/delete/(?P<uuid>[a-zA-Z0-9\-]+)$', hosts.delete, name='manage_hosts_delete'),
    
    url(r'domains$', domains.index, name='manage_domains_index'),
    url(r'domains/add$', domains.add, name='manage_domains_add'),
    url(r'domains/update$', domains.update, name='manage_domains_update'),
    url(r'domains/updateLogo$', domains.updateLogo, name='manage_domains_updateLogo'),
    url(r'domains/updateDefaultpage$', domains.updateDefaultpage, name='manage_domains_updateDefaultpage'),
    url(r'domains/delete/(?P<uuid>[a-zA-Z0-9\-]+)$', domains.delete, name='manage_domains_delete'),
    
    url(r'admins$', admins.index, name='manage_admins_index'),
    url(r'admins/add$', admins.add, name='manage_admins_add'),
    url(r'admins/update$', admins.update, name='manage_admins_update'),
    url(r'admins/delete/(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)$', admins.delete, name='manage_admins_delete'),
    url(r'admins/resetpwd$', admins.resetPasswd, name='manage_admins_resetpwd'),
    
    # Domain account only
    url(r'baskets$', baskets.index, name='manage_baskets_index'),
    url(r'baskets/add$',baskets.add, name='manage_baskets_add'),
    url(r'baskets/addRule$',baskets.addBasketRule, name='manage_baskets_addBasketRule'),
    url(r'baskets/update$', baskets.update, name='manage_baskets_update'),
    url(r'baskets/updateRule$', baskets.updateBasketRule, name='manage_baskets_updateBasketRule'),
    url(r'baskets/delete/(?P<uuid>[a-zA-Z0-9\-]+)$', baskets.delete, name='manage_baskets_delete'),
    url(r'baskets/(?P<uuid>[a-zA-Z0-9\-]+)/delRule/(?P<ruuid>[a-zA-Z0-9\-]+)', baskets.deleteRule, name='manage_baskets_deleteRule'),

    
    # Domain chrono only
    url(r'chrono$', chrono.index, name='manage_chrono_index'),
    url(r'chrono/update$', chrono.update, name='manage_chrono_update'),
    url(r'chrono/reserve$', chrono.reserve, name='manage_chrono_reserve'),
    
    url(r'users$', users.index, name='manage_users_index'),
    url(r'users/add$', users.add, name='manage_users_add'),
    url(r'users/update$', users.update, name='manage_users_update'),
    url(r'users/delete/(?P<domainUuid>[a-zA-Z0-9\-]+)/(?P<uuid>[a-zA-Z0-9\-]+)$', users.delete, name='manage_users_delete'),
    url(r'users/resetpwd$', users.resetPasswd, name='manage_users_resetpwd'),
    
    url(r'groups$', groups.index, name='manage_groups_index'),
    url(r'groups/add$', groups.add, name='manage_groups_add'),
    url(r'groups/update$', groups.update, name='manage_groups_update'),
    url(r'groups/delete/(?P<uuid>[a-zA-Z0-9\-]+)$', groups.delete, name='manage_groups_delete'),
    url(r'groups/(?P<domainUuid>[a-zA-Z0-9\-]+)/_all', groups._all, name='manage_groups_all'),

    url(r'tags$', domainTags.displayTags),
    url(r'tags/add', domainTags.addTag),
    url(r'tags/update', domainTags.updateTag),
    url(r'tags/delete/(?P<uuid>[a-zA-Z0-9\-]+)', domainTags.delTag),
    
    url(r'smtp$', smtp.index, name='manage_smtp'),
    url(r'smtp/update', smtp.update),
]
