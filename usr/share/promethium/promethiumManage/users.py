#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| /   \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_||_____||_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
from promethiumClass.auth import _cypher
from . import groups as Groups

def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Users',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'domain': request.session['user']['domain'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'Groups': Groups._all(request,request.session['user']['domain']),
    }
    

    promethiumAuth_views.authPingPM(request)
    
    context.update(getHtmlDatas(request))

    
    return render(request, 'manage/users.htm', context)
    
    
    
def getHtmlDatas(request):
    """
    """  
    data = {'Users': None}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain'] + "/_all"
    with requests.Session() as s:
        rd = s.get( URI, headers=Headers).json()
    
    if rd['all'] is not None:
        data['Users'] = rd['all']
        
    return data



def add(request): 
    """
    /api/user/{domainUuid}/add
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Users',
        'WEBSITE_TITLE': 'Promethium',
        'domain': request.session['user']['domain'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    


    if request.method == 'POST': 
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain'] + "/add"
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        groupList              =  [ group for group in request.POST.getlist('subscribedguid')]
        JSON['subscribedguid'] = groupList

        del(JSON['csrfmiddlewaretoken'])
       
        with requests.Session() as s:
            rad = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rad['status']:                   
            i_log = request.session['user']['login'] + ' adds new domain user  "' + request.POST.get('login') + '"'
            frontOfficeLog.info(i_log)
            return redirect('/manage/users')
        else:
            i_log = request.session['user']['login'] + ' failed adding new domain user "' + request.POST.get('login') + '"'   
            frontOfficeLog.info(i_log)
            context['errorText'] = rad['errorText']
            context.update(getHtmlDatas(request))
            return render(request, 'manage/users.htm', context)
        


def update(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Users',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.POST.get('uuid') + "/details" ;
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']

        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.POST.get('uuid') + "/update"

        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        groupList              =  [ group for group in request.POST.getlist('subscribedguid')]
        JSON['subscribedguid'] = groupList

        if 'enable' not in request.POST.keys():
            JSON['enable'] = False
        
        del(JSON['csrfmiddlewaretoken'])
       
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' updates domain user "' + request.POST.get('login') + '"'  
            frontOfficeLog.info(i_log)
            return redirect('/manage/users')
        else:
            i_log = request.session['user']['login'] + ' failed to update domain user "' + request.POST.get('login') + '"'
            context['errorText'] = rh['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'manage/users.htm', context)
        
        

def delete(request,domainUuid,uuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Users',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/user/" + domainUuid + "/" + uuid + "/delete"
        
        with requests.Session() as s:
            rad = s.delete( URI, headers=Headers).json()
        
        if rad['status']:                   
            i_log = 'Deleting domain user "' + uuid + '" succeed'
        else:
            i_log = 'Deleting domain user failed'       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/manage/users')



def resetPasswd(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Users',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.POST.get('uuid') + "/details" ;
        
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']
        JSON['newpasswd'] = request.POST.get('passwd')
    
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.POST.get('uuid') + "/update"
        
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' resets '  + request.POST.get('login') + ' password'   
            frontOfficeLog.info(i_log)
            return redirect('/manage/users')
        else:
            i_log = request.session['user']['login'] + ' failed reset '  + request.POST.get('login') + 'password'
            context['errorText'] = rh['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'manage/users.htm', context)
    
    
