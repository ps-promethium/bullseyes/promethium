#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
from itertools import groupby
import json
import os
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import tempfile

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone
from django.template.loader import render_to_string

# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
import promethiumManage.groups as promethiumManage_groups




def _print(request, uuid, duuid, muuid): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'muuid':muuid, 
        'duuid':duuid,
    }
    
    promethiumAuth_views.authPingPM(request)     
        
    return render(request, 'message/print.htm', context)
    




    






    
    






