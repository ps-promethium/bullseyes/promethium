#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
from datetime import datetime
from itertools import groupby
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
import promethiumManage.groups as promethiumManage_groups


def displayForm(request,muuid):
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'muuid': muuid,
    }
    
    promethiumAuth_views.authPingPM(request)  

    return render(request, 'message/alert.htm', context)





def add(request, muuid):
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'close': 'yes',
    }
    
    promethiumAuth_views.authPingPM(request)

    if request.method == 'POST':      
        
        
        JSON = { }
        JSON['startdate'] = int(datetime.timestamp(datetime.now())) * 1000
        JSON['enddate']   = int(datetime.timestamp(datetime.now())) * 1000
        
        JSON['subject']     = request.POST.get('subject')
        JSON['accessright'] = request.POST.get('accessright')
        if len(request.POST.get('startdate')) > 0:
            JSON['startdate']   = int(datetime.timestamp(datetime.strptime(request.POST.get('startdate') , '%Y/%m/%d %H:%M'))) * 1000
        if len(request.POST.get('enddate')) > 0:
            JSON['enddate']     = int(datetime.timestamp(datetime.strptime(request.POST.get('enddate') , '%Y/%m/%d %H:%M'))) * 1000
            
        if JSON['enddate'] < JSON['startdate']:
            JSON['startdate'] = int(datetime.timestamp(datetime.strptime(request.POST.get('enddate') , '%Y/%m/%d %H:%M'))) * 1000
            JSON['enddate']   = int(datetime.timestamp(datetime.strptime(request.POST.get('startdate') , '%Y/%m/%d %H:%M'))) * 1000
            
            
        JSON['body']        = request.POST.get('body')
        JSON['uuid']        = muuid
        JSON['u_uuid']      = request.POST.get('u_uuid')
        
        
        origin = "promethium-ui"
        Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

        URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['uuid'] +'/alert/' + muuid + '/add'
        
        with requests.Session() as s:
            rd = s.post( URI, data=json.dumps(JSON), headers=Headers)
        
    return render(request, 'message/alert.htm', context)



def delete(request, uuid, muuid): 
    """
    """
    if request.method == 'GET':        
        
        promethiumAuth_views.authPingPM(request)
        
        origin = "promethium-ui"
        Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

        URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['uuid'] +'/alert/' + muuid + '/delete'       
        
        with requests.Session() as s:
            rd = s.delete( URI, headers=Headers)

    
    return redirect('/message')

    
    
    






