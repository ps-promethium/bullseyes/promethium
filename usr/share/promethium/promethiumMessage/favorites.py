#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
 
# Python lib
from itertools import groupby
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
import promethiumManage.groups as promethiumManage_groups
import promethiumUser.favorites as promethiumUser_favorites   


    

def favoritesUpdate(request, muuid):
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'close':  'yes',
        'muuid': muuid, 
        'uuid': request.session['user']['uuid'],
    }
    
    promethiumAuth_views.authPingPM(request)

    Favorites = [ node for node in request.POST.getlist('favNodes')]
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    data = {'m_uuid': muuid, 'u_uuid':request.session['user']['uuid']}
    URI = 'http://127.0.0.1:8000/api/user/' + request.session['user']['uuid'] + '/favorites/unsubscribeAll/' + muuid
    
    with requests.Session() as s:
        rd = s.delete( URI, data=json.dumps(data), headers=Headers).json()
    
    if len(Favorites)>0:    
        with requests.Session() as s:
            for f_uuid in Favorites:
                URI = 'http://127.0.0.1:8000/api/user/' + request.session['user']['uuid'] + '/favorites/' + f_uuid + '/subscribe/' + muuid
                rd = s.post( URI, headers=Headers).json()
    
    
    return render(request, 'message/favorites.htm', context)



def favoritesMod(request, muuid): 
    """
    """
    nodeList = []
    hiddenSelectorData = []
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'muuid':  muuid,
    }
    
    promethiumAuth_views.authPingPM(request) 
    allNode = promethiumUser_favorites.getData(request)

    if len(allNode) >0: 
        nodeList = makeTree(allNode['favoriteNodes'])
        for entry in allNode['favoriteNodes']:
            hiddenSelectorData.append({'uuid':entry['uuid'], 'selected':''})

    context['nodes'] = json.dumps(nodeList)
    context['hiddenSelectorData'] = hiddenSelectorData
    return render(request, 'message/favorites.htm', context)



# Src Code : https://stackoverflow.com/questions/54553328/python-convert-flat-list-of-dicts-to-hierarchy-tree
def makeTree(nodes): 
    tree = {}
    parents = set()
    root_id = 'Root'

    for node in nodes:
        uuid, puuid, displayname = node['uuid'], node['parent'], node['displayname']  
        # create a copy of emp, and add a "children" list
        
        true          = True

        tree[uuid] = { 
            'text': displayname,
            'parent': puuid,
            'selectable': true,
            'id': uuid,
            'nodes': []
        }
        parents.add(puuid)
        
        if uuid == puuid:
            # the root of the tree references itself as the manager
            root_id = uuid

    # add empty manager entries for missing manager IDs, reporting to root ID.
    for uuid in  parents - tree.keys():
        true          = True
        checkedStatus = False
        
        tree[uuid] = { 
            'text': 'Root',
            'parent': 'Root',
            'selectable': true,
            'id': uuid,
            'nodes': []
        }

    for parent, node in tree.items():
        parents = tree[node.pop('parent')]
        if parent != root_id:  # don't add the root to anything
            parents['nodes'].append(node)
    
    if root_id in tree.keys():
        return tree[root_id]['nodes']
    else: 
        return tree
