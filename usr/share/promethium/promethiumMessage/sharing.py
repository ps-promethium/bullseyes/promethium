#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
 
# Python lib
from itertools import groupby
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
import promethiumManage.groups as promethiumManage_groups


def getMessageDetail(request, muuid):    
    data = {'dispatchedguid': None}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['uuid'] +'/message/' + muuid 
    
    with requests.Session() as s:
        rd = s.get( URI, data=json.dumps(data), headers=Headers).json()
    if rd['msg'] is not None:
        data['dispatchedguid'] = rd['msg']['dispatchedguid']
        
    return data

    

def sharingsUpdate(request, muuid):
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'close':  'yes',
        'muuid': muuid, 
        'uuid': request.session['user']['uuid'],
    }
    
    promethiumAuth_views.authPingPM(request)
    Group = [ group for group in request.POST.getlist('subscribedguid')]
    
    data = { 'dispatchedguid': Group } 
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['domain'] +  '/' + request.session['user']['uuid'] +'/subscription/' + muuid 
    
    with requests.Session() as s:
        rd = s.post( URI, data=json.dumps(data), headers=Headers).json()
    
    
    return render(request, 'message/sharing.htm', context)



def sharingsMod(request, muuid): 
    """
    """
    groupList = []
    
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'muuid':  muuid,
    }
    
    promethiumAuth_views.authPingPM(request) 
    allGroup = promethiumManage_groups._all(request, request.session['user']['domain'])
    dispatchedguid = getMessageDetail(request, muuid)

    hiddenSelectorData = []
    for entry in allGroup:
        if entry['uuid'] in dispatchedguid['dispatchedguid']: 
            hiddenSelectorData.append({'uuid':entry['uuid'], 'selected':'selected'})
        else:
            hiddenSelectorData.append({'uuid':entry['uuid'], 'selected':''})
   

    groupList = makeTree(allGroup, dispatchedguid['dispatchedguid'])
    
    context['hiddenSelectorData'] = hiddenSelectorData
    context['nodes'] = json.dumps(groupList)
     
    return render(request, 'message/sharing.htm', context)




# Src Code : https://stackoverflow.com/questions/54553328/python-convert-flat-list-of-dicts-to-hierarchy-tree
def makeTree(nodes, checked): 
    tree = {}
    parents = set()
    root_id = 'Root'

    for node in nodes:
        uuid, puuid, displayname = node['uuid'], node['parent'], node['displayname']  
        # create a copy of emp, and add a "children" list
        
        true          = True
        checkedStatus = False

        if uuid in checked:
            checkedStatus = True

        tree[uuid] = { 
            'text': displayname,
            'parent': puuid,
            'selectable': true,
            'state': {'checked': checkedStatus, 'selected': checkedStatus },
            'id': uuid,
            'nodes': []
        }
        parents.add(puuid)
        
        if uuid == puuid:
            # the root of the tree references itself as the manager
            root_id = uuid

    # add empty manager entries for missing manager IDs, reporting to root ID.
    for uuid in  parents - tree.keys():
        true          = True
        checkedStatus = False
        
        tree[uuid] = { 
            'text': 'Root',
            'parent': 'Root',
            'selectable': true,
            'state': {'checked': checkedStatus, 'selected': checkedStatus },
            'id': uuid,
            'nodes': []
        }

    for parent, node in tree.items():
        parents = tree[node.pop('parent')]
        if parent != root_id:  # don't add the root to anything
            parents['nodes'].append(node)
    
    if root_id in tree.keys():
        return tree[root_id]['nodes']
    else: 
        return tree
