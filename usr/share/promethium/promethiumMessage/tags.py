#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
 
# Python lib
from itertools import groupby
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
import promethiumAuth.views as promethiumAuth_views
import promethiumUser.tags as promethiumUser_tags
import promethiumManage.domainTags as promethiumManage_domainTags



def get(request, muuid):   
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'close':  'no',
        'muuid': muuid, 
        'uuid': request.session['user']['uuid'],
    }
    
    promethiumAuth_views.authPingPM(request) 
    context.update(promethiumUser_tags.getData(request))
    #context.update(getSubscribedTags(request,muuid))
    context.update(promethiumManage_domainTags.getData(request))
    
    return render(request, 'message/tag.htm', context)


def getSubscribedTags(request,muuid):
    data = {'subscribedTags': None}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['uuid'] +'/tag/' + muuid 
    
    with requests.Session() as s:
        rd = s.get( URI, data=json.dumps(data), headers=Headers).json()
        if rd['tags'] is not None:
            data['subscribedTags'] = rd['tags']

    return data
    
    

def upsert(request, muuid):
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'close':  'yes',
        'muuid': muuid, 
        'uuid': request.session['user']['uuid'],
    }
    
    data = {'subscribedtags': []}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    if len(request.POST.getlist('subscribedtags'))>0: 
        data['subscribedtags'] = request.POST.getlist('subscribedtags')
        
    with requests.Session() as s:
        if len(request.POST.get('newTag'))>0:
            URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['uuid'] + "/tags/add"
            JSON = {'displayname': request.POST.get('newTag')} 
            rh = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
            
            URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['uuid'] + "/tags/uuidFromName"
            res_uuid = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
            if res_uuid['uuid'] is not None:
                data['subscribedtags'].append(res_uuid['uuid'])

        if len(data['subscribedtags'])>0:
            URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['uuid'] +'/tag/' + muuid + '/upsert'
            s.post( URI, headers=Headers, data=json.dumps(data))
        
    return render(request, 'message/tag.htm', context)





