#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*
try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url

from django.urls import include, path


from . import views
from . import sharing
from . import alert
from . import comment
from . import _print
from . import delete
from . import favorites
from . import tags
from . import forward

urlpatterns = [
    #path('', views.index, name='index'),
    url(r'^$', views.index, name='index'),
    url(r'^(?P<muuid>[a-zA-Z0-9\-]+)$', views.index, name='index'),
    
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/sharings/modify', sharing.sharingsMod, name='mod_sharings'),
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/sharings/update', sharing.sharingsUpdate, name='update_sharings'),
    
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/favorites/modify', favorites.favoritesMod, name='add_favorites'),
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/favorites/update', favorites.favoritesUpdate, name='update_favorites'),
    
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/alert/addForm', alert.displayForm, name='alert_displayForm'),
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/alert/add', alert.add, name='alert_add'),
    url(r'(?P<uuid>[a-zA-Z0-9\-]+)/alert/(?P<muuid>[a-zA-Z0-9\-]+)/delete', alert.delete, name='alert_delete'),
    
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/comment/addForm', comment.displayForm, name='comment_displayForm'),
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/comment/add', comment.add, name='comment_add'),
    url(r'(?P<uuid>[a-zA-Z0-9\-]+)/comment/(?P<muuid>[a-zA-Z0-9\-]+)/delete', comment.delete, name='comment_delete'),
    
    url(r'(?P<uuid>[a-zA-Z0-9\-]+)/print/(?P<duuid>[a-zA-Z0-9\-]+)/(?P<muuid>[a-zA-Z0-9\-]+)', _print._print, name='_print_print'),
    
    #url(r'(?P<uuid>[a-zA-Z0-9\-]+)/forward/(?P<muuid>[a-zA-Z0-9\-]+)/forwardForm$', forward.forwardForm, name='forward_displayForm'),
    url(r'(?P<uuid>[a-zA-Z0-9\-]+)/forward/(?P<muuid>[a-zA-Z0-9\-]+)/send', forward.forward, name='forward_forward'),
    url(r'(?P<uuid>[a-zA-Z0-9\-]+)/forward/(?P<muuid>[a-zA-Z0-9\-]+)/forwardForm', forward.forwardForm, name='forward_displayForm'),    
        
    url(r'(?P<uuid>[a-zA-Z0-9\-]+)/delete/(?P<muuid>[a-zA-Z0-9\-]+)', delete.delete, name='delete_delete'),
    
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/tag$', tags.get, name='tags_get'),
    
    #url(r'(?P<muuid>[a-zA-Z0-9\-]+)/tag/addForm', tags.displayForm, name='tags_displayForm'),
    url(r'(?P<muuid>[a-zA-Z0-9\-]+)/tag/upsert', tags.upsert, name='tags_upsert'),
    
]
