#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
from itertools import groupby
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import logging
frontOfficeLog = logging.getLogger(__name__)

# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone


# promethium lib/class
from promethiumClass.auth import _control
import promethiumAuth.views as promethiumAuth_views
import promethiumManage.groups as promethiumManage_groups
import promethiumUser.favorites as promethiumUser_favorites
import promethiumHome.views as promethiumHome_views
import promethiumUser.tags as promethiumUser_tags
import promethiumManage.domainTags as promethiumManage_domainTags

def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'subscribedguid': [],
    }
    
    promethiumAuth_views.authPingPM(request)   
    context.update(getHtmlDatas(request))   
    
    if request.session['user']['subscribedguid'] is not None:
        context['subscribedguid'] = request.session['user']['subscribedguid']
    
    context['favoriteNodes']        = promethiumUser_favorites.constructFavoriteTree(request)  
    context['favoriteNodesLength']  = len(context['favoriteNodes'])
    context['forwardEnabled']       = isSmtpEnabled(request)
    context['hiddenItems']          = getUserHiddenItems(request)
 
    context.update(promethiumHome_views.getAlerts(request))
    context['alertsArrayLength'] = len(context['alerts'])
    context.update(promethiumUser_tags.getData(request))
    context.update(promethiumManage_domainTags.getData(request))

    return render(request, 'message/messages.htm', context)



def getUserHiddenItems(request):
    hiddenItems = []
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
                 
    URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/details" ;
    
    with requests.Session() as s:
        ru = s.get( URI, headers=Headers).json()
    
    usr = ru['details'][0]['user']
    if 'hideItems' in usr.keys():
        hiddenItems = usr['hideItems']
        
    return hiddenItems



def isSmtpEnabled(request):
    enabled = False
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = 'http://127.0.0.1:8000/api/domain/' + _control._control().retrieveUserElementFromToken(request.session['token'])['domain'] +'/smtp/enabled'
    try:
        with requests.Session() as s:
            rd = s.get(URI, headers=Headers).json()
        if rd['enabled'] is not None:
            enabled = rd['enabled']
    except:
        pass
    return enabled
    
    


def getHtmlDatas(request):    
    data = {'Messages': None}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    if 'nbOfDays' in request.session['user'].keys():
        data = {'nbOfDays':request.session['user']['nbOfDays']}
    else:
        data = {}

    URI = 'http://127.0.0.1:8000/api/message/' + request.session['user']['uuid'] +'/message/_bydate'
    
    try:
        with requests.Session() as s:
            rd = s.post( URI, data=json.dumps(data), headers=Headers).json()
        if rd['msg'] is not None:
            data['Messages'] = rd['msg']
    except:
        pass
    return data




    






    
    






