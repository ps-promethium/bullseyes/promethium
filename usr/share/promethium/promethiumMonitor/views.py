#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone

import logging
frontOfficeLog = logging.getLogger(__name__)

# promethium lib/class
import promethiumAuth.views as promethiumAuth_views


def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Monitor',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'domainUuid': request.session['user']['domain'],
    }
   
    promethiumAuth_views.authPingPM(request)
    context['domainCounters'] =  getHtmlDatas(request)
    context['dbstat'] = getDbStat(request)['stat']
    return render(request, 'monitor/monitor.htm', context)


def getHtmlDatas(request):
    """
    """  
    domain = ""
    data = None
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/monitor/" + request.session['user']['domain']
    try:
        with requests.Session() as s:
            data = s.get( URI, headers=Headers).json()
    except:
        pass
    return data


def getDbStat(request):
    """
    """  
    domain = ""
    data = None
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}

    URI = "http://127.0.0.1:8000/api/monitor/" + request.session['user']['domain'] + "/dbstat"
    try:
        with requests.Session() as s:
            data = s.get( URI, headers=Headers).json()
    except:
        pass
    return data



