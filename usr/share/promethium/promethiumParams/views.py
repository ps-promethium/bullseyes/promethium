#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
from collections import defaultdict
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone

import logging
frontOfficeLog = logging.getLogger(__name__)

# promethium lib/class
from promethiumClass.config import config
import promethiumAuth.views as promethiumAuth_views
from . import timeZone

def index(request): 
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'user': request.session['user'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'timeZoneList': timeZone.timeZoneList,
        'availableLanguage': getAvailableLanguageList(request),
        'TIME_ZONE': request.session['user_time_zone'],
        'lang': request.session['user']['lang'],
        'theme': request.session['user']['theme'],
        'defaultNbOfDays': config.config().user['nbOfDays'],
        'checked': getHiddenItems(request),
    }
    promethiumAuth_views.authPingPM(request)
    return render(request, 'params/params.htm', context)



def getHiddenItems(request):
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
                 
    URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/details" ;
    
    with requests.Session() as s:
        ru = s.get( URI, headers=Headers).json()
    
    usr = ru['details'][0]['user']
    checked = defaultdict(dict)
    if 'hideItems' in usr.keys():
        for item in usr['hideItems']:
            checked[item] = "checked"
    
    return checked


def getAvailableLanguageList(request): 
    """
    
    """
    availableLanguage = {}
    for ln in settings.LANGUAGES:
         availableLanguage[ln[0]] = ln[1]
        
    return availableLanguage



def setTimezone(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/details" ;
        
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']
        JSON['timez'] = request.POST.get('timez')
    
    
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/update"
        
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
    
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' sets its own new timezone'   
            frontOfficeLog.info(i_log)
            
            timezone.activate(request.POST.get('timez'))
            request.session['user_time_zone'] = request.POST.get('timez')   
            
            return redirect('/params')
        else:
            i_log = request.session['user']['login'] + ' failed setting its own new timezone'
            context['errorText'] = rh['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'params/params.htm', context)



def setLang(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/details" ;
        
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']
        JSON['lang'] = request.POST.get('lang')
    
        
    
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/update"
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' sets its own new language' 
            frontOfficeLog.info(i_log)
            
            translation.activate(request.POST.get('lang'))
            request.session[translation.LANGUAGE_SESSION_KEY] = request.POST.get('lang') 
            
            return redirect('/params')
        else:
            i_log = request.session['user']['login'] + ' failed setting its own new language'
            context['errorText'] = rh['text']
            frontOfficeLog.info(i_log)
            return render(request, 'params/params.htm', context)
        
        
        
def resetPasswd(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/details" ;

        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']
        JSON['newpasswd'] = request.POST.get('passwd')
    
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/update"
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:  
            i_log = request.session['user']['login'] + ' resets its own password'   
            frontOfficeLog.info(i_log)
            return redirect('/params')
        else:
            i_log = request.session['user']['login'] + ' failed resetting its own password'
            context['errorText'] = rh['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'params/params.htm', context)



def setNbOfDays(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/details" ;
        
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']
        JSON['nbOfDays'] = request.POST.get('nbOfDays')
    
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/update"
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        if rh['status']:  
            i_log = request.session['user']['login'] + ' sets its new nbOfDays value' 
            frontOfficeLog.info(i_log)    
            request.session['user']['nbOfDays'] = request.POST.get('nbOfDays')
            return redirect('/params')
        else:
            i_log = request.session['user']['login'] + ' failed setting its new setNbOfDays value' 
            context['errorText'] = rh['text']
            frontOfficeLog.info(i_log)
            return render(request, 'params/params.htm', context)



def hideItems(request):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'POST':             
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/details" ;

        items = [ i for i in request.POST.keys() ]
        items.remove('csrfmiddlewaretoken')
        
        with requests.Session() as s:
            ru = s.get( URI, headers=Headers).json()
        
        JSON = ru['details'][0]['user']
        JSON['hideItems'] = items       
        
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['domain']  + "/" + request.session['user']['uuid'] + "/update"
        with requests.Session() as s:
            rh = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        if rh['status']:  
            i_log = request.session['user']['login'] + ' sets/updates its messages items' 
            frontOfficeLog.info(i_log)    
            return redirect('/params')
        else:
            i_log = request.session['user']['login'] + ' failed setting/updating its new  messages items' 
            context['errorText'] = rh['text']
            frontOfficeLog.info(i_log)
            return render(request, 'params/params.htm', context)
