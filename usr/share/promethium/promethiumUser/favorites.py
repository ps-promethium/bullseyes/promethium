#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

 
# Python lib
from itertools import groupby
import json

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# Django lib
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.conf import settings
from django.utils import translation, timezone

import logging
frontOfficeLog = logging.getLogger(__name__)



# promethium lib/class
import promethiumAuth.views as promethiumAuth_views


def constructFavoriteTree(request):
    """
    """
    allNode = []
    
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    allNode = getData(request)
    if len(allNode) >0: 
        nodeList = makeTree(allNode['favoriteNodes'])
    
    return nodeList


def updateFavoritesNode(request):
    context = {
        'PAGE_TITLE': 'Messages',
        'WEBSITE_TITLE': 'Promethium',
        'user': request.session['user'],
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
        'close':  'yes',
    }
    
    promethiumAuth_views.authPingPM(request)
    if request.method == 'POST': 
        
        origin = "promethium-ui"
        Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
        
        
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['uuid']  + "/favorites/" + request.POST.get('uuid') + "/details" ;
        with requests.Session() as s:
            rgd = s.get( URI, headers=Headers).json()
        
        JSON = rgd['details']

        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['uuid']  + "/favorites/" + request.POST.get('uuid') + "/update"

        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)
       
        del(JSON['csrfmiddlewaretoken'])
       
        with requests.Session() as s:
            rpd = s.post( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rpd['status']:  
            i_log = request.session['user']['login'] + ' updates user favorite"' + request.POST.get('displayname') + '"'  
            frontOfficeLog.info(i_log)
            return redirect('/user/favorites')
        else:
            i_log = request.session['user']['login'] + ' failed to update user favorite "' + request.POST.get('displayname') + '"'
            context['errorText'] = rh['text']
            context.update(getHtmlDatas(request))
            frontOfficeLog.info(i_log)
            return render(request, 'user/favorites.htm', context)

    
  
def addFavoritesNode(request):
    """
    /api/group/{domainUuid}/add
    """
    context = {
        'PAGE_TITLE': 'Authentication',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['uuid'] + "/favorites/add"


    if request.method == 'POST': 
        
        
        JSON = { }
        for key in request.POST.keys():
            JSON[key] = request.POST.get(key)

        del(JSON['csrfmiddlewaretoken'])
        with requests.Session() as s:
            rh = s.put( URI, headers=Headers, data=json.dumps(JSON)).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] +  ' adds new favorites node "' + request.POST.get('displayname') + '"'
        else:
            i_log = request.session['user']['login'] +  ' failed adding new favorites node "' + request.POST.get('displayname') + '"'      
        
        frontOfficeLog.info(i_log)


    return redirect('/user/favorites')
  
  

def delFavoritesNode(request, uuid):
    """
    """
    if not 'user' in request.session.keys():
        return redirect('/') 
    
    context = {
        'PAGE_TITLE': 'Manage Groups',
        'WEBSITE_TITLE': 'Promethium',
        'role': request.session['user']['role'],
        'token': request.session['token'],
        'domainName' : request.session['domainName'],
        'logo': request.session['logo'],
        'lang': request.session['user']['lang'],
    }
    
    promethiumAuth_views.authPingPM(request)
    
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    
    if request.method == 'GET':       
        URI = "http://127.0.0.1:8000/api/user/" + request.session['user']['uuid'] + "/favorites/" + uuid + "/delete"
        
        with requests.Session() as s:
            rh = s.delete( URI, headers=Headers).json()
        
        if rh['status']:                   
            i_log = request.session['user']['login'] + ' deletes favorite "' + uuid + '"'
        else:
            i_log = request.session['user']['login'] + ' failed deleting favorite "' + uuid + '"'       
        
        frontOfficeLog.info(i_log)
        
    return redirect('/user/favorites')
                    
                    
                    
def getData(request):    
    data = {'favoriteNodes': []}
    origin = "promethium-ui"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-PM-ApiKey':request.session['token']}
    
    URI = 'http://127.0.0.1:8000/api/user/' + request.session['user']['uuid'] +'/favorites/_all' 
    
    with requests.Session() as s:
        rd = s.get( URI, data=json.dumps(data), headers=Headers).json()
    
    s.close()
    
    if rd['favoriteNodes'] is not None:

        nodeName = { node['uuid']:node['displayname'] for node in  rd['favoriteNodes']}
        nodeName['Root'] = 'Root'
        
        for node in rd['favoriteNodes']:

            if node['parent'] in nodeName.keys(): 
                node['parentDisplayname'] = nodeName[node['parent']]              
                data['favoriteNodes'].append(node)
   
    return data  


  
# Src Code : https://stackoverflow.com/questions/54553328/python-convert-flat-list-of-dicts-to-hierarchy-tree
def makeTree(nodes):
    tree = {}
    parents = set()
    root_id = 'Root'

    for node in nodes:
        uuid, puuid, displayname = node['uuid'], node['parent'], node['displayname']  
        # create a copy of emp, and add a "children" list
        
        #if node['parent'] is None:
            #puuid = 'Root'
        
        tree[uuid] = { 'text': displayname,
        'href': '#',
        'parent': puuid,
        'onClick': 'getFavoriteMsgList(\'' + uuid +'\')',
        'idRef': uuid,
        'nodes': []
        }
        parents.add(puuid)
        
        if uuid == puuid:
            # the root of the tree references itself as the manager
            root_id = uuid

    # add empty manager entries for missing manager IDs, reporting to root ID.
    for uuid in  parents - tree.keys():
        tree[uuid] = { 'text': 'Root',
        'href': '#',
        'parent': 'Root',
        'onClick': 'getFavoriteMsgList(\'' + uuid +'\')',
        'idRef': uuid,
        'nodes': []
        }

    for parent, node in tree.items():
        parents = tree[node.pop('parent')]
        if parent != root_id:  # don't add the root to anything
            parents['nodes'].append(node)
    
    if root_id in tree.keys():
        return tree[root_id]['nodes']
    else: 
        return tree

