#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

try:
	from django.conf.urls import url
except:
	from django.urls import re_path as url

from django.urls import include, path

from . import views
from . import favorites
from . import tags

urlpatterns = [
    
    url(r'favorites$', views.displayFavorites),
    url(r'favorites/constructTree', favorites.constructFavoriteTree),
    url(r'favorites/addNode', favorites.addFavoritesNode),
    url(r'favorites/updateFavoritesNode', favorites.updateFavoritesNode),
    url(r'favorites/deleteNode/(?P<uuid>[a-zA-Z0-9\-]+)', favorites.delFavoritesNode),

    url(r'tags$', views.displayTags),
    url(r'tags/add', tags.addTag),
    url(r'tags/update', tags.updateTag),
    url(r'tags/delete/(?P<uuid>[a-zA-Z0-9\-]+)', tags.delTag),
]
