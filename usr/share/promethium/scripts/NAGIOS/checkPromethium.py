#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


"""
NAGIOS Script : check promethium  
    
Installation:
    This script needs additional lib :
        requests (Cmd install : pip install requests)
    
    
After :
    Status cases  returned :
        0 : OK       : The WEB API and mongodb respond well
        2 : CRITICAL : Unavailable to connect to the server
        2 : CRITICAL : Connected to server, but some error on mongodb

Usage :
    python3 checkPromethium.py [ -s|--promethiumServer ] [ -p|--httpProto ] [ -v|--verifyPeer ]
    
    By default : 
        -s|--promethiumServer set to "127.0.0.1:8000" (aka the internal access)
        -p|--httpProto  set to http
        -v|--verifyPeer set to False (does not verify peer server certificate)

        
    Example : 
        python3 checkPromethium.py --promethiumServer="promethium.intra"
        
    About Nagios:
        if some NRPE agent is installed on Promethium Web Server, you can add the line above in "nrpe commands" file :
        
            command['checkPromethium'] = /usr/share/promethium-p3/bin/python3 /usr/share/promethium/scripts/NAGIOS/checkPromethium.py
    
        And reload agent after
"""


# Python lib
import base64
from collections import defaultdict
import getopt
import json
import os
import requests 

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import sys
import urllib




"""
PARAMS : init the default params in this script 

"""
params = defaultdict(dict)
params = {'httpProto': "http",
          'verifyPeer': False, #Check or not the server certificate
          'promethiumServer': '127.0.0.1:8000',
          }

"""
getopt
"""
options, remainder = getopt.getopt(sys.argv[1:], 's:p:v:', 
                                   ['promethiumServer=', 
                                    'httpProto=',
                                    'verifyPeer=',
                                    ])



for opt, arg in options:
    if opt in ('-s', '--promethiumServer'):
        params['promethiumServer'] = arg
        
    elif opt in ('-p', '--httpProto'):
        params['httpProto'] = arg
        
    elif opt in ('-v', '--verifyPeer'):
        params['verifyPeer'] = arg

"""
CLASS

"""

class checkPromethium():
    def __init__(self):
        self.params = {}

    def session(self):
        self.s = requests.Session()

    def setParams(self,**kwargs):
        if 'promethiumServer' in kwargs.keys():
            self.params['promethiumServer'] = kwargs['promethiumServer']

        if 'httpProto' in kwargs.keys():
            self.params['httpProto'] = kwargs['httpProto']

        if 'verifyPeer' in kwargs.keys():
            self.params['verifyPeer'] = kwargs['verifyPeer']


    def callAPIControl(self): 
        STATUS = [ None, None]

        Verify   = self.params['verifyPeer']
        Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'}
        base = self.params['httpProto'] + '://' + self.params['promethiumServer'] + '/api'
        getUri = '/monitor/controlAccessToDb'

        try:
            test = self.s.get( base + getUri,verify=Verify, headers=Headers).json()
            if 'dbAccess' in test.keys():
                if 'OK' in test['dbAccess']:
                    STATUS = ['OK', "Promethium Infra is OK"]
                else:
                    STATUS = ['CRITICAL', "connected to API but the mongoDb does not respond"]
        except requests.exceptions.HTTPError as errh:
            STATUS = [None, errh]
        except requests.exceptions.ConnectionError as errc:
            STATUS = [None, errc]
        except requests.exceptions.Timeout as errt:
            STATUS = [None, errt]
        except requests.exceptions.RequestException as err:
            STATUS = [None, err]
            
        return STATUS



"""
Call the test
"""

pm = checkPromethium()
pm.session()
pm.setParams(**params)
tcs = pm.callAPIControl()


# Case of no access to API
if tcs[0] is None:
    print("CRITICAL : " + str(tcs[1]))
    exit(2)


# Case of safe instance
if tcs[0] == 'OK':
    print("OK : The instance is safe, " + str(tcs[1]))
    exit(0)

    
# Case of mongodb error
if tcs[0] == 'CRITICAL':
    print("CRITICAL : An error occured, " + str(tcs[1]))
    exit(2)
