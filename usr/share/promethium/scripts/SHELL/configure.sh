#!/bin/bash

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


version=1.0.0

#
# Because we need administrative rights
#

if [ $(whoami) != "root" ] ; then echo "This script must be launched with superuser administrative rights ! Bye" ; exit 1 ;  fi




############################################################################################################################################################################################# 
# Local definition                                                                                                                                                                          #
############################################################################################################################################################################################# 
HOSTNAME=$(hostname)
FQDN=$(hostname -f)
LOCALIP=$(hostname -I |cut -d" " -f1)

PRIMARYNODE=""
INSTALLTYPE=""

#############################################################################################################################################################################################             
# Options list                                                                                                                                                                              #
############################################################################################################################################################################################# 

TEMP=`getopt -o fdp:F:n:P:hvC -l full,distributed,pmNodes:,FQDN:,nosqlNodes:nosqlPromethiumPw:,help,version,checkenv -n "$(basename $0)" -- "$@"`
if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

eval set -- "$TEMP"

esc=$(printf '\033')

#############################################################################################################################################################################################             
# Local functions                                                                                                                                                                           #
############################################################################################################################################################################################# 

#
# Display "version" 
#
version () {
        echo -e ""
        echo -e "\tAbout \"$(basename $0)\" : "
        echo -e "\t\tVersion : $version"
        echo -e "\t\tLicense : GPL V3"
        echo -e "\t\tAuteur  : Pascal SALAUN <pascal.salaun@interieur.gouv.fr>"
        echo -e ""
}


#
# Display "usage" 
#
usage () {
        echo -e "\n  USAGE of \"$(basename $0)\"\n"
        echo -e ""
        echo -e "\t-v, --version : Display the version "
        echo -e ""
        echo -e "\t-h, --help || ? : Display this help"
        echo -e ""
        echo -e "\t-C, --checkenv || ? : check this environment"
        echo -e ""
        echo -e "\tOtherwise, you have to use it as :"
        echo -e "\t $0  -f|--full -d|--distributed -P|--nosqlPromethiumPw -F|--FQDN[ -p|--pmNodes -c|--nosqlNodes ]"
                
        echo -e "" 
        echo -e "\tWhere"
      
        echo -e "" 
        echo -e "\t -f|--full :"
        echo -e "\t\t\t in case you want install all stuff on this server. Suppose promethium-mongodb already installed "  
        
        echo -e "" 
        echo -e "\t -d|--distributed :"
        echo -e "\t\t\t in case you want install all necessary stuff on this server Promethium node"  
        echo -e "\t\t\t It supposes you've already a NoSQL cluster on line"
        
        echo -e "" 
        echo -e "\t -P|--nosqlPromethiumPw :"
        echo -e "\t\t\t is the NoSQL promethium account Password"  
        
        echo -e "" 
        echo -e "\t -p|--pmNodes :"
        echo -e "\t\t\t is the list of Promethium nodes (FQDN)"  
        
        echo -e "" 
        echo -e "\t -n|--nosqlNodes :"
        echo -e "\t\t\t is the list of NoSQL nodes (FQDN))"  
        
        echo -e "" 
        echo -e "\t -F|--FQDN :"
        echo -e "\t\t\t is the FQDN of your public access FQDN" 
                
}

#
# Display few env elements
#
checkEnv () {
    
        firewalldStatus=$(service firewalld status |grep -v grep| grep Active: > /tmp/1 ; cat /tmp/1 | tr -s " " | sed "s/Active: //" ; rm -f /tmp/1)
        selinuxConf=$(grep -e '^SELINUX=' /etc/sysconfig/selinux | cut -d "=" -f2)
        
        
        if [ -z "$selinuxConf" ] ; then 
                selinuxConf='Not set'
        fi
        
        Java=$(java -version 2>&1 >/dev/null | grep 'version' | awk '{print $3}')
        echo -e ""
        echo -e "\t Please, before configuring Filebeat, check if these values are correct "
        echo -e ""
        echo -e "\t\t hostname        : $hostname"
        echo -e "\t\t firewalld       :$firewalldStatus"
        echo -e "\t\t SELINUX         : $selinuxConf"
        echo -e ""

}

# 
# stip all spaces (beginning, ending, multiple) 
#
stripSpaces () {
    CMD="echo \"$1\"" 
    CMD=$CMD" | tr -s ' ' "
    CMD=$CMD" | sed -e 's/^ //' -e 's/ $//'"
    eval $CMD
}


#
# Check if this host is primary Promethium node.
# In this case, will do some stuff (creating indexex...)
#
controlIfPrimaryNode () {
    if [[ "$LOCALIP" == *"$PRIMARYNODE"* ]] || [[ "$FQDN" == *"$PRIMARYNODE"* ]] ; then
        echo 0
    else
        echo 1
    fi
}


#
# Configure NoSQL
#


#
# Configure memcached
#
configureMemcached() {
    echo  "Configuring memcached"

    mv /etc/memcached.conf /etc/memcached.conf-$(date +%F).tpl
    cp /usr/share/promethium/scripts/templates/etc/memcached.conf.tpl /etc/memcached.conf

    systemctl enable memcached
    systemctl restart memcached

}


testIfMemcacheWellWritten() {
    T=$(echo "$MEMCACHESERVERSLIST" | grep -c 12111)
    if [ "${T}" = "0" ] ;  then
        return 1
    else
        return 0
    fi
}
#
# Configure promethium
#
configurePromethium() {  
    NOSQLCYPHERKEY=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-64};echo;)
    CSCYPHERKEY=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-64};echo;)
    CSRF_TRUSTED_ORIGINS="'http://$HOSTNAME', 'https://$HOSTNAME', 'http://$FQDN', 'https://$FQDN', 'http://$LOCALIP', 'https://$LOCALIP', 'http://$FRONTFQDN', 'https://$FRONTFQDN',"

    
    
    if [ ! -f "/usr/share/promethium/scripts/SHELL/pmctl.sh" ] ; then
       cp /usr/share/promethium/scripts/SHELL/pmctl.sh.sample /usr/share/promethium/scripts/SHELL/pmctl.sh
    fi
    
    if [ ! -h "/usr/bin/pmctl" ] ; then
        ln -s /usr/share/promethium/scripts/SHELL/pmctl.sh /usr/bin/pmctl
    fi
    
    
    echo  "Configuring Promethium"
    if [ "$INSTALLTYPE" == "FULL" ]  ; then
        MEMCACHESERVERSLIST="$FQDN:11211"
        NOSQLSERVERSLIST=$FQDN
    fi

    if  [ "$INSTALLTYPE" == "DISTRIBUTED" ]  ; then
        echo "File /etc/promethium/promethium.yml must be the same on each promethium node"  
        MEMCACHESERVERSLIST=$(echo $PMNODES|sed "s/ /:11211','/")
        NOSQLSERVERSLIST=$(echo $NOSQLSERVERS|sed "s/ /','/")
        if ! testIfMemcacheWellWritten ; then 
            MEMCACHESERVERSLIST="$MEMCACHESERVERSLIST:11211"
        fi 
        
        CMD="sed -i "     
        CMD=$CMD" -e '/#FULL/d' "
        CMD=$CMD" /usr/share/promethium/scripts/SHELL/pmctl.sh "
        eval $CMD  
        
    fi
    
    echo "File : /etc/promethium/promethium.yml"
    if [ -f /etc/promethium/promethium.yml ] ; then
        mv /etc/promethium/promethium.yml /etc/promethium/promethium-$(date +%F).yml
    fi
    cp /etc/promethium/promethium.yml.sample /etc/promethium/promethium.yml
    
    CMD="sed -i "
    CMD=$CMD" -e 's;%%NOSQLCYPHERKEY%%;$NOSQLCYPHERKEY;'"
    CMD=$CMD" -e 's;%%NOSQLSERVERS%%;$NOSQLSERVERSLIST;'"
    CMD=$CMD" -e 's;%%NOSQLPROMETHIUMPWD%%;$NOSQLPROMETHIUMPWD;'"  
    CMD=$CMD" -e 's;%%MEMCACHESERVERSLIST%%;$MEMCACHESERVERSLIST;'" 
    CMD=$CMD" /etc/promethium/promethium.yml"
    eval $CMD    
     
     
    for f in `cd /usr/share/promethium/promethium ; ls *.sample` ; do 
        echo "File : /usr/share/promethium/promethium/"$f
        F=${f%.*}
        CMD="cp /usr/share/promethium/promethium/$f /usr/share/promethium/promethium/$F"             
        eval $CMD
    
        CMD="sed -i "
        CMD=$CMD" -e 's;%%CSCYPHERKEY%%;$CSCYPHERKEY;'"
        CMD=$CMD" -e 's;%%NOSQLSERVERS%%;$NOSQLSERVERS;'"  
        CMD=$CMD" -e 's;%%MEMCACHESERVERSLIST%%;$MEMCACHESERVERSLIST;'" 
        CMD=$CMD" -e \"s;%%CSRF_TRUSTED_ORIGINS%%;$CSRF_TRUSTED_ORIGINS;\""
        CMD=$CMD" /usr/share/promethium/promethium/$F "
        eval $CMD    
    done
        
}



loadNoSQLObjects() {
    echo -e "Loading now all primary objects on mongoDb, as collections, indexes, admin0@global.virt account... "
    CMD="/usr/share/promethium-p3/bin/python3 /usr/share/promethium/scripts/mapping/_nosql/setPrimaryData.py"
    eval $CMD

}



configureNginx() {

    echo "Configure Nginx"
    cd /etc/nginx/sites-enabled
    rm -f default
    ln -s /etc/nginx/sites-available/promethium.conf
    
    service nginx reload
    
    systemctl disable nginx

}



configueRsyslog() {
    DATE=$(date +%F_%H-%M-%S)
    sed -i.promethium.${DATE} '/^\*\.\*.*local5.none.*/!s/\*\.\*;/\*\.\*;local5.none;/' /etc/rsyslog.conf

    if diff -q /etc/rsyslog.conf /etc/rsyslog.conf.promethium.${DATE} ; then
        rm -f /etc/rsyslog.conf.promethium.${DATE}
    fi

    FileOwner=$(grep FileOwner /etc/rsyslog.conf|cut -d " " -f2)
    FileGroup=$(grep FileGroup /etc/rsyslog.conf|cut -d " " -f2)
    CMD="chown $FileOwner.$FileGroup -R /var/log/promethium"
    eval $CMD

    echo ""
    echo "Redirecting Logs from local5" 
    if [ ! -f /etc/rsyslog.d/10-promethium.conf ] ; then
        cp /usr/share/promethium/scripts/templates/etc_rsyslogd/10-promethium.conf.tpl /etc/rsyslog.d/10-promethium.conf
        echo "You can configure /etc/rsyslog.d/10-promethium.conf with your own values"
    fi
    service rsyslog restart


    echo ""
    echo "Define log rotation " 
    if [ ! -f /etc/logrotate.d/promethium ] ; then
        cp /usr/share/promethium/scripts/templates/etc_logrotated/promethium.tpl /etc/logrotate.d/promethium
        echo "You can configure /etc/logrotate.d/promethium with your own values"
    fi

}

# 
# MAIN 
#
while true ; do
    case "$1" in
        -v | --version )
            version
            exit ;;
            
        ? | -h | --help )
            usage
            exit ;;
            
        -C | --checkenv )
            checkEnv
            exit ;;

        -f | --full)
            INSTALLTYPE="FULL"; shift ;;
            
        -d | --distributed)
            INSTALLTYPE="DISTRIBUTED"; shift ;;
            
        -p | --pmNodes)
            PMNODES=$(stripSpaces "$(echo $2)") ; shift 2 
            PRIMARYNODE=$(echo "$PMNODES"|cut -d " " -f1) ;;
               
        -n | --nosqlNodes)
            NOSQLSERVERS=$(stripSpaces "$(echo $2)") ; shift 2 ;;
            
        -P | --nosqlPromethiumPw)
            NOSQLPROMETHIUMPWD=$(stripSpaces "$(echo $2)") ; shift 2 ;;        
       
        -F|--FQDN)
            FRONTFQDN=$(stripSpaces "$(echo $2)") ; shift 2 ;;
            
            
        --) shift; break ;;
        *) break ;;
    esac
done



if [ "$INSTALLTYPE" == "" ] ; then
    usage
    exit 1 
fi



if [ "$INSTALLTYPE" == "FULL" ] ; then
    configureMemcached
    configurePromethium
    loadNoSQLObjects
    configureNginx
    configueRsyslog
fi

if [ "$INSTALLTYPE" == "DISTRIBUTED" ] ; then
    configureMemcached
    configurePromethium
    configureNginx
    configueRsyslog
    if controlIfPrimaryNode ; then
        loadNoSQLObjects
    fi

fi

systemctl enable pmctl


echo  ""
echo -e "To manage all services, use one of these commands :" 
echo -e "\t service pmctl {start|stop|status|restart} "
echo -e "\t systemctl {start|stop|status|restart} pmctl"

exit 0
