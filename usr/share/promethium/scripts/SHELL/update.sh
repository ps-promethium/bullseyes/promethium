#!/bin/bash

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


VERSION=$(dpkg -l promethium |grep promethium|tr -s " " |cut -d " " -f3)
echo $VERSION > /usr/share/promethium/version

if [ -f /etc/logrotate.d/promethium ] ; then
    chmod 644 /etc/logrotate.d/promethium
fi


if [ -f /etc/rsyslog.d/10-promethium.conf ] ; then
    chmod 644 /etc/rsyslog.d/10-promethium.conf
    systemctl restart rsyslog 
fi


source /usr/share/promethium-p3/bin/activate
python -m pip install --upgrade pip
pip3 install -r /usr/share/promethium/scripts/SHELL/requirements.pip


sed -i 's/tika-app-1\.20/tika-app-1\.27/' /etc/promethium/promethium.yml

sed -i 's/memcached\.MemcachedCache/memcached\.PyMemcacheCache/'  /usr/share/promethium/promethium/settings.py

exit 0
