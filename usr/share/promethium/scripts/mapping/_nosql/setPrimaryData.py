#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


# Native Python Lib/Class 
import json
import jwt
import os
import sys
import uuid
import yaml
import pymongo

sys.path.append(os.path.abspath("/usr/share/promethium"))
from promethiumClass.auth import _cypher
from promethiumClass._nosql import _nosql

nosql = _nosql._nosql()


test = [ r for r in nosql.search(bucket='pm_user', query = {'login':'admin0@global.virt'})]
if len(test):
    print('Nothing to do !')
    print('Account "admin0@global.virt" already exists!')
    print('Suppose that all other stuff exist too')
    exit(0)



Domain = { 
    'uuid': str(uuid.uuid4()).upper(),
    'displayname': 'global.virt',
    'storeserver': str(uuid.uuid4()).upper(),
    'authserver' : str(uuid.uuid4()).upper()
}
nosql.upsert(bucket='pm_domain', upsert=Domain)


STORE = {
'uuid':Domain['storeserver'],
'displayname': 'Store Point',
'role': 'store',
'mechanism': 'Internal',
'storePoint':'/var/lib/promethium'
}
nosql.upsert(bucket='pm_host', upsert=STORE)

AUTH = {
'uuid': Domain['authserver'],
'displayname': 'Auth Server',
'role': 'auth',
'mechanism': 'Internal'
}
nosql.upsert(bucket='pm_host', upsert=AUTH)


USER = {
'uuid' : str(uuid.uuid4()).upper(),
'firstname': '',
'lastname': 'admin0',
'login': 'admin0@global.virt',
'role': 'admin',
'domain': Domain['uuid'],
'subscribedguid': None,
'passwd': _cypher._cypher().hashPassword('admin'),
'lang' : 'fr',
'theme': 'default',
'enable': True,
}
nosql.upsert(bucket='pm_user', upsert=USER)

# Index Creation
idx = [('from', pymongo.TEXT ),('to', pymongo.TEXT ),('cc', pymongo.TEXT ),('bcc', pymongo.TEXT ),('subject', pymongo.TEXT ),('body', pymongo.TEXT )]
nosql.createIndex(bucket='pm_message', idx=idx)

idx = [('body', pymongo.TEXT )] 
nosql.createIndex(bucket='pm_tika', idx=idx)
