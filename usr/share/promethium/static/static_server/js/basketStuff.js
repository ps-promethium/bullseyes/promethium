function cancelAddBasket(){
    document.getElementById('addBasket').style.display='none';
}


function cancelEditBasket(){
    document.getElementById('editBasket').style.display='none';
}



function editBasket(uuid, domain, token) {
    
    document.getElementById('editBasket').style.display='block' ;
    document.getElementById('editUuid').value   = uuid;
    
    var url = "/api/basket/" + domain  + "/" + uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data) {
        var qty = data.length;
        
          
        var enable = data['details'][0]['basket']['enable'];
        if( enable == 'True') {
            checked = 'checked';
            document.getElementById('editEnable').checked = checked;            
        };
                
        var displayname = data['details'][0]['basket']['displayname'];
        document.getElementById('editDisplayname').value = displayname;
        
        var defaultColor = data['details'][0]['basket']['defaultColor'];
        document.getElementById('editDefaultColor').value = defaultColor;
         
        var server = data['details'][0]['basket']['server'];
        document.getElementById('editServer').value = server;       
        
        
        var account = data['details'][0]['basket']['account'];
        document.getElementById('editAccount').value = account;
        
        var passwd = data['details'][0]['basket']['passwd'];
        document.getElementById('editPasswd').value = passwd;
        
        var protocol = data['details'][0]['basket']['protocol'];
        document.getElementById('editProtocol').value = protocol;
        
        var port = data['details'][0]['basket']['port'];
        document.getElementById('editPort').value = port;
        
        var update =  data['details'][0]['basket']['every'];
        document.getElementById('editUpdate').value = update ;
   
        var imapBackupFolder = "";   
        if ( Object.keys(data['details'][0]['basket']).includes("imapBackupFolder")) {
            imapBackupFolder = data['details'][0]['basket']['imapBackupFolder'];
        };
        document.getElementById('EditImapBackupFolder').value = imapBackupFolder;

        var imapFolder = "";
        if (Object.keys(data['details'][0]['basket']).includes("imapFolder")) {
            imapFolder = data['details'][0]['basket']['imapFolder'];
        };
        document.getElementById('EditImapFolder').value = imapFolder;
        
        var filterFile = data['details'][0]['basket']['filterFile'].join(' ');
        document.getElementById('editFilterFile').value = filterFile;
        
        var asSender = data['details'][0]['basket']['asSender'].join('\r\n');
        document.getElementById('editAsSender').value = asSender ;
        
        var xHeader = data['details'][0]['basket']['x-header'].join('\r\n');
        document.getElementById('editXHeader').value = xHeader ;
        
        var enable = data['details'][0]['basket']['verifypeer'];
        if( enable == 'True') {
            checked = 'checked';
            document.getElementById('editVerifypeer').checked = checked;            
        };
        
        var groupList    = data['details'][0]['basket']['subscribedguid'];   
        $('#editSubscribedguid').val(groupList).trigger("chosen:updated");

    });

}



function setPort(protocol, port) {

    var protocol = document.getElementById(protocol).value;
    
    if ( protocol == 'imaps') {
        document.getElementById(port).value = 993 ; 
        document.getElementById('displayImapFolder').style.display='block' ;
        document.getElementById('displayEditImapFolder').style.display='block' ;
        document.getElementById('displayImapBackupFolder').style.display='block' ;
        document.getElementById('displayEditImapBackupFolder').style.display='block' ;

    }
    
    if ( protocol == 'imap') {
        document.getElementById(port).value = 143 ;
        document.getElementById('displayImapFolder').style.display='block' ;
        document.getElementById('displayEditImapFolder').style.display='block' ;
        document.getElementById('displayImapBackupFolder').style.display='block' ;
        document.getElementById('displayEditImapBackupFolder').style.display='block' ;
    }
    
    if ( protocol == 'pops') {
        document.getElementById(port).value = 995 ; 
        document.getElementById('displayImapFolder').style.display='none' ;
        document.getElementById('displayEditImapFolder').style.display='none' ;
        document.getElementById('displayImapBackupFolder').style.display='none' ;
        document.getElementById('displayEditImapBackupFolder').style.display='none' ;
    }
    
    if ( protocol == 'pop') {
        document.getElementById(port).value = 110;
        document.getElementById('displayImapFolder').style.display='none' ;
        document.getElementById('displayEditImapFolder').style.display='none' ;
        document.getElementById('displayImapBackupFolder').style.display='none' ;
        document.getElementById('displayEditImapBackupFolder').style.display='none' ;
    }
    
    
}




/* basket Rules specs  */

function closeList() {
    document.getElementById('listRules').style.display='none' ;
}

function closeListRules(uuid, domain, token) {
    document.getElementById('listRules').style.display='none' ;
}

function listRules(uuid, domain, token) {
    document.getElementById('listRules').style.display='block' ;  
    document.getElementById('newBasketRule').value   = uuid;
    
    getBasketRules(uuid, domain, token);
}

function editRule(uuid, domain, token) {
    document.getElementById('editRule').style.display='block' ;    
}


function cancelAddRule(){
    document.getElementById('addRule').style.display='none';
}


function cancelEditRule(){
    document.getElementById('editRule').style.display='none';
}


//FUNCTION TO ADD TEXT BOX ELEMENT
function addRuleElement(uuid) {
        if (uuid == "add") {
            intTextBox = intTextBox + 1 ; 
            var contentID = document.getElementById('Rules-'+uuid);
            var newP = document.createElement('p');
            
            newP.setAttribute('id','Rule'+intTextBox);
            newP.innerHTML  = "<select name='field" + intTextBox + "'> " +
                                "<option value='From'>" + translateThis('From') + "</option>" +
                                "<option value='Subject'>" + translateThis('Subject') + "</option>" +
                                "<option value='Body'>" + translateThis('Body') + "</option>" +
                            "</select> &nbsp;&nbsp;"  +                          
                            "<select name='verb" + intTextBox + "'> " +
                                "<option value='is'>" + translateThis('is') + "</option>" +
                                "<option value='contains'>" + translateThis('contains') + "</option>" +
                            "</select> &nbsp;&nbsp;"  +
                            "<input class='w3-border w3-padding' type='text' style='width:65%; margin-top: 5px;' name='string" + intTextBox + "' > " +
                            "<a href='javascript:removeRuleElement(\"" + uuid + "\",\"" + intTextBox + "\")'><i class=\"fas fa-trash-alt\" style='color: red'></i></a> " +
                            "";
        }
        
        if (uuid == "edit") {
            intEditTextBox = intEditTextBox + 1 ; 
            var contentID = document.getElementById('Rules-'+uuid);
            var newP = document.createElement('p');
            
            newP.setAttribute('id','Rule'+intEditTextBox);
            newP.innerHTML  = "<select name='field" + intEditTextBox + "'> " +
                                "<option value='From'>" + translateThis('From') + "</option>" +
                                "<option value='Subject'>" + translateThis('Subject') + "</option>" +
                                "<option value='Body'>" + translateThis('Body') + "</option>" +
                            "</select> &nbsp;&nbsp;"  +                          
                            "<select name='verb" + intEditTextBox + "'> " +
                                "<option value='is'>" + translateThis('is') + "</option>" +
                                "<option value='contains'>" + translateThis('contains') + "</option>" +
                            "</select> &nbsp;&nbsp;"  +
                            "<input class='w3-border w3-padding' type='text' style='width:65%; margin-top: 5px;' name='string" + intEditTextBox + "' > " +
                            "<a href='javascript:removeRuleElement(\"" + uuid + "\",\"" + intEditTextBox + "\")'><i class=\"fas fa-trash-alt\" style='color: red'></i></a> " +
                            "";
        }
        contentID.appendChild(newP);
}


//FUNCTION TO REMOVE TEXT BOX ELEMENT
function removeRuleElement(uuid,id) {
    var contentID = document.getElementById('Rules-'+uuid);
    contentID.removeChild(document.getElementById('Rule'+id));
    
    if (uuid == "add") {
        if(intTextBox != 0) { 
            intTextBox = intTextBox - 1 ;
        }
    }
    
    if (uuid == "edit") {
        if(intEditTextBox != 0) { 
            intEditTextBox = intEditTextBox - 1 ;
        }
    }
}


function getBasketRules(uuid, domain, token) {
        
    var url = "/api/basket/" + domain  + "/" + uuid + "/getRules" ;
    var buuid = uuid ;    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data) {
        var ulRulesList = document.getElementById('Rules');  
        ulRulesList.innerHTML = '';
 
        var rules = data['rules'] ;

        for (const [uuid, rule] of Object.entries(rules)){
            addRuleToList(uuid, rule, domain, token, buuid);          
        };
    });
}



function ruleGetGroupDisplaynameInLine(guid, ruuid, domain, token) {
        
    var url = '/api/group/' + domain + '/' + guid + '/displayname';
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){ 
        var div = document.getElementById('distrib-'+ruuid);
        var li = document.createElement("li");     
        li.style.display= "inline";
        li.style.padding = "0px 14px 0px 0px";
        var html = '<i class="fas fa-users"></i> ' + data['displayname'][0]['displayname'];    
        li.innerHTML = html ;
        div.appendChild(li);
        
    });
}

function ruleEditGroup(guid, domain, token) {
        
    var url = '/api/group/' + domain + '/' + guid + '/displayname';

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
                  
    getJSON(url, token).then(function(data){ 
        var select = document.getElementById('selectedRuleSubscribedguid');
        var opt    = document.createElement("option");
        opt.value = guid ;
        opt.text  = data['displayname'][0]['displayname'] ;
//         opt.selected = 'selected';
        select.add(opt, 0);
        
    });
}


function addRuleToList(uuid, rule, domain, token, buuid) {       
    
    var main = document.getElementById("Rules");      
    var div = document.createElement("div");
                                   
    var html = '</br>' ;
    html += '<div class="w3-card" id="" >' ;
        html += '<header class="w3-container w3-blue">' ;
        html += '<h6 class="" style="padding-left:2px!important; padding-top:2px!important;padding-bottom:2px!important;"><a href=\"javascript:showHide(\''+uuid+'\')\" > ' + translateThis('Rule') + ' : ' + rule['displayname'] + '</a>'; 
        
        html += '<a class="w3-right" href="/manage/baskets/' + buuid +'/delRule/' + uuid + '"> <i class="fas fa-trash fa-lg" style="color: red;" ></i></a>';
        html += '&nbsp; &nbsp;';
        html += '<a class="w3-right" style="padding-left: 8px; padding-right: 8px;"  href="javascript:editRule(\''+buuid+'\', \''+uuid+'\',\''+domain+'\', \''+token+'\')" title="'+ translateThis('Edit') +'"> <i class="far fa-edit fa-lg"></i></a>';
        

        html += '</h6></header>';
    
        html += '<div class="w3-row" id="'+ uuid +'" style="display: none; padding: 4px">';
            html += '<div class="w3-container" id="match-' + uuid +'"> <label><b>' + translateThis('Apply if') + ' ';
            if (rule['match'] == "AND")
                html += translateThis('all conditions');  
            else {
                html += translateThis('one or more conditions');                
            };
            html += '</b></label></div></br>';
            
            
            html += '<div class="w3-container w3-padding" id="cond-' + uuid +'"> <label><b>' + translateThis('Conditions') + '</b></label></br>';
            html += '<ul class="w3-ul">';
            if( rule['rules'].length > 0 ) {
                rules = rule['rules'];
                for (n=0 ; n < rules.length; n++) {
                    html += '<li> '+ translateThis(rules[n]['field']) + " " +  translateThis(rules[n]['verb']) + " \"" + rules[n]['string'] + "\" </li>" ;
                }
            };
            
            
            html += '</ul>';
        
            html += '</div>';
            html += '</br>';
            html += '<div class="w3-container w3-padding" id="distrib-' + uuid +'"> <label><b>' + translateThis('SharedToGroups') + '</b></label></br>';
            html += '</div>';
            
        html += '</div>'; 
    
    html += '</div>'; 
        
        
    if( rule['subscribedguid'].length > 0 ) {
        groups = rule['subscribedguid'];
        for (n=0 ; n < groups.length; n++) {
            
            ruleGetGroupDisplaynameInLine(groups[n], uuid, domain, token);
        }
    };

        
    div.innerHTML = html ;
    main.appendChild(div);

}  



function editRule(buuid, ruuid, domain, token) {
    document.getElementById('editRule').style.display='block';
    document.getElementById('Rules-edit').innerHTML = '';
    var main = document.getElementById("Rules-edit");  
  
    var url = "/api/basket/" + domain  + "/" + buuid + "/getRules" ;
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    
    getJSON(url, token).then(function(data) {
        var rules = data['rules'] ;
        var html ='' ;
        for (const [uuid, rule] of Object.entries(rules)){
            if (uuid == ruuid) {      
                 
                var match = rule['match'];
                if (match == "AND") {
                    document.getElementById('and-edit').checked = "checked";
                } else {
                    document.getElementById('or-edit').checked = "checked";
                };
  
                var displayname = rule['displayname'];
                document.getElementById('displayname-edit').value = displayname;
                
                intEditTextBox =  rule['rules'].length-1 ;   
                
                html += '<label><b>' + translateThis('addCondition') +'</b> ( <a href="javascript:addRuleElement(\'edit\');"><i class="fas fa-traffic-light fa-lg" style="color: green"></i>+</a> )</label>';
                
                for (var n=0; n < rule['rules'].length ; n++) {
                                                    
                    html += '<p id="Rule'+n+'" >';
                    html += '<select name="field'+ n +'">';
                    html += '<option value="' + rule['rules'][n]['field'] + '">' + translateThis(rule['rules'][n]['field']) +'</option>';        
                    html += '<option value="From">' + translateThis('From') +'</option>';
                    html += '<option value="Subject">' + translateThis('Subject') +'</option>';
                    html += '<option value="Body">' + translateThis('Body') + '</option>';
                    html += '</select>&nbsp;&nbsp;'                     
                            
                    html += '<select name="verb' + n +'">'; 
                    html += '<option value="' + rule['rules'][n]['verb'] + '">' + translateThis(rule['rules'][n]['verb']) +'</option>'; 
                    html += '<option value="is">' + translateThis('is') + '</option>';
                    html += '<option value="contains">' + translateThis('contains') + '</option>';
                    html += '</select>&nbsp;&nbsp;';  
                    html += '<input class="w3-border w3-padding" type="text" style="width:65%; margin-top: 5px;" name="string' + n + '" required value="' + rule['rules'][n]['string'] + '"> '; 
                    if (n > 0) {
                        html += '<a href=\'javascript:removeRuleElement("edit","' + n + '")\'><i class="fas fa-trash-alt" style="color: red"></i></a>';
                    };
                    html += '</p>';                    
                    
                };
                
                $('#selectedRuleSubscribedguid').val(rule['subscribedguid']).trigger("chosen:updated");
            }; 
        };
        
        document.getElementById('basketUuid').value = buuid;
        document.getElementById('ruleUuid').value = ruuid;
        main.innerHTML = html ;      
    });
    
}

