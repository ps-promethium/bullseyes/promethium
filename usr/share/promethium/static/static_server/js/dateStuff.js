// function printPrettyLocalDatetime(date) {
//     
//     var msec = Date.parse(date);
//     var d = new Date(msec);
//     
//     d = new Date(msec).toLocaleString();
//     document.write(d);  
//     
// }

function printPrettyLocalDatetimeFromMicrotimestp(msec,tz) {
    
    var d = new Date(msec);
    if  (tz == 'UTC') {
        ofst = d.getTimezoneOffset() ;    
        var localmsec  =  msec + ofst*60*1000*-1 ;    
    } else {
        var localmsec  =  msec ;
    };
    
    D = new Date(localmsec).toLocaleString();
    document.write(D);  
    
}


function returnPrettyLocalDatetimeFromMicrotimestp(msec,tz) {
    
    var d = new Date(msec);
    
    if  (tz == 'UTC') {
        ofst = d.getTimezoneOffset() ;    
        var localmsec  =  msec + ofst*60*1000*-1 ;    
    } else {
        var localmsec  =  msec ;
    } ;
    
    D = new Date(localmsec).toLocaleString();
    return D ;  
    
}


