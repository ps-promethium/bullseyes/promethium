function cancelAddDomain(){
    document.getElementById('addDomain').style.display='none';
}


function cancelEditDomain(){
    document.getElementById('editDomain').style.display='none';
}



function editDomain(uuid, token) {
    document.getElementById('editDomain').style.display='block' ;
    document.getElementById('uuid').value   = uuid;
    
    var url = "/api/domain/"+ uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    getJSON(url, token).then(function(data) {
        var qty = data.length;

        var displayname = data['details'][0]['pm_domain']['displayname'];
        document.getElementById('editDisplayname').value = displayname;
        
        var cepdTreshold = data['details'][0]['pm_domain']['cepdTreshold'];
        document.getElementById('editCepdTreshold').value = cepdTreshold;
        
        if (displayname == 'global.virt') {
            document.getElementById("editDisplayname").readOnly = true; 
        }
        
        var authServerUuid = data['details'][0]['pm_domain']['authserver'];
        setOption(authServerUuid, token, 'editAuthserver');
        
        var storeServerUuid = data['details'][0]['pm_domain']['storeserver'];
        setOption(storeServerUuid, token, 'editStoreserver');
    });
    
}



function setOption(uuid, token, selectId) {

    var url = "/api/host/"+ uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data) {
        
        var displayname = data['details'][0]['host']['displayname'];
           
        var select = document.getElementById(selectId);
        var opt    = document.createElement("option");
        
        opt.value = uuid ;
        opt.text  = displayname ;
        opt.selected = 'selected';
        select.add(opt, 0);

    });

}

