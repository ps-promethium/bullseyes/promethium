$(function() {
    $('#favTree').treeview({
        enableLinks: true, 
        color: "#428bca",
        expandIcon: 'glyphicon glyphicon-chevron-right',
        collapseIcon: 'glyphicon glyphicon-chevron-down',
        nodeIcon: 'glyphicon glyphicon-bookmark',
        data: favoriteNodes
    });
});
