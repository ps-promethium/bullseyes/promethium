function filterInTable(inputId, tableId, colNumber) {
  var input, filter, table, tr, td, i;

  input = document.getElementById(inputId);
  filter = input.value.toUpperCase();

  table = document.getElementById(tableId);

  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[colNumber];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}


