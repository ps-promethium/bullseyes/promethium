/* Forward tab */

function closeForward() {
    document.getElementById('listRules').style.display='none' ;
}

//FUNCTION TO ADD TEXT BOX ELEMENT
function addFwdElement() {
    
    intTextBox = intTextBox + 1 ; 
    var contentID = document.getElementById('Forward');
    var newP = document.createElement('p');
            
    newP.setAttribute('id','forward'+intTextBox);
    newP.innerHTML  = "<select name='action" + intTextBox + "'> "
        + "<option value='to'>" + translateThis('To') + "</option>"
        + "<option value='cc'>" + translateThis('Cc') + "</option>"
        + "</select> &nbsp;&nbsp;"                     
         + "<input class='w3-border w3-padding' type='email' style='width:65%; margin-top: 5px;' name='address" + intTextBox + "' > "
        + "<a href='javascript:removeForwardElement(\"" + intTextBox + "\")'><i class=\"fas fa-trash-alt\" style='color: red'></i></a> "
        + "";
        
    contentID.appendChild(newP);   
    
        
}


//FUNCTION TO REMOVE TEXT BOX ELEMENT
function removeForwardElement(id) {
    var contentID = document.getElementById('Forward');
    contentID.removeChild(document.getElementById('forward'+id));
    
    if(intTextBox != 0) { 
            intTextBox = intTextBox - 1 ;
    }
}




