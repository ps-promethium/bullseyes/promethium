function cancelAddGroup(){
    document.getElementById('addGroup').style.display='none';
}


function cancelEditGroup(){
    document.getElementById('editGroup').style.display='none';
}



function editGroup(uuid, domainUuid, token) {
    document.getElementById('editGroup').style.display='block' ;
    document.getElementById('uuid').value   = uuid;
    
    var url = "/api/group/" + domainUuid + "/" + uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    getJSON(url, token).then(function(data) {
        var qty = data.length;

        var displayname = data['details'][0]['pm_group']['displayname'];
        document.getElementById('editDisplayname').value = displayname;
              
        var parentUuid = data['details'][0]['pm_group']['parent'];
        setOption(parentUuid, domainUuid, token, 'editParent');

        
    });
    
}



function setOption(uuid, domainUuid, token, selectId) {

   var url = "/api/group/" + domainUuid + "/" + uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data) {
        
        var displayname = data['details'][0]['pm_group']['displayname'];
           
        var select = document.getElementById(selectId);
        var opt    = document.createElement("option");
        
        opt.value = uuid ;
        opt.text  = displayname ;
        opt.selected = 'selected';
        select.add(opt, 0);

    });

}


function closeGroupMembers() {
    document.getElementById('groupMembers').style.display='none';
}

function showMembers(uuid, domainUuid, groupName, token) {
    document.getElementById('groupMembers').style.display='block' ;
    
    var url = "/api/group/" + domainUuid + "/" + uuid + "/_allSubscribed" ;

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    
    getJSON(url, token).then(function(data) {
        
        var spanGroupName = document.getElementById('spanGroupName');
        spanGroupName.innerHTML =  groupName;
        
        var listElement = document.getElementById('members');
        listElement.innerHTML = '';

            
        for (var n  in data['subscribedUser']) {
            
            var login     = data['subscribedUser'][n]['login'] ;
            var firstname = data['subscribedUser'][n]['firstname'];
            var lastname  = data['subscribedUser'][n]['lastname'];
            
            var string  = '<div class="w3-bar-item">'
                string += '<span class="w3-large">' + firstname + ' ' + lastname + ' (' + login + ') </span><br>'
                string += '</div>';
            
            var listItem = document.createElement('li');
            listItem.classList.add("w3-bar");
            listItem.innerHTML = string;
            listElement.appendChild(listItem);
            
        }
    });

}

    


function closeEditGroupMembers(){
    document.getElementById('editGroupMembers').style.display='none';

}


function cancelEditGroupMembers(){
    document.getElementById('editGroupMembers').style.display='none';

}


function editGroupMembers(uuid, domainUuid, groupName, token) {
    document.getElementById('editGroupMembers').style.display='block' ;
    
    var url = "/api/group/" + domainUuid + "/" + uuid + "/_allSubscribed" ;

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    
    getJSON(url, token).then(function(data) {
        
        var spanGroupName = document.getElementById('spanGroupNameSubscribed');
        spanGroupName.innerHTML =  groupName;
        
        var listElement = document.getElementById('subscribed');
        listElement.innerHTML = '';

            
        for (var n  in data['subscribedUser']) {
            
            var login     = data['subscribedUser'][n]['login'] ;
            var firstname = data['subscribedUser'][n]['firstname'];
            var lastname  = data['subscribedUser'][n]['lastname'];
            
           // var string  = '<div class="w3-bar-item">'
           //     string += '<span class="w3-large">' + firstname + ' ' + lastname + ' (' + login + ') </span><br>'
           //     string += '</div>';
            
	    var string  = firstname + ' ' + lastname + ' (' + login + ') + <i class="fas fa-right fa-lg"></i>'

            var listItem = document.createElement('li');
            //listItem.classList.add("w3-bar");
            listItem.innerHTML = string;
            listElement.appendChild(listItem);
            
        }
    });

}
