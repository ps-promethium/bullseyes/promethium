function cancelAddGroup(){
    document.getElementById('addGroup').style.display='none';
}


function cancelEditGroup(){
    document.getElementById('editGroup').style.display='none';
}



function editGroup(uuid, domainUuid, token) {
    document.getElementById('editGroup').style.display='block' ;
    document.getElementById('uuid').value   = uuid;
    
    var url = "/api/group/" + domainUuid + "/" + uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    getJSON(url, token).then(function(data) {
        var qty = data.length;

        var displayname = data['details'][0]['pm_group']['displayname'];
        document.getElementById('editDisplayname').value = displayname;
              
        var parentUuid = data['details'][0]['pm_group']['parent'];
        setOption(parentUuid, domainUuid, token, 'editParent');

        
    });
    
}



function setOption(uuid, domainUuid, token, selectId) {

   var url = "/api/group/" + domainUuid + "/" + uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data) {
        
        var displayname = data['details'][0]['pm_group']['displayname'];
           
        var select = document.getElementById(selectId);
        var opt    = document.createElement("option");
        
        opt.value = uuid ;
        opt.text  = displayname ;
        opt.selected = 'selected';
        select.add(opt, 0);

    });

}


function closeGroupMembers() {
    document.getElementById('groupMembers').style.display='none';
}

function showMembers(uuid, domainUuid, groupName, token) {
    document.getElementById('groupMembers').style.display='block' ;
    
    var url = "/api/group/" + domainUuid + "/" + uuid + "/_allSubscribed" ;

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    
    getJSON(url, token).then(function(data) {
        
        var spanGroupName = document.getElementById('spanGroupName');
        spanGroupName.innerHTML =  groupName;
        
        var listElement = document.getElementById('members');
        listElement.innerHTML = '';

            
        for (var n  in data['subscribedUser']) {
            
            var login     = data['subscribedUser'][n]['login'] ;
            var firstname = data['subscribedUser'][n]['firstname'];
            var lastname  = data['subscribedUser'][n]['lastname'];
            
            var string  = '<div class="w3-bar-item">'
                string += '<span class="w3-large">' + firstname + ' ' + lastname + ' (' + login + ') </span><br>'
                string += '</div>';
            
            var listItem = document.createElement('li');
            listItem.classList.add("w3-bar");
            listItem.innerHTML = string;
            listElement.appendChild(listItem);
            
        }
    });

}

    


function closeEditGroupMembers(){
    document.getElementById('editGroupMembers').style.display='none';

}


function cancelEditGroupMembers(){
    document.getElementById('editGroupMembers').style.display='none';

}


function getUnsubscribedGroupMembers(groupid, domainUuid, groupName, token) {
    var url = "/api/group/" + domainUuid + "/" + groupid + "/_allUnsubscribed" ;

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    
    getJSON(url, token).then(function(data) {
        
        var listElement = document.getElementById('unsubscribed');
        listElement.innerHTML = '';

            
        for (var n  in data['unsubscribedUser']) {
            
            var login     = data['unsubscribedUser'][n]['login'] ;
            var firstname = data['unsubscribedUser'][n]['firstname'];
            var lastname  = data['unsubscribedUser'][n]['lastname'];
            var uuid      = data['unsubscribedUser'][n]['uuid'];             

	    var string  = firstname + ' ' + lastname + ' (' + login + ') <a href="javascript:moveThisUser(\''+uuid+'\',\''+ domainUuid +'\',\''+ groupName+'\',\''+ token +'\', \''+ firstname +'\',\''+ lastname+'\',\''+ login +'\',\'add\')"> <i class="fas fa-plus-circle fa-lg" style="color: green; "></i> </a>' ;

            var listItem = document.createElement('li');
            listItem.id = 'add_'+login ;
            listItem.innerHTML = string;
            listElement.appendChild(listItem);
            
        }
    });

}

function getSubscribedGroupMembers(groupid, domainUuid, groupName, token) {
    var url = "/api/group/" + domainUuid + "/" + groupid + "/_allSubscribed" ;

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    
    getJSON(url, token).then(function(data) {
        
        var spanGroupName = document.getElementById('spanGroupNameSubscribed');
        spanGroupName.innerHTML =  groupName;
        
        var listElement = document.getElementById('subscribed');
        listElement.innerHTML = '';

            
        for (var n  in data['subscribedUser']) {
            
            var login     = data['subscribedUser'][n]['login'] ;
            var firstname = data['subscribedUser'][n]['firstname'];
            var lastname  = data['subscribedUser'][n]['lastname'];
            var uuid      = data['subscribedUser'][n]['uuid'];            


            var string  = firstname + ' ' + lastname + ' (' + login + ') <a href="javascript:moveThisUser(\''+uuid+'\',\''+ domainUuid +'\',\''+ groupName+'\',\''+ token +'\', \''+ firstname +'\',\''+ lastname+'\',\''+ login +'\', \'del\')"> <i class="fas fa-minus-circle fa-lg" style="color: red; "></i></a>';

            var listItem = document.createElement('li');
            listItem.id = 'del_'+login ;
            listItem.innerHTML = string;
            listElement.appendChild(listItem);
            
        }
    });

}


function editGroupMembers(uuid, domainUuid, groupName, token) {
	document.getElementById('editGroupMembers').style.display='block' ;
	
	getSubscribedGroupMembers(uuid, domainUuid, groupName, token);
	getUnsubscribedGroupMembers(uuid, domainUuid, groupName, token);
}


function  moveThisUser(uuid, domainUuid, groupName, token, firstname, lastname, login, action) {
	if (action == "add") {
		addToGroup(uuid, domainUuid, groupName, token, firstname, lastname, login);
	};

	if (action == "del") {
                delFromGroup(uuid, domainUuid, groupName, token, firstname, lastname, login);
        }

}

function  addToGroup(uuid, domainUuid, groupName, token, firstname, lastname, login) {
	var delId = 'del_' + login ;
	var addId = 'add_' + login ;
	var string  = firstname + ' ' + lastname + ' (' + login + ') <a href="javascript:moveThisUser(\''+uuid+'\',\''+ domainUuid +'\',\''+ groupName+'\',\''+ token +'\', \''+ firstname +'\',\''+ lastname+'\',\''+  login +'\',\'del\')"> <i class="fas fa-minus-circle fa-lg" style="color: red; "></i></a>';
	
	document.getElementById(addId).remove();
	addLiItem("subscribed", delId, string);

	updateUserGroupSubscription(uuid, domainUuid, groupName, token, "add")
	
}



function delFromGroup(uuid, domainUuid, groupName, token, firstname, lastname, login) {

        var delId = 'del_' + login ;
        var addId = 'add_' + login ;
        var string  = firstname + ' ' + lastname + ' (' + login + ') <a href="javascript:moveThisUser(\''+uuid+'\',\''+ domainUuid +'\',\''+ groupName+'\',\''+ token +'\', \''+ firstname +'\',\''+ lastname+'\',\''+  login +'\',\'add\')"> <i class="fas fa-plus-circle fa-lg" style="color: green; "></i></a>';

        document.getElementById(delId).remove();
	addLiItem("unsubscribed", addId, string);	

       updateUserGroupSubscription(uuid, domainUuid, groupName, token, "del") 

}


function addLiItem(ulId, liId, liText) {
  	var ul = document.getElementById(ulId);
  	var li = document.createElement("li");
  	//li.appendChild(document.createTextNode(liText));
  	li.setAttribute("id", liId); 
  	ul.appendChild(li);
	document.getElementById(liId).innerHTML = liText;

}


function updateUserGroupSubscription(uuid, domainUuid, groupName, token, action) {
        var url = "/api/user/" + domainUuid + "/" + uuid + "/updateSubscribedGroup" ;

        var xhr = new XMLHttpRequest();
        xhr.open('post', url, true);
        xhr.setRequestHeader("X-PM-ApiKey", token);
        xhr.responseType = 'json';
        xhr.send(JSON.stringify({ "action": action, "groupname": groupName}));
}
