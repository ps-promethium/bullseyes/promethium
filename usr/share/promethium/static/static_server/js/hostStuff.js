function cancelAddHost(){
    
    
    document.getElementById("role").selectedIndex = "0"; 
    
    var selDiv = document.getElementById('mechanism');
    selDiv.style.display = "none";
    
    var optL = document.getElementById('mechanismList');
    while (optL.hasChildNodes()) {
      optL.removeChild(optL.firstChild);
    }
    
    var mechDef = document.getElementById('mechanismElement');
    while (mechDef.hasChildNodes()) {
      mechDef.removeChild(mechDef.firstChild);
    }
    
    document.getElementById('addHost').style.display='none';
}


function displayMechanism() {
    var list = [] ;
    var role =  document.getElementById("role").value;
    
    var selDiv = document.getElementById('mechanism');
    selDiv.style.display = "block";

    
    // init 
    var optL = document.getElementById('mechanismList');
    while (optL.hasChildNodes()) {
      optL.removeChild(optL.firstChild);
    }
    
    // init 
    var mechDef = document.getElementById('mechanismElement');
    while (mechDef.hasChildNodes()) {
      mechDef.removeChild(mechDef.firstChild);
    }
    
    if (role == "0") {
        selDiv.style.display = "none";
    }
    
    if (role == "auth"){
        list = ['Internal', 'LDAP', 'BlueMind', 'SAML'];
    }
        
    if (role == "store") {
        list = ['Internal', 'S3'];        
    }
    
    var opt = document.createElement('option');
    opt.text = " ";
    opt.value = "0";
    optL.add(opt, null);
    
    for (i = 0; i < list.length; i++) {
        var opt = document.createElement('option');
        opt.text = list[i];
        opt.value = list[i];
        
        optL.add(opt, null);
    }
}


function displayMechanismElement() {
    
    var role = document.getElementById("role").value;
    var mech = document.getElementById("mechanismList").value;
    
    var mechDef = document.getElementById('mechanismElement');
    while (mechDef.hasChildNodes()) {
      mechDef.removeChild(mechDef.firstChild);
    }
    
    
    mechDef.style.display = "block";
    
    
    var newD = document.createElement('div');
    

    if ( role == 'auth') {
        if( mech == 'LDAP' ) {
            newD.innerHTML = '<label><b>LDAP URI</b></label>'  
                            + '\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="URI" placeholder="ldap://ldap.tld:389" required>'
                            + '<label><b>Base DN</b></label>' 
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="basedn" placeholder="ou=domain, dc=local" required>'
                            + '\n'
                            + '<label><b>User DN</b></label>' 
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="userDn" placeholder="uid=admin, dc=local" required>'
                            + '\n'
                            + '<label><b>User Password</b></label>' 
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="userPassword" placeholder="password" required>'
                            + '\n'
                            + '<label><b>Search attribute</b></label>' 
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="searchAttribute" placeholder="uid" required>'
                            + '\n'
                            + '<label><b>Enable failback</b></label> ' 
                            + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="failback" value="True">'
                            + '\n';
            
            
            mechDef.appendChild(newD);
        };
        
        if( mech == 'SAML' ) {
            newD.innerHTML = '</br>\n'
                            + '<header class="w3-teal" style="padding: 0px;"> <h6 style="letter-spacing: 2px;"><b>Service Provider (SP)</b></h6></header>'  
                            + '</br>\n'
                            + '<label><b>URL</b></label>'
                            + '</br>\n'
                            + '<select class="" name="sp_proto">'
                            + '<option value="https" selected> https </option> '
                            + '<option value="http"> http </option> '
                            + '</select>'
                            + '&nbsp; &nbsp;'
                            + '<input class="" type="text" name="sp_fqdn" placeholder="your.domain.tld" style="width: 400px;" required >'
                            + '</br>\n'
                            + '</br>\n'
                            + '<label><b>SP X509 Cert</b></label>' 
                            + '</br>\n'
                            + '<textarea class="w3-textarea w3-border w3-margin-bottom" name="sp_x509" style="width: 575px; height: 225px" required></textarea>'
                            + '</br>\n'
                            + '</br>\n'
                            + '</br>\n'
                            + '</br>\n'
                            + '<header class="w3-teal" style="padding: 0px;"> <h6 style="letter-spacing: 2px;"><b>Identifier Provider (IdP)</b></h6></header>'  
                            + '</br>\n'
                            + '<label><b>Entity Id</b></label>'
                            + '</br>\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="idp_entity_id" placeholder="some/sso/your_app" required>'
                            + '</br>\n'
                            + '<label><b>SSO URL</b></label>' 
                            + '</br>\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="url" name="idp_url" placeholder="https://your-sso.domain.tld" required>'
                            + '</br>\n'
                            + '<label><b>IDP X509 Cert</b></label>' 
                            + '</br>\n'
                            + '<textarea class="w3-textarea w3-border w3-margin-bottom" name="idp_x509" style="width: 575px; height: 225px" required></textarea>'
                            + '</br>\n'

            
            
            mechDef.appendChild(newD);
        };
        
        
        if ( mech == 'BlueMind' ) {
            $('mechDef').empty();
            
            newD.innerHTML = '<label><b>Bluemind Server Name</b></label>'  
                            + '\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="URI" placeholder="www.bluemind.tld" required>'
                            + '\n'
                            + '<label><b>Enable failback</b></label> ' 
                            + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="failback" value="True">'
                            + '\n'
                            + '<label><b>Verify peer</b></label> ' 
                            + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="verifypeer" value="True">'
                            + '\n';
            
            mechDef.appendChild(newD);
        
        };
    };
    
    if (role == 'store') {
        if ( mech == 'Internal' ) {
            $('mechDef').empty();
            
            newD.innerHTML = '<label><b>Store Point</b></label>'  
                            + '\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="storePoint" placeholder="/some/path/to/store/data" required>'
                            + '\n'
                            + '<label><b>Encrypt</b></label> ' 
                            + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="encrypt" value="True">'
                            + '\n';
            
            mechDef.appendChild(newD);
        
        };
        
        if ( mech == 'S3' ) {
            $('mechDef').empty();
            
            newD.innerHTML = '<label><b>Store Point</b></label>'  
                            + '\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="storePoint" placeholder="S3 URI" required>'
                            +'<label><b>User</b></label>'  
                            + '\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="usr" placeholder="user.s3" required>'
                            + '\n'
                            + '<label><b>Password</b></label>'  
                            + '\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="passwd" placeholder="user.s3.password" required>'
                            + '\n'
                            + '<label><b>Verify peer</b></label> ' 
                            + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="verifypeer" value="True">'
                            + '\n'
                            + '<label><b>Encrypt</b></label> ' 
                            + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="encrypt" value="True">'
                            + '\n';
            
            
            mechDef.appendChild(newD);
        
        };
    };
}



function cancelEditHost(){
    
    
//     document.getElementById("role").selectedIndex = "0"; 
//     
//     var selDiv = document.getElementById('mechanism');
//     selDiv.style.display = "none";
//     
//     var optL = document.getElementById('mechanismList');
//     while (optL.hasChildNodes()) {
//       optL.removeChild(optL.firstChild);
//     }
//     
//     var mechDef = document.getElementById('mechanismElement');
//     while (mechDef.hasChildNodes()) {
//       mechDef.removeChild(mechDef.firstChild);
//     }
    
    document.getElementById('editHost').style.display='none';
}


function editHost(uuid, token) {
    document.getElementById('editHost').style.display='block' ;
    document.getElementById('uuid').value   = uuid;
    
    var url = "/api/host/"+ uuid + "/details" ;

    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    getJSON(url, token).then(function(data) {
        var qty = data.length;

        var displayname = data['details'][0]['host']['displayname'];
        var role        = data['details'][0]['host']['role'];
        var mechanism   = data['details'][0]['host']['mechanism'];
        
        document.getElementById('editDisplayname').value = displayname;   
        document.getElementById('editRole').value        = role;
        document.getElementById('editMechanism').value   = mechanism;

        
        var checkedF = '' ;
        var checkedV = '' ;
        var checkedE = '' ;
 
        if (data['details'][0]['host']['failback'] == "True") { checkedF = 'checked'; } ;
        if (data['details'][0]['host']['verifypeer'] == "True") { checkedV = 'checked'; } ;
        if (data['details'][0]['host']['encrypt'] == "True") { checkedE = 'checked'; } ;
                             
        var mechDef = document.getElementById('editMechanismElement');                     
        var newD = document.createElement('div');
                             
        if ( role == 'auth') {
            if( mechanism == 'LDAP' ) {
                newD.innerHTML = '<label><b>LDAP URI</b></label>'  
                                + '\n'
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="URI" value="' + data['details'][0]['host']['URI'] + '" required>'
                                + '\n'
                                + '<label><b>Base DN</b></label>' 
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="basedn" value="' + data['details'][0]['host']['basedn'] + '"  required>'
                                + '\n'
                                + '<label><b>User DN</b></label>' 
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="userDn" value="' + data['details'][0]['host']['userDn'] + '"  required>'
                                + '\n'
                                + '<label><b>User Password</b></label>' 
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="userPassword" value="' + data['details'][0]['host']['userPassword'] + '"  required>'
                                + '\n'
                                + '<label><b>Search attribute</b></label>' 
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="searchAttribute" value="' + data['details'][0]['host']['searchAttribute'] + '"  required>'
                                + '\n'
                                + '<label><b>Enable failback</b></label> ' 
                                + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="failback" value="True" ' + checkedF + '>'
                                + '\n'
                                + '<label><b>Verify peer</b></label> ' 
                                + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="verifypeer" value="True" ' + checkedV + '>'
                                + '\n';
                
                
                mechDef.appendChild(newD);
            };
            
            if ( mechanism == 'SAML' ) {
                $('mechDef').empty();
                newD.innerHTML = '</br>\n'
                            + '<header class="w3-teal" style="padding: 0px;"> <h6 style="letter-spacing: 2px;"><b>Service Provider (SP)</b></h6></header>'  
                            + '</br>\n'
                            + '<label><b>URL</b></label>'
                            + '</br>\n'
                            + '<select class="" name="sp_proto">'
                            + '<option value="' + data['details'][0]['host']['sp_proto'] + '">' + data['details'][0]['host']['sp_proto'] + '</option>'
                            + '<option value="https"> https </option> '
                            + '<option value="http"> http </option> '
                            + '</select>'
                            + '&nbsp; &nbsp;'
                            + '<input class="" type="text" name="sp_fqdn" placeholder="your.domain.tld" style="width: 400px;" value="' + data['details'][0]['host']['sp_fqdn'] + '" required >'
                            + '</br>\n'
                            + '</br>\n'
                            + '<label><b>SP X509 Cert</b></label>' 
                            + '</br>\n'
                            + '<textarea class="w3-textarea w3-border w3-margin-bottom" name="sp_x509" style="width: 575px; height: 225px" required>' + data['details'][0]['host']['sp_x509'] + '</textarea>'
                            + '</br>\n'
                            + '</br>\n'
                            + '</br>\n'
                            + '<header class="w3-teal" style="padding: 0px;"> <h6 style="letter-spacing: 2px;"><b>Identifier Provider (IdP)</b></h6></header>'  
                            + '</br>\n'
                            + '<label><b>Entity Id</b></label>'
                            + '</br>\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="idp_entity_id" placeholder="some/sso/your_app" value="' + data['details'][0]['host']['idp_entity_id'] + '" required>'
                            + '</br>\n'
                            + '<label><b>SSO URL</b></label>' 
                            + '</br>\n'
                            + '<input class="w3-input w3-border w3-margin-bottom" type="url" name="idp_url" placeholder="https://your-sso.domain.tld" value="' + data['details'][0]['host']['idp_url'] + '" required>'
                            + '</br>\n'
                            + '<label><b>X509 Cert</b></label>' 
                            + '</br>\n'
                            + '<textarea class="w3-textarea w3-border w3-margin-bottom" name="idp_x509" style="width: 575px; height: 225px" required>' + data['details'][0]['host']['idp_x509'] + '</textarea>'
                            + '</br>\n'
                
                mechDef.appendChild(newD);
            }; 
            
            if ( mechanism == 'BlueMind' ) {
                $('mechDef').empty();
                
                newD.innerHTML = '<label><b>Bluemind Server Name</b></label>'  
                                + '\n'
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="URI" value="' + data['details'][0]['host']['URI'] + '"required>'
                                + '\n'
                                + '<label><b>Enable failback</b></label> ' 
                                + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="failback" value="True" ' + checkedF + '>'
                                + '\n'
                                + '<label><b>Verify peer</b></label> ' 
                                + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="verifypeer" value="True" ' + checkedV + '>'
                                + '\n';
                
                mechDef.appendChild(newD);
            
            };
        };
        
        if ( role == 'store') {
            if ( mechanism == 'Internal' ) {
                $('mechDef').empty();
                
                var storePoint = '/var/lib/promethium' ;
                
                if ( data['details'][0]['host']['storePoint'] ) { storePoint = data['details'][0]['host']['storePoint']; } ;
                
                newD.innerHTML = '<label><b>Store Point</b></label>'  
                                + '\n'
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="storePoint" value="' + storePoint + '" required>'
                                + '\n'
                                + '<label><b>Encrypt</b></label> ' 
                                + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="encrypt" value="True" ' + checkedE + '>'
                                + '\n';
                
                mechDef.appendChild(newD);
            
            };
            
            if ( mechanism == 'S3' ) {
                $('mechDef').empty();
                
                newD.innerHTML = '<label><b>Store Point</b></label>'  
                                + '\n'
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="storePoint" value="' + data['details'][0]['host']['storePoint'] + '"required>'
                                +'<label><b>User</b></label>'  
                                + '\n'
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="usr" value="' + data['details'][0]['host']['usr'] + '"required>'
                                + '\n'
                                + '<label><b>Password</b></label>'  
                                + '\n'
                                + '<input class="w3-input w3-border w3-margin-bottom" type="text" name="passwd" value="' + data['details'][0]['host']['passwd'] + '"required>'
                                + '\n'
                                + '<label><b>Verify peer</b></label> ' 
                                + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="verifypeer" value="True" ' + checkedV + '>'
                                + '\n'
                                + '<label><b>Encrypt</b></label> ' 
                                + '<input class="w3-check w3-border w3-margin-bottom" type="checkbox" name="encrypt" value="True" ' + checkedE + '>'
                                + '\n';
                
                
                mechDef.appendChild(newD);
            
            };
        
        }
        
    });
    
}
