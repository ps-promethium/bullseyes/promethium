function translateThis(word) {
 
    var dict = {};
    dict['domain']          = "Administrateur" ;
    dict['dispatcher']      = "Dispatcher" ;
    dict['user']            = "Utilisateur" ;
    dict['chooseGroups']    = "Choisissez un ou plusieurs groupes" ;
    dict['technicalInfo']   = "Informations à caractère technique" ;
    
    
    dict['Subject']          = "Sujet" ;
    dict['From']             = "Expéditeur" ;
    dict['To']               = "Pour" ;
    dict['Cc']               = "En copie" ;
    dict['Bcc']              = "En copie cachée" ;
    dict['recipients']       = "Les destinataires" ;
    dict['prioHigh']         = "Priorité HAUTE" ;
    dict['prioMaximal']      = "Priorité MAXIMALE" ;
    dict['SharedToGroups']   = "Diffusé aux groupes" ;
    dict['modifySharing']    = "Modifier la diffusion" ;
    dict['AccessedBy']       = "Accédé par";
    dict['thisIsYou']        = "Vous";
    
    
    
    return  dict[word];
}

$.datetimepicker.setLocale('en');

$( function() {
    $("#startdate").datetimepicker({
        format: 'Y/m/d H:i',
        formatTime: 'H:i',
        formatDate: 'Y/m/d',
        startDate:  false, // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
        step: 30,

    });
    

});

$( function() {
    $('#openStart').click(function(){
        $('#startdate').datetimepicker('show');
    });
});


$( function() {
    $("#enddate").datetimepicker({
        format: 'Y/m/d H:i',
        formatTime: 'H:i',
        formatDate: 'Y/m/d',
        startDate:  true, // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
        step: 30,

    });
});
   

$( function() {
    $('#openEnd').click(function(){
        $('#enddate').datetimepicker('show');
    });
});


$( function() {
    $('#openMainDate').click(function(){
        $('#mainDate').datetimepicker('show');
    });
});


$( function() {
    $("#mainDate").datetimepicker({
        format: 'Y/m/d',
        formatDate: 'Y/m/d',
        startDate:  true, // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
        step: 30,
        timepicker:false,

    });
});
