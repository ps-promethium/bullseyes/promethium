var dateFormat="YYYY/MM/DD HH:mm"; 

function translateThis(word) {
 
    var dict = {};
    dict['domain']          = "Administrateur" ;
    dict['dispatcher']      = "Dispatcher" ;
    dict['delegated']       = "Délégué" ;    
    dict['user']            = "Utilisateur" ;
    dict['chooseGroups']    = "Choisissez un ou plusieurs groupes" ;
    dict['technicalInfo']   = "Informations à caractère technique" ;
    dict['chooseWhat']      = "Quoi" ;
    
    dict['The message']       = "Le message" ;
    dict['has been saved in'] = "a bien été enregistré dans le dossier" ;
    
    dict['Subject']            = "Sujet" ;
    dict['From']               = "Expéditeur" ;
    dict['Body']               = "Corps";
    dict['To']                 = "Pour" ;
    dict['Cc']                 = "En copie" ;
    dict['Bcc']                = "En copie cachée" ;
    dict['recipients']         = "Les destinataires" ;
    dict['prioHigh']           = "Priorité HAUTE" ;
    dict['prioMaximal']        = "Priorité MAXIMALE" ;
    dict['SharedToGroups']     = "Diffusé aux groupes" ;
    dict['AddMyGroups']        = "Ajouter un de mes groupes";
    dict['modifySharing']      = "Modifier la diffusion" ;
    dict['AccessedBy']         = "Accédé par";
    dict['thisIsYou']          = "Vous";
    dict['addAlert']           = "Ajouter une alerte sur ce message"; 
    dict['addComment']         = "Ajouter un commentaire sur ce message"; 
    dict['wrote']              = "a écrit"; 
    dict['Iwrote']             = "avez écrit";
    dict['removeFromFavorite'] = "Supprimer de vos favoris";
    dict['addedToFavorite']    = "Message ajouté à vos favoris";
    dict['addedToFavoriteNullBranch']    = "Une erreur due au cache est survenue. Merci de vous reconnecter"
    
    dict['addTag']                  = "Ajouter un tag";
    dict['chooseTag']               = "Choisir un tag";
    dict['removeThisTag']           = "Supprimer ce tag";
    dict['getAllMesageWithThisTag'] = "Retrouver les messages depuis ce tag";
    
    dict['Edit']                       = "Modifier"; 
    dict['Apply if']                   = "S'applique quand "; 
    dict['all conditions']             = "toutes les conditions sont validées";
    dict['one or more conditions']     = "au moins une des conditions est validée" ;
    dict['Conditions']                 = "Conditions";  
    dict['is']                         = "est";
    dict['contains']                   = "contient";
    dict['Edit Rules']                 = "Modifier les règles";
    dict['Rule']                       = "Règle";
    dict['all match']                  = "valident toutes les conditions";
    dict['one or more match']          = "valident au moins une des conditions";
    dict['Rule Name']                  = "Nom de la règle";
    dict['addCondition']               = "Ajouter une condition";
    dict['AutomaticDistributionGroup'] = "Activer la distribution automatique aux groupes";
    dict['Cancel']                     = "Annuler";
    dict['Submit']                     = "Valider";
    
    dict['nbMsgOverview'] = "Nombre de messages pour le domaine ";
    dict['Received'] = "Reçus";
    dict['Sent'] = "Emis";
    
    return  dict[word];
}

$.datetimepicker.setLocale('fr');

$( function() {
    $("#startdate").datetimepicker({
        format: 'Y/m/d H:i',
        formatTime: 'H:i',
        formatDate: 'Y/m/d',
        startDate:  false, // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
        step: 30,

    });
    

});

$( function() {
    $('#openStart').click(function(){
        $('#startdate').datetimepicker('show');
    });
});


$( function() {
    $("#enddate").datetimepicker({
        format: 'Y/m/d H:i',
        formatTime: 'H:i',
        formatDate: 'Y/m/d',
        startDate:  true, // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
        step: 30,

    });
});
   

$( function() {
    $('#openEnd').click(function(){
        $('#enddate').datetimepicker('show');
    });
});


var jqb = {
  "__locale": "French (fr)",
  "__author": "Damien \"Mistic\" Sorel, http://www.strangeplanet.fr",

  "add_rule": "Ajouter une règle",
  "add_group": "Ajouter un groupe",
  "delete_rule": "Supprimer",
  "delete_group": "Supprimer",

  "conditions": {
    "AND": "ET",
    "OR": "OU",
    "NOT": "N'est pas",
  },

  "operators": {
    "equal": "est égal à",
    "not_equal": "n'est pas égal à",
    "in": "est compris dans",
    "not_in": "n'est pas compris dans",
    "less": "est inférieur à",
    "less_or_equal": "est inférieur ou égal à",
    "greater": "est supérieur à",
    "greater_or_equal": "est supérieur ou égal à",
    "between": "est entre",
    "not_between": "n'est pas entre",
    "begins_with": "commence par",
    "not_begins_with": "ne commence pas par",
    "contains": "contient",
    "not_contains": "ne contient pas",
    "ends_with": "finit par",
    "not_ends_with": "ne finit pas par",
    "is_empty": "est vide",
    "is_not_empty": "n'est pas vide",
    "is_null": "est nul",
    "is_not_null": "n'est pas nul",

  },

  "errors": {
    "no_filter": "Aucun filtre sélectionné",
    "empty_group": "Le groupe est vide",
    "radio_empty": "Pas de valeur selectionnée",
    "checkbox_empty": "Pas de valeur selectionnée",
    "select_empty": "Pas de valeur selectionnée",
    "string_empty": "Valeur vide",
    "string_exceed_min_length": "Doit contenir au moins {0} caractères",
    "string_exceed_max_length": "Ne doit pas contenir plus de {0} caractères",
    "string_invalid_format": "Format invalide ({0})",
    "number_nan": "N'est pas un nombre",
    "number_not_integer": "N'est pas un entier",
    "number_not_double": "N'est pas un nombre réel",
    "number_exceed_min": "Doit être plus grand que {0}",
    "number_exceed_max": "Doit être plus petit que {0}",
    "number_wrong_step": "Doit être un multiple de {0}",
    "number_between_invalid": "Valeurs invalides, {0} est plus grand que {1}",
    "datetime_empty": "Valeur vide",
    "datetime_invalid": "Fomat de date invalide ({0})",
    "datetime_exceed_min": "Doit être après {0}",
    "datetime_exceed_max": "Doit être avant {0}",
    "datetime_between_invalid": "Valeurs invalides, {0} est plus grand que {1}",
    "boolean_not_valid": "N'est pas un booléen",
    "operator_not_multiple": "L'opérateur \"{1}\" ne peut utiliser plusieurs valeurs"
  }
}
