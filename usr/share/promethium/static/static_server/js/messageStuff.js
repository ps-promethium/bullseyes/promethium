/* Message List */
function ping() {
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 
    
    url = '/api/auth/ping' ;
    
    var xhr = new XMLHttpRequest();
    xhr.open('post', url, true);
    xhr.setRequestHeader("X-PM-ApiKey", token);
    xhr.responseType = 'json';
    xhr.send(); 
}


function goToMessageList() {
    ping() ;
        
    autoRefresh('on');
    var today = new Date().toISOString().slice(0, 10) ;
    document.getElementById("retrieveListByDate").value = today; 
    getListByDate();

}

function getThisDayMessages() {

    autoRefresh('off');
    getListByDate();

}


function changeOrderByDate(newOrder) {
        var datasString = document.getElementById("sessInfo").value;
        var subs = datasString.split(';');
        subs[3] = newOrder ;
        var newLine = subs.join(';');
        
        document.getElementById("sessInfo").value = newLine;
        reloadMessageList("")
}
        
        
function reloadMessageList(date) {
    ping();
    var reloadStatus = document.getElementById('reloadStatus').value; 
    
    if  (reloadStatus == "on") {
        var datasString = document.getElementById("sessInfo").value;
        var subs = datasString.split(';');
        var uuid  = subs[0] ; var token = subs[1] ;  var nbOfDays = subs[2] ; var orderByDate = subs[3] ;
        
        var url = '/api/message/' + uuid +'/message/_bydate';        
        
        var getJSON = function(url, token) {
            return new Promise(function(resolve, reject) {
                var xhr = new XMLHttpRequest();
                xhr.open('post', url, true);
                xhr.setRequestHeader("X-PM-ApiKey", token);
                xhr.responseType = 'json';
                xhr.onload = function() {
                    var status = xhr.status;
                    if (status == 200) {
                        resolve(xhr.response);
                    } else {
                        reject(status);
                    }
                };
                xhr.send(JSON.stringify({ "nbOfDays": nbOfDays, "orderByDate": orderByDate}));
            });
        };
        
        getJSON(url, token).then(function(data){    
            
            var ulMessageList = document.getElementById('ulMessageList');  
            ulMessageList.innerHTML = '';
    
            var msg = data['msg'] ;
            
            for (n=0 ; n < data['total'] ; n++) {
                addMessageToList(uuid, msg[n], token, "");          
            };
            
        });
    }
}


function getListByDate() {
    ping();
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ;  var nbOfDays = subs[2] ; var orderByDate = subs[3] ; 
    
    var date = Date.parse(document.getElementById("retrieveListByDate").value);
    document.getElementById("retrieveListByDate").value = "";
    
    var url = '/api/message/' + uuid +'/message/_bydate';
     
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('post', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send(JSON.stringify({ "date": date}));
        });
    };
    
    getJSON(url, token).then(function(data){    
        
        var ulMessageList = document.getElementById('ulMessageList');  
        ulMessageList.innerHTML = '';
 
        var msg = data['msg'] ;
        
        for (n=0 ; n < data['total'] ; n++) {
            addMessageToList(uuid, msg[n], token, "");          
        };
        
    });
    
    
}


function searchTextInMessage() {
    
    autoRefresh('off');
    ping();
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 
    
   
    var e = document.getElementById("quickSearchField");
//     var quickSearchField = e.options[e.selectedIndex].value;
    var quickSearchText  = document.getElementById("quickSearchText").value;
    var domainUuid       = document.getElementById("domainid").value;
      
    var url = '/api/search/' + domainUuid +'/message/quick';
     
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('post', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send(JSON.stringify({"string": quickSearchText }));
        });
    };
    
    getJSON(url, token).then(function(data){    
        
        var ulMessageList = document.getElementById('ulMessageList');  
        ulMessageList.innerHTML = '';
 
        var msg = data['msg'] ;
        
        for (n=0 ; n < data['total'] ; n++) {
            addMessageToList(uuid, msg[n], token, "");          
        };
        
    });
}


function addMessageToList(uuid, msg, token, fuuid) {       
    
        var ul = document.getElementById("ulMessageList");
        
        var li = document.createElement("li");
        li.classList.add("w3-bar");
        li.classList.add("w3-border-left-blue");
        li.classList.add("w3-hover-bolder");

        if (msg['defaultColor']) {
            li.setAttribute("style", "border-color:"+ msg['defaultColor'] +"!important");
        };
        
        li.setAttribute("draggable", "true");
        li.setAttribute("ondragstart", "return dragStart(event)");
        li.id = msg['uuid'];

                                    
        var html = '' ;
        
        if (msg['status'] == 'read') {
            html += '<div class="w3-bar-item" id="readStatus-'+ msg['uuid'] +'" >' ;
        } else {
            html += '<div class="w3-bar-item-bold" id="readStatus-'+ msg['uuid'] +'" >';
        };
        
        html +='<div class="w3-row" style="padding-bottom: 5px;">';
        
        html +=' <div class="w3-col s1">';

        if (msg['outBound']) {
            html += '<i class="fas fa-upload " style="color: red;"></i>';
        } else {
            html += '<i class="fas fa-download " style="color: blue;"></i>';    
        };
        
        
        if (msg['dispatched']) {
            html += '' ;
        } else {
            html += '<i class="fas fa-share-alt fa-blink " id="shareStatus-'+ msg['uuid'] +'"  style="color: red;"></i>';
        }
        html +=' </div>';
        // Medium & large screen    
        html +=' <div class="w3-col s11 w3-small w3-hide-small" style="font-weight: bolder">';
        html += '<a href=\'#\' onclick="displayMsgDetails(\''+ uuid +'\',\''+ msg['uuid'] +'\',\''+ token +'\')">' ;
        html += msg['subject'] ;         
        html +='</a>';
        html +=' </div>';
        // Small screen
        html +=' <div class="w3-col s11 w3-small w3-hide-medium w3-hide-large" style="font-weight: bolder">';
        html += '<a href=\'#\' onclick="smallScreenDisplayMsgDetails(\''+ uuid +'\',\''+ msg['uuid'] +'\',\''+ token +'\')">' ;
        html += msg['subject'] ;         
        html +='</a>';
        html +=' </div>';
        
        html +='</div>'; 
                        
        
        html += '<div class="w3-row" style="padding-bottom: 5px;">';
        html += '   <div class="w3-col s1">';
        
        if (msg['attachment']) {
             html +=' <i class="fas fa-paperclip"></i>';
        }
                                            
        if (msg['x_priority'] == '1') { 
//             li.classList.add("w3-hover-red");  
            html +='<i class="fas fa-exclamation-circle" style="color: red;"></i>'; 
            
        } else if (msg['x_priority'] == '2') { 
//             li.classList.add("w3-hover-orange");  
            html +='<i class="fas fa-exclamation-circle" style="color: orange;" ></i>' ; 
            
        } else {
//             li.classList.add("w3-hover-light-grey");    
        };
                                            
        html += '   </div>';
        html += '   <div class="w3-col s11 w3-small w3-text-indigo">'; 
        html += msg['fromPretty'];
        html += '   </div>';
        html += '</div> ';
           
        html += '<span class="w3-date-left w3-small" style="font-weight: bolder; font-color:#00008B!important;"> Chrono: ' + msg['chrono'] + '</span>';
        html += '<span class="w3-date-right w3-small ">' + returnPrettyLocalDatetimeFromMicrotimestp(msg['date'],'UTC') ;
        
        if (fuuid) {
                html += ' <a href=\'#\' onclick=\'removeFromFavorite("'+ uuid +'", "'+ fuuid+'" , "'+ msg['uuid'] +'")\' title="'+ translateThis('removeFromFavorite') +'">'; 
                html += '<i class="fas fa-trash"  style="color: red;"> </i> </a>' ;
            
        };
        
        html += '</span>' ;
        html += '</div>'; 
        
        li.innerHTML = html ;
        ul.appendChild(li);

}        
        
        
function getFavoriteMsgList(fuuid) {
    ping();
    autoRefresh('off');
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ;   
    
    var ulMessageList = document.getElementById('ulMessageList');  
    ulMessageList.innerHTML = '';
    
    var url = '/api/user/' + uuid +'/favorites/' + fuuid + '/_allSubscribed' ;

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){    
        
        var ulMessageList = document.getElementById('ulMessageList');  
        ulMessageList.innerHTML = '';
 
        var msg = data['msg'] ;
        
        for (var n=0 ; n < data['total'] ; n++) {
            addMessageToList(uuid, msg[n], token, fuuid);          
        };
        
    }); 
    
    
    
}


function autoRefresh(onOff) {

    if (onOff == "on") {
        $('#reloadStatus').val('on');
        document.getElementById("retrieveListByDate").value = "";
        reloadMessageList("");
    }; 
    
    if (onOff == "off") {
        document.getElementById("retrieveListByDate").value = "";
        $('#reloadStatus').val('off');
    };
}


/* Message/main Tab */
function displayMsgDetails(uuid, muuid, token) {
        
    document.getElementById('msgDetails').style.display='block' ;

    var url = "/api/message/" + uuid +"/message/"+ muuid;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){    
        
        resetTab() ;
        
        var userRole = document.getElementById('userRole').value;
        
        /* Message Tab */
        var message = document.getElementById('message');  
        while (message.firstChild) {
            message.removeChild(message.firstChild);
        };
        
        
        /* Inner info for all tabs  */
        var tabContent = document.createElement('div');
        tabContent.style.display = "none";
        tabContent.innerHTML = ''
                               + '<input type="text" id="tabInfo" value="' + uuid +';' + muuid + ';' + token + ';' + data['msg']['attachment']+'">'
                               + '';
        message.appendChild(tabContent);
        
        
        
        var msgContent = document.createElement('div');
        msgContent.classList.add("w3-container");
        msgContent.classList.add("w3-margin");
        var x_priority = "";
        
        if (data['msg']['x_priority'] == 2) {
            x_priority =  '<div class="w3-panel w3-orange"><h4 class="w3-center">' + translateThis("prioHigh") + '</h4></div>' ;
        };
        
        if (data['msg']['x_priority'] == 1) {
            x_priority =  '<div class="w3-panel w3-red"><h4 class="w3-center">' + translateThis("prioMaximal") + '</h4></div>' ;
        };
        
        
        msgContent.innerHTML = '<br>'  
                                + x_priority
                                + '<h3 class=""><span>' + translateThis("Subject") + ' : ' + data['msg']['subject'] + '</span></h3>'
                                + '<h6><i class="fas fa-at"></i> ' + translateThis("From") + ' : ' + data['msg']['from'].replace('<', '&lt;').replace('>', '&gt;') + '</h6>'
                                + '<h6><i class="far fa-clock"></i> ' + returnPrettyLocalDatetimeFromMicrotimestp(data['msg']['date'],'UTC')  + '</h6>' 
                                + '<h6><ul id="idGroupListInLine" style="padding:0;"><i class="fas fa-share-alt" style="margin-right: 14px;"></i> </ul></h6>';
                                
        if (data['msg']['chrono']) {
            msgContent.innerHTML += '<h6>Chrono : ' + data['msg']['chrono'] + '</h6>' ;            
        }
                
        
        
        if( data['msg']['dispatchedguid'].length > 0 ) {
            groups = data['msg']['dispatchedguid'];
            for (n=0 ; n < groups.length; n++) {
                getGroupDisplayname(groups[n],userRole);
                getGroupDisplaynameInLine(groups[n]);
            };
        };

        myAvailableSubscribedguid = mySubscribedguid ;

        if ( myAvailableSubscribedguid ) {
            for (k=0; k < myAvailableSubscribedguid.length ; k++) {
                getGroupDisplaynameInLineForDelegated(myAvailableSubscribedguid[k], userRole);   
            };
        };
           
        msgContent.innerHTML += ''                       
                                + '<div id="iframeMsg"></div>'
                                + '<hr>';
        message.appendChild(msgContent);
        
        var newIframe = document.createElement('iframe');
        newIframe.classList.add("w3-iframe");
        newIframe.src = 'about:blank';       
        message.appendChild(newIframe);
        newIframe.contentWindow.document.open('text/html', 'replace');
        
        if (data['msg']['body'].includes("</body>")) {        
            newIframe.contentWindow.document.write(data['msg']['body']);
        } else {
            var preH = "<pre>" +  data['msg']['body'] + "</pre>" ;
            newIframe.contentWindow.document.write(preH);
        }
        
        newIframe.contentWindow.document.close();
           
        
        
        /* Info Tab */
        if (!hiddenItems.includes('Info')) {
            var info = document.getElementById('info');  
            while (info.firstChild) {
                info.removeChild(info.firstChild);
            }
            
            var Headers = "";
            if( data['msg']['x_headers'].length > 0 ) {
                for (n=0 ; n < data['msg']['x_headers'].length; n++) {
                    Headers += '<h6> <font style="font-weight: bolder;">' + data['msg']['x_headers'][n]['name'] + '</font>:  <font class="w3-medium"> ' +  data['msg']['x_headers'][n]['value'] + '</font></h6>' 
                }
            };
            var infoContent = document.createElement('div');
            infoContent.classList.add("w3-container");
            infoContent.classList.add("w3-margin");
            infoContent.innerHTML = ''
                                    + '<div class="w3-container w3-green"><h5 class="w3-margin-left">'
                                    +  translateThis("recipients")
                                    + '</h5></div>' 
                                    + '<div class="w3-margin-left">' 
                                    + '<h6> <font style="font-weight: bolder;">' + translateThis("To") + '</font>: <font class="w3-medium"> ' + data['msg']['to'].replace('<', '&lt;').replace('>', '&gt;') + '</font></h6>' 
                                    + '<h6> <font style="font-weight: bolder;">' + translateThis("Cc") + '</font>:  <font class="w3-medium"> ' + data['msg']['cc'].replace('<', '&lt;').replace('>', '&gt;') + '</font></h6>' 
                                    + '<h6> <font style="font-weight: bolder;">' + translateThis("Bcc") + '</font>:  <font class="w3-medium"> ' + data['msg']['bcc'].replace('<', '&lt;').replace('>', '&gt;') + '</font></h6>' 
                                    + '</div>'
                                    + '<br/>'
                                    + '<div class="w3-container w3-blue"><h5 class="w3-margin-left">'
                                    +  translateThis("technicalInfo")
                                    + '</h5></div>' 
                                    + '<div class="w3-margin-left">' 
                                    + '<h6> <font style="font-weight: bolder;">Message-ID</font>: <font class="w3-medium"> ' + data['msg']['message_id'] + '</font></h6>' 
                                    + Headers
                                    + '</div>'       
                                    + '';
            
            info.appendChild(infoContent);
        }    

        
        /* Share tab */ 
        var manageGroupsHTML = '' ;
        if ( userRole == "domain" || userRole == "dispatcher") {
               manageGroupsHTML = '<div class="w3-container ">' 
                 + '<button class="w3-button w3-teal"  onclick=\'popupwindow("/message/' + muuid + '/sharings/modify","Sharing",600,400)\' ><i class="fas fa-users"></i> '+ translateThis("modifySharing") +'</button>'
                 + '</div>'
                 + '';           
        };     
        
        var manageDelegatedGroupsHTML = '' ;
        if ( userRole == "delegated" ) {
               manageDelegatedGroupsHTML =   '<div id="availableGroup" class="w3-container w3-light-grey">' 
                                           + '<h6 class="w3-margin-left">'
                                           + translateThis("AddMyGroups")
                                           + '</h6>'
                                           + '</div>'
                                           + '<h6> <ul class=""  id="idAvailableGroup" style="padding:0;">' 
                                           + '</ul></h6>'
                                           + '';           
        };   
        
        var share = document.getElementById('share');  
        while (share.firstChild) {
            share.removeChild(share.firstChild);
        }
        
        var shareContent = document.createElement('div');
        shareContent.classList.add("w3-container");
        shareContent.classList.add("w3-margin");
        shareContent.innerHTML = ''
                                + '<div class="w3-container w3-green"><h5 class="w3-margin-left">'
                                + translateThis("SharedToGroups")
                                + '</h5></div>' 
                                + '<br/>'
                                + manageGroupsHTML
                                + '<div class="w3-margin-left" id="shareList">'
                                + '<ul class="w3-ul w3-margin w3-padding  "  id="idGroupList">' 
                                + '</ul>'
                                +  manageDelegatedGroupsHTML
                                + '</div>'      
                                + '';  
        

        
        share.appendChild(shareContent);  
        
        var Shares = "";
        
        
        /* Access tab */
        var access = document.getElementById('access');  
        while (access.firstChild) {
            access.removeChild(access.firstChild);
        }
        
        var accessContent = document.createElement('div');
        accessContent.classList.add("w3-container");
        accessContent.classList.add("w3-margin");
        accessContent.innerHTML = ''
                                + '<div class="w3-container w3-green"><h5 class="w3-margin-left">'
                                + translateThis("AccessedBy")
                                + '</h5></div>' 
                                + '<div class="w3-margin-left" id="">'
                                + '<ul class="w3-ul w3-margin w3-padding" id="idDispatchedList">' 
                                + '</ul>'
                                + '</div>'      
                                + '';     
        
        access.appendChild(accessContent); 
        
        //Display enabled 
        if( data['msg']['dispatched'].length > 0 ) {
            acs = data['msg']['dispatched'];
            for (n=0 ; n < acs.length; n++) {
                getUserDisplayname(data['msg']['domain'], acs[n]);
            }
        };
        
        /* Favorites tab */
        
        /* Tag tab */       
        
        
        // This list is finally updated when access tab is selected
        // Html would be full filled

        
        /* Alert tab */               
        
        /* Comment tab */ 
                
        /* Print tab */
        
        /* Forward tab */     
        
        /* Delete tab */
        

        /* Set msg as read */
        idReadStatus = document.getElementById('readStatus-'+muuid); 
        idReadStatus.classList.replace('w3-bar-item-bold','w3-bar-item');

    });    

}
   

/* Attachment Tab */
function downloadThis(uuid, token, fuuid) {
    
    var url = "/api/message/" + uuid +"/attachment/"+ fuuid  + "/download" ;

    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    
    getJSON(url, token).then(function(data){ 
         
        var filename    = data['payload']['filename']; 
        var contentType = data['payload']['content_type'];
        var b64Data     = data['payload']['base64'];
   
        
        
        // Source code from :
        // https://stackoverflow.com/a/40831598
        //
        const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
            const byteCharacters = atob(b64Data);
            const byteArrays = [];

            for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                const slice = byteCharacters.slice(offset, offset + sliceSize);

                const byteNumbers = new Array(slice.length);
                for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);
                byteArrays.push(byteArray);
            }
                
            const blob = new Blob(byteArrays, {type: contentType});
            return blob;
        }
        
        
        var blob = b64toBlob(b64Data, contentType);
       
        // Source :
        // https://blog.jayway.com/2017/07/13/open-pdf-downloaded-api-javascript/
        
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, filename);
            return;
        }
                    
        var blobUrl = URL.createObjectURL(blob);
        var link = document.createElement('a');
        link.href = blobUrl;
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        
        
    });
    
}



function addFileItemToList(uuid,fuuid,token) {
    
    var url = "/api/message/" + uuid +"/attachment/"+ fuuid + "/details";
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    var dict = {};
    
    getJSON(url, token).then(function(data){  
        var filename = data['attachment']['filename'];
        var filesize = data['attachment']['filesize'];
        var fuuid    = data['attachment']['uuid'] ;
        
        var ul = document.getElementById("idAttachList");
        var li = document.createElement("li");
        li.classList.add("w3-bar");
        li.classList.add("w3-large");
        li.classList.add("w3-padding");
        
        var html = '<button class="w3-button w3-circle w3-blue" onclick="downloadThis(\'' + uuid +'\', \'' + token + '\', \'' + fuuid + '\')"><i class="fas fa-cloud-download-alt"></i></button> '+ filename + ' (' + returnBytesToSize(filesize) +  ')' ;  
  
        li.innerHTML = html ;
        ul.appendChild(li);
        
    });
}



function displayAttachment(datasString) {
 
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var  muuid = subs[1] ; var token = subs[2] ;
    
    var html = "</br>" ;
    
    
    var attachment = document.getElementById('attachment');  
    while (attachment.firstChild) {
        attachment.removeChild(attachment.firstChild);
    }
        
    if ( subs[3].length > 0 ) {
        
        var attachmentContent = document.createElement('ul');
        attachmentContent.classList.add("w3-ul");
        attachmentContent.classList.add("w3-card-4");
        attachmentContent.classList.add("w3-margin");
        attachmentContent.classList.add("w3-padding");
        attachmentContent.id = 'idAttachList';
        
        attachmentContent.innerHTML = html ;
        attachment.appendChild(attachmentContent);
    
    
        fuuids = subs[3].split(',');
        for (n=0 ; n < fuuids.length ; n++) {
            addFileItemToList(uuid, fuuids[n], token);           
          
        };
    };
}


/* Share Tab */

function getGroupDisplayname(guid, userRole) {
    
    var datasString = document.getElementById("tabInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var  muuid = subs[1] ; var token = subs[2] ;   
    
    var domainUuid = document.getElementById("domainid").value;
    var url = '/api/group/' + domainUuid + '/' + guid + '/displayname';
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){ 
        var removeButton = '' ;
        if (userRole == 'delegated') {
            if (mySubscribedguid.includes(guid)) {
                removeButton = '<a href="javascript:delThisGroupFromMyDelegation(\''+ guid +'\',\''+ data['displayname'][0]['displayname'] +'\')"> <i class="fas fa-minus-circle fa-sm" style="color: red; "></i></a>';
                myAvailableSubscribedguid = myAvailableSubscribedguid.filter(item => item !== guid)
            };
        };    
                
        var ul = document.getElementById("idGroupList");
        var li = document.createElement("li");
        li.id = "li-del-"+guid ;
        li.classList.add("w3-bar");
        li.classList.add("w3-large");
        li.classList.add("w3-padding");
        
        var html = '<i class="fas fa-users fa-sm"></i> ' + data['displayname'][0]['displayname'] + ' ' + removeButton ;  
  
        li.innerHTML = html ;
        ul.appendChild(li);
        
    });
}


function getGroupDisplaynameInLineForDelegated(guid, userRole) {

    var datasString = document.getElementById("tabInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var  muuid = subs[1] ; var token = subs[2] ;   
    
    var domainUuid = document.getElementById("domainid").value;
    var url = '/api/group/' + domainUuid + '/' + guid + '/displayname';
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){ 
        var addButton = '' ;
        if (userRole == 'delegated') {
            if (myAvailableSubscribedguid.includes(guid)) {
                addButton = '<a href="javascript:addThisGroupFromMyDelegation(\''+ guid +'\',\''+ data['displayname'][0]['displayname'] +'\')"> <i class="fas fa-plus-circle fa-sm" style="color: green; "></i></a>';

                var div = document.getElementById("idAvailableGroup");
                var li = document.createElement("li");   
                li.id = "li-add-"+guid ;
                li.style.display= "inline";
                li.style.padding = "0px 14px 0px 0px";
                var html = '<i class="fas fa-users fa-sm"></i> ' + data['displayname'][0]['displayname'] + ' ' + addButton ;    
                li.innerHTML = html ;
                div.appendChild(li);
            };
        };   
                
    });
    
}

function addThisGroupFromMyDelegation(guid, groupDisplayname) {

    var addId = "li-add-"+guid ;
    var delId = "li-del-"+guid ;
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    var domainUuid = document.getElementById("domainid").value;
    
    var url = '/api/message/' + domainUuid + '/' + uuid + '/subscription/' + muuid +'/updateSubscribedGroupByDelegated';

    var xhr = new XMLHttpRequest();
    xhr.open('post', url, true);
    xhr.setRequestHeader("X-PM-ApiKey", token);
    xhr.responseType = 'json';
    xhr.send(JSON.stringify({ "action": "add", "groupid": guid}));

    document.getElementById(addId).remove();

    var ul = document.getElementById('idGroupList');
    var li = document.createElement("li");
    li.setAttribute("id", delId);
    li.classList.add("w3-bar");
    li.classList.add("w3-large");
    li.classList.add("w3-padding");
    var removeButton = '<a href="javascript:delThisGroupFromMyDelegation(\''+ guid +'\',\''+ groupDisplayname +'\')"> <i class="fas fa-minus-circle fa-sm" style="color: red; "></i></a>';
    var liText = '<i class="fas fa-users fa-sm"></i> ' + groupDisplayname + ' ' + removeButton ;
    li.innerHTML = liText
    ul.appendChild(li);

}


function delThisGroupFromMyDelegation(guid, groupDisplayname) {
    var addId = "li-add-"+guid ;
    var delId = "li-del-"+guid ;
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    var domainUuid = document.getElementById("domainid").value;
    
    var url = '/api/message/' + domainUuid + '/' + uuid + '/subscription/' + muuid +'/updateSubscribedGroupByDelegated';

    var xhr = new XMLHttpRequest();
    xhr.open('post', url, true);
    xhr.setRequestHeader("X-PM-ApiKey", token);
    xhr.responseType = 'json';
    xhr.send(JSON.stringify({ "action": "remvove", "groupid": guid}));

    document.getElementById(delId).remove();

    var ul = document.getElementById('idAvailableGroup');
    var li = document.createElement("li");
    li.setAttribute("id", addId);
    li.style.display= "inline";
    li.style.padding = "0px 14px 0px 0px";
    var removeButton = '<a href="javascript:addThisGroupFromMyDelegation(\''+ guid +'\',\''+ groupDisplayname +'\')"> <i class="fas fa-plus-circle fa-sm" style="color: green; "></i></a>';
    var liText = '<i class="fas fa-users fa-sm"></i> ' + groupDisplayname + ' ' + removeButton ;
    li.innerHTML = liText
    ul.appendChild(li);
}

function getGroupDisplaynameInLine(guid) {
    var datasString = document.getElementById("tabInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var  muuid = subs[1] ; var token = subs[2] ;   
    
    var domainUuid = document.getElementById("domainid").value;
    var url = '/api/group/' + domainUuid + '/' + guid + '/displayname';
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){ 
    
        var div = document.getElementById("idGroupListInLine");
        var li = document.createElement("li");   
        li.style.display= "inline";
        li.style.padding = "0px 14px 0px 0px";
        var html = '<i class="fas fa-users fa-sm"></i> ' + data['displayname'][0]['displayname'];    
        li.innerHTML = html ;
        div.appendChild(li);
        
    });
}



function displaySharing(datasString) {
 
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var  muuid = subs[1] ; var token = subs[2] ;
    
    var html = "</br>" ;
    
    
    var share = document.getElementById('share');  
    while (share.firstChild) {
        share.removeChild(share.firstChild);
    }
        

}






/* Event/Access Tab */
function getUserDisplayname(domainUuid, uuid) {
    
    var datasString = document.getElementById("tabInfo").value;
    var subs = datasString.split(';');
    var token = subs[2] ;   
    
    var domainUuid = document.getElementById("domainid").value;
    var url = '/api/user/' + domainUuid + '/' + uuid + '/displayname';
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){ 
                    
        var ul = document.getElementById("idDispatchedList");
        var li = document.createElement("li");
        li.classList.add("w3-bar");
        li.classList.add("w3-medium");
        li.classList.add("w3-padding");

        li.id = "li_" + uuid ;
        var html = '<i class="far fa-eye-slash" style="color: red;"></i> ' 
                   + data['details'][0]['user']['firstname'] 
                   + ' ' 
                   + data['details'][0]['user']['lastname']
                   + ' ' 
                   + '(' + data['details'][0]['user']['login'] + ')'
                   +'' ;  
  
        li.innerHTML = html ;
        ul.appendChild(li);
        
    });
    
    
    
}



function getEvent() {
       
    var datasString = document.getElementById("tabInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;   


    var url = "/api/message/" + uuid + "/event/"+ muuid;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){  
        
        var users =  data['events'] ;

        for (var n=0; n < users.length ; n++) {
            
            var tmstp = users[n]['@timestamp'];
            var ruuid  = users[n]['uuid'];           
            var datasString = $('#li_' + ruuid).text();  

            if (datasString.split(')').length < 3 ) {
                var newLi = '<i class="far fa-eye" style="color: green;"></i> ' 
                        + datasString 
                        + ' ( ' 
                        +  returnPrettyLocalDatetimeFromMicrotimestp(tmstp,'L')  
                        + ' )';
                
                $('#li_' + ruuid).html(newLi);
            }
        }
        
    })
}


/* Alert Tab */
function getAlerts() {
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var  muuid = subs[1] ; var token = subs[2] ;
    
    /* Reset all displayed before */
    var alert = document.getElementById('alert');  
    while (alert.firstChild) {
        alert.removeChild(alert.firstChild);
    }
        
    var alertContent = document.createElement('div');
    alertContent.classList.add("w3-container");
    alertContent.classList.add("w3-margin");
    alertContent.id ='alertList';       
    alert.appendChild(alertContent);  
        
    
    
    /* */
    var url = '/api/message/' + uuid + '/alert/' + muuid;
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){
        
        var alerts = data['alerts'] ;
        
        var addButton = '<div class="w3-container ">' 
                 + '<button class="w3-button w3-teal"  onclick=\'popupwindow("/message/' + muuid + '/alert/addForm","Alert",650,550)\' ><i class="far fa-bell"></i> '+ translateThis("addAlert") +'</button>'
                 + '</div>'
                 + '';
        
        var html = '' ;
    
        for (n=0 ; n < alerts.length ; n++) {

            var privacy = "" ;
            if (alerts[n]['ownerPrettyName'] == 'thisIsYou') {
                addButton = ''; 
                author = translateThis('thisIsYou') ;
                //delUrl = '<a href="/message/' + alerts[n]['u_uuid']  + '/alert/' + alerts[n]['uuid'] + '/delete"><i class="far fa-trash-alt" style="color: red;"></i></a>' ;
                delUrl = '<a href=\'#\' onclick="delAlert()"><i class="far fa-trash-alt" style="color: red;"></i></a>' ;
            } else {
                author = alerts[n]['ownerPrettyName'];
                delUrl = '' ;
            };
            
            
            if (alerts[n]['accessright'] == 'private') {
                privacy = '<i class="fas fa-eye-slash" style="color: red;"></i>';
            };
            
            html += ''
                  + '<div class="w3-container w3-card-2 w3-margin w3-padding">'
                  + '<h5 class="w3-opacity">'+ privacy +'<b>' + alerts[n]['subject'] + '</b> (' +  author +') ' + delUrl + ' </h5>'
                  + '<h6 class="w3-text-teal"><i class="fas fa-calendar fa-fw w3-margin-right"></i>'
                  + returnPrettyLocalDatetimeFromMicrotimestp(alerts[n]['startdate'],'L') 
                  + ' - '
                  + returnPrettyLocalDatetimeFromMicrotimestp(alerts[n]['enddate'],'L')
                  + '</h6>'
                  + '<p class=""> ' + alerts[n]['body'] 
//                   + '</br>'
//                   + '<hr>'
                  + '</p>'
                  + '</div>'  
                  + '';
          
        };
        
        var HTML = addButton + html ;
        $('#alertList').html(HTML);        
    });
 
}


function delAlert() {
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    
    var url = '/api/message/' + uuid + '/alert/' + muuid + '/delete';
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('delete', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){
        displayMsgDetails(uuid, muuid, token);
    });
}


/* Comment Tab */
function getComments() {
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var  token = subs[2] ;
    
    /* Reset all displayed before */
    var comment = document.getElementById('comment');  
    while (comment.firstChild) {
        comment.removeChild(comment.firstChild);
    }
        
    var commentContent = document.createElement('div');
    commentContent.classList.add("w3-container");
    commentContent.classList.add("w3-margin");
    commentContent.id ='commentList';       
    comment.appendChild(commentContent);  
        
    
    
    /* */
    var url = '/api/message/' + uuid + '/comment/' + muuid;
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){
        
        var comment = data['comments'] ;
        
        
        var addButton = '<div class="w3-container ">' 
                 + '<button class="w3-button w3-teal"  onclick=\'popupwindow("/message/' + muuid + '/comment/addForm","Comment",650,550)\' ><i class="far fa-comments"></i> '+ translateThis("addComment") +'</button>'
                 + '</div>'
                 + '';
        
        var html = '' ;
    
        for (n=0 ; n < comment.length ; n++) {

            var privacy = "" ;
            if (comment[n]['ownerPrettyName'] == 'thisIsYou') {
                addButton = ''; 
                author = translateThis('thisIsYou') ;
//                 delUrl = '<a href="/message/' + comment[n]['u_uuid']  + '/comment/' + comment[n]['uuid'] + '/delete"><i class="far fa-trash-alt" style="color: red;"></i></a>' ;
                delUrl = '<a href=\'#\' onclick="delComment()"><i class="far fa-trash-alt" style="color: red;"></i></a>' ;
                wrote = translateThis("Iwrote");
            } else {
                author = comment[n]['ownerPrettyName'];
                delUrl = '' ;
                wrote = translateThis("wrote");
            };
            
            
            if (comment[n]['accessright'] == 'private') {
                privacy = '<i class="fas fa-eye-slash" style="color: red;"></i>';
            };
            
            html += ''
                  + '<div class="w3-container w3-card-2 w3-margin w3-padding">'
                  + '<h5 class="w3-opacity">'+ author + ' ' + wrote + ' ' + delUrl + ' </h5>'
                  + '<p class=""> ' + comment[n]['body'] 
                  + '</p>'
                  + '<p class="w3-right"> ' + returnPrettyLocalDatetimeFromMicrotimestp(comment[n]['date'],'L') 
                  + '</p>'
                  + '</div>'  
                  + '';
          
        };
        
        var HTML = addButton + html ;
        $('#commentList').html(HTML);        
    });
 
}


function delComment() {
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    
    var url = '/api/message/' + uuid + '/comment/' + muuid + '/delete';
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('delete', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){
        displayMsgDetails(uuid, muuid, token);
    });
}


/* Tags Tab */
function getTags() {
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var  token = subs[2] ;
    
    /* Reset all displayed before */
    var tag = document.getElementById('tag');  
    while (tag.firstChild) {
        tag.removeChild(tag.firstChild);
    }
        
    var ul = document.createElement('ul');
    ul.classList.add("w3-container");
    ul.classList.add("w3-margin");
    ul.id ='tagList';       
    tag.appendChild(ul);  
        
    
    
    /* */
    var url = '/api/message/' + uuid + '/tag/' + muuid;
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){
        
        var tags = data['tags'] ;
        
        var addButton = '<div class="w3-container ">' 
                 + '<button class="w3-button w3-teal"  onclick=\'popupwindow("/message/' + muuid + '/tag","Tag",650,550)\' ><i class="fas fa-tags"></i> '+ translateThis("addTag") +'</button>'
                 + '</div>'
                 + '</br>'
                 + '';
        
        
        $('#tagList').html(addButton);   
    
        var lgt = tags.length ;
        for (n=0 ; n < lgt ; n++) {
            var li = document.createElement("li");
            li.classList.add("w3-bar");
            li.classList.add("w3-padding");
        
            var htm = '<i class="fas fa-tag"></i> '
                      + '<a href=\'#\' onclick=\'getAllMessageWithThisTag("'+ tags[n]['t_uuid'] + '")\' title="'+ translateThis('getAllMesageWithThisTag') + '">' + tags[n]['displayname'] + '</a>'
                      + ' '
                      + '<a href=\'#\' onclick=\'removeThisTag("'+ tags[n]['uuid'] +'")\' title="'+ translateThis('removeThisTag') +'">'
                      + '<i class="fas fa-trash"  style="color: red;"> </i> </a>' ;
             
  
            li.innerHTML = htm ;
            ul.appendChild(li);       
        };

        tag.appendChild(ul)
     
    });
 
}


function removeThisTag(t_uuid) {
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    
    var url = '/api/message/' + uuid + '/tag/' + t_uuid + '/delete';
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('delete', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){
        getTags();
    });
}


function getAllMessageWithThisTag(t_uuid) {
    
    autoRefresh('off');
    ping();
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var  token = subs[2] ;
 
    var url = '/api/message/' + uuid + '/tag/' + t_uuid +'/retrieve';
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){

        var ulMessageList = document.getElementById('ulMessageList');  
        ulMessageList.innerHTML = '';
 
        var msg = data['msg'] ;
        for (n=0 ; n < data['total'] ; n++) {
            addMessageToList(uuid, msg[n], token, "");          
        };
        
    });
    
}
function logThis(value) {
 console.log(value);   
}


function getAllMessageWithThisSelectedTag(uuid, t_uuid, token) {
    
    autoRefresh('off');
    ping();   
    
    var url = '/api/message/' + uuid + '/tag/' + t_uuid +'/retrieve';
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){

        var ulMessageList = document.getElementById('ulMessageList');  
        ulMessageList.innerHTML = '';
 
        var msg = data['msg'] ;
        for (n=0 ; n < data['total'] ; n++) {
            addMessageToList(uuid, msg[n], token, "");          
        };
        
    });
    
}


/* Tab management */
function resetTab() {
    var i;
        var x = document.getElementsByClassName("tab");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
    }
    
    tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-blue", "");
    }
    
    document.getElementById('message').style.display = "block";
    document.getElementById('tabMsg').className += " w3-blue";
}



function openTab(evt, tabName) {
    var i;
    var x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
    }
    
    tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-blue", "");
    }
    
    if ( tabName === "attachment" ) {
        var datasString = document.getElementById("tabInfo").value; 
        displayAttachment(datasString) ;
    };
    
    if ( tabName === "access" ) {
        getEvent() ; 
    };
    
    if ( tabName === "alert" ) {
        getAlerts();
    };
    
    if ( tabName === "comment" ) {
        getComments();
    };
    
    if ( tabName === "tag" ) {
        getTags();
    };
    
//     if ( tabName === "forward" ) {
//         forwardMessage();
//     };
    
    
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " w3-blue";
}



/* Tab Print  */
function Print() {   
    
    var dataString = document.getElementById("tabInfo").value;
    var domainUuid = document.getElementById("domainid").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    var url = '/message/' + uuid + '/print/' + domainUuid + '/'+ muuid ;
    
    popupwindow(url,"Sharing",600,400) ;
   
   
};

/* Tab Forward  */
function forwardMessage() {   
    
    var dataString = document.getElementById("tabInfo").value;
    var domainUuid = document.getElementById("domainid").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;

    var url = '/message/' + uuid + '/forward/'+ muuid + '/forwardForm' ;
    
    popupwindow(url,"Forward",600,400) ;
    
   
   
};

/* Tab Favorites */
function openFavoriteBox() {
    
    var dataString = document.getElementById("tabInfo").value;
    var domainUuid = document.getElementById("domainid").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    
    var url = '/message/' + muuid + '/favorites/modify' ;
    
    popupwindow(url,"Favorites",600,400) ;
   
}


/* Tab delete */
function Delete() {
    
    var dataString = document.getElementById("tabInfo").value;
    var subs = dataString.split(';');
    var uuid  = subs[0] ; var muuid = subs[1] ; var token = subs[2] ;
    
    var url = '/message/' + uuid + '/delete/' + muuid ;
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){
        reloadMessageList("") ;
        document.getElementById('msgDetails').style.display='none' ;
    });
}



/* Favorites  */

function removeFromFavorite(uuid,fuuid, muuid) {

    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 
    
    var url = "/api/user/" + uuid + "/favorites/" + fuuid + "/unsubscribe/" + muuid ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('delete', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){    
        setTimeout(getFavoriteMsgList(fuuid), 500) ;
    });
    
}


function addToFavorite(muuid, fuuid) {
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 
        
    var url = "/api/user/" + uuid + "/favorites/" + fuuid + "/subscribe/" + muuid ;
        
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('post', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    
    getJSON(url, token).then(function(data){    
//         if (data['subscribed']) {
//             var subject = getMessageSubject(muuid) ;
//             var folder  = getFolderName(fuuid) ;
//             setTimeout(addedToFavorite(), 500) ;
//         }
        setTimeout(getFavoriteMsgList(fuuid), 500) ;
    });

    
}   


function addedToFavorite(subject, folder) {
    var subject = document.getElementById("dragMsgSubject").value ;
    document.getElementById("dragMsgSubject").value = "" ;
    
    var folder  = document.getElementById("dragMsgToFolder").value ;
    document.getElementById("dragMsgToFolder").value = "" ;
    
    var msg = translateThis('The message') + " '" + subject + "' " +  translateThis('has been saved in') + " '" +  folder + "'" ;
    
    var dgc = document.getElementById('dragAndDropPopupContent');
    var newP = document.createElement('p');
    newP.innerHTML = msg ;
    dgc.appendChild(newP);
    
    document.getElementById('dragAndDropPopup').style.display = 'block' ;
    
    

}


function getMessageSubject(muuid) {
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 
    
    var url = "/api/message/" + uuid +"/message/"+ muuid;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
 
    
    getJSON(url, token).then(function(data){    
        document.getElementById("dragMsgSubject").value = data['msg']['subject'] ; 
    });
}    


function getFolderName(fuuid) {
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 
    
    var url = "/api/user/" + uuid +"/favorites/" + fuuid + "/details";
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){    
        document.getElementById("dragMsgToFolder").value = data['details']['displayname'] ; 
    });
}  


function cancelDragAndDropPopup(){
    document.getElementById('dragAndDropPopup').style.display='none';   
    var node = document.getElementById("dragAndDropPopupContent");
    if (node.parentNode) {
        node.parentNode.removeChild(node);
    }
    
}
 
 
function dragStart(ev) {
    ev.dataTransfer.effectAllowed='move';
    ev.dataTransfer.setData("Text", ev.target.id);
    ev.dataTransfer.setDragImage(ev.target,0,0);

    return true;
}


function dragEnter(ev) {
    event.preventDefault();
    return true;
}
   
 
function dragOver(ev) {
    return false;
}
 
    
function dragDrop(ev) {
    
    var muuid = ev.dataTransfer.getData("Text");
    var fuuid = ev.target.getAttribute('idRef');
    addToFavorite(muuid, fuuid); 
    
    ev.stopPropagation();
        
    return false;
}


function returnBytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    
    var txt =  (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i] ;
    
    return txt ;
}


function printBytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    
    var txt =  (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i] ;
    
    document.write(txt) ;
}


function testIfLeftColAsNode() {
    
    if ( Number(alertsArrayLength) > 0 ) {
        document.getElementById('leftCol').style.display = "block";
    };

    if ( Number(favoriteNodesLength) > 0 ) {
        document.getElementById('leftCol').style.display = "block";
    };
}



function smallScreenDisplayMsgDetails(uuid, muuid, token) {
    document.getElementById('mainMsgList').style.display = "none";
    displayMsgDetails(uuid, muuid, token);
    
}


function smallClose() {
    document.getElementById('msgDetails').style.display = "none";
    document.getElementById('mainMsgList').style.display = "block";
}
