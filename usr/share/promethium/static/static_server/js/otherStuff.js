function showHide(id) {
    var x = document.getElementById(id);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
} 

function showHideWithIcon(id) {
    var x = document.getElementById(id);
    var spanId = 'deploy-' + id;
    var Hcode = ' <i class="far fa-minus-square"> ' ;
    if (x.style.display === "none") {
        x.style.display = "block";
        Hcode =' <i class="far fa-minus-square"></i> ';
    } else {
        x.style.display = "none";
        Hcode =' <i class="far fa-plus-square"></i> ';
    }
    
    document.getElementById(spanId).innerHTML = Hcode;
} 


