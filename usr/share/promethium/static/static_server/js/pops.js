var my_window = null;

function popupwindow(url, title, w, h)  {    
    var screenLeft=0, screenTop=0;
    var windowWidth=0, windowHeight=0;
    var newTop=0, newLeft=0;

    if (typeof window.screenLeft !== 'undefined') {
        screenLeft = window.screenLeft;
        screenTop = window.screenTop;
    } else if(typeof window.screenX !== 'undefined') {
        screenLeft = window.screenX;
        screenTop = window.screenY;
    } ;

    //console.log(screenLeft + ',' + screenTop);

    windowWidth = window.innerWidth;
    windowHeight = window.innerHeight;

    //console.log(windowWidth + ',' + windowHeight);

    newLeft = screenLeft + (windowWidth / 2) - (w / 2);
    screenTop + (windowHeight / 2);

    if (my_window==null) {
        my_window = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=1, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + newTop + ', left=' + newLeft);
    } else {
        if (my_window.closed) {
            my_window = null;
            my_window = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=1, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + newTop + ', left=' + newLeft);
        } else{
            my_window.moveTo(newLeft,newTop);
        }
    }

    my_window.focus();
    return false;
}

