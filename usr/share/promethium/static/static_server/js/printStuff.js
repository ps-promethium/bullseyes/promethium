/* Message/main Tab */

function prepareMessageToPrint(uuid, duuid, muuid, token) {
    
    var url = "/api/message/" + uuid +"/message/"+ muuid;
    
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){    
        
       
        /* Message Tab */
        var message = document.getElementById('message');  
        while (message.firstChild) {
            message.removeChild(message.firstChild);
        };
        
        var msgContent = document.createElement('div');
        msgContent.classList.add("w3-container");
        msgContent.classList.add("w3-margin");
        
        var x_priority = "";
        
        if (data['msg']['x_priority'] == 2) {
            x_priority =  '<div class="w3-panel w3-orange"><h4 class="w3-center">' + translateThis("prioHigh") + '</h4></div>' 
        };
        
        if (data['msg']['x_priority'] == 1) {
            x_priority =  '<div class="w3-panel w3-red"><h4 class="w3-center">' + translateThis("prioMaximal") + '</h4></div>' 
        };
        
        msgContent.innerHTML = '<br>'  
                                + x_priority
                                + '<h3 class=""><span>' + translateThis("Subject") + ' : ' + data['msg']['subject'] + '</span></h3>'
                                + '<h6><i class="fas fa-at"></i> ' + translateThis("From") + ' : ' + data['msg']['from'].replace('<', '&lt;').replace('>', '&gt;') + '</h6>'
                                + '<h6><i class="far fa-clock"></i> ' + returnPrettyLocalDatetimeFromMicrotimestp(data['msg']['date'],'UTC')  + '</h6>' 
                                + '<h6><ul id="idGroupListInLine" style="padding:0;"><i class="fas fa-share-alt" style="margin-right: 14px;"></i> </ul></h6>';
                                
        if (data['msg']['chrono']) {
            msgContent.innerHTML += '<h6>Chrono : ' + data['msg']['chrono'] + '</h6>' ;            
        }
              
            
        if( data['msg']['dispatchedguid'].length > 0 ) {
            groups = data['msg']['dispatchedguid'];
            for (n=0 ; n < groups.length; n++) {
                getGroupDisplaynameInLine(duuid,groups[n],token);
            }
        };    
            
        msgContent.innerHTML += ''                       
                                + '<div id="iframeMsg"></div>'
                                + '<hr>';
        message.appendChild(msgContent);
        
        var iframeMsg = document.getElementById('iframeMsg');        
      
        if (data['msg']['body'].includes("</body>")) {  
            var el = document.createElement( 'html' );
            el.innerHTML = data['msg']['body'];
            var bodyT = el.getElementsByTagName( 'body' )[0].innerHTML ;
        } else {
            var bodyT = "<pre>" +  data['msg']['body'] + "</pre>" ;         
        } ;
        
        iframeMsg.innerHTML = bodyT;
        
    });    

}


function getGroupDisplaynameInLine(duuid, guid, token) {
    var url = '/api/group/' + duuid + '/' + guid + '/displayname';
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data){ 
    
        var div = document.getElementById("idGroupListInLine");
        var li = document.createElement("li");     
        li.style.display= "inline";
        li.style.padding = "0px 14px 0px 0px";
        var html = '<i class="fas fa-users"></i> ' + data['displayname'][0]['displayname'];    
        li.innerHTML = html ;
        div.appendChild(li);
        
    });
}



















     






