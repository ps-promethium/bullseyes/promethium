// Full text bloc search

var mongo_fulltext = {
  "$and": [
    {
      "body": {
        "$regex": "mot"
      }
    }
  ]
}

var fulltext_options = {  
    plugins: [
        'bt-tooltip-errors',
        'not-group'
    ],
    lang: jqb,
    lang_code: lang,
    filters: [
        {id: 'body', label: {en: "Full text", fr: "Full text"},type: 'string'},
    ]
};

$('#builder-fulltext').queryBuilder(fulltext_options);

$('#btn-reset').on('click', function() {
  $('#builder-fulltext').queryBuilder('reset');
});
$('#btn-set-mongo').on('click', function() {
  $('#builder-fulltext').queryBuilder('setRulesFromMongo', mongo_fulltext);
});


// Message bloc search 
var mongo_message = {
  "$and": [
    {
      "body": {
        "$regex": "mot"
      }
    }
  ]
}

var message_options = {  
    plugins: [
        'bt-tooltip-errors',
        'not-group'
    ],
    lang_code:  lang,
    lang: jqb,
    filters: [
        {id: 'litDate', label: {en: 'Date',fr: 'Date'}, type: 'date', validation: {format: 'YYYY-MM-DD'}, plugin: 'datepicker', plugin_config: {format: 'yyyy-mm-dd', todayBtn: 'linked', todayHighlight: true, autoclose: true }},
        {id: 'body',label: {en: 'Body',fr: 'Corps'}, type: 'string'},
        {id: 'subject',label: {en: 'Subject',fr: 'Sujet'}, type: 'string'},
        {id: 'from',label: {en: 'From',fr: 'Emetteur'}, type: 'string'},
        {id: 'to',label: {en: 'To',fr: 'Destinataire'}, type: 'string'},
        {id: 'cc',label: {en: 'Cc',fr: 'En Copie'}, type: 'string'},
        {id: 'bcc',label: {en: 'BCc',fr: 'En Copie Cachée'}, type: 'string'}
    ]
};

$('#builder-message').queryBuilder(message_options);

$('#btn-reset').on('click', function() {
  $('#builder-message').queryBuilder('reset');
});
$('#btn-set-mongo').on('click', function() {
  $('#builder-message').queryBuilder('setRulesFromMongo', mongo_message);
});

$("#date").datepicker({
    dateFormat:"yyyy/mm/dd",
    minDate:new Date(new Date().getTime())
});


// reset builder
$('.reset').on('click', function() {
  var target = $(this).data('target');

  $('#builder-'+target).queryBuilder('reset');
});
$('.set-mongo').on('click', function() {
  var target = $(this).data('target');
  var mongo = window['mongo_'+target];

  $('#builder-'+target).queryBuilder('setRulesFromMongo', mongo);
});

$('.parse-mongo').on('click', function() {
    var target = $(this).data('target');
    var result = $('#builder-'+target).queryBuilder('getMongo');

    if (!$.isEmptyObject(result)) {
        complexSearch(JSON.stringify(result, null, 2), target);
    }
});


