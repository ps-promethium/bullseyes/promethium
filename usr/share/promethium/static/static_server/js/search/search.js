function complexSearch(jsonQuery, target) {
    
    ping();
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 

    var domainUuid       = document.getElementById("domainid").value;
      
    if (target == 'fulltext') {
        var url = '/api/search/' + domainUuid +'/message/fulltext';
    }
    
    if (target == 'message') {
        var url = '/api/search/' + domainUuid +'/message/complexSearch';
    }
        
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('post', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send(jsonQuery);
        });
    };
    
    getJSON(url, token).then(function(data){    
        
        var ulMessageList = document.getElementById('ulMessageList');  
        ulMessageList.innerHTML = '';
 
        var msg = data['msg'] ;
        
        for (n=0 ; n < data['total'] ; n++) {
            addMessageToList(uuid, msg[n], token, "");          
        };
        
    });
}
