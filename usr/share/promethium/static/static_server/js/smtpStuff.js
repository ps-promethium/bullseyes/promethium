
function setPort(protocol, port) {

    var protocol = document.getElementById(protocol).value;
    
    if ( protocol == 'smtp') {
        document.getElementById(port).value = 25 ; }
    
    if ( protocol == 'ssl-tls') {
        document.getElementById(port).value = 465 ; }
    
    if ( protocol == 'smtps') {
        document.getElementById(port).value = 587 ; }
};



function resetSmtp() {
 
    document.getElementById("editServer").value           = "";
    document.getElementById("editAccount").value          = "";
    document.getElementById("editPasswd").value           = "";
    document.getElementById("editPort").value             = "25" ;
    document.getElementById("editProtocol").selectedIndex = 0;
    
    resetSmtpStoredParam();
    
    
};


function resetSmtpStoredParam() {
    
    var datasString = document.getElementById("sessInfo").value;
    var subs = datasString.split(';');
    var uuid  = subs[0] ; var token = subs[1] ; 
    
    url = '/api/domain/'+ uuid +'/smtp/reset' ;
    
    var xhr = new XMLHttpRequest();
    xhr.open('post', url, true);
    xhr.setRequestHeader("X-PM-ApiKey", token);
    xhr.responseType = 'json';
    xhr.send(); 
}   
