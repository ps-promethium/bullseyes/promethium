function cancelAddFavoritesNode(){
    document.getElementById('addFavoritesNode').style.display='none';
}


function cancelEditFavoritesNode(){
    document.getElementById('editFavoritesNode').style.display='none';
}



function editFavoritesNode(uuid, fuuid, token) {
    document.getElementById('editFavoritesNode').style.display='block' ;
   
    var url = "/api/user/" + uuid + "/favorites/" + fuuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    getJSON(url, token).then(function(data) {
        var qty = data.length;

        document.getElementById('uuid').value = fuuid;
        document.getElementById('owner').value = uuid;
        
        var displayname = data['details']['displayname'];
        document.getElementById('editDisplayname').value = displayname;
              
        var parentUuid = data['details']['parent'];
        setOption(parentUuid, uuid, token, 'editParent');

        
    });
    
}


function setOption(uuid, owner, token, selectId) {

   var url = "/api/user/" + owner + "/favorites/" + uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data) {
        
        if (data['details'] !== null ) {
            var displayname = data['details']['displayname'];
            
            
            var select = document.getElementById(selectId);
            var opt    = document.createElement("option");
            
            opt.value = uuid ;
            opt.text  = displayname ;
            opt.selected = 'selected';
            select.add(opt, 0);
        };
    });

}




    
