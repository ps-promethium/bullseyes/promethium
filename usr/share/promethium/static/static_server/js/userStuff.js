function cancelAddUser() {
    document.getElementById('addUser').style.display='none';
//     document.getElementById('subscribedguid').value   ='';
}


function cancelEditUser() {
    document.getElementById('editUser').style.display='none';
}


function cancelResetPasswd(){
    document.getElementById('resetPasswd').style.display='none';
}

function editUser(uuid, domain, token) {
    
    document.getElementById('editUser').style.display='block' ;
    document.getElementById('editUuid').value   = uuid;
    
    var url = "/api/user/" + domain  + "/" + uuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    getJSON(url, token).then(function(data) {
        var qty = data.length;
        
        var firstname = data['details'][0]['user']['firstname'];
        document.getElementById('editFirstname').value = firstname;
        
        var lastname = data['details'][0]['user']['lastname'];
        document.getElementById('editLastname').value = lastname;
        
        var login = data['details'][0]['user']['login'];
        document.getElementById('editLogin').value = login;
        
        var enable = data['details'][0]['user']['enable'];
        if( enable == 'True') {
            checked = 'checked';
            document.getElementById('editEnable').checked = checked;            
        };
        
        var select = document.getElementById('editRole');
        $('#editRole').val(data['details'][0]['user']['role']);        

        var groupList    = data['details'][0]['user']['subscribedguid'];        
        $('#editSubscribedguid').val(groupList).trigger("chosen:updated");

    });

}


function resetPasswd(uuid,login) {
    document.getElementById('resetPasswd').style.display = 'block' ;
    document.getElementById('resetUuid').value           = uuid; 
    document.getElementById('resetPwd').value            = "";  
    document.getElementById('resetLogin').value          = login;
}
