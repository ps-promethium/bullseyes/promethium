function cancelAddTag(){
    document.getElementById('addTag').style.display='none';
}


function cancelEditTag(){
    document.getElementById('editTag').style.display='none';
}



function editTag(uuid, tuuid, token) {
    document.getElementById('editTag').style.display='block' ;
   
    var url = "/api/user/" + uuid + "/tags/" + tuuid + "/details" ;
    
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };

    getJSON(url, token).then(function(data) {
        var qty = data.length;

        document.getElementById('uuid').value = tuuid;
        document.getElementById('owner').value = uuid;
        
        var displayname = data['details']['displayname'];
        document.getElementById('editDisplayname').value = displayname;
        
    });
    
}



function updateTagList(uuid, muuid, token) {

    var url = "/api/message/" + uuid + "/tag/" + muuid 
    var getJSON = function(url, token) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.setRequestHeader("X-PM-ApiKey", token);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };
    
    getJSON(url, token).then(function(data) {   

        var subscribedTags = [] ;
        
        var msg = data['tags'] ;
        var lgt = msg.length
        for (n=0 ; n < lgt ; n++) {
            subscribedTags[n] = msg[n]['t_uuid']
        };
        $('#subscribedtags').val(subscribedTags).trigger("chosen:updated");

    });

}
    
